/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
/********************************************************
 ********************************************************
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/times.h>

#include "CSRTypes.h"
char ITemplateFichName[BUFSIZ] = "\0";
char IBankFichName[BUFSIZ]     = "\0";
char IPairRules[BUFSIZ]        = "\0";
char OLogFichName[BUFSIZ]      = "\0";
char OPDBFichName[BUFSIZ]      = "\0";
char ORasmolFichName[BUFSIZ]   = "\0";
char OPymolFichName[BUFSIZ]    = "\0";
int  noTemplateCrds = 1;
int    gStrictMatch = 0;
int    gNIter   = 2000;
int    gMinAt   = 5;
double gDMax    = 5.;
double gRMSd    = 5.;
double gTol     = 5.;
int    gWhat    = DcALL;
int    DEBUG    = 0;
int    gVerbose = 0;
long int gSeed  = -1;
int    gTempFormat = -1;
int    gBankFormat = -1;
char   gNoH     = 0;

/* ------------------------------------------------------------
 * Possibles arguments de la ligne de commande. --------------- 
 * ------------------------------------------------------------
 */
void printUsage()
{
  fprintf(stderr,"LigCSRre: a program to search for the maximal common sub-structure\n");
  fprintf(stderr,"  usage: LigCSRre <args>\n");
  fprintf(stderr,"  input data     :\n");
  fprintf(stderr,"   basic input   :\n");
  fprintf(stderr,"         -it<format> <Template File> : Template file (query)\n");
  fprintf(stderr,"         -ib<format> <Bank File>     : Bank file\n");
  fprintf(stderr,"             format can be either `pdb' or `mol2'\n");
  fprintf(stderr,"         -irules <Rules File>   : Atoms pairing rules (mol2 only)\n");
  fprintf(stderr,"         -o      <Log File>     : Log file for the run\n");
  fprintf(stderr,"         -opdb   <path>         : Targets parts (the matches)\n");
  fprintf(stderr,"         -tOut                  : Do not output template crds (saves disk space)\n");
  fprintf(stderr,"         -ormol  <path>         : Rasmol script (the matches)\n");
  fprintf(stderr,"         -opmol  <path>         : Pymol script (the matches)\n");

  fprintf(stderr,"         -dmax             : threshold distance to compute distances (SDM)\n");
  fprintf(stderr,"         -niter            : number of iterations\n");
  fprintf(stderr,"         -minat            : minimal number of atoms to accept a match (-1 means max)\n");
  fprintf(stderr,"         -rmsd             : minimal RMSd to accept a match\n");
  fprintf(stderr,"         -tol              : tolerance (largest deviation for a pair)\n");
  fprintf(stderr,"         -strict           : strict atom / res name matching\n");
  fprintf(stderr,"         -sc               : protein SC atoms only are matched\n");
  fprintf(stderr,"         -bb               : protein BB atoms only are matched\n");
  fprintf(stderr,"         -seed             : seed for pseudo-random generator\n");
  fprintf(stderr,"    mol2 only    :\n");
  fprintf(stderr,"         -noH              : hydrogen atoms are not matched\n");

  fprintf(stderr,"   miscellaneous :\n");

  fprintf(stderr,"         -v                : Verbose log\n");
  fprintf(stderr,"         -h                : To get this information\n");
}

void parseargstr(int argc,char *argv[])
{
  int i;

  if (argc == 1) {
    printUsage();
    exit(0);
  }

  for (i=0;i<argc;i++) {

    if (!strcmp(argv[i],"-h")) { /* print usage */
      printUsage();
      exit(0);
    }

    /* Le fichier config (input) ---------------------------- */
//     if (!strcmp(argv[i],"-it")) {
//       strcpy(ITemplateFichName,argv[i+1]);
//       i++;
//     }
    else if (!strcmp(argv[i],"-itpdb")) {
      gTempFormat = PDB;
      strcpy(ITemplateFichName, argv[i+1]);
      i++;
    }
    else if (!strcmp(argv[i],"-itmol2")) {
      gTempFormat = MOL2;
      strcpy(ITemplateFichName, argv[i+1]);
      i++;
    }
//     if (!strcmp(argv[i],"-ib")) {  /* Newer formulation */
//       strcpy(IBankFichName,argv[i+1]);
//       i++;
//     }
    else if (!strcmp(argv[i],"-ibpdb")) {
      gBankFormat = PDB;
      strcpy(IBankFichName, argv[i+1]);
      i++;
    }
    else if (!strcmp(argv[i],"-ibmol2")) {
      gBankFormat = MOL2;
      strcpy(IBankFichName, argv[i+1]);
      i++;
    }
    /* Le fichier règles d'appariements ------------------- */
    else if (!strcmp(argv[i],"-irules")) {
      strcpy(IPairRules, argv[i+1]);
      i++;
    }
    /* Le fichier log (output) ---------------------------- */
    else if (!strcmp(argv[i],"-o")) {
      strcpy(OLogFichName,argv[i+1]);
      i++;
    }
    /* Le fichier pdb (output) ---------------------------- */
    else if (!strcmp(argv[i],"-opdb")) {
      strcpy(OPDBFichName,argv[i+1]);
      i++;
    }
    /* Le fichier pdb (output) ---------------------------- */
    else if (!strcmp(argv[i],"-tOut")) {
      noTemplateCrds = 0;
      i++;
    }
    /* Le fichier script rasmol (output) ------------------ */
    else if (!strcmp(argv[i],"-ormol")) {
      strcpy(ORasmolFichName,argv[i+1]);
      i++;
    }
    /* Le fichier script pymol (output) ------------------ */
    else if (!strcmp(argv[i],"-opmol")) {
      strcpy(OPymolFichName,argv[i+1]);
      i++;
    }
    /* Nombre d'iterations -------------------------------- */
    else if (!strcmp(argv[i],"-niter")) {
      gNIter = atoi(argv[i+1]);
      i++;
    }
    /* Nombre minimal d'atome pour un match --------------- */
    else if (!strcmp(argv[i],"-minat")) {
      gMinAt = atoi(argv[i+1]);
      i++;
    }
    /* Distance maximale pour SDM ------------------------- */
    else if (!strcmp(argv[i],"-dmax")) {
      gDMax = atof(argv[i+1]);
      i++;
    }
    /* RMSd maximal pour un match ------------------------- */
    else if (!strcmp(argv[i],"-rmsd")) {
      gRMSd = atof(argv[i+1]);
      i++;
    }
    /* Tolerance maximale pour un match ------------------- */
    else if (!strcmp(argv[i],"-tol")) {
      gTol = atof(argv[i+1]);
      i++;
    }
    /* SC only -------------------------------------------- */
    else if (!strcmp(argv[i],"-sc")) {
      gWhat = DcSC;
    }
    /* BB only -------------------------------------------- */
    else if (!strcmp(argv[i],"-bb")) {
      gWhat = DcBB;
    }
    /* No hydrogen matched -------------------------------- */
    else if (!strcmp(argv[i],"-noH")) {
      gNoH = 1;
    }

    /* Strict atom/res name correspondence ---------------- */
    else if (!strcmp(argv[i],"-strict")) {
      gStrictMatch = 1;
    }

    else if (!strcmp(argv[i],"-seed")) {     /* drand48 seed */
      gSeed = atol(argv[i+1]);
      ++i;
    }

    else if (!strcmp(argv[i],"-dbg")) {
      DEBUG = 1;
    }
    else if (!strcmp(argv[i],"-v")) {
      gVerbose = 1;
    }
  }
}

void delimiter(FILE *f)
{
  fprintf(f,"----------------------------------------------------\n");
}

/* ------------------------------------------------------------
 * Un petit utilitaire ...
 * ------------------------------------------------------------
 */
void splitLine(char *line,  char **tok, int *nTok)
{
  *nTok = 0;
  tok[0] = strtok(line," ");
  while ((tok[++(*nTok)] = strtok(NULL," ")) != NULL);
}

void summary(FILE *f)
{
  long myclock;
  myclock = time(0);

  delimiter(f);
  fprintf(f,"LigCSRre (C implementation, 2006-2008)   %s\n",(char *)ctime(&myclock));
  fprintf(f,"Template : %s\n",ITemplateFichName);
//  fprintf(f,"format   : %s\n", (gTempFormat == PDB) ? "PDB" : "mol2");
  fprintf(f,"Bank     : %s\n",IBankFichName);
//  fprintf(f,"format   : %s\n", (gBankFormat == PDB) ? "PDB" : "mol2");

  if (IPairRules[0] != 0)
    fprintf(f, "Rules    : %s\n", IPairRules);

  fprintf(f,"Log      : %s\n",OLogFichName);
  fprintf(f,"PDB      : %s\n",OPDBFichName);
  fprintf(f,"Threshold: %.2lf\n",gDMax);
  fprintf(f,"nIter    : %d\n",gNIter);
  fprintf(f,"minAtm   : %d\n",gMinAt);
  fprintf(f,"max RMSd : %.2lf\n",gRMSd);
  fprintf(f,"max tol  : %.2lf\n",gTol);
  fprintf(f,"what     : %d (3: all, 2: SC, 1: BB)\n",gWhat);

  if (gNoH)
    fprintf(f,"hydrogens: no\n");

  fprintf(f,"strict matching : %d\n",gStrictMatch);
  fprintf(f,"Pseudo-Random Generator Seed : %ld\n", gSeed);
  delimiter(f);
}
