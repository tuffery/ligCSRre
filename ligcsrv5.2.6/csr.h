/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/times.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>


#define boucle(i,min,max) for((i)=(min);(i)<(max);(i)++)
#define max(a,b) (a<b?b:a)
#define min(a,b) (a<b?a:b)
#define DtFloat         double
#define DcPI 3,1415926535;
#define EPS 1.e-10

long int gSeed = -1;




typedef double DtMatrix3x3[3][3];
typedef double DtMatrix4x4[4][4];
typedef double DtPoint4[4];
typedef double DtPoint3[3]; /* (x,y,z) */
typedef DtPoint3 *DtCrds;

typedef char DtStr3[4];
typedef char DtStr4[5];

typedef struct sNuage {
  int n;              /* Nombre de points */
  DtPoint3 *crds;     /* Coordonnees */
  DtStr4   *atmName;  /* Nom atome   */
  DtStr3   *resName;  /* Nom residu  */
} DtNuage;


typedef struct sNuagesPts {
  int n1; /*  nombre de points i du nuage 1 */
  DtPoint3 *crds1; /* tableau de coordonnees des points de 1 */


  int n2; /* nombre de points j du nuage 2 */
  DtPoint3 *crds2; /*tableau de coordonnees des points de 2 */
} *DtNuagesPts;



typedef struct sNuagesAt {
  DtNuagesPts points;

  char **typeAt1;
  char **typeRes1;
  int *numAt1;

  char **typeAt2;
  char **typeRes2;
  int *numAt2;
} *DtNuagesAt;






typedef struct sDist {
  double longueur;
  int liaison;
} DtDistRec;

typedef DtDistRec *DtpDistRec;



typedef struct sReponse {
  int nbAtMax; /* nombre d'atomes apparies maximum */
  DtDistRec *dist; /* tableau des distances et des appariements */
  double rmsd; /* somme minimale des carres des distances interatomiques */
  DtMatrix4x4 M; /* matrice 4x4 de rotation contenant la translation */
} *DtReponse;


typedef struct sListe_Reponses {
  DtReponse rep;
  struct sListe_Reponses *suiv;
} *DtListRep;



typedef struct sListe_atomes{
  int at; /* numero de l'atome dans son nuage */
  struct liste_atomes *suiv; /* pointeur vers l'atome suivant */
} *DtAtList;



void strupper(char *s);
void getAtomNum(char *line,int *anum);
void getAtomName(char *line,char *anom);
void getResName(char *line,char *rnom);
void getAtomCoords(char *line,DtFloat *x,DtFloat *y,DtFloat *z);
int getPDBLineStatus(char *d);
char ** txtFileRead(char *fname, int *lSze);
char ** txtFileFree(char **l, int lSze);
void lectPdbCsr (char *fname, DtNuage *n);
void printNuage(FILE *f, DtNuage *n, char *msg);
void initRnd(long int *seed);
int irand(int n);
double distance(DtPoint3 R,DtPoint3 K);
int cmpdist(double *dist, double *dist2);
void printDtpDistRec (DtDistRec *dist, int nbPrint, DtNuage ng1,DtNuage ng2);
void printNgCrds(DtCrds crds, int nbCrds);
void printPoint3Crds(DtPoint3 *crds, int i);
void mulMat4x4(DtMatrix4x4 a,DtMatrix4x4 b,DtMatrix4x4 c);
void MkArbitraryAxisRotMat4x4(DtPoint3 A,DtPoint3 B,DtFloat angle,DtMatrix4x4 M);
DtFloat norme(DtPoint3 a);
void MkNullMat4x4(DtMatrix4x4 m);
void printMat4x4(DtMatrix4x4 m);
void singleRotate(DtPoint3 p, DtMatrix4x4 rmat);
void rotateCrds(DtCrds crds2, int nbPoints, DtMatrix4x4 rmat);
void simpleTranslate(DtPoint3 p, DtPoint3 tr);
void negatePoint3(DtPoint3 p1);
void copyPoint3(DtPoint3 p1, DtPoint3 p2);
void copyMat4x4(DtMatrix4x4 a,DtMatrix4x4 b);
void copyDtpDistRec(int kds, DtDistRec *dist1, DtDistRec *dist2);
void stockRep(int kds,int nbAtApp, DtDistRec *dist, double rmsd, DtMatrix4x4
mat, DtReponse rep); // stockage de la reponse (cas ou nbAtApp > nbAtMax)
void rotAl (DtMatrix4x4 mat); // matrice de rotation aleatoire
void posAl (DtNuage ng1, DtNuage ng2, DtMatrix4x4 mat); // rotation et positionnement initial al�atoires
void posAl2(DtNuage ng1, DtNuage ng2, DtMatrix4x4 mat);
void printNbPtsApp (DtNuage ng1, DtNuage ng2, int *kds,DtDistRec *dist);
int sdm (DtNuage ng1, DtNuage ng2, double dseuil, int dimd, int *kds, DtDistRec 
*dist); // algorithme SDM, renvoie le nombre d'atomes apparies
int sdm2 (DtNuage ng1, DtNuage ng2, double dseuil, int dimd, int *kds, DtDistRec *dist,DtAtList *masques); // algorithme SDM2 (avec masques selon appariements impossibles)
void crdsASuperposer (DtNuage ng1, DtNuage ng2, int nbAtApp, DtDistRec *dist, DtPoint3 *crdsSup1, DtPoint3 *crdsSup2);
static double *product(double **A, double *x, int n);
DtMatrix3x3 *XYCov(DtMatrix3x3 *pM,DtPoint3 *X,DtPoint3 *Y,DtPoint3
Xmean,DtPoint3 Ymean,int aSze);
void MkTrnsIIMat4x4(DtMatrix4x4 m, DtPoint3 tr);
int largestEV4(double R[4][4], double v[4], double *vp);
int inverse_power(double a[4][4], int n, int maxiter, double eps, double *v,
double *w);
int shift_power(double *a, int n, int maxiter, double eps, double *v, double
*w);
double *random_vect(int n);
static double inner(double *x, double *y, int n);
double lmax_estim (double a[4][4], int n);
static int lu_c (double a[4][4],  int n);
static void resol_lu(double a[4][4], double *b, int n);
double best_shift(double *a[], int n);
int power(double *a[], int n, int maxiter, double eps, double *v, double *w);
void free_mat(double **mat, int n);
double **alloc_mat(int n, int m);
DtFloat squared_distance(DtPoint3 R,DtPoint3 K);


double zuker_superpose(DtPoint3 *c1, DtPoint3 *c2, int len, DtMatrix4x4 M);


DtReponse csr (DtNuage ng1, DtNuage ng2, double dseuil, int dimd, int *kds, DtDistRec*dist, DtMatrix4x4 mat, DtPoint3 *crdsSup1, DtPoint3 *crdsSup2,int nbIter,
DtReponse rep);
