/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
#ifndef __ZUKER_H__
#define __ZUKER_H__

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "CSRTypes.h"

extern int     power(double *a[], int n, int maxiter, double eps, double *v, double *w);
extern DtFloat squared_distance(DtPoint3 R,DtPoint3 K);
extern double  zuker_superpose(DtPoint3 *c1, DtPoint3 *c2, int len, DtMatrix4x4 M);
extern double  zuker_superpose_weighted(DtPoint3 *c1, DtPoint3 *c2, int len, DtMatrix4x4 M, double *w1, double *w2);

#endif

