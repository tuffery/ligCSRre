/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
#ifndef _MOL2_H_
#define _MOL2_H_

extern void atmMol2InternalName(char *atmName, char *atmNm2);
extern int computeConnect(int *nConnect, double *nConnectW, DtNuage *pNg1, DtNuage *pNg2, DtReponse *rep);
extern void freeConnectMatrix(char **matrix, int n);
extern void getMol2AtomCoords(char *line,DtFloat *x,DtFloat *y,DtFloat *z);
extern void getMol2AtomName(char *line,char *anom);
extern void getMol2AtomNum(char *line,int *anum);
extern int  getMol2LineStatus(char *d);
extern void getMol2ResChainIde(char *line,char *chn);
extern void lectMol2Csr (char *fname, int what, int strict, DtNuage **pN, int *onNuages, DtReRec **pReList, int verbose, DtRuleList *pRuleList);

#endif
