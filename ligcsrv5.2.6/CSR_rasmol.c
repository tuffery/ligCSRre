/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
    x.out(outfile+".pdb")
    fd = open(outfile+".ras","w")
    fd.write("load %s.pdb\n" % outfile)
    fd.write("wireframe off\n")
    if mode == "ribbons":
        fd.write("ribbons\n")
    elif mode == "trace":
        fd.write("trace 110\n")
    elif mode == "backbone":
        fd.write("backbone 110\n")
    fd.write("zoom 120\n")
    fd.write("set ambient 60\n")
    #fd.write("set shadepower 100\n")
    fd.write("select all\n")
    fd.write("color black\n")
    fd.write("background black\n")

    for i in range(0,len(hmmseq)):
        aseq   = hmmseq[i]
        if aseq == []:
            continue
        chnlbl = hmmchn[i]
        rnum   = hmmrNum[i]
        # print aseq
        # print chnlbl
        # print rnum
        for apos in range(0,len(aseq[1])):
            if chnlbl[apos] == ' ':
                fd.write("select %s\n" % rnum[apos])
            else:
                fd.write("select %s%s\n" % (rnum[apos],string.lower(chnlbl[apos])))
            acode = hmmcode(aseq[1][apos])
            # print aseq[1][apos],acode,rnum[apos],string.lower(chnlbl[apos]),"<br>"
            if acode == -1:
                acode = 0
            fd.write("colour [%d,%d,%d]\n" % (HMMColors[acode][0],HMMColors[acode][1],HMMColors[acode][2]))
            if (i == 0 and apos == 0) or (i == len(hmmseq)-1 and apos == len(aseq[1]) -1):
                if chnlbl[apos] == ' ':
                    fd.write("select %s and backbone and *.CA\n" % rnum[apos])
                else:
                    fd.write("select %s%s and backbone and *.CA\n" % (rnum[apos],string.lower(chnlbl[apos])))
                fd.write("set fontsize 30\n")
                fd.write("set fontstroke 2\n")
                fd.write("colour white\n")
                if (i == 0 and apos == 0):
                    fd.write("label \"Nter\"\n")
                if (i == len(hmmseq)-1 and apos == len(aseq[1]) -1):
                    fd.write("label \"Cter\"\n")

    fd.write("write %s.gif\n" % outfile)
    fd.write("quit\n")
    fd.close()
    cmd = "/data/bin/rasmol_32BIT -nodisplay < "+outfile+".ras > /dev/null"
    status = os.system(cmd)
    cmd = "convert "+outfile+".gif "+outfile+".png"
    status = os.system(cmd)

load pdb inline
Select *
color [160,160,160]
Select *A & 1-141
color blue
Select *A & 18-20
color [192,192,255]
Select *B & 2-146
color magenta
Select *B & 48-54
color [255,192,255]
Select *
wireframe off
backbone 50
exit
exit
HEADER    PROTEIN STRUCTURE ALIGNMENT                                           
COMPND    (A) 4HHB:A(141) [1-141] (B) 4HHB:B(146) [2-146]
REMARK   1 
REMARK   1 Alignment length=147 Sequence identity=139 Gaps=8(6%)
REMARK   1 
ATOM      0  N   VAL A   1       6.204  16.869   4.854
ATOM      1  CA  VAL A   1       6.913  17.759   4.607
ATOM      2  C   VAL A   1       8.504  17.378   4.797
ATOM      3  O   VAL A   1       8.805  17.011   5.943
ATOM      4  CB  VAL A   1       6.369  19.044   5.810
ATOM      5  CG1 VAL A   1       7.009  20.127   5.418
ATOM      6  CG2 VAL A   1       5.246  18.533   5.681
ATOM      7  N   LEU A   2       9.096  18.040   3.857
ATOM      8  CA  LEU A   2      10.600  17.889   4.283
ATOM      9  C   LEU A   2      11.265  19.184   5.297
ATOM     10  O   LEU A   2      10.813  20.177   4.647
ATOM     11  CB  LEU A   2      11.099  18.007   2.815
ATOM     12  CG  LEU A   2      11.322  16.956   1.934
ATOM     13  CD1 LEU A   2      11.468  15.596   2.337
ATOM     14  CD2 LEU A   2      11.423  17.268   0.300
ATOM     15  N   SER A   3      11.584  18.730   6.148
ATOM     16  CA  SER A   3      12.263  19.871   7.087
ATOM     17  C   SER A   3      13.304  20.329   6.300
ATOM     18  O   SER A   3      14.085  19.818   5.364
ATOM     19  CB  SER A   3      12.744  19.045   8.223
ATOM     20  OG  SER A   3      13.781  18.286   8.179
ATOM     21  N   PRO A   4      14.196  21.422   7.097
ATOM     22  CA  PRO A   4      15.048  21.890   6.206
ATOM     23  C   PRO A   4      16.464  21.282   6.288
ATOM     24  O   PRO A   4      17.212  20.899   5.409
ATOM     25  CB  PRO A   4      15.814  23.113   7.166
