/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "3DFuncs.h"
#include "CSRTypes.h"
#include "Zuker.h"

/* ==================================================================
 * XYCov = Calcule la matrice d'inertie des points
 *
 * id      : indices des atomes dans At
 * from,tto: indices extremes
 * P       : le barycentre
 * ==================================================================
 */

static double *product(double **A, double *x, int n) 
{
  int i, j;
  double sum;
  double *y=(double *)calloc(n, sizeof(double));

  for (i=0; i<n; i++) {
    sum=0;
    for (j=0; j<n; j++)
      sum+=A[i][j]*x[j];
    y[i]=sum;
  }
  return y;
}

DtMatrix3x3 *XYCov(DtMatrix3x3 *pM,DtPoint3 *X,DtPoint3 *Y,DtPoint3 Xmean,DtPoint3 Ymean,int aSze)
{
  int i;
  double Xx,Xy,Xz;
  double Yx,Yy,Yz;
  double daSze;

  /*   fprintf(stdout,"inertia, len %d\n",aSze); */

  /* X average*/
  Xmean[0] = Xmean[1] = Xmean[2] = 0.;
  for (i=0;i<aSze;i++) {
    Xmean[0] += X[i][0];
    Xmean[1] += X[i][1];
    Xmean[2] += X[i][2];
  }
  if (aSze) {
    daSze = (double) aSze;
    Xmean[0] /= daSze;
    Xmean[1] /= daSze;
    Xmean[2] /= daSze;
  }

  /* Y average*/
  Ymean[0] = Ymean[1] = Ymean[2] = 0.;
  for (i=0;i<aSze;i++) {
    Ymean[0] += Y[i][0];
    Ymean[1] += Y[i][1];
    Ymean[2] += Y[i][2];
  }
  if (aSze) {
    daSze = (double) aSze;
    Ymean[0] /= daSze;
    Ymean[1] /= daSze;
    Ymean[2] /= daSze;
  }

  /* Covariance matrix */

  if (pM == NULL) {
    pM = (DtMatrix3x3 *) calloc(1,sizeof(DtMatrix3x3));
  } else {
    memset((void *) pM, 0, sizeof(DtMatrix3x3));
  }

  for (i=0;i<aSze;i++) {
    Xx = (double) X[i][0] - Xmean[0];
    Xy = (double) X[i][1] - Xmean[1];
    Xz = (double) X[i][2] - Xmean[2];
    Yx = (double) Y[i][0] - Ymean[0];
    Yy = (double) Y[i][1] - Ymean[1];
    Yz = (double) Y[i][2] - Ymean[2];

    (*pM)[0][0] += Xx*Yx;
    (*pM)[0][1] += Xx*Yy;
    (*pM)[0][2] += Xx*Yz;

    (*pM)[1][0] += Xy*Yx;
    (*pM)[1][1] += Xy*Yy;
    (*pM)[1][2] += Xy*Yz;

    (*pM)[2][0] += Xz*Yx;
    (*pM)[2][1] += Xz*Yy;
    (*pM)[2][2] += Xz*Yz;
  }

  return pM;
}


/* ====================================================================
 * Matrice 4x4 Translation. PREMULTIPLIE -> Y = XM (X vecteur ligne) 
 * ====================================================================
 */

void MkTrnsIIMat4x4(DtMatrix4x4 m, DtPoint3 tr)
{
 m[0][0] = 1.;    m[0][1] = 0.;    m[0][2] = 0.;    m[0][3] = 0.;
 m[1][0] = 0.;    m[1][1] = 1.;    m[1][2] = 0.;    m[1][3] = 0.;
 m[2][0] = 0.;    m[2][1] = 0.;    m[2][2] = 1.;    m[2][3] = 0.;
 m[3][0] = tr[0]; m[3][1] = tr[1]; m[3][2] = tr[2]; m[3][3] = 1.;
}





/* ====================================================================
 * Allocation et libération de l'espace mémoire pour une matrice 4x4.
 * ====================================================================
 */

void free_mat(double **mat, int n)
{
  int i;
  for (i=0; i<n; i++)
    free(mat[i]);
  free(mat);
}

double **alloc_mat(int n, int m) 
{
  int i;
  double **mat = (double **)calloc(m,sizeof(double *));
  for (i=0; i<n; i++)
    mat[i]=(double *)calloc(n, sizeof(double));
  return mat;
}



/* =========================================================
 * Calcul de la distance au carré entre deux points.
 * =========================================================
 */

DtFloat squared_distance(DtPoint3 R,DtPoint3 K)
{
  return (DtFloat) ((K[0] - R[0]) * (K[0] - R[0]) +
                    (K[1] - R[1]) * (K[1] - R[1]) +
                    (K[2] - R[2]) * (K[2] - R[2]));
}

double *random_vect(int n)
{
  int i;
  double *v=(double *) calloc(n, sizeof(double));
  for (i=0; i<n; i++)
    v[i]= (double)rand()/(RAND_MAX+1.0);
  return(v);
}

static double inner(double *x, double *y, int n)
{
  int i;
  double sum;

  for (sum=0, i=0; i<n; sum+=x[i]*y[i],i++);
  return sum;

}

double best_shift(double *a[], int n)
{
  double m, M, s;
  double t, sum;
  int i, j;
  t=a[0][0];
  for (i=1; i<n; i++) t=max(t, a[i][i]);
  M=t;
  t=a[0][0];
  for (i=0; i<n; i++) {
    for (sum=0,j=0; j<n; j++)
      if (j!=i) sum+=fabs(a[i][j]);
    t=min(t, a[i][i]-sum);
  }
  m=t;
  s=-0.5*(M+m);
  for (i=0; i<n; i++)
    a[i][i]=a[i][i]+s;
  return s;
}


int shift_power(double *a, int n, int maxiter, double eps, double *v, double *w)
{
  double **tmp;
  double sh;
  int niter;
  int i,j;

  tmp=alloc_mat(n, n);
  /* copyMat(a, tmp, n, n); */
  for (i=0; i<n; i++) {
    for (j=0; j<n; j++) {
      tmp[i][j] = a[i*n+j];
    }
  }
  sh=best_shift(tmp, n);

  niter=power(tmp, n, maxiter, eps, v, w);
  *v=*v-sh;
  free_mat(tmp, n);
  return niter;
}

double lmax_estim (double a[4][4], int n)
{
  double t, sum;
  int i, j;
  t=a[0][0];
  for (i=0; i<n; i++) {
    for (sum=0,j=0; j<n; j++)
      if (j!=i) sum+=fabs(a[i][j]);
    t=max(t, a[i][i]+sum);
  }
  return t;
}

static int lu_c (double a[4][4],  int n)
{
 int i,j,k,err;
 double pivot,coef;

 err=1;

 k=0;
 while (err==1 && k<n) {
  pivot=a[k][k];
  if(fabs(pivot)>=EPS) {
    for(i=k+1;i<n;i++) {
      coef=a[i][k]/pivot;
      for(j=k;j<n;j++)
        a[i][j] -= coef*a[k][j];
      a[i][k]=coef;
    }
  }
  else err=0;
  k++;
 }
 if(a[n-1][n-1]==0) err=0;
 return err;
}

static void resol_lu(double a[4][4], double *b, int n)
{
 int i,j;
 double sum;
 double y[n];
 y[0]=b[0];
 for(i=1;i<n;i++) {
  sum=b[i];
  for(j=0;j<i;j++)
    sum-=a[i][j]*y[j];
  y[i]=sum;
 }
 b[n-1]=y[n-1]/a[n-1][n-1];
 for(i=n-1;i>=0;i--) {
  sum=y[i];
  for(j=i+1;j<n;j++)
     sum-=a[i][j]*b[j];
  b[i]=sum/a[i][i];
 }
}

int inverse_power(double a[4][4], int n, int maxiter, double eps, double *v, double *w)
{
  int niter,i;
  double *y;
  double r, sum, l, normy, d;
  y=random_vect(n);
  niter=0;

  r=lmax_estim(a, n);
  for (i=0; i<n; i++) a[i][i]=a[i][i]-r;
  if (lu_c(a, n)==0) {
    /* fprintf(stderr,"ATTENTION ! cas singulier de inverse_power\n"); */
    free(y);     //exit(0);
    return 0;
  }

  do {
    normy=sqrt(inner(y,y,n));
    for (i=0; i<n; i++) {
      w[i]=y[i]/normy;
      y[i]=w[i];
    }

    resol_lu(a, y, n);
    l=inner(w,y,n);
    niter++;
    for (sum=0,i=0; i<n; i++) {
      d=y[i]-l*w[i];
      sum+=d*d;
    }
    d=sqrt(sum);
  } while (d>eps*fabs(l) && niter<maxiter);
  free(y);
  *v=r+1.0/l;
  return niter;
}


int power(double *a[], int n, int maxiter, double eps, double *v, double *w)
{
  int niter,i;
  double *y;
  double sum, l, normy, d;
  y=random_vect(n);
  niter=0;
  do {
    normy=sqrt(inner(y,y,n));
    for (i=0; i<n; i++) w[i]=y[i]/normy;
    y=product(a, w, n);
    l=inner(w,y,n);
    niter++;
    for (sum=0,i=0; i<n; i++) {
      d=y[i]-l*w[i];
      sum+=d*d;
    }
    d=sqrt(sum);
  } while (d>eps*fabs(l) && niter<maxiter);
  free(y);
  *v=l;
  return niter;
}





/* ===================================================================
 * Fonctions utilitaires pour l'algorithme de superposition de Zuker.
 * ====================================================================
 */

int largestEV4(double R[4][4], double v[4], double *vp)
{

  double M2[4][4];
  int rs;

  memcpy(M2,R,sizeof(double)*16);

  rs = inverse_power(R, 4, 10000, 1.e-8, vp, v);

  if (!rs)
    return shift_power(&M2[0][0], 4, 10000, 1.e-8, vp, v);

  return rs;

}

/* =============== ALGORITHME DE SUPERPOSITION (ZUKER) =======================
 * Best fit matrix as proposed by:
 * Zuker & Somorjai, Bulletin of Mathematical Biology,
 * vol. 51, No 1, p 55-78, 1989.
 *
 * Returns M, a transformation matrix ready for use
 * ===========================================================================
 */
// double zuker_superpose(DtPoint3 *c1, DtPoint3 *c2, int len, DtMatrix4x4 M)
// {
//   DtMatrix3x3  C;
//   DtMatrix3x3 *pC;
//   DtMatrix4x4  RM;
//   DtMatrix4x4  TMP;
//   DtMatrix4x4  TX;
//   DtMatrix4x4  TY;
//   DtMatrix4x4  P;
//   DtPoint4     V;
//   DtPoint3     bc1, bc2;
//   DtPoint3     try;
// 
//   double eval;
//   double squared_rms = 0.;
// 
//   int nCycles;
//   int aDot;
// 
//   /* Compute transformation matrix as proposed by zuker */
// 
//   pC = &C;
//   pC = XYCov(pC, (DtPoint3 *) c1, (DtPoint3 *) c2, bc1, bc2, len);
// 
//   P[0][0] = -C[0][0]+C[1][1]-C[2][2];
//   P[0][1] = P[1][0] = -C[0][1]-C[1][0];
//   P[0][2] = P[2][0] = -C[1][2]-C[2][1];
//   P[0][3] = P[3][0] =  C[0][2]-C[2][0];
// 
//   P[1][1] = C[0][0]-C[1][1]-C[2][2];
//   P[1][2] = P[2][1] = C[0][2]+C[2][0];
//   P[1][3] = P[3][1] = C[1][2]-C[2][1];
// 
//   P[2][2] = -C[0][0]-C[1][1]+C[2][2];
//   P[2][3] = P[3][2] = C[0][1]-C[1][0];
// 
//   P[3][3] = C[0][0]+C[1][1]+C[2][2];
// 
//   /* #if 0
//   printMat4x4("zuker P", P);
// #endif */
// 
//   nCycles = largestEV4(P, V, &eval);
// 
//   RM[0][0] = -V[0]*V[0]+V[1]*V[1]-V[2]*V[2]+V[3]*V[3];
//   RM[1][0] =  2*(V[2]*V[3]-V[0]*V[1]);
//   RM[2][0] =  2*(V[1]*V[2]+V[0]*V[3]);
//   RM[3][0] =  0.;
// 
//   RM[0][1] = -2*(V[0]*V[1]+V[2]*V[3]);
//   RM[1][1] = V[0]*V[0]-V[1]*V[1]-V[2]*V[2]+V[3]*V[3];
//   RM[2][1] =  2*(V[1]*V[3]-V[0]*V[2]);
//   RM[3][1] =  0.;
// 
//   RM[0][2] =  2*(V[1]*V[2]-V[0]*V[3]);
//   RM[1][2] = -2*(V[0]*V[2]+V[1]*V[3]);
//   RM[2][2] = -V[0]*V[0]-V[1]*V[1]+V[2]*V[2]+V[3]*V[3];
//   RM[3][2] =  0.;
// 
//   RM[0][3] =  0.;
//   RM[1][3] =  0.;
//   RM[2][3] =  0.;
//   RM[3][3] =  1.;
//   /* printMat4x4("zuker RM", RM); */
// 
//   /* Solution eprouvee ! */
//   try[0] = - bc2[0]; try[1] = - bc2[1]; try[2] = - bc2[2];
//   MkTrnsIIMat4x4(TY, try);
//   MkTrnsIIMat4x4(TX, bc1);
//   mulMat4x4(TY,RM,TMP);
//   mulMat4x4(TMP, TX, M);
// 
//   /* Now superpose the coordinates */
//   for (aDot=0;aDot<len;aDot++) {
//     singleRotate(c2[aDot],M);
//   }
// 
//   /* Compute squared RMSd */
//   for (aDot=0;aDot<len;aDot++) {
//     squared_rms += squared_distance(c1[aDot],c2[aDot]);
//   }
// 
//   return squared_rms / (double) len;
// }

double zuker_superpose_weighted(DtPoint3 *c1, DtPoint3 *c2, int len, DtMatrix4x4 M,
                                double *w1, double *w2)
{
  DtMatrix3x3  C;
  DtMatrix3x3 *pC;
  DtMatrix4x4  RM;
  DtMatrix4x4  TMP;
  DtMatrix4x4  TX;
  DtMatrix4x4  TY;
  DtMatrix4x4  P;
  DtPoint4     V;
  DtPoint3     bc1, bc2;
  DtPoint3     try;

  double eval;
  double squared_rms = 0.;
  double wSum = 0.;

  int nCycles;
  int aDot;

  /* Compute transformation matrix as proposed by zuker */

  pC = &C;
  pC = XYCov(pC, (DtPoint3 *) c1, (DtPoint3 *) c2, bc1, bc2, len);

  P[0][0] = -C[0][0]+C[1][1]-C[2][2];
  P[0][1] = P[1][0] = -C[0][1]-C[1][0];
  P[0][2] = P[2][0] = -C[1][2]-C[2][1];
  P[0][3] = P[3][0] =  C[0][2]-C[2][0];

  P[1][1] = C[0][0]-C[1][1]-C[2][2];
  P[1][2] = P[2][1] = C[0][2]+C[2][0];
  P[1][3] = P[3][1] = C[1][2]-C[2][1];

  P[2][2] = -C[0][0]-C[1][1]+C[2][2];
  P[2][3] = P[3][2] = C[0][1]-C[1][0];

  P[3][3] = C[0][0]+C[1][1]+C[2][2];

  /* #if 0
  printMat4x4("zuker P", P);
#endif */

  nCycles = largestEV4(P, V, &eval);

  RM[0][0] = -V[0]*V[0]+V[1]*V[1]-V[2]*V[2]+V[3]*V[3];
  RM[1][0] =  2*(V[2]*V[3]-V[0]*V[1]);
  RM[2][0] =  2*(V[1]*V[2]+V[0]*V[3]);
  RM[3][0] =  0.;

  RM[0][1] = -2*(V[0]*V[1]+V[2]*V[3]);
  RM[1][1] = V[0]*V[0]-V[1]*V[1]-V[2]*V[2]+V[3]*V[3];
  RM[2][1] =  2*(V[1]*V[3]-V[0]*V[2]);
  RM[3][1] =  0.;

  RM[0][2] =  2*(V[1]*V[2]-V[0]*V[3]);
  RM[1][2] = -2*(V[0]*V[2]+V[1]*V[3]);
  RM[2][2] = -V[0]*V[0]-V[1]*V[1]+V[2]*V[2]+V[3]*V[3];
  RM[3][2] =  0.;

  RM[0][3] =  0.;
  RM[1][3] =  0.;
  RM[2][3] =  0.;
  RM[3][3] =  1.;
  /* printMat4x4("zuker RM", RM); */

  /* Solution eprouvee ! */
  try[0] = - bc2[0]; try[1] = - bc2[1]; try[2] = - bc2[2];
  MkTrnsIIMat4x4(TY, try);
  MkTrnsIIMat4x4(TX, bc1);
  mulMat4x4(TY, RM, TMP);
  mulMat4x4(TMP, TX, M);

  /* Now superpose the coordinates */
  for (aDot = 0; aDot < len; aDot++) {
    singleRotate(c2[aDot],M);
  }

  /* Compute squared RMSd */
  for (aDot = 0; aDot < len; aDot++) {
    squared_rms += squared_distance(c1[aDot],c2[aDot]) * w1[aDot] * w2[aDot];
    wSum += w1[aDot] * w2[aDot];
  }

  return squared_rms / wSum;
}
