/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
/* ================================================================
 * CpuTime.h
 * Version 2.0 (P. Tuffery 07-1997)
 * Set of functions to get elapsed time during a process
 * ================================================================
 */
#ifndef __CpuTime_h__
#define __CpuTime_h__

#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <sys/times.h>

#define CLK_TCK CLOCKS_PER_SEC

extern char *mygetdate();
extern void gettime(FILE *f, struct tms *t);
extern void elapsedUserTime(FILE *f, struct tms *ti, struct tms *tf);
extern void elapsedSystemTime(FILE *f, struct tms *ti, struct tms *tf);
extern void elapsedRealTime(FILE *f, struct tms *ti, struct tms *tf);

#endif
