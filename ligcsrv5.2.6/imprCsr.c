/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */

/* ===================================================================
 * Copie d'un tableau de type DtpDistRec (recopie dist1 dans dist2). 
 * ===================================================================
 */

void copyDtpDistRec(int kds, DtDistRec *dist1, DtDistRec *dist2)
{
  int i,j;

  for(i=0 ; i < kds ; i++){
    for(j=0 ; j < 2 ; j++){
      dist2[i].longueur=dist1[i].longueur;
      dist2[i].liaison=dist1[i].liaison;
    }
  }
}



/* ===================================================================
 * Stockage d'une réponse pour CSR (kds,nbAtApp,dist,rmsd,Mat4x4).
 * ===================================================================
 */

void stockRep(int kds, int nbAtApp, DtDistRec *dist, double rms, DtMatrix4x4 mat, DtReponse rep)
{
  rep->nbAtMax=nbAtApp;
  rep->rmsd=rms;
  copyMat4x4(mat, rep->M);
  copyDtpDistRec(kds,dist,rep->dist);
}



/* ===============================================
 * Création d'une matrice de rotation aléatoire
 * ===============================================
 */


void rotAl (DtMatrix4x4 mat)
{
  double teta;
  DtPoint3 U;
  double N;
  int compteur=0;

  teta=drand48()*DcPI;

  /* printf("*teta=%.8lf\t",teta); */
  
  U[1]=-1+drand48()*2; //réel compris entre -1 et 1
  U[2]=-1+drand48()*2;
  U[3]=-1+drand48()*2;
  N=norme(U);
 
  while (N>1 && compteur < 1000){

    /* printf("*X=%.8lf\t",U[1]);
    printf("Y=%.8lf\t",U[2]);
    printf("Z=%.8lf\n",U[3]);
    printf("* /!\\========PROBLEME==========/!\\\n"); */
    U[1]=-1+drand48()*2;
    U[2]=-1+drand48()*2;
    U[3]=-1+drand48()*2;
    N=norme(U);
    compteur++;
  }
  if(compteur < 1000){
    /* printf("X=%.8lf\t",U[1]);
    printf("Y=%.8lf\t",U[2]);
    printf("Z=%.8lf\n",U[3]);
    printf("*norme(U)=%.8lf\n\n",N); */
  }
  else {
    /* printf("/!\\========NORME DE U SUPERIEURE A 1==========/!\\\n\n"); */
  }
  MkNumXRotMat4x4(U,teta,mat);
  
}



/* ===================================================
 * Positionnement aléatoire de deux nuages de points.
 * ===================================================
 */

void posAl (DtNuage ng1, DtNuage ng2, DtMatrix4x4 mat)
{
  int nAl1, nAl2,i;
  DtPoint3 tr1, tr2;


  /* --- rotation aléatoire --- */

  rotAl(mat); 

  /* --- positionnement aléatoire --- */

  nAl1=irand(ng1.n);
  nAl2=irand(ng2.n);

  /* printf("*COUPLE (ATOME 1, ATOME 2) TIRE AU HASARD : (%d,%d).\n*\n"
     ,nAl1,nAl2); */


  /* --- superposition des deux nuages de points --- */

  /* rotation de tout le nuage 2 */
  for(i=0 ; i < (ng2.n) ; i++){
    singleRotate(ng2.crds[i], mat);
  }

  tr1[0]=ng1.crds[nAl1][0];
  tr1[1]=ng1.crds[nAl1][1];
  tr1[2]=ng1.crds[nAl1][2];

  tr2[0]=ng2.crds[nAl2][0];
  tr2[1]=ng2.crds[nAl2][1];
  tr2[2]=ng2.crds[nAl2][2];
  negatePoint3(tr2);

  /* superposition du nuage 2 sur le nuage 1 */
  for(i=0 ; i < (ng2.n) ; i++){
    simpleTranslate(ng2.crds[i],tr2);
    simpleTranslate(ng2.crds[i],tr1);
  }
}



/* ========================== ALGORITHME SDM =================================
 * Calcul de toutes les distances entre deux nuages de points (ng).
 * Conservation de toutes les distances inférieures à (dseuil) dans un 
 *   tableau (dist) de type DtpDistRec. 
 *   dist[k].longueur correspond à la k-ième distance inférieure à dseuil
 *   dist[k].liaison permet de retrouver de quels points s'agit cette longueur.
 *
 * Retourne le nombre de points maximum que l'on peut apparier.
 * ===========================================================================
 */

int sdm (DtNuage ng1, DtNuage ng2, double dseuil, int dimTab, int *kds, DtDistRec *dist)
{
  char at1App[ng1.n]; //at1App[k]=1 si le k-ème point de ng1 fait déjà parti d'un appariement, 0 sinon
  char at2App[ng2.n]; //at2App[k]=1 si le k-ème atome de ng2 fait déjà parti d'un appariement, 0 sinon
  int i,i1,i2,shift,stop,nbAtApp;
  double lg;


  if ((ng1.n)==0 || (ng2.n)==0){
    *kds=0;
    nbAtApp=0;
    return nbAtApp;
  }


  /* --- calcul des distances entre les points --- */

  if (dseuil>=0){
    *kds=0;
    for (i2=0 ; i2 <(ng2.n) ; i2++){
      shift=i2*(ng1.n);
      for (i1=0 ; i1 <(ng1.n) ; i1++){

	lg=distance((ng1.crds[i1]),(ng2.crds[i2]));
	
	if(lg <= dseuil){
	  if(*kds <= dimTab){ 
	    (dist[*kds]).longueur = lg; 
	    (dist[*kds]).liaison = i1 + shift; 
	    *kds=(*kds)+1;
	  }
	}
      } 
    } 
    
    if (*kds>dimTab) return (-1); /* dimd trop petit */
    if (*kds==0) return (-2); /* dseuil trop petit */
    
  } 
  
  else { /* dseuil < 0 (équivalent de dseuil infini) */

    *kds=(ng1.n)*(ng2.n);

    if (*kds>dimTab) return 1; /* dimd trop petit */
    for (i2=0 ; i2<(ng2.n) ; i2++){
      shift=i2*(ng1.n);
      for (i1=0 ; i1 <(ng1.n) ; i1++){
	dist[i1+shift].longueur=distance((ng1.crds[i1]),(ng2.crds[i2]));
	dist[i1+shift].liaison=i1+shift;

      } 
    } 
  }
  
  /* ---  tri du tableau des distances par ordre croissant de distances --- */

  /* printDtpDistRec(dist,*kds,ng1,ng2);
     printf("\n"); */ 
  
  qsort(dist,*kds,sizeof(DtDistRec), cmpdist); 

  /* printDtpDistRec(dist,*kds,ng1,ng2); */



  /* --- calcul du nombre de points appariés --- */

  memset (at1App, 0, sizeof(char)*(ng1.n));
  memset (at2App, 0, sizeof(char)*(ng2.n));
  
  i=stop=0;
  
  while(i<(*kds) && stop==0){
    i1=(dist[i].liaison)%(ng1.n);
    i2=(dist[i].liaison)/(ng1.n);

    /* printf("*%d\tAT1 - AT2 : %d - %d\t%lf\n",
       i,i1,i2,dist[i].longueur); */
    /* printPoint3Crds(ng1.crds,i1);
    printPoint3Crds(ng2.crds,i2); */

    if(at1App[i1]==1){
      stop=1;
      nbAtApp=i;
      /* printf("*=> ATOME %d DU NUAGE 1 DEJA APPARIE.\n",i1); */
    } else {
      at1App[i1]=1;
    }
    if(at2App[i2]==1){
      stop=1;
      nbAtApp=i;
      /* printf("*=> ATOME %d DU NUAGE 2 DEJA APPARIE.\n",i2); */
    } else {
      at2App[i2]=1;
    }
    i++;
  }
  
  if(nbAtApp==32){
    printDtpDistRec(dist,nbAtApp+15,ng1,ng2); 
  }
 
  if (nbAtApp==(*kds) && nbAtApp!=min((ng1.n),(ng2.n))) return (-3);
    
  
  return (int) (nbAtApp);
  
}





/* =========================================================================
 * Transfert les données de sdm à zuker_superpose.
 * (renvoie les (nbAtApp) coordonnées à superposer au mieux l'une sur l'autre).
 * =========================================================================
 */

void crdsASuperposer (DtNuage ng1, DtNuage ng2, int nbAtApp, DtDistRec *dist, DtPoint3 *crdsSup1, DtPoint3 *crdsSup2)
{
  int i;
  int i1;
  int i2;

  for(i=0 ; i < nbAtApp ; i++){
    i1=(dist[i].liaison)%(ng1.n);
    i2=(dist[i].liaison)/(ng1.n);
    copyPoint3(ng1.crds[i1],crdsSup1[i]);
    copyPoint3(ng2.crds[i2],crdsSup2[i]);
  }
}


/* ====================== ALGORITHME CSR =====================================
 * Superpose au mieux deux nuages de points avec SDM et zuker_superpose.
 * Retourne : 
 *   le nombre de points appariés.
 *   la matrice de rotation 4x4 correspondant à la meilleure superposition.
 *   le tableau trié des distances entre points après rotation et translation.
 * ===========================================================================
 */

DtReponse csr (DtNuage ng1, DtNuage ng2, double dseuil, int dimd, int *kds, DtDistRec *dist, DtMatrix4x4 mat, DtPoint3 *crdsSup1, DtPoint3 *crdsSup2, int nbIter, DtReponse rep)
{
  int compteur,nbAtApp,nbAtTemp;
  double rmsd;
  long int gSeed = -1;
  long int *seed;
  
  seed=calloc(1,sizeof(long int*));
  *seed=gSeed;
  
  compteur = 0;
  while (compteur < nbIter){

    rmsd=-1;
    nbAtTemp=1;
    initRnd(seed);
    MkNullMat4x4(mat);
    posAl(ng1, ng2, mat);
    nbAtApp=sdm(ng1,ng2,dseuil,dimd,kds,dist);

    /* printf("%d\t",nbAtApp); */

    /*
    crdsASuperposer(ng1,ng2,nbAtApp,dist,crdsSup1,crdsSup2);
    printf("rms1 %lf\t",rmsd);
    rmsd=zuker_superpose(crdsSup1,crdsSup2,nbAtApp,mat);
    printf("rms2 %lf\n",rmsd);
    rotateCrds(ng2.crds,ng2.n,mat);
    */

    while(nbAtApp > nbAtTemp){

      /* --- Stockage de la réponse si meilleure --- */

      if(nbAtApp > (rep->nbAtMax)){
	printf("itération : %d\t nombre d'atomes appariés : %d\n",
	 compteur,nbAtApp);
	stockRep(*kds,nbAtApp,dist,rmsd,mat,rep);
      }
      
      
      
      /* --- Nouvel appel de zuker-sdm pour un meilleur appariement--- */

      nbAtTemp=nbAtApp;
      
      /* printf("rms1 %lf\t",rmsd); */
      
      crdsASuperposer(ng1,ng2,nbAtApp,dist,crdsSup1,crdsSup2);
      rmsd=zuker_superpose(crdsSup1,crdsSup2,nbAtApp,mat);
      rotateCrds(ng2.crds,ng2.n,mat);
      
      /* printf("rms2 %lf\n",rmsd); */
      
      nbAtApp=sdm(ng1,ng2,dseuil,dimd,kds,dist);
      
      
      /* printf("%d\t",nbAtApp); */
      
      
      /*
	crdsASuperposer(ng1,ng2,nbAtApp,dist,crdsSup1,crdsSup2);
	printf("rms1 %lf\t",rmsd);
	rmsd=zuker_superpose(crdsSup1,crdsSup2,nbAtApp,mat);
	printf("rms2 %lf\n",rmsd);
	rotateCrds(ng2.crds,ng2.n,mat);
      */
    }

    /* printf("\nSortie boucle : %d <= %d\n",nbAtApp,nbAtTemp); */
    

    compteur=compteur+1;
    /* printf("\n"); */
  }

  return rep;
} 
