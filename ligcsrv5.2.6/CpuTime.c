/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
/* ================================================================
 * Pierre Tuffery, November 1993
 * Version 1.5
 * ================================================================
 */
/* cputime.c ------------------------------------------ */
/* ------------------------------------------------------
 * functions to get CPU time
 * ------------------------------------------------------
 */
#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <sys/times.h>

struct tms time0, time1, time2;

#define CLK_TCK CLOCKS_PER_SEC

/* -----------------------------------------------
 * La date sous forme jour, heure, min, etc...
 * On rend un pointeur sur la chaine de caracteres
 * -----------------------------------------------
 */
char *mygetdate()
{
  time_t myclock;

  myclock = time(0);
  return(ctime(&myclock));
}

/* -----------------------------------------------
 * On prend note du temps courant dans t
 * -----------------------------------------------
 */
void gettime(FILE *f, struct tms *t)
{
  if(times(t) == -1) {
    fprintf(f,"WARNING: Could not run Elapsed time procedure\n");
  }
}

/* -----------------------------------------------
 * Affiche le temps CPU ecoule entre ti et tf
 * -----------------------------------------------
 */
void elapsedUserTime(FILE *f, struct tms *ti, struct tms *tf)
{
  fprintf(f,"\nElapsed cpu time: %.2lf sec.\n", 
	  (double) (tf->tms_utime - ti->tms_utime) / (1. * CLK_TCK));
}

/* -----------------------------------------------
 * Affiche le temps CPU ecoule entre ti et tf
 * -----------------------------------------------
 */
void elapsedSystemTime(FILE *f, struct tms *ti, struct tms *tf)
{
  fprintf(f,"\nElapsed system time: %.2lf sec.\n", 
	  (double) (tf->tms_stime - ti->tms_stime) / (1. * CLK_TCK));
}

/* -----------------------------------------------
 * Affiche le temps CPU ecoule entre ti et tf
 * -----------------------------------------------
 */
void elapsedRealTime(FILE *f, struct tms *ti, struct tms *tf)
{
  fprintf(f,"\nElapsed real time: %.2lf sec.\n", 
	    (double) (tf->tms_utime - ti->tms_utime) / (1. * CLK_TCK)
	  + (double) (tf->tms_stime - ti->tms_stime) / (1. * CLK_TCK));
}

