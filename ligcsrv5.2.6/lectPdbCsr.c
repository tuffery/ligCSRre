/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/times.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include "utilCsr.h"



/* ===================================================
* Renvoie une chaine de caractères en majuscules.
* ===================================================
*/

void strupper(char *s)
{
  while (*s != '\0') {
    *s = toupper(*s);
    s++;
  }
}





/* ====================================================
* Numero d'atome pour le residu
* ====================================================
*/
void getAtomNum(char *line,int *anum)
{
  char buff[30], charAnum[6];

  charAnum[5] = '\0';
  if (sscanf(line,"%6c%5c",buff,charAnum) != EOF);
  *anum = atoi(charAnum);
}





/* ====================================================
* Nom de l'atome
* 3 char lus + 1 blanc pour atom index
* ====================================================
*/
void getAtomName(char *line,char *anom)
{
    char buff[30];

    anom[3] = ' ';
    anom[4] = '\0';
    if (sscanf(line,"%13c%3c",buff,anom) != EOF);
    strupper(anom);
}





/* ====================================================
* Index d'atome pour le residu (cf params.c)
* ====================================================
*/
void getResName(char *line,char *rnom)
{
    char buff[30];

    rnom[3] = '\0';
    if (sscanf(line,"%17c%3c",buff,rnom) != EOF);
    strupper(rnom);
}





/* ====================================================
* Coordonnees de l'atome
* ====================================================
*/
void getAtomCoords(char *line,DtFloat *x,DtFloat *y,DtFloat *z)
{
  char buff[30], charX[9],charY[9],charZ[9];

  charX[8] = charY[8] = charZ[8] = '\0';
  if (sscanf(line,"%30c%8c%8c%8c",buff,charX,charY,charZ) != EOF);
  *x = (DtFloat) atof(charX);
  *y = (DtFloat) atof(charY);
  *z = (DtFloat) atof(charZ);
}





/* ====================================================
* Type d'info contenu dans la ligne du fichier PDB
* 0: info non codee ou Pb.
* 1: ATOM
* 2: HELIX
* 3: SHEET
* 4: TURN
* 5: TER
* 6: CONECT
* 7: END
* 8: MODEL
* 9: ENDMDL
* ====================================================
*/
int getPDBLineStatus(char *d)
{
  char Entete[7];

  if (sscanf(d,"%s",&Entete) == EOF) return 0;
  if (strcmp(Entete,"ATOM") == 0) return 1; //int%Gï¿½%@ressant
  if (strcmp(Entete,"HETATM") == 0) return 8; //int%Gï¿½%@ressant
  if (strcmp(Entete,"HELIX") == 0) return 2;
  if (strcmp(Entete,"SHEET") == 0) return 3;
  if (strcmp(Entete,"TURN") == 0) return 4;
  if (strcmp(Entete,"TER") == 0) return 5;
  if (strcmp(Entete,"CONECT") == 0) return 6;
  if (strcmp(Entete,"END") == 0) return 7;
  if (strcmp(Entete,"MODEL") == 0) return 8;
  if (strcmp(Entete,"ENDMDL") == 0) return 9;
  return 0;
}


/* ===========================================================
* Renvoie les lignes d'un fichier et leur nombre de lignes.
* ===========================================================
*/

char ** txtFileRead(char *fname, int *lSze)
{
  struct stat buf;
  char *tmp;
  char **line;
  int fd;
  int fSze, done;
  int nLines;
  int i,j;

  if ((fd = open(fname,O_RDONLY)) < 0) {
    return NULL;
  }

  if (fstat(fd,&buf)) {
    close(fd);
    return NULL;
  }

  if (!S_ISREG (buf.st_mode)) {
    close(fd);
    return NULL;
  }

  if (!(fSze = buf.st_size)) {
    close(fd);
    return NULL;
  }

  if (!(tmp = malloc((fSze+1) * sizeof(char)))) {
    close(fd);
    return NULL;
  }

  done = 0;
  while (done < fSze) {
    int r = read(fd, &(tmp[done]), fSze - done);
    if (r < 0) {
      free(tmp);
      close(fd);
      return NULL;
    }
    done += r;
  }
  close(fd);

  nLines = 0;
  for (i = 0; i < fSze; i++) {
    if (tmp[i] == '\n') nLines++;
  }
  if (tmp[fSze-1] != '\n') nLines++;

  if (!(line = malloc((nLines+2) * sizeof(char *)))) {
      free(tmp);
      return NULL;
  }

  line[0] = tmp;
  j = 1;
  for (i = 0; i < fSze; i++) {
    if (tmp[i] == '\n') {
      line[j++] = &tmp[i+1];
      tmp[i] = '\000';
    }
  }

  line[j] = NULL;
  *lSze = nLines;

  return (line);
}




/* ===============================================
* Réalloue la mémoire occupée par txtFileRead.
* ===============================================
*/

char ** txtFileFree(char **l, int lSze)
{
  /* Attention: on ne libere que la premiere ligne,
     car c'est un seul malloc pour l'ensemble des lignes
  */
  free(l[0]);
  free(l);
  return (NULL);
}


/* 
============================================================================
* Remplie les tableaux de donn%Gï¿½%@es utiles %Gï¿½%@ Csr apr%Gï¿½%@s lecture des fichiers
Pdb.
* Alloue la m%Gï¿½%@moire pour la structure DtNuages.
* (n1ou2) est un param%Gï¿½%@tre pour savoir quel jeux de coordonn%Gï¿½%@es l'utilisateur
d%Gï¿½%@sire rentrer, celui de la prot%Gï¿½%@ine 1 ou celui de la prot%Gï¿½%@ine 2.
* ============================================================================
*/


void lectPdbCsr (char *fname, DtNuage *n)
{
  int nLines;
  int aLine;
  int lineStat;
  char **lines;


  lines=txtFileRead(fname,&nLines);

  // fprintf(stderr,"read %d lines from %s\n",nLines, fname);


  // Nombre d'atomes ?
  n->n = 0;
  for (aLine=0; aLine < nLines; aLine++) {
      lineStat=getPDBLineStatus(lines[aLine]);
      if(lineStat==1){
	n->n++;
      }
      if(lineStat==8){
	n->n++;
      }
  }

  // Allocation memoire
  n->crds = (DtPoint3 *) calloc(n->n, sizeof(DtPoint3));
  n->atmName = (DtStr4 *) calloc(n->n, sizeof(DtStr4));
  n->resName = (DtStr3 *) calloc(n->n, sizeof(DtStr3));

  // Lecture donnees
  n->n = 0;
  for (aLine=0; aLine < nLines; aLine++) {
      lineStat=getPDBLineStatus(lines[aLine]);
      if ((lineStat!=1) && (lineStat!=8)) continue;

      getAtomCoords(lines[aLine],&n->crds[n->n][0],&n->crds[n->n][1],&n->crds[n->n][2]);
      getAtomName(lines[aLine],n->atmName[n->n]);
      getResName(lines[aLine],n->resName[n->n]);
      n->n ++;
  }

  // liberation memoire
  txtFileFree(lines,nLines);
}


void printNuage(FILE *f, DtNuage *n, char *msg)
{
  int aDot;
  fprintf(f, "%s\n",msg);
  fprintf(f, "n points: %d\n",n->n);
  for (aDot = 0; aDot < n->n; aDot++) {
    fprintf(f, "%s %s %lf %lf %lf\n",
	    n->atmName[aDot],n->resName[aDot], 
	    n->crds[aDot][0],n->crds[aDot][1],n->crds[aDot][2]);
  }
}

/* ====================== FONCTION MAIN ======================================
* ===========================================================================
*/

int main (int argc,char *argv[])
{
  DtNuage n1;
  DtNuage n2;
  char *fname1,*fname2;


  fname1=argv[1];
  lectPdbCsr(fname1,&n1);
  printNuage(stderr, &n1, "Nuage1");


  fname2=argv[2];
  lectPdbCsr(fname2, &n2);
  printNuage(stderr, &n2, "Nuage2");

  return 0;
}
