/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/times.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include "CSRTypes.h"
#include "PDB.h"
#include "Random.h"
#include "3DFuncs.h"
#include "TxtFile.h"
#include "Zuker.h"
#include "regex.h"
#include "mol2.h"

// Atom pairs, (-1 = BC), teta, dir
#define DcNDrawings 200
double Drawings[200][6] = {
{ 90, 41, 2.35916893650697, 1.29571412130762, -2.27899019262253, -0.541775744101193},
{ 27, 13, 0.223583132961503, 1.60651957199581, -0.178142851870047, 0.207905286751998},
{ 263, 29, 3.866349935456030E-003, 0.882399194261013, 0.241945221389524, 0.826866565860554},
{ 23, 23, 1.60671986487422, -0.540513591970099, -2.16600803466422, 2.14874117505887},
{ 44, 44, 0.712994181606531, -0.249369350578340, -0.940919862779302, -9.558909062836132E-002},
{ 30, 27, 0.160789558728651, -1.02690032760264, 1.47811904027470, -1.13631558071933},
{ 45, 34, 2.83049049118152, -0.178242309327540, -1.62958419298795, -2.22116361109492},
{ 40, 15, 1.88202871679651, 1.69669157055185, -0.518309768473713, 0.532797631291160},
{ 14, 40, 0.620740064055963, -0.914874098220875, 0.744258036032914, 1.26891664150302},
{ 30, 6,  2.96553349016997, 0.148746570685647, 1.20676055406209, 0.181827764343887},
{ 37, 37, 1.93520512323077, 0.133131651186945, 0.408179451953088, 1.52051295798055},
{ 3, 3, 2.22236294860227, 0.327077154963423, -4.839152599927829E-003, -0.736630334228331},
{ 31, 31, 0.152834797953860, 0.400116480279146, 8.322087063649747E-002, 0.167346958266905},
{ -1, -1, 1.308176818537489E-002, 2.17974405469959, 4.017183066919418E-002, -0.612256682967671},
{ 20, 8, 1.04469971495410, -1.51135121577944, 1.33153608789644, -0.491676686290307},
{ 25, 20, 1.08945322581507, 1.37235170806852, -0.517199157797379, -0.596392376517571},
{ -1, -1, 0.494274316817246, 0.215192263425746, -0.848720649087391, 1.27643546549365},
{ 1, 39, 0.851041703841393, 1.45666801237773, -1.22610971371825, -0.335414027443132},
{ 21, 8, 2.42666667965968, -0.204633482806018, 1.65220788466060, -0.466724295930646},
{ 13, 11, 2.80205986807679, 1.12861321333419, -0.173713958970791, 0.899701580624935},
{ 20, 22, 1.98853422290922, -0.325219631745050, -1.96380002786205, -0.555373978694642},
{ 30, 30, 2.40068585956538, -0.630096297153852, -1.41781271208902, 0.529673416039202},
{ 25, 25, 1.34408672940023, 0.306441652263022, -0.341147157528923, 1.41443788058487},
{ 27, 27, 0.870139941539584, -0.747818990152379, 1.53134195294231, -0.754481218465239},
{ -1, -1, 0.400209841964191, -1.11085317082519, 6.468318805197625E-002, 3.766854080533361E-002},
{ 9, 9, 1.67199082812252, 0.427538174895285, 0.760788568463942, 0.838618871247605},
{ 22, 38, 2.70702094420733, 0.904012882829351, 0.352770651748799, -1.04651582286500},
{ 32, 14, 1.91208372070161, -1.14309930396894, 0.707919821603522, -0.483129876512661},
{ 18, 18, 1.56615379592071, -4.358590542585430E-002, -1.17216726397522, 1.05823893152068},
{ -1, -1, 2.07193414837958, 0.338443709723167, -0.491002741572549, 1.39204333657616},
{ 17, 17, 0.978940881770439, 1.05956305526741, 0.129312186101252, -1.20938029882181},
{ 42, 32, 2.65609485938012, 1.13844979560428, -0.200006714907721, -0.675898070645562},
{ -1, -1, 1.92769633083005, 2.44782945261817, 2.24825142378626, -0.349939356172844},
{ 33, 33, 2.27612298544888, 1.60844449014341, -0.637151708811869, -1.310232688712073E-003},
{ 22, 22, 2.84243584909927, 0.484787330932326, 1.91045758984064, -0.707893444829750},
{ 33, 14, 2.02358408382514, -0.323433107127571, 2.02241083850126, 0.176251478825491},
{ 4, 4, 2.92915891058806, 0.304280238755563, -0.679725736574181, -0.534055288571600},
{ 27, 42, 1.69225796306567, -1.27615728698369, 1.83232348296146, -1.06520247655205},
{ 1, 33, 1.37568757061624, 0.136515399801609, -1.22707810337319, 0.496682803318444},
{ 37, 37, 2.90598721992813, 1.02573420369759, 2.05237764553584, 0.881763742517334},
{ -1, -1, 2.02777061346983, 0.506600530215442, -5.803692221909437E-002, -0.803445620070241},
{ -1, -1, 2.45236283685956, -0.956676098690975, 0.795977808140137, -1.32680219735306},
{ 29, 29, 1.33273922173204, 0.471067993274407, -0.323588038052348, 1.86797361482402},
{ 34, 13, 2.45495214176363, -1.47259600312490, 0.193277231373866, 1.54331050945415},
{ 36, 1, 0.694733045941575, -6.938910888803128E-002, -0.577082163885422, -0.149047931192312},
{ 23, 21, 1.96741799856967, -1.02309968848097, 0.690224996038730, 5.342138996010716E-002},
{ 19, 19, 2.35356846201475, -1.30583608735526, -1.00906973061201, 1.14206346289684},
{ -1, -1, 2.06895966664663, -7.790784594745108E-002, 1.44960417591805, -0.529911399852813},
{ 1, 31, 2.12447026605169, 0.282369836625281, -1.15652397147965, 1.99515463166176},
{ 40, 32, 2.58316128522923, -1.21393295997948, -0.198215677552442, -0.153747648960719},
{ 4, 4, 0.834875363910542, -1.77596516934663, 0.160023310733108, 0.104291530930692},
{ -1, -1, 2.18475264752311, 1.56719283535425, -1.83357295618496, -0.256458976630688},
{ -1, -1, 1.43047367081420, 0.298261856432420, 0.651555063947840, -0.611338995231427},
{ 37, 13, 2.76364985321641, 1.06204724804404, 0.851401388940131, 0.245290136022895},
{ 41, 12, 2.51936672365432, 1.22103113067915, 0.109432919117043, 1.50063045244471},
{ 15, 15, 2.56957149382891, 2.04163808144996, -5.162610931593661E-002, -0.358542220072297},
{ 34, 27, 2.83963148928679, -0.737956582993614, 2.35500449087740, -0.267130611548580},
{ 32, 41, 0.303729573230365, -0.494260665802514, 0.953959739262264, -0.171632311744267},
{ -1, -1, 1.48647382111344, 1.34163024039263, -0.202055758897665, -0.515644014644237},
{ 38, 41, 2.17294623595345, 3.322810583124444E-002, -1.76359362244162, 0.938571475362609},
{ 37, 1, 2.09050617521498, -2.29399092968175, -0.107482040945598, -0.295770670746558},
{ 7, 7, 1.82062161321727, 1.07777356261304, 0.219712087321762, -1.36289873316904},
{ 15, 15, 2.00319394190301, 0.449995096965633, -0.216047787759893, 1.203818075006672E-002},
{ 19, 7, 1.85143248782333, 1.18253236584814, -1.44816897533441, -0.515338089841949},
{ 4, 4, 0.485531914950157, 0.764264716265087, -0.189675872713523, 1.49870331006091},
{ 5, 35, 2.25262533967826, 0.201076259953359, -1.51335403685103, -1.22243553909865},
{ 36, 36, 1.06744018390919, 2.75463769454289, -0.200476507150939, -0.471121150694466},
{ 21, 27, 0.322102774636255, 0.632523584588950, -0.714937992686999, 1.33105086110177},
{ 10, 26, 0.771077797464104, -0.647092727368392, -1.97122693761930, 1.66617355404085},
{ -1, -1, 0.198776735035437, 0.593110451491836, -0.486552752291745, -0.487438188490716},
{ 46, 46, 1.29777450996982, 0.318723602156032, -0.340118413269346, -0.787940590213061},
{ 39, 39, 2.89895905021123, 0.596580497663680, 0.961165010931108, -0.645372677334236},
{ 8, 25, 1.64440142754653, -1.12278077465275, -6.990954146617787E-002, 0.485786769433790},
{ 40, 16, 2.89842587691882, -0.950585925535758, -0.411119835059685, 0.658098822528779},
{ 10, 10, 0.804895987583269, 0.578778917777455, -0.541812922418790, 0.731593894684165},
{ 18, 18, 3.04145886501494, -1.39408627097576, 0.656867211356694, -0.587941186638788},
{ 32, 16, 1.243296967430822E-002, -0.834620511219181, -0.983903179481520, -1.50023206321111},
{ 23, 25, 0.168744542687048, -6.845258182885747E-002, 4.405834786661968E-002, 0.243432789064215},
{ 12, 12, 1.97318655567892, -0.810334123218761, -8.522273847060961E-002, 0.734760523338111},
{ 7, 7, 2.76061194806174, 0.609903758661441, 1.70935852131360, 0.352464568426572},
{ 34, 34, 2.21583595536128, 0.835638112334703, -0.509267336736663, 0.105425471055016},
{ 35, 16, 1.39713362895936, -0.157502548698983, -1.54045399224653, -0.461011944584358},
{ 12, 12, 2.90246446501743, -0.604383317344090, 6.656086457104347E-002, -0.930154984444424},
{ 40, 20, 0.654371635608613, -1.22886113152815, -0.101774670322083, -1.57449607328773},
{ 3, 9, 1.41252477734108, -0.949567615530729, -0.504572333747489, 1.48718013779263},
{ 18, 9, 1.767853173324638E-002, 0.658606188823989, -1.74196692778840, -1.27349651972307},
{ 20, 20, 1.99700672088404, -1.43546439709210, 0.648152789102465, 1.01832570866264},
{ 45, 21, 1.47100127662162, -0.686547135770512, 5.939553190370499E-002, 0.255727083908573},
{ 32, 11, 2.89469625392403, 0.679438414524362, 0.987078084972424, 0.614417553730062},
{ 23, 46, 0.613723678336652, 0.530087776580937, -1.63969335602064, -0.884941357365900},
{ 33, 33, 1.76933888541059, 1.20897233489788, -0.519371507895193, 1.77568222038071},
{ 4, 22, 2.56498991806814, -0.254769069952563, -0.146849991002528, -2.77761320198712},
{ 20, 37, 0.895503367830975, 0.231546500423225, -0.472063844803393, -1.29387416424107},
{ 34, 26, 0.449162511546044, 0.627668391475141, 3.00516056265289, 0.484961767215190},
{ 26, 26, 3.067364885841975E-002, 1.10693211928469, 0.449397802634852, 0.718624765073815},
{ 17, 17, 1.730962516243195E-002, 0.483556978068928, -0.341438565830829, -0.723814667572812},
{ -1, -1, 0.824268123741748, 1.88763686690780, 0.378875221206294, -2.03052603561603},
{ 17, 17, 0.769562078966559, -0.429562069255769, 2.22508801493434, 1.27800681490493},
{ 38, 38, 9.320859201961089E-002, 1.37634102583385, 0.804234443040204, -0.542614132707150},
{ 26, 26, 1.65567803525525, 1.20789064208446, -0.981358102611060, 0.328257782959870},
{ -1, -1, 0.523477534823651, -1.08546631731189, 0.182235292135814, 1.03534065050161},
{ 16, 19, 2.47717317900463, -0.703946980929101, 0.358655092452540, -1.15255888650582},
{ -1, -1, 3.06911759953455, -1.43599315617486, 0.114565841722916, 0.492912215502415},
{ 35, 35, 2.57588479920695, -0.187466337630814, 0.689265247506717, 1.82722548434715},
{ 15, 8, 1.11572136873168, -9.695351382211986E-002, -0.720678536672535, -0.937926474967986},
{ 14, 14, 0.261585324565321, -1.11299002657416, 1.52117337278487, 0.204114398515297},
{ 14, 27, 3.06014191660327, 0.571604777515602, -1.43370388882117, -1.13640736386241},
{ 23, 23, 1.63276510318921, -1.57942346098219, 0.515022237960637, 0.214735995936570},
{ 19, 40, 3.12880458273468, -1.40913024690515, -0.542348453297304, 1.45349592827905},
{ -1, -1, 2.38143421159641, 0.911738834336572, -0.366135961212600, 0.955362387061787},
{ 18, 38, 2.06322944554198, -0.922091092845172, -1.18998460256261, 0.601902090980464},
{ 36, 6, 6.235180984756292E-002, -1.34264416928450, -0.461748090276950, -0.754502105081136},
{ 46, 11, 2.24763093911953, -1.49200901351235, -1.18957660318051, 0.865720422710463},
{ 17, 17, 0.368711171374132, -1.80685356793162, 1.694747177434859E-002, -0.802633966139432},
{ 17, 41, 2.45092543318602, 0.340522305373042, 1.04503254439940, -0.332452598778638},
{ 45, 43, 0.236103244110057, -0.403254523189380, -0.672267623701800, 0.930573048667301},
{ 22, 45, 2.83999141854739, -0.311030363957222, 1.74967056724328, 0.368306532586717},
{ -1, -1, 0.155261005727719, -0.911220971327727, -2.06332786097818, 0.142054044097055},
{ 28, 28, 0.861438860467646, -0.406668538677454, 0.174918778509282, 0.561159565545989},
{ 18, 18, 1.74853845189395, 1.22811455553430, 0.526012785880993, 0.236317067507231},
{ 11, 18, 1.76922629512359, -0.174681618480682, -0.571669130057752, 0.170097194300992},
{ 36, 18, 0.975835471861156, -1.07739984554207, -0.636657746924034, -0.617455296366306},
{ 23, 23, 2.78986880576290, 2.04889345753897, -0.269316831936945, -1.46371811140438},
{ -1, -1, 1.03272534521769, -0.899864466849861, 1.29189404561719, 1.53271744317283},
{ 40, 34, 2.65561209712904, 3.919770240452474E-002, -0.211007468105820, 0.857134855996505},
{ -1, -1, 1.86391807785274, -1.97398746512565, 0.607600775232529, -1.89211386363120},
{ 28, 28, 2.83328566439623, -2.18661785947454, -7.223806488644505E-003, 2.06720810540572},
{ 15, 44, 2.37651623607873, 1.57192397689965, 0.668633314691468, 6.969286719631189E-002},
{ 15, 15, 1.07366538125155, -0.986241904380498, -0.193833899425404, 1.38023589380068},
{ 17, 23, 0.251605962131806, 0.203178648250824, -0.689957990089710, -0.235348412007915},
{ 34, 9, 1.75236701622333, 0.735811788212354, 0.814978022918582, -1.29731114404702},
{ 41, 42, 2.39612487453899, 0.293140381446302, 0.915759474847746, -1.11030318170836},
{ 6, 6, 0.958252610224728, -0.688448876262934, 0.929433024885786, 1.02516136253272},
{ -1, -1, 4.296252521551784E-002, -2.344438190689955E-002, -0.745390546232327, 2.655223461669536E-003},
{ 24, 24, 0.121503255559223, 0.397564669520010, -0.592637155780565, -4.060750621146296E-002},
{ 39, 39, 0.951042617272907, 0.818658421566832, -0.610729048446555, -0.344315056747589},
{ 34, 4, 1.06511945533957, 0.810486475031127, 2.13714791873966, -0.903470356996425},
{ 20, 20, 0.483813796938891, -1.53014895153694, 0.874053488112664, 2.15752205454356},
{ 40, 43, 1.79424751077738, 1.62734555852157, 0.463526461142151, 0.617500299517865},
{ 40, 45, 1.32768481434829, 0.502894414518013, 0.817282401331378, -2.46953786704248},
{ 27, 27, 2.79713173069603, 1.13387883153979, 0.360852284737235, 1.32842970489227},
{ 10, 10, 0.945703166853186, -1.22923708839810, -0.727468078893312, -6.145505879827452E-002},
{ 25, 6, 2.87232413523111, 2.77191144675481, 1.04487400213463, -0.376899375342363},
{ 13, 13, 0.727411977045047, -0.255256992354461, -0.785989350029545, 1.20095195070981},
{ 1, 1, 0.606346587349438, 0.742009658846409, 0.226584873053088, -0.265949844383258},
{ -1, -1, 0.970611525030927, 0.744035355550475, 0.878082894920203, 1.26171209073024},
{ 23, 43, 0.319011819766203, -0.389358148706544, -0.147847818733806, 0.806276354190310},
{ 40, 40, 5.223439475092183E-002, -0.199149620358853, -0.340936541423076, 0.949085231613502},
{ 2, 2, 1.97854764726125, -0.198519938194855, 0.924537968544057, -0.400215916073951},
{ 11, 11, 1.07090630514375, 0.131144384637277, 2.47349497171919, -0.540020165948720},
{ 27, 44, 2.96518174775373, -0.814466506526747, 1.14200695610968, -0.426166926739571},
{ 1, 1, 0.517437561621666, -1.06763445323979, 0.394243124547324, 0.215480638683899},
{ 11, 11, 2.36380903870366, 1.87404949354629, -1.22148910885993, -1.04581191373212},
{ -1, -1, 1.74938873019489, 1.95247729147822, 0.461972544843279, -9.745583911958793E-002},
{ 8, 8, 0.328876709134491, -0.432345312785947, 0.253913549144404, 1.17024114132222},
{ 44, 44, 3.06778536742070, -0.573743540898973, -0.768312470964717, -0.740481418505687},
{ -1, -1, 1.45943824702047, 0.315229523698588, 0.947622183422580, 2.944808784738540E-002},
{ 3, 3, 2.33830919166846, -0.951201509500078, 1.22632350512963, 8.336448441730619E-002},
{ 45, 40, 0.652762749961287, 0.525349684527993, 0.540160156502531, -0.295425275972086},
{ 37, 37, 2.66667625521289, -8.715109506027469E-002, -1.72139796639339, 1.13271592896667},
{ -1, -1, 2.64095811984418, 0.569385295082765, -0.193833414528393, 0.334457521531970},
{ 41, 41, 1.10451162618088, 0.435287082720267, -2.59811690899968, -1.20622327569944},
{ 38, 18, 0.758721874404822, -0.557092423213380, -0.282775112224569, 1.23529739288616},
{ 18, 10, 1.02661700995940, 1.34639646153274, 4.105109477842014E-002, -1.49078047470422},
{ 39, 15, 2.07841260760738, -0.683205128697185, 0.110791880042019, 0.142851316325168},
{ 28, 28, 1.10746657824270, 0.417554110624091, -0.189861229398820, -0.657261728887546},
{ 19, 40, 2.05556988551362, 1.52076809587078, 0.233337222691775, 0.617251054645601},
{ 20, 20, 0.709055546936387, -1.53141488382010, -0.986133347625469, -1.52057376767462},
{ 26, 25, 2.72887217262093, 2.21945563518202, -0.460897233946233, 1.56028241732054},
{ 21, 21, 0.357709828009246, 1.00969762090465, -1.91967340348396, -3.643341156540848E-002},
{ 37, 32, 2.18358192583642, 0.249292508789554, 0.567592889210034, 1.05622155505922},
{ 10, 10, 0.311766697801475, -0.739007103059440, 0.461209243564008, -0.138845410413663},
{ 45, 7, 0.140590790864898, -0.429442076519979, 1.47207707293464, 2.18095303738076},
{ -1, -1, 1.24185283356298, 0.293104093218085, -0.225854763341038, 0.221456025517029},
{ -1, -1, 2.94898359325826, 0.831682574395152, 0.456296173692579, 1.24860511606823},
{ 24, 24, 1.28943818170199, 1.03646625494725, -0.153585527403272, -1.88934973820192},
{ 14, 14, 2.15330131957459, -0.422980426835730, -0.886185262703140, -0.175935662452670},
{ 12, 12, 2.60611588439567, 0.228220175591408, 0.444069941531449, -1.54216034492753},
{ 11, 11, 0.578303189485760, 0.108808392169411, -1.43956239991087, 3.61241815216172},
{ 34, 31, 0.705995015246886, 0.248024731698834, -0.273011173006329, 2.20384432080564},
{ 43, 17, 0.296658570589946, -0.723437145748469, 2.03842998087412, -0.830324600881671},
{ -1, -1, 2.23102378800889, 1.93589673440404, -0.406701971871076, -1.11972489809039},
{ -1, -1, 2.35073921164489, -9.631767764535665E-002, -0.453439687362130, 3.998919890460619E-002},
{ 20, 20, 2.91843065013884, -1.03793024600241, 0.601341762822782, -2.34180545646771},
{ 13, 13, 1.60089489312907, 1.02295452322383, -0.173134831982458, -0.182918246557593},
{ 4, 4, 1.82645958356284, -0.129765395623781, 0.593360856386038, -0.309412313022530},
{ 44, 44, 1.32314916259934, -0.558008720750952, 0.356913541058009, -1.09108915077602},
{ 28, 21, 1.62992000732039, -0.352939770005342, 0.824282112761133, 0.197031580926425},
{ 24, 21, 2.23059685549385, 1.34311322505566, -1.58200326155009, -2.20870893676260},
{ 33, 33, 1.36473384425010, -1.42177337174670, 1.70651920383912, 1.04587029599539},
{ -1, -1, 1.95871024742133, -0.926346029061042, -1.02264447936474, -1.30500612650520},
{ 29, 29, 1.85329507669234, -1.31242393920608, -0.502386698958694, -1.35414184223986},
{ 1, 3, 0.310569295170921, -3.958354191572211E-002, -1.42291366439895, -0.688291993255210},
{ 37, 37, 0.953121006996896, -0.255152992139541, -0.111672830452298, 0.791388205362356},
{ 44, 34, 0.579649800621960, 1.28407907283993, -0.450902460052374, 1.23714397507650},
{ 1, 25, 0.125697148028084, 1.30651360526600, -1.00735308364726, -1.44335435974715},
{ 19, 15, 0.863782043691838, 0.139118261069025, 0.697342898766352, 0.533338239917598},
{ 22, 22, 2.04013186632130, -0.448939448949794, -0.493903496094715, -0.123258145965217},
{ 43, 2, 0.323208797300757, -6.641176766580527E-002, -0.377113089850292, 0.829739030637381},
{ 25, 25, 0.880696602356702, -2.17019808443133, 1.99069357505259, -0.445849788491253},
};

/* =====================================================
 * Allocation memoire d'un nuage pour une taille donnee
 * =====================================================
 */
DtNuage *allocNuage(int sze)
{
  DtNuage *pNg;

  pNg           = calloc(1,sizeof(DtNuage));
  pNg->crds     = (DtPoint3 *) calloc(sze,sizeof(DtPoint3));
  pNg->atmName  = (DtStr4 *)   calloc(sze,sizeof(DtStr4));
  pNg->atmNme   = (DtStr4 *)   calloc(sze,sizeof(DtStr4));
  pNg->resName  = (DtStr3 *)   calloc(sze,sizeof(DtStr3));
  pNg->resNum   = (int *)      calloc(sze,sizeof(int));
  pNg->chn      = (char *)     calloc(sze,sizeof(char));
  pNg->skip     = (char *)     calloc(sze,sizeof(char));
//  pNg->reLcl    = (char *)     calloc(sze,sizeof(char));
  pNg->re       = (DtReRec **) calloc(sze,sizeof(DtReRec *));
  pNg->n        = sze;

  return pNg;
}

/* =====================================================
 * Allocation memoire d'un nuage pour une taille donnee
 * =====================================================
 */
void freeNuage(DtNuage *pNg)
{

  if (pNg != NULL) {
    if (pNg->crds    != NULL) free(pNg->crds);
    if (pNg->atmName != NULL) free(pNg->atmName);
    if (pNg->atmNme  != NULL) free(pNg->atmNme);
    if (pNg->resName != NULL) free(pNg->resName);
    if (pNg->resNum  != NULL) free(pNg->resNum);
    if (pNg->chn     != NULL) free(pNg->chn);
    if (pNg->skip    != NULL) free(pNg->skip);
    if (pNg->re      != NULL) free(pNg->re);
    freeConnectMatrix(pNg->connectMatrix, pNg->n);
    free(pNg);
  }
}

/* ============================================================================
 * Recherche d'une re a partir des re atomes et res.
 * Retourne NULL si aucun match.
 * ============================================================================
 */
DtReRec *rePtrFromStr(DtReRec *pReList, char *atmStrRe, char *resStrRe)
{
  while (pReList != NULL) {
    if ((!strcmp(atmStrRe, pReList->reAtmStr)) && (!strcmp(resStrRe, pReList->reResStr)) ) {
      return pReList;
    }
    pReList = pReList->next;
  }
  return NULL;
}

int rePtrListLen(DtReRec *pReList)
{
  int n = 0;
  while (pReList != NULL) {
    n++;
    pReList = pReList->next;
  }
  return n;
}

void atmInternalName(char *atmName)
{
  char atmStrRe[BUFSIZ];

  if ((atmName[0] == 'H') 
      || ((atmName[0] >= '1') && (atmName[0] <= '4') && (atmName[1] == 'H'))
      || ((atmName[0] == ' ') && (atmName[1] == 'H')) ) { /* Hydrogens */
    if (atmName[0] == 'H') {
      strcpy(atmStrRe, atmName);
    } else {
      strcpy(atmStrRe, atmName+1);
    }
    if ((atmStrRe[1] >= 'A' ) && (atmStrRe[1] <= 'Z')) atmStrRe[2] = '\0';
    else atmStrRe[1] = '\0';
  } else { /* Heavy atom */
    if (atmName[0] != ' ') {
      strcpy(atmStrRe+1, atmName);
      atmStrRe[0] = ':';
      atmStrRe[3] = '\0';
    } else {
      strcpy(atmStrRe, atmName+1);
      if ((atmName[2] >= 'A' ) && (atmName[2] <= 'Z')) atmStrRe[2] = '\0';
      else atmStrRe[1] = '\0';
    }
  }
  // fprintf(stderr,"%s %s\n",atmName, atmStrRe);
  strcpy(atmName, atmStrRe);
}

/* ======================================================
 * Build a re string from atom name
 * ======================================================
 */
void atmName2dftlReStr(char *atmName, char *atmStrRe)
{
  int l;

  l = strlen(atmName);
  strcpy(atmStrRe+1,atmName);
  atmStrRe[0] = '^';
  atmStrRe[l+1] = '$';
  atmStrRe[l+2] = '\0';
  // fprintf(stderr,"%s %s\n",atmName, atmStrRe);
}

void atmName2fuzzyReStr(char *atmName, char *atmStrRe)
{
  int l;

  l = strlen(atmName);
  strcpy(atmStrRe+1,atmName);
  atmStrRe[0] = '^';
  atmStrRe[l+1] = '$';
  atmStrRe[l+2] = '\0';
  // fprintf(stderr,"%s %s\n",atmName, atmStrRe);
}

/* =====================================================
 * For each regexp of the pRe list, we setup the mask of 
 * the matching atoms in nuage p2.
 * ie: we prepare p1 to be confronted with only some of p1.
 * for each RE, nMask reports the number ot atoms concerned
 * atmMask[i] is 0 or 1. 1 implies atom matches RE.
 * =====================================================
 */
void setupMaskFromRe(DtReRec *pRe, DtNuage*p2)
{
  int aAtm;

  int lp = 0;

  // if (!strcmp(p2->header,"FX.xray.inh1.1F0R"))
  //  lp = 1;
  // if (lp) fprintf(stderr,"Entering setupMaskFromRe for: %s\n", p2->header);
  while (pRe != NULL) {
    pRe->atmMask = realloc(pRe->atmMask, sizeof(char)*p2->n);
    memset(pRe->atmMask,0,sizeof(char)*p2->n);
    pRe->nMask = 0;

    for (aAtm = 0; aAtm<p2->n; aAtm++) {
      if (p2->skip[aAtm]) {
	// if (lp) fprintf(stderr,"skipping %s %s\n", p2->atmName[aAtm], p2->resName[aAtm]);
	continue;
      }
      // if (lp) fprintf(stderr,"%s %s for %s %s",pRe->reAtmStr, pRe->reResStr, p2->atmName[aAtm], p2->resName[aAtm]);
      if ( (!regmatch(p2->atmName[aAtm],pRe->pAtmRe)) && (!regmatch(p2->resName[aAtm],pRe->pResRe)) ) {
	pRe->atmMask[aAtm] = 1;
	pRe->nMask++;
        // if (lp) fprintf(stderr, " OK");
	// if (lp) fprintf(stderr,"%s %s : OK for %s %s\n",pRe->reAtmStr, pRe->reResStr, p2->atmName[aAtm], p2->resName[aAtm]);
      }
      // if (lp) fprintf(stderr,"\n");
    }
    // if (lp) fprintf(stderr,"setupMaskFromRe: %s %s %d\n", pRe->reAtmStr, pRe->reResStr, pRe->nMask);
    pRe = pRe->next;
  }
  // if (lp) fprintf(stderr,"setupMaskFromRe: Done...\n");
  // if (!strcmp(p2->header,"FX.xray.inh1.1F0R"))
  //    exit(0);
}

/* get RE from line such as:
REMARK     MATCH. O.*
*/
void getAtmRe(char *line, char *atmStrRe)
{
  char *pC;
  int aPos = 0;

  atmStrRe[0] = '\0';
  if (strlen(line) >= 19) {
    pC = line+18;
//    while ((*pC != ' ') && (*pC != '\0')) {
    while ((*pC != ' ') && (*pC != '\0') && (*pC != '\r')) {
      atmStrRe[aPos] = *pC;
      aPos++;
      pC++;
    }

    atmStrRe[aPos] = '\0';

    /* add '^' at the beginning of the regex if necessary (1) */
    if (aPos > 0 && atmStrRe[0] != '^') {
      memmove(atmStrRe+1, atmStrRe, (aPos+1)*sizeof(char));
      atmStrRe[0] = '^';
      aPos++;
    }

    /* add '$' at the end of the regex if necessary (1) */
    if (aPos > 0 && atmStrRe[aPos-1] != '$') {
      atmStrRe[aPos] = '$';
      atmStrRe[aPos+1] = 0;
    }

    /* (1) otherwise, a regex such as 'C.*' will match '^.*C.*$' */
  }

}

/* Get RE from line such as:
REMARK     RESMATCH. SER
*/
void getResRe(char *line, char *resStrRe)
{
  char *pC;
  int aPos = 0;

  resStrRe[0] = '\0';
  // fprintf(stderr,"getResre: %s\n", line);
  if (strlen(line) >= 22) {
    pC = line+21;
    while ((*pC != ' ') && (*pC != '\0')) {
      resStrRe[aPos] = *pC;
      aPos++;
      pC++;
    }
    resStrRe[aPos] = '\0';
    // fprintf(stderr,"%s\n", line);
    // fprintf(stderr,"%s\n", resStrRe);
  }
}

/* Get atom weight from line such as:
REMARK     WEIGHT. 2.0
*/
double getAtomWeight(char *line)
{
  char *pC;
  int aPos = 0;
  char buff[30];
  double w = 0.0;

  if (strlen(line) >= 20) {
    pC = line+19;
    while ((*pC != ' ') && (*pC != '\0') && (aPos < 29)) {
      buff[aPos] = *pC;
      aPos++;
      pC++;
    }
    buff[aPos] = '\0';
    w = atof(buff);
  }

  return w;
}

/* Initialise la liste des poids associés aux atomes à 1.0 */
void initWeight(double *w, int n)
{
  n--;
  while (n >= 0) {
    w[n] = 1.0;
    n--;
  }
}


/* ============================================================================
 * Remplit les tableaux de données utiles à Csr après lecture des fichiers Pdb.
 * Alloue la mémoire pour la structure DtNuage.
 * if (strcmp(Entete,"REMARK     MATCH.") == 0)  return 12;
 * if (strcmp(Entete,"REMARK     RESMATCH.") == 0)  return 13;
 * ============================================================================
 */
void lectPdbCsr(char *fname, int what, int strict, DtNuage **pN, int *onNuages, DtReRec **pReList, int verbose)
{
  DtNuage *n;
  char   **lines;
  int     *limits;

  DtReRec *pRe;

  int  nLines;
  int  aLine;
  int  lineStart;
  int  lineStat;
//  int  lastCrd;
  int  nMol, nMod;
  int  molOpened, modOpened;
  int  nNuages;
  int  aNuage;
  int  nAtom;
  char atmStrRe[BUFSIZ];
  char resStrRe[BUFSIZ];
  char buff[BUFSIZ];
  char *cmpnd;
  char EC[BUFSIZ];
  char HETGRP[BUFSIZ];

  lines=txtFileRead(fname,&nLines);

  if (!lines) {
    exit(0);
  }

  // fprintf(stderr,"read %d lines from %s\n",nLines, fname);

  /* -- Nombre de molecules -- */
  nMol    = 0;
  nMod    = 0;
  nNuages = 0;
  molOpened = modOpened = 0;
//  lastCrd = 0;
  limits  = NULL;
  for (aLine=0; aLine < nLines; aLine++) {
    lineStat=getPDBLineStatus(lines[aLine]);
    if (lineStat == 10) { /* HEADER */
      molOpened = 1;
      nMol++;
      nNuages++;
      limits = realloc(limits, sizeof(int)*nNuages);
    }
    if (lineStat == 8) {  /* MODEL */
      modOpened = 1;
      nMod++;
      nNuages++;
      limits = realloc(limits, sizeof(int)*nNuages);
    }
    if (lineStat == 9) {  /* ENMDL */
      if (modOpened) {
	modOpened = 0;
	limits[nNuages-1] = aLine;
      } else {
	fprintf(stderr,"Incorrect file format: ENDMDL does not match MODEL\n");
	exit(0);
      }
    }
    if (lineStat == 7) { /* END */
      if (molOpened) {
	molOpened = 0;
	limits[nNuages-1] = aLine;
      } else {
	fprintf(stderr,"Incorrect file format: END does not match HEADER\n");
	exit(0);
      }
    }
#if 0
    if ((lineStat == 1) || (lineStat==11)) { /* ATOM / HETATM */
      lastCrd = aLine;
    }
#endif
  }
  if ((molOpened) || (modOpened)) {
    if (verbose)
      fprintf(stderr,"molOpened: may be missing END (nNuages %d aLine %d)\n",nNuages, aLine);
    limits[nNuages-1] = aLine - 1;
  }

  if (verbose)
    fprintf(stderr,"File %s: Found %d Mols, %d Models, %d Nuages (vs %d)\n", fname, nMol, nMod, nNuages, nMol+nMod);

  /* -- Allocate nuages -- */
  if (nNuages != (nMol + nMod)) {
    fprintf(stderr,"Inconsistent number of molecules + models vs Nuages\n");
    exit(0);
  }
  *onNuages = nNuages;
  *pN  = calloc(nNuages, sizeof(DtNuage));


  // fprintf(stderr,"lectPdbCsr: will setup nuages\n");


  /* -- Input des nuages -- */
  for (aNuage = 0; aNuage < nNuages; aNuage++) {

    // fprintf(stderr,"Will input Nuage %d limits %d\n",aNuage, limits[aNuage]);
    /* -- Taille du nuage -- */
    n    = &(*pN)[aNuage];
    n->n = 0;
    // fprintf(stderr,"Will input Nuage %d\n",aNuage);
    lineStart = (aNuage) ? limits[aNuage-1]: 0;
    if (lineStart < 0) { continue; } // /!\ limits[0] < 0 if several models in template file
    // fprintf(stderr,"Nuage %d (lines %d - %d)\n",aNuage,lineStart,limits[aNuage],n->n);
    for (aLine=lineStart; aLine < limits[aNuage]; aLine++) {
      lineStat=getPDBLineStatus(lines[aLine]);
      if ((lineStat==1) || (lineStat==11)){ /* ATOM | HETATM */
	n->n++;
      }
    }
    // fprintf(stderr,"Nuage %d (lines %d - %d): %d crds\n",aNuage,lineStart,limits[aNuage],n->n);

    /* -- Allocation memoire -- */
    n->crds     =  (DtPoint3 *) calloc(n->n, sizeof(DtPoint3));
    n->atmName  =  (DtStr4 *)   calloc(n->n, sizeof(DtStr4));
    n->atmNme   =  (DtStr4 *)   calloc(n->n, sizeof(DtStr4));
    n->atmNum   =  (int *)      calloc(n->n, sizeof(int));
    n->resName  =  (DtStr3 *)   calloc(n->n, sizeof(DtStr3));
    n->resNum   =  (int *)      calloc(n->n, sizeof(int));
    n->chn      =  (char *)     calloc(n->n, sizeof(char));
    n->skip     =  (char *)     calloc(n->n, sizeof(char));
    n->re       =  (DtReRec **) calloc(n->n, sizeof(DtReRec *));
    n->EC       =  (char *)     malloc(sizeof(char) * 1);
    n->EC[0]    =  '\0';
    n->cmpnd    =  (char *)     malloc(sizeof(char) * 1);
    n->cmpnd[0] =  '\0';
    n->id       =  (char *)     malloc(sizeof(char) * 1);
    n->id[0]    =  '\0';
    n->nHtGrps  = 0;
    n->htGrps   = NULL;
/*     n->hetGrps  =  (char *)    malloc(sizeof(char) * 1); */
/*     n->hetGrps[0]=  '\0'; */
    n->header   =  (char *)     malloc(sizeof(char) * 1);
    n->header[0]=  '\0';
    n->weight   = NULL;
    nAtom = n->n;

    /* -- Lecture donnees -- */
    n->n = 0;
    // fprintf(stderr,"Reading Crds (from %d to %d\n",lineStart, limits[aNuage] );
    for (aLine=lineStart; aLine < limits[aNuage]; aLine++) {
      lineStat=getPDBLineStatus(lines[aLine]);
      // fprintf(stderr,"%d %s\n", lineStat, lines[aLine]);
      if ((lineStat!=1) && (lineStat!=11) && (lineStat!=12) && (lineStat!=13) && (lineStat!=14) && (lineStat!=15) && (lineStat!=16) && (lineStat!=10)) continue;

      if (lineStat == 14) {  /* EC Number -> EC */
	if (sscanf(lines[aLine],"%14c%s",buff,EC) != EOF);
	// fprintf(stderr,"%s\n",lines[aLine]);
	n->EC = realloc(n->EC,strlen(EC)+1);
	strcpy(n->EC,EC);
      }
      else if (lineStat == 15) { /* COMPND -> cmpnd */
	// fprintf(stderr,"%s\n",lines[aLine]);
	cmpnd = lines[aLine]+10;
	n->cmpnd = realloc(n->cmpnd,strlen(cmpnd)+1);
	strcpy(n->cmpnd, cmpnd);
	cmpnd = n->cmpnd + strlen(n->cmpnd) - 1;
	while (*cmpnd == ' ') cmpnd--;
	cmpnd++;
	*cmpnd = '\0';
      }
      else if (lineStat == 16) { /* HETGRP -> hetGrps */
	// fprintf(stderr,"%s\n",lines[aLine]);
	if (sscanf(lines[aLine],"%17c%s",buff,HETGRP) != EOF);
	n->nHtGrps += 1;
	n->htGrps = (char **) realloc(n->htGrps, sizeof(char *) * n->nHtGrps);
	n->htGrps[n->nHtGrps -1]  =  (char *)    malloc(sizeof(char) * (strlen(HETGRP) + 1));
	strcpy(n->htGrps[n->nHtGrps -1], HETGRP);
	// fprintf(stderr,"Read %s\n",HETGRP);
      }
      else if (lineStat == 10) {  /* HEADER -> id */
	// fprintf(stderr,"%s\n",lines[aLine]);
	cmpnd = lines[aLine]+9;
	n->header = realloc(n->header,strlen(cmpnd)+1);
	strcpy(n->header, cmpnd);
	cmpnd = n->header + strlen(n->header) - 1;
	while (*cmpnd == ' ') cmpnd--;
	cmpnd++;
	*cmpnd = '\0';

	if (strlen(lines[aLine]) >= 66) {
	  n->id = realloc(n->id,sizeof(char)*5);
	  n->id[0] = lines[aLine][62];
	  n->id[1] = lines[aLine][63];
	  n->id[2] = lines[aLine][64];
	  n->id[3] = lines[aLine][65];
	  n->id[4] = '\0';
	} else {
	  n->id = realloc(n->id,sizeof(char)*5);
	  n->id[0] = 'u';
	  n->id[1] = 'n';
	  n->id[2] = 'k';
	  n->id[3] = 'n';
	  n->id[4] = '\0';
	}
      }
      else if ((lineStat==1) || (lineStat==11)) { /* ATOM HETATM */
	if (n->n) {
	  /* Assign RE to previous atom */
	  /* Was the RE already setup in the RE list ? (same masks on both atom and residue) */
	  pRe = rePtrFromStr(*pReList, atmStrRe, resStrRe); /* Peut etre NULL */
	  /* No: we create new RE, add it to the RE list */
	  if (pRe == NULL) {
	    pRe = (DtReRec *) calloc(1, sizeof(DtReRec));
	    pRe->next = *pReList;
	    *pReList  = pRe;
	    pRe->reAtmStr  = calloc (strlen(atmStrRe)+1, sizeof(char));
	    strcpy(pRe->reAtmStr, atmStrRe);
	    pRe->reResStr  = calloc (strlen(resStrRe)+1, sizeof(char));
	    strcpy(pRe->reResStr, resStrRe);
	    pRe->pAtmRe = reginit(pRe->reAtmStr, pRe->pAtmRe);
	    pRe->pResRe = reginit(pRe->reResStr, pRe->pResRe);
	    // fprintf(stderr,"%p %p\n",pRe->pAtmRe,  pRe->pResRe);
	    // fprintf(stderr,"CA : %d ZN : %d\n",regmatch("CA",pRe->pAtmRe),   regmatch("CA ",pRe->pAtmRe));
	    // exit(0);
	  }
	  n->re[n->n -1] = pRe;
	}
	// fprintf(stderr,"%s\n",lines[aLine]);
	
	getAtomCoords(lines[aLine],&n->crds[n->n][0],&n->crds[n->n][1],&n->crds[n->n][2]);
	getAtomName(lines[aLine],n->atmName[n->n]);
	// stripspace(n->atmName[n->n], n->atmNme[n->n]);
	strcpy(n->atmNme[n->n], n->atmName[n->n]);
	getAtomNum(lines[aLine],&n->atmNum[n->n]);

	/* Some masks to select SC atoms only */
	if (what == DcSC) {
	  if (!strcmp(n->atmNme[n->n]," N  "))   n->skip[n->n] = 1;
	  if (!strcmp(n->atmNme[n->n]," CA "))  n->skip[n->n] = 1;
	  if (!strcmp(n->atmNme[n->n]," C  "))   n->skip[n->n] = 1;
	  if (!strcmp(n->atmNme[n->n]," O  "))   n->skip[n->n] = 1;
	  if (!strcmp(n->atmNme[n->n]," OXT")) n->skip[n->n] = 1;
	} else if (what == DcBB) {
	  if ( strcmp(n->atmNme[n->n]," N  ") && 
	       strcmp(n->atmNme[n->n]," CA ") &&
	       strcmp(n->atmNme[n->n]," C  ") &&
	       strcmp(n->atmNme[n->n]," O  ") &&
	       strcmp(n->atmNme[n->n]," OXT") ) n->skip[n->n] = 1;
	}
	  // strcpy(n->atmNme[n->n], n->atmName[n->n]);
	getResName(lines[aLine],n->resName[n->n]);
	getResNum(lines[aLine],&n->resNum[n->n]);
	getResChainIde(lines[aLine],&n->chn[n->n]);

	atmInternalName(n->atmName[n->n]);
	// fprintf(stderr,"Read Crd %d\n", n->n);

	/* Setup ptr to default re */
	atmName2dftlReStr(n->atmName[n->n], atmStrRe);
	if (strict == 1) {
	  strcpy(resStrRe, n->resName[n->n]);
	} else {
	  resStrRe[0] = '.';
	  resStrRe[1] = '*';
	  resStrRe[2] = '\0';
	}
	n->n ++;
      } else {
	/* Here, we define the local regexps, overloading the default regexps strings */
	if (lineStat==12) {  /* MATCH. */
	  getAtmRe(lines[aLine], atmStrRe);
	}
	else if (lineStat==13) {  /* RESMATCH. */
	  getResRe(lines[aLine], resStrRe);
	}

        else if (lineStat==17) { /* WEIGHT. */
          if (!n->weight) {
            n->weight = (double *)calloc(nAtom, sizeof(double));
            initWeight(n->weight, nAtom);
          }
          n->weight[n->n -1] = getAtomWeight(lines[aLine]);
        }
      }
    }

    if (n->n) {
      /* Assign RE to previous atom */
      // fprintf(stderr,"%p\n",*pReList);
      // fflush(stderr);
      pRe = rePtrFromStr(*pReList, atmStrRe, resStrRe); /* Peut etre NULL */
      if (pRe == NULL) {
	pRe = (DtReRec *) calloc(1, sizeof(DtReRec));
	pRe->next = *pReList;
	*pReList  = pRe;
	pRe->reAtmStr  = calloc (strlen(atmStrRe)+1, sizeof(char));
	strcpy(pRe->reAtmStr, atmStrRe);
	pRe->reResStr  = calloc (strlen(resStrRe)+1, sizeof(char));
	strcpy(pRe->reResStr, resStrRe);
	pRe->pAtmRe = reginit(pRe->reAtmStr, pRe->pAtmRe);
	pRe->pResRe = reginit(pRe->reResStr, pRe->pResRe);
	// fprintf(stderr,"%p %p\n",pRe->pAtmRe,  pRe->pResRe);
	// fprintf(stderr,"CA : %d ZN : %d\n",regmatch("CA",pRe->pAtmRe),   regmatch("CA ",pRe->pAtmRe));
	// exit(0);
      }
      n->re[n->n -1] = pRe;
    }
#if 0
    for (aLine = 0; aLine < n->n; aLine++) {
      fprintf(stderr,"Atm %s %s regexp %s %s\n", n->atmName[aLine], n->resName[aLine],n->re[aLine]->reAtmStr, n->re[aLine]->reAtmStr);
    }
#endif

    // fprintf(stderr,"Nuage %d #atoms %d Id: %s COMPND %s EC %s\n",aNuage,n->n, n->id, n->cmpnd, n->EC);
    // exit(0);

  }

  // fprintf(stderr,"lectPdbCsr: will free memory\n");
  /* -- liberation memoire -- */
  txtFileFree(lines,nLines);
  free(limits);

  if (verbose)
    fprintf(stderr,"Currently %d regexp set\n", rePtrListLen(*pReList));
}


void outPDBNuage(char *fname, DtNuage *pN)
{
  FILE *fd;
  int aAtm;

  fd = fopen(fname, "w");
  if (fd == NULL) return;
  fprintf(fd,"HEADER CSRMATCH\n");
  for (aAtm = 0; aAtm<pN->n; aAtm++) {
    outputPDBAtomLine(fd,
		      aAtm, pN->atmName[aAtm], ' ', /* acode */
		      pN->resName[aAtm], pN->chn[aAtm], pN->resNum[aAtm], ' ', /* icode */
		      pN->crds[aAtm][0], pN->crds[aAtm][1], pN->crds[aAtm][2]);
  }
  fprintf(fd,"TER\n");
  fprintf(fd,"END\n");
  fclose(fd);
}

/* ======================================================
 * Write part of template that matches
 * ======================================================
 */
void outPDBTemplate(FILE *fd, DtReponse *rep, DtNuage *pNg)
{
  int i, i1, i2;

  fprintf(fd,"HEADER   %s\n",pNg->header);
  if (pNg->cmpnd[0] != '\0')
    fprintf(fd,"COMPND   %s\n",pNg->cmpnd);
  if (pNg->EC[0] != '\0')
    fprintf(fd,"REMARK    EC NUMBER: %s\n",pNg->EC);
  fprintf(fd,"REMARK    RMSD %.2lf\n",rep->lrmsd);
//   boucle(i,0,rep->nLPairs) {
//     i1=(rep->lpairs[i].liaison)%(pNg->n);
// //    i2=(rep->lpairs[i].liaison)/(pNg->n);
  for (i1 = 0; i1 < pNg->n; i1++) {
    outputPDBAtomLine(fd,
		      pNg->atmNum[i1], pNg->atmNme[i1], ' ', /* acode */
		      pNg->resName[i1], pNg->chn[i1], pNg->resNum[i1], ' ', /* icode */
		      pNg->crds[i1][0], pNg->crds[i1][1], pNg->crds[i1][2]);
  }
  fprintf(fd,"END\n");
}

/* ======================================================
 * Write part of template that matches
 * ======================================================
 */
void outPDBMatch(FILE *fd, DtReponse *rep, DtNuage *pNg, DtNuage *pNg1)
{
  int i, i1, i2;

  //fprintf(fd,"HEADER    1avf site    1                                      1avf\n");
  fprintf(fd,"HEADER   %s\n",pNg->header);
  if (pNg->cmpnd[0] != '\0')
    fprintf(fd,"COMPND   %s\n",pNg->cmpnd);
  if (pNg->EC[0] != '\0')
    fprintf(fd,"REMARK    EC NUMBER: %s\n",pNg->EC);
  fprintf(fd,"REMARK    RMSD %.2lf\n",rep->lrmsd);
  fprintf(fd,"REMARK    MATRIX  %10.6lf %10.6lf %10.6lf %10.6lf \n", rep->rM[0][0], rep->rM[0][1], rep->rM[0][2], rep->rM[0][3] );
  fprintf(fd,"REMARK    MATRIX  %10.6lf %10.6lf %10.6lf %10.6lf \n", rep->rM[1][0], rep->rM[1][1], rep->rM[1][2], rep->rM[1][3] );
  fprintf(fd,"REMARK    MATRIX  %10.6lf %10.6lf %10.6lf %10.6lf \n", rep->rM[2][0], rep->rM[2][1], rep->rM[2][2], rep->rM[2][3] );
  fprintf(fd,"REMARK    MATRIX  %10.6lf %10.6lf %10.6lf %10.6lf \n", rep->rM[3][0], rep->rM[3][1], rep->rM[3][2], rep->rM[3][3] );
//   boucle(i,0,rep->nLPairs) {
// //    i1=(rep->lpairs[i].liaison)%(pNg1->n);
//     i2=(rep->lpairs[i].liaison)/(pNg1->n);
  for (i2 = 0; i2 < pNg->n; i2++) {
    outputPDBAtomLine(fd, 
		      pNg->atmNum[i2], pNg->atmNme[i2], ' ', /* acode */
		      pNg->resName[i2], pNg->chn[i2], pNg->resNum[i2], ' ', /* icode */ 
		      pNg->crds[i2][0], pNg->crds[i2][1], pNg->crds[i2][2]);
  }
  fprintf(fd,"REMARK    END MATCH\n" );
  fprintf(fd,"END\n");
}

void barycentre(DtNuage *n)
{
  DtFloat x, y, z;
  DtFloat tmpF;
  int i;

  x=y=z=0.;
  for(i=0 ; i < (n->n) ; i++){
    x += n->crds[i][0];
    y += n->crds[i][1];
    z += n->crds[i][2];
  }

  tmpF = (DtFloat) n->n;
  n->bc[0]=x/(tmpF);
  n->bc[1]=y/(tmpF);
  n->bc[2]=z/(tmpF);
}

void printNuage(FILE *f, DtNuage *n, char *msg)
{
  int aDot;
  fprintf(f, "%s\n",msg);
  fprintf(f, "n points: %d\n",n->n);
  for (aDot = 0; aDot < n->n; aDot++) {
    fprintf(f, "%s %s %lf %lf %lf\n",
	    n->atmName[aDot],n->resName[aDot],
	    n->crds[aDot][0],n->crds[aDot][1],n->crds[aDot][2]);
  }
}


/* ============================================================
 * Imprime les (nbPrint) premières lignes d'un DtpDistRec
 * des nuage d'atomes (ng1) et (ng2).
 * ============================================================
 */

// void printDtpDistRec(DtDistRec *dist, int nbPrint, DtNuage * ng1,DtNuage * ng2)
// {
//   int i,i1,i2;
//   for(i=0; i<nbPrint ; i++){
//     i1=(dist[i].liaison)%(ng1->n);
//     i2=(dist[i].liaison)/(ng1->n);
//     printf("distance %d\t longueur=%.8lf\t liaison=(%d,%d)\n",i,dist[i].longueur,i1,i2);
//   }
// }

/* =======================================================
 * Imprime les (nbCrds) premières coordonnées
 * d'un tableau de coordonnées.
 * =======================================================
 */
void printNgCrds(DtPoint3 *crds, int nbCrds)
{
  int i,j;

  for(i=0; i<nbCrds ; i++){
    fprintf(stderr, "*Atome %d :\t",i);
    for (j=0 ; j< 3 ; j++){
      fprintf(stderr, "%.8lf\t",crds[i][j]);
    }
    fprintf(stderr, "\n");
  }
  fprintf(stderr, "*\n");
}

/* ===================================================================
 * Copie d'un tableau de type DtpDistRec (recopie dist1 dans dist2). 
 * ===================================================================
 */

void copyDtpDistRec2(int kds, DtDistRec *dist1, DtDistRec *dist2)
{
  memcpy(dist2, dist1, kds * sizeof(DtDistRec));
}

// void copyDtpDistRec(int kds, DtDistRec *dist1, DtDistRec *dist2)
// {
//   int i,j;
// 
//   for(i=0 ; i < kds ; i++){
//     for(j=0 ; j < 2 ; j++){
//       dist2[i].longueur=dist1[i].longueur;
//       dist2[i].liaison=dist1[i].liaison;
//     }
//   }
// }


/* ===================================================================
 * Stockage d'une réponse pour CSR (kds,nbAtApp,dist,rmsd,Mat4x4).
 * ===================================================================
 */

void stockRep2(int kds, int nbAtApp, DtDistRec *dist, double rms, DtMatrix4x4 rmat, DtReponse *rep)
{
  rep->nbAtMax=nbAtApp;
  rep->rmsd=rms;
  copyMat4x4(rmat, rep->rM);
  copyDtpDistRec2(kds,dist,rep->dist);
}

// void stockRep(int kds, int nbAtApp, DtDistRec *dist, double rms, DtMatrix4x4 rmat, DtReponse *rep)
// {
//   rep->nbAtMax=nbAtApp;
//   rep->rmsd=rms;
//   copyMat4x4(rmat, rep->rM);
//   copyDtpDistRec(kds,dist,rep->dist);
// }


/* ===============================================
 * Création d'une matrice de rotation aléatoire
 * ===============================================
 */


void rotAl(DtMatrix4x4 mat)
{
  double teta;
  DtPoint3 U;
  double N;
  int compteur=0;

  teta=drand48()*DcPI;

  /* printf("*teta=%.8lf\t",teta); */

  U[1]=-1+drand48()*2; //réel compris entre -1 et 1
  U[2]=-1+drand48()*2;
  U[0]=-1+drand48()*2;
  N=norme(U);

  while (N>1 && compteur < 1000){

    /* printf("*X=%.8lf\t",U[1]);
    printf("Y=%.8lf\t",U[2]);
    printf("Z=%.8lf\n",U[3]);
    printf("* /!\\========PROBLEME==========/!\\\n"); */
    U[1]=-1+drand48()*2;
    U[2]=-1+drand48()*2;
    U[0]=-1+drand48()*2;
    N=norme(U);
    compteur++;
  }
  if(compteur < 1000){
    /* printf("X=%.8lf\t",U[1]);
    printf("Y=%.8lf\t",U[2]);
    printf("Z=%.8lf\n",U[3]);
    printf("*norme(U)=%.8lf\n\n",N); */
  }
  else {
    /* printf("/!\\========NORME DE U SUPERIEURE A 1==========/!\\\n\n"); */
  }


  teta=0.211091720422362 ;
  U[0]=0.776475976742458 ;
  U[1]=-1.12080975556336 ;
  U[2]=-0.06429069295656 ;


  //MkArbitraryAxisRotMat4x4(O,U,teta,mat);

}

/* ===================================================
 * Positionnement aléatoire de deux nuages de points.
 * ===================================================
 */

// void posAl (DtNuage * ng1, DtNuage * ng2, DtMatrix4x4 mat)
// {
//   int nAl1, nAl2,i;
//   DtPoint3 tr1, tr2;
// 
// 
//   /* --- rotation aléatoire --- */
// 
//   rotAl(mat); 
// 
//   /* --- positionnement aléatoire --- */
// 
//   nAl1=irand(ng1->n);
//   nAl2=irand(ng2->n);
// 
//   /* printf("*COUPLE (ATOME 1, ATOME 2) TIRE AU HASARD : (%d,%d).\n*\n"
//      ,nAl1,nAl2); */
// 
// 
//   /* --- superposition des deux nuages de points --- */
// 
// 
//   nAl1=41;
//   nAl2=38;
// 
// 
//   /* rotation de tout le nuage 2 */
//   /* for(i=0 ; i < (ng2->n) ; i++){
//     singleRotate(ng2->crds[i], mat);
//   }
// 
//   tr1[0]=ng1->crds[nAl1][0];
//   tr1[1]=ng1->crds[nAl1][1];
//   tr1[2]=ng1->crds[nAl1][2];
// 
//   tr2[0]=ng2->crds[nAl2][0];
//   tr2[1]=ng2->crds[nAl2][1];
//   tr2[2]=ng2->crds[nAl2][2];
//   negatePoint3(tr2); */
// 
//   /* superposition du nuage 2 sur le nuage 1 */
//   /* for(i=0 ; i < (ng2->n) ; i++){
//     simpleTranslate(ng2->crds[i],tr2);
//     simpleTranslate(ng2->crds[i],tr1);
//     } */
// 
// 
// 
//   tr1[0]=ng1->crds[nAl1][0];
//   tr1[1]=ng1->crds[nAl1][1];
//   tr1[2]=ng1->crds[nAl1][2];
// 
//   tr2[0]=ng2->crds[nAl2][0];
//   tr2[1]=ng2->crds[nAl2][1];
//   tr2[2]=ng2->crds[nAl2][2];
// 
//   negatePoint3(tr2);
// 
//   mat[3][0]=tr1[0]+tr2[0];
//   mat[3][1]=tr1[1]+tr2[1];
//   mat[3][2]=tr1[2]+tr2[2];
// 
//   for(i=0 ; i < (ng2->n) ; i++){
//     singleRotate(ng2->crds[i], mat);
//   }
// 
// }

/* ===================================================
 * Positionnement aléatoire de deux nuages de points.
 * ng1 est fixe, ng2 est mobile (rotation + translation)
 * mat est la matrice de transformation aléatoire.
 * (mat est juste un espace de travail, ecrasee a la sortie
 * de posAl2)
 * ===================================================
 */
void posAl2(DtNuage *ng1, DtNuage *ng2, DtMatrix4x4 mat){
  double teta;
  DtPoint3 U;
  DtPoint3 tr2dt;
  double N;
  int nAl1, nAl2,i,tirage;
  DtPoint3 tr1, tr2;
  int compteur=0;
  double temp,x,y,z;
//  static int rRank = 0;

#if 1
  /* angle aléatoire entre O et Pi */
  teta=drand48()*DcPI;

  /* printf("*teta=%.8lf\t",teta); */

  /* vecteur aléatoire */
  U[1]=-1+drand48()*2; //réel compris entre -1 et 1
  U[2]=-1+drand48()*2;
  U[0]=-1+drand48()*2;
  N=norme(U);

  /* compteur implémenté pour limiter, car des fois plus d'une minute sans solution */
  while ((N>1.) && (compteur < 1000)){

    /* printf("*X=%.8lf\t",U[1]);
    printf("Y=%.8lf\t",U[2]);
    printf("Z=%.8lf\n",U[3]);
    printf("* /!\\========PROBLEME==========/!\\\n"); */
    U[1]=-1+drand48()*2;
    U[2]=-1+drand48()*2;
    U[0]=-1+drand48()*2;
    N=norme(U);
    compteur++;
  }
//  if(compteur < 1000){
/*     printf("X=%.8lf\t",U[1]); */
/*     printf("Y=%.8lf\t",U[2]); */
/*     printf("Z=%.8lf\n",U[3]); */
/*     printf("*norme(U)=%.8lf\n\n",N);  */
//  }
//  else {
/*     printf("/!\\========NORME DE U SUPERIEURE A 1==========/!\\\n\n"); */
//  }

 /* --- positionnement aléatoire --- */


  tirage=irand(6);

  //  fprintf(stderr,"TIRAGE : %d\n",tirage);

  if (tirage==0) {
    //fprintf(stderr,"OK 0");
    //fflush(stderr);
    /* -- Calcul du barycentre du nuage de points 1 -- */
    tr1[0]= ng1->bc[0];
    tr1[1]= ng1->bc[1];
    tr1[2]= ng1->bc[2];

    tr2[0]= ng2->bc[0];
    tr2[1]= ng2->bc[1];
    tr2[2]= ng2->bc[2];
  }

//  else if((tirage>0) && (tirage < 3)) {
  else if (tirage < 3) {
  // if((tirage>0) && (tirage < 0)) {
    //fprintf(stderr,"OK 1..2 ng2->n %d ng1->n %d", ng2->n, ng1->n);
    //fflush(stderr);
    if(ng2->n < ng1->n) {
      // fprintf(stderr,"ng2->n < ng1->n\n");
      // fflush(stderr);
      do {
	do {
	  nAl2=irand(ng2->n);
	  // fprintf(stderr,"nAl2 %d ng2->skip[nAl2] %d\n", nAl2, ng2->skip[nAl2]);
	} while ((ng2->skip[nAl2]));
	nAl1=nAl2+irand((ng1->n)-(ng2->n));
	// fprintf(stderr,"nAl1 %d ng1->skip[nAl1] %d ng1->re[nAl1]->nMask %d ng1->re[nAl1]->atmMask[nAl2] %d\n",nAl1, ng1->skip[nAl1], ng1->re[nAl1]->nMask, ng1->re[nAl1]->atmMask[nAl2]);
      } while ( (ng1->skip[nAl1]) || (!ng1->re[nAl1]->nMask) || (!ng1->re[nAl1]->atmMask[nAl2]) );
      //printf("%d\t%d\n",nAl1,nAl2);
    }
    else if(ng2->n > ng1->n) {
      // fprintf(stderr,"ng2->n > ng1->n\n");
      // fflush(stderr);
      do {
	do {
	  nAl1=irand(ng1->n);
	} while ((ng1->skip[nAl1]) || (!ng1->re[nAl1]->nMask));
	nAl2=nAl1+irand((ng2->n)-(ng1->n));
      } while ( (ng2->skip[nAl2]) || (!ng1->re[nAl1]->atmMask[nAl2]) );
      //printf("%d\t%d\n",nAl1,nAl2);
    }
    /* si ng1->n == ng2->n, les ièmes atomes sont appariés, quels qu'ils soient ?! */
    else {
      // fprintf(stderr,"ng2->n == ng1->n\n");
      // fflush(stderr);
      do {
	nAl1=irand(ng1->n);
      } while ((ng1->skip[nAl1]) || (!ng1->re[nAl1]->nMask));
      nAl2=nAl1;
    }

    tr1[0]=ng1->crds[nAl1][0];
    tr1[1]=ng1->crds[nAl1][1];
    tr1[2]=ng1->crds[nAl1][2];

    tr2[0]=ng2->crds[nAl2][0];
    tr2[1]=ng2->crds[nAl2][1];
    tr2[2]=ng2->crds[nAl2][2];
  }
  else {
//  else if(tirage>2) {
    // fprintf(stderr,"tirage>2 (%d)\n", ng1->n);
    // fflush(stderr);

    compteur = 0;
    do {
      nAl1=irand(ng1->n);
      compteur += 1;
      // fprintf(stderr,"nAl1 %d, skip %d, nMask %d\n",nAl1, ng1->skip[nAl1], ng1->re[nAl1]->nMask);
    } while ( (compteur < 1000) && ( (ng1->skip[nAl1]) || (!ng1->re[nAl1]->nMask) ));
    // } while ( (compteur < 1000) && ( (ng1->skip[nAl1]) || (!ng1->re[nAl1]->nMask) ));
    // nAl2=irand(ng2->n);
    // fprintf(stderr,"nAl1 %d (/%d, nMask %d)\n", nAl1, ng1->n, ng1->re[nAl1]->nMask);
    // fflush(stderr);
    nAl2 = irand(ng1->re[nAl1]->nMask);
    // fprintf(stderr,"nAl2 %d\n", nAl2);
    // fflush(stderr);
    for (i=0; i < ng2->n; i++) {
      if (ng1->re[nAl1]->atmMask[i]) {
	nAl2--;
	// fprintf(stderr,"%d %d\n",i, nAl2);
	if (nAl2 < 0) {
	  nAl2 = i;
	  break;
	}
      }
    }

    tr1[0]=ng1->crds[nAl1][0];
    tr1[1]=ng1->crds[nAl1][1];
    tr1[2]=ng1->crds[nAl1][2];

    // fprintf(stderr,"nAl2 is %d ng2 is %d\n",nAl2, ng2->n);
    tr2[0]=ng2->crds[nAl2][0];
    tr2[1]=ng2->crds[nAl2][1];
    tr2[2]=ng2->crds[nAl2][2];
  }

  tr2dt[0]=tr2[0] + U[0];
  tr2dt[1]=tr2[1] + U[1];
  tr2dt[2]=tr2[2] + U[2];

  //nAl1=41;
  //nAl2=38;



   /* printf("*COUPLE (ATOME 1, ATOME 2) TIRE AU HASARD : (%d,%d).\n*\n"
     ,nAl1,nAl2); */


#else
  /* Orientation imposée du programme Fortran pour le fichier pdbtest.pdb
   */

  /* teta=0.211091720422362 ;
  U[0]=0.776475976742458 ;
  U[1]=-1.12080975556336 ;
  U[2]=-0.06429069295656 ; */
// Atom pairs, (-1 = BC), teta, dir
// #define DcNDrawings 200
// Drawings[200][6] = {
// { 13, 41, 2.35916893650697, 1.29571412130762, -2.27899019262253, -0.541775744101193},

  if (rRank >= DcNDrawings)
    exit(0);
  teta = Drawings[rRank][2];
  U[0] = Drawings[rRank][3];
  U[1] = Drawings[rRank][4];
  U[2] = Drawings[rRank][5];
  nAl1 = Drawings[rRank][0];
  nAl2 = Drawings[rRank][1];
  if (nAl1 != -1) nAl1--;
  if (nAl2 != -1) nAl2--;
  rRank ++;

  if (nAl1 != -1) {
    tr1[0]=ng1->crds[nAl1][0];
    tr1[1]=ng1->crds[nAl1][1];
    tr1[2]=ng1->crds[nAl1][2];
  } else {
#if 0
    /* -- Calcul du barycentre du nuage de points 1 -- */
    x=y=z=0.;
    for(i=0 ; i < (ng1->n) ; i++){
      x += ng1->crds[i][0];
      y += ng1->crds[i][1];
      z += ng1->crds[i][2];
    }

    tr1[0]=x/(ng1->n);
    tr1[1]=y/(ng1->n);
    tr1[2]=z/(ng1->n);
#else
    tr1[0]= ng1->bc[0];
    tr1[1]= ng1->bc[1];
    tr1[2]= ng1->bc[2];
#endif
  }

  if (nAl2 != -1) {
    tr2[0]=ng2->crds[nAl2][0];
    tr2[1]=ng2->crds[nAl2][1];
    tr2[2]=ng2->crds[nAl2][2];
  } else {
#if 0
    /* -- Calcul du barycentre du nuage de points 2 -- */
    x=y=z=0.0;
    for(i=0 ; i < (ng2->n) ; i++){
      x += ng2->crds[i][0];
      y += ng2->crds[i][1];
      z += ng2->crds[i][2];
    }

    tr2[0]=x/(ng2->n);
    tr2[1]=y/(ng2->n);
    tr2[2]=z/(ng2->n);
#else

    tr2[0]= ng2->bc[0];
    tr2[1]= ng2->bc[1];
    tr2[2]= ng2->bc[2];

#endif
  }

  tr2dt[0]=tr2[0] + U[0];
  tr2dt[1]=tr2[1] + U[1];
  tr2dt[2]=tr2[2] + U[2];


  fprintf(stderr,"i1 %d i2 %d teta: %lf U %lf %lf %lf, tr1 %lf %lf %lf tr2 %lf %lf %lf\n",
	  nAl1,nAl2,teta,U[0],U[1],U[2],tr1[0],tr1[1],tr1[2],tr2[0],tr2[1],tr2[2]);
#endif


  /* --- superposition des deux nuages de points --- */

  /* -- Création de la matrice de rotation aléatoire -- */
  MkArbitraryAxisRotMat4x4(tr2,tr2dt,teta,mat);

  /* -- Transposée de la matrice de rotation 3x3 dans la matrice 4x4 -- */
  temp=mat[0][1];
  mat[0][1]=mat[1][0];
  mat[1][0]=temp;

  temp=mat[0][2];
  mat[0][2]=mat[2][0];
  mat[2][0]=temp;

  temp=mat[1][2];
  mat[1][2]=mat[2][1];
  mat[2][1]=temp;


  //printMat4x4(mat);

  //printf("%lf %lf %lf\n",tr1[0],tr1[1],tr1[2]);
  //printf("%lf %lf %lf\n",tr2[0],tr2[1],tr2[2]);



  /* -- Translation du nuage 2 par rapport à tr2 -- */
  for(i=0 ; i < (ng2->n) ; i++){
    ng2->crds[i][0] -= tr2[0];
    ng2->crds[i][1] -= tr2[1];
    ng2->crds[i][2] -= tr2[2];
  }

  /* -- Rotation du nuage 2 par la matrice de rotation aléatoire -- */
  for(i=0 ; i < (ng2->n) ; i++){
    x=ng2->crds[i][0];
    y=ng2->crds[i][1];
    z=ng2->crds[i][2];

    ng2->crds[i][0]=mat[0][0]*(x)+mat[0][1]*(y)+mat[0][2]*(z);
    ng2->crds[i][1]=mat[1][0]*(x)+mat[1][1]*(y)+mat[1][2]*(z);
    ng2->crds[i][2]=mat[2][0]*(x)+mat[2][1]*(y)+mat[2][2]*(z);
  }

  /* -- Superposition du nuage 2 sur le nuage 1 via la translation tr1 -- */
  for(i=0 ; i < (ng2->n) ; i++){
    ng2->crds[i][0] += tr1[0];
    ng2->crds[i][1] += tr1[1];
    ng2->crds[i][2] += tr1[2];
  }

  /* printf("DISTANCE : %lf\n",distance(ng1->crds[nAl1],ng2->crds[nAl2])); */
}

/* ===========================================================
 * Imprime quels points sont appariés deux à deux par sdm.
 * ===========================================================
 */

void printNbPtsApp (DtNuage * ng1, DtNuage * ng2, int *kds,DtDistRec *dist)
{
  int i,i1,i2,nbAtApp,stop;
  char at1App[ng1->n];
  char at2App[ng2->n];

  memset (at1App, 0, sizeof(char)*(ng1->n));
  memset (at2App, 0, sizeof(char)*(ng2->n));

  i=stop=0;

  while(i<(*kds) && stop==0){
    i1=(dist[i].liaison)%(ng1->n);
    i2=(dist[i].liaison)/(ng1->n);

    printf("*AT1 - AT2 : %d - %d\n",i1,i2);
    /* printPoint3Crds(ng1->crds,i1);
       printPoint3Crds(ng2->crds,i2); */

    if(at1App[i1]==1){
      stop=1;
      nbAtApp=i;
      printf("*=> ATOME %d DU NUAGE 1 DEJA APPARIE.\n",i1); 
    } else {
      at1App[i1]=1;
    }
    if(at2App[i2]==1){
      stop=1;
      nbAtApp=i;
      printf("*=> ATOME %d DU NUAGE 2 DEJA APPARIE.\n",i2);
    } else {
      at2App[i2]=1;
    }
    i++;
  }
}

/* ========================== ALGORITHME SDM =================================
 * Calcul de toutes les distances entre deux nuages de points (ng).
 * Conservation de toutes les distances inférieures à (dseuil) dans un 
 *   tableau (dist) de type DtpDistRec. 
 *   dist[k].longueur correspond à la k-ième distance inférieure à dseuil
 *   dist[k].liaison permet de retrouver de quels points s'agit cette longueur.
 *
 * Retourne le nombre de points maximum que l'on peut apparier.
 * ===========================================================================
 */

#define SQUAREDDISTANCE(A,B) ( (A[0]-B[0] * A[0]-B[0]) + (A[1]-B[1] * A[1]-B[1]) + (A[2]-B[2] * A[2]-B[2]) )
int sdm2(DtNuage *ng1, DtNuage *ng2, double dseuil, int dimTab, int *kds, DtDistRec *dist)
{
  char at1App[ng1->n]; //at1App[k]=1 si le k-ème point de ng1 fait déjà parti d'un appariement, 0 sinon
  char at2App[ng2->n]; //at2App[k]=1 si le k-ème atome de ng2 fait déjà parti d'un appariement, 0 sinon
  int i,i1,i2,shift,nbAtApp;
  double lg2;
  double dseuil2 = dseuil * dseuil;


  if ((ng1->n)==0 || (ng2->n)==0){
    *kds=0;
    nbAtApp=0;
    // fprintf(stderr,"NVoid nuage !!\n");
    return nbAtApp;
  }

  /* --- calcul des distances entre les points --- */
  if (dseuil>=0){
    *kds=0;
    for (i1=0 ; i1 <(ng1->n) ; i1++){
      if (ng1->skip[i1]) continue;
      if (ng1->re[i1]->nMask == 0) continue;
      for (i2=0 ; i2 <(ng2->n) ; i2++){
	if (ng2->skip[i2]) continue;
	if (!ng1->re[i1]->atmMask[i2]) continue;

	// fprintf(stderr,"Matching %s %s and %s %s\n", ng1->atmNm2[i1], ng1->atmName[i1], ng2->atmNm2[i2], ng2->atmName[i2]);

	shift=i2*(ng1->n);

	lg2=squared_distance((ng1->crds[i1]),(ng2->crds[i2]));

	if(lg2 <= dseuil2){
	  if(*kds <= dimTab){
	    (dist[*kds]).longueur2 = lg2;
	    (dist[*kds]).liaison = i1 + shift;
	    *kds=(*kds)+1;
	  }
	}
      }
    }
    if (*kds>dimTab) return (-1); /* dimd trop petit */
    if (*kds==0) return (-2); /* dseuil trop petit */
    // exit(0);
  }

  else { /* dseuil < 0 (équivalent de dseuil infini) */

    *kds=(ng1->n)*(ng2->n);

    if (*kds>dimTab) return 1; /* dimd trop petit */
    for (i2=0 ; i2<(ng2->n) ; i2++){
      shift=i2*(ng1->n);
      for (i1=0 ; i1 <(ng1->n) ; i1++){
	dist[i1+shift].longueur2=squared_distance((ng1->crds[i1]),(ng2->crds[i2]));
	dist[i1+shift].liaison=i1+shift;

      }
    }
  }

  /* ---  tri du tableau des distances par ordre croissant de distances --- */
  qsort(dist,*kds,sizeof(DtDistRec), cmpdistRec2);

  /* --- calcul du nombre de points appariés --- */

  memset (at1App, 0, sizeof(char)*(ng1->n));
  memset (at2App, 0, sizeof(char)*(ng2->n));

  i=0;
  nbAtApp = 0;
  while(i<(*kds)) {
    i1=(dist[i].liaison)%(ng1->n);
    i2=(dist[i].liaison)/(ng1->n);

    if(at1App[i1]==1){
      nbAtApp=i;
      break;
    } else {
      at1App[i1]=1;
    }

    if(at2App[i2]==1){
      nbAtApp=i;
      break;
    } else {
      at2App[i2]=1;
    }
    i++;
  }

  if (nbAtApp==(*kds) && nbAtApp!=min((ng1->n),(ng2->n))) return (-3);

  // exit(0);
  return (int) (nbAtApp);
}

// int sdm(DtNuage * ng1, DtNuage * ng2, double dseuil, int dimTab, int *kds, DtDistRec *dist)
// {
//   char at1App[ng1->n]; //at1App[k]=1 si le k-ème point de ng1 fait déjà parti d'un appariement, 0 sinon
//   char at2App[ng2->n]; //at2App[k]=1 si le k-ème atome de ng2 fait déjà parti d'un appariement, 0 sinon
//   int i,i1,i2,shift,nbAtApp;
//   double lg;
// 
// 
//   if ((ng1->n)==0 || (ng2->n)==0){
//     *kds=0;
//     nbAtApp=0;
//     // fprintf(stderr,"NVoid nuage !!\n");
//     return nbAtApp;
//   }
// 
//   /* --- calcul des distances entre les points --- */
//   if (dseuil>=0){
//     *kds=0;
//     for (i1=0 ; i1 <(ng1->n) ; i1++){
//       if (ng1->skip[i1]) continue;
//       if (ng1->re[i1]->nMask == 0) continue;
//       // fprintf(stderr,"SDM: i1 %d %s %s\n",i1, ng1->resName[i1],  ng1->atmName[i1] );
//       for (i2=0 ; i2 <(ng2->n) ; i2++){
// 	// fprintf(stderr,"SDM: i2 %d\n",i2);
// 	if (ng2->skip[i2]) continue;
// 	if (!ng1->re[i1]->atmMask[i2]) continue;
// 	// fprintf(stderr,"SDM: i2 %d %s %s\n",i2, ng2->resName[i2],  ng2->atmName[i2] );
// 
// 	shift=i2*(ng1->n);
// 
// 	lg=distance((ng1->crds[i1]),(ng2->crds[i2]));
// 
// //         fprintf(stderr, "%lf\n", lg);
// 
// 	// lg = SQUAREDDISTANCE((ng1->crds[i1]),(ng2->crds[i2]));
// 
// 	/* liaison: liaison / ng1->n -> i2
// 	            liaison % ng1->n -> i1
// 	*/
// 	if(lg <= dseuil){
// 	  if(*kds <= dimTab){ 
// 	    (dist[*kds]).longueur = lg; 
// 	    (dist[*kds]).liaison = i1 + shift; 
// 	    *kds=(*kds)+1;
// 
// 	    /* printf("dist :%lf\tcor : %d\n",lg*lg,i1+shift); */
// 
// 	  }
// 	}
//       }
//     }
//     if (*kds>dimTab) return (-1); /* dimd trop petit */
//     if (*kds==0) return (-2); /* dseuil trop petit */
//     // exit(0);
//   }
// 
//   else { /* dseuil < 0 (équivalent de dseuil infini) */
// 
//     *kds=(ng1->n)*(ng2->n);
// 
//     if (*kds>dimTab) return 1; /* dimd trop petit */
//     for (i2=0 ; i2<(ng2->n) ; i2++){
//       shift=i2*(ng1->n);
//       for (i1=0 ; i1 <(ng1->n) ; i1++){
// 	dist[i1+shift].longueur=distance((ng1->crds[i1]),(ng2->crds[i2]));
// 	// dist[i1+shift].longueur=SQUAREDDISTANCE((ng1->crds[i1]),(ng2->crds[i2]));
// 	dist[i1+shift].liaison=i1+shift;
// 
//       }
//     }
//   }
// 
//   /* ---  tri du tableau des distances par ordre croissant de distances --- */
// 
//   /* printDtpDistRec(dist,*kds,ng1,ng2);
//      printf("\n"); */
// 
// //  qsort(dist,*kds,sizeof(DtDistRec), cmpdist);
//   qsort(dist,*kds,sizeof(DtDistRec), cmpdistRec);
// 
//   // fprintf(stderr,"%d distances calculated\n",*kds);
//   // printDtpDistRec(dist,*kds,ng1,ng2);
//   // printDtpDistRec(dist,10,ng1,ng2);
// 
//   /* --- calcul du nombre de points appariés --- */
// 
//   memset (at1App, 0, sizeof(char)*(ng1->n));
//   memset (at2App, 0, sizeof(char)*(ng2->n));
// 
//   i=0;
//   nbAtApp = 0;
//   while(i<(*kds) ) {
//     i1=(dist[i].liaison)%(ng1->n);
//     i2=(dist[i].liaison)/(ng1->n);
// 
// #if 0
//     // Uncomment here to debug, if doubt on the sort
//     fprintf(stderr,"%d : %d - %d : %lf (%.3lf %.3lf %.3lf / %.3lf %.3lf %.3lf)\n",
// 	    i,i1,i2,dist[i].longueur,
// 	    ng1->crds[i1][0], ng1->crds[i1][1], ng1->crds[i1][2],
// 	    ng2->crds[i2][0], ng2->crds[i2][1], ng2->crds[i2][2]);
// #endif
// 
//     if(at1App[i1]==1){
//       nbAtApp=i;
//       break;
//       /* printf("*=> ATOME %d DU NUAGE 1 DEJA APPARIE.\n",i1); */
//     } else {
//       at1App[i1]=1;
//     }
//     if(at2App[i2]==1){
//       nbAtApp=i;
//       break;
//       /* printf("*=> ATOME %d DU NUAGE 2 DEJA APPARIE.\n",i2); */
//     } else {
//       at2App[i2]=1;
//     }
//     i++;
//   }
//  
//   // fprintf(stderr,"N apparies: %d\n",nbAtApp);
//   if (nbAtApp==(*kds) && nbAtApp!=min((ng1->n),(ng2->n))) return (-3);
// 
//   return (int) (nbAtApp);
// }


/* ========================== ALGORITHME SDM =================================
 * Reanalyse the dist array, so as to enlarge the matches less than given threshold.
 *   tableau (dist) de type DtpDistRec. 
 *   dist[k].longueur correspond à la k-ième distance inférieure à dseuil
 *   dist[k].liaison permet de retrouver de quels points s'agit cette longueur.
 *
 * Retourne le nombre de points maximum que l'on peut apparier.
 * ===========================================================================
 */
// int enlarge(DtNuage *ng1, DtNuage *ng2, DtReponse *rep, int nbAtApp, int kds, DtDistRec *dist, double tol, int verbose)
// {
//   char at1App[ng1->n]; //at1App[k]=1 si le k-ème point de ng1 fait déjà parti d'un appariement, 0 sinon
//   char at2App[ng2->n]; //at2App[k]=1 si le k-ème atome de ng2 fait déjà parti d'un appariement, 0 sinon
//   int i,i1,i2;
// //  int shift;
// //  double lg;
//   double sqrsum = 0.;
// 
// 
//   rep->lpairs = realloc(rep->lpairs, sizeof(DtDistRec) * kds);
//   rep->nLPairs = 0;
// 
//   /* --- calcul du nombre de points appariés --- */
// 
//   memset (at1App, 0, sizeof(char)*(ng1->n));
//   memset (at2App, 0, sizeof(char)*(ng2->n));
// 
//   i=0;
//   nbAtApp = 0;
//   while(i<(kds) ) {
//     i1=(dist[i].liaison)%(ng1->n);
//     i2=(dist[i].liaison)/(ng1->n);
// 
//     if (verbose) {
//       fprintf(stderr,"enlarge pair %d : %s %d %c - %d : %s %d %c dist %.5lf (%.3lf %.3lf %.3lf / %.3lf %.3lf %.3lf)\n",
// 	      i1, ng1->resName[i1], ng1->resNum[i1], ng1->chn[i1],
// 	      i2, ng2->resName[i2], ng2->resNum[i2], ng2->chn[i2],dist[i].longueur,
// 	      ng1->crds[i1][0], ng1->crds[i1][1], ng1->crds[i1][2],
// 	      ng2->crds[i2][0], ng2->crds[i2][1], ng2->crds[i2][2]);
//       /* printf("*%d\tAT1 - AT2 : %d - %d\t%lf\n",
// 	 i,i1,i2,dist[i].longueur); */
//       /* printPoint3Crds(ng1->crds,i1);
// 	 printPoint3Crds(ng2->crds,i2); */
//     }
// 
//     if(at1App[i1]==1){
//       if (verbose) {
// 	fprintf(stderr,"enlarge: skipping on i1\n");
//       }
//       goto LCONT;
//       /* printf("*=> ATOME %d DU NUAGE 1 DEJA APPARIE.\n",i1); */
//     } else {
//       at1App[i1]=1;
//     }
//     if(at2App[i2]==1){
//       if (verbose) {
//         fprintf(stderr,"enlarge: skipping on i2\n");
//       }
//       goto LCONT;
//       /* printf("*=> ATOME %d DU NUAGE 2 DEJA APPARIE.\n",i2); */
//     } else {
//       at2App[i2]=1;
//     }
//     if (dist[i].longueur < tol) {
//       rep->lpairs[rep->nLPairs].liaison = dist[i].liaison;
//       rep->lpairs[rep->nLPairs].longueur = dist[i].longueur;
//       rep->nLPairs ++;
//       sqrsum += (dist[i].longueur * dist[i].longueur);
//     } else {
//       break;
//     }
// 
//   LCONT:;
//     i++;
//   }
// 
//   rep->lrmsd = sqrt(sqrsum / (double) rep->nLPairs);
//   // fprintf(stderr,"N lapparies: %d\n",rep->nLPairs);
// 
//   return (int) (rep->nLPairs);
// }

int enlarge_weighted2(DtNuage *ng1, DtNuage *ng2, DtReponse *rep, int nbAtApp, int kds, DtDistRec *dist, double tol, int verbose)
{
  char at1App[ng1->n]; //at1App[k]=1 si le k-ème point de ng1 fait déjà parti d'un appariement, 0 sinon
  char at2App[ng2->n]; //at2App[k]=1 si le k-ème atome de ng2 fait déjà parti d'un appariement, 0 sinon
  int i,i1,i2;
  double sqrsum = .0, sqrsumW = .0;
  double w1, w2, wSum = .0;
  double tol2 = tol * tol;

  rep->lpairs = realloc(rep->lpairs, sizeof(DtDistRec) * kds);
  rep->nLPairs = 0;

  /* --- calcul du nombre de points appariés --- */

  memset (at1App, 0, sizeof(char)*(ng1->n));
  memset (at2App, 0, sizeof(char)*(ng2->n));

  i=0;
  nbAtApp = 0;
  while(i<(kds) ) {
    i1=(dist[i].liaison)%(ng1->n);
    i2=(dist[i].liaison)/(ng1->n);

    if (verbose) {
      fprintf(stderr,"enlarge pair %d : %s %d %c - %d : %s %d %c dist %.5lf (%.3lf %.3lf %.3lf / %.3lf %.3lf %.3lf)\n",
	      i1, ng1->resName[i1], ng1->resNum[i1], ng1->chn[i1],
	      i2, ng2->resName[i2], ng2->resNum[i2], ng2->chn[i2], sqrt(dist[i].longueur2),
	      ng1->crds[i1][0], ng1->crds[i1][1], ng1->crds[i1][2],
	      ng2->crds[i2][0], ng2->crds[i2][1], ng2->crds[i2][2]);
    }

    if(at1App[i1]==1){
      if (verbose) {
        fprintf(stderr,"enlarge: skipping on i1\n");
      }
      goto LCONT;
      /* printf("*=> ATOME %d DU NUAGE 1 DEJA APPARIE.\n",i1); */
    } else {
      at1App[i1]=1;
    }
    if(at2App[i2]==1){
      if (verbose) {
        fprintf(stderr,"enlarge: skipping on i2\n");
      }
      goto LCONT;
      /* printf("*=> ATOME %d DU NUAGE 2 DEJA APPARIE.\n",i2); */
    } else {
      at2App[i2]=1;
    }

    // Adjustment by P. Tuffery, September 2008
    if (!ng1->re[i1]->atmMask[i2]) continue;
    // End adjustment

    if (dist[i].longueur2 < tol2) {
      rep->lpairs[rep->nLPairs].liaison = dist[i].liaison;
      rep->lpairs[rep->nLPairs].longueur2 = dist[i].longueur2;
      rep->nLPairs ++;
      sqrsum += dist[i].longueur2;
      w1 = (ng1->weight) ? ng1->weight[i1] : 1.0;
      w2 = (ng2->weight) ? ng2->weight[i2] : 1.0;
      sqrsumW += dist[i].longueur2 * w1 * w2;
      wSum += w1 * w2;
    } else {
      break;
    }

  LCONT:;
    i++;
  }

  rep->lrmsd = sqrt(sqrsum / (double) rep->nLPairs);
  rep->lrmsdWeight = sqrt(sqrsumW / wSum);

  return (int) (rep->nLPairs);
}

// int enlarge_weighted(DtNuage *ng1, DtNuage *ng2, DtReponse *rep, int nbAtApp, int kds, DtDistRec *dist, double tol, int verbose)
// {
//   char at1App[ng1->n]; //at1App[k]=1 si le k-ème point de ng1 fait déjà parti d'un appariement, 0 sinon
//   char at2App[ng2->n]; //at2App[k]=1 si le k-ème atome de ng2 fait déjà parti d'un appariement, 0 sinon
//   int i,i1,i2;
//   double sqrsum = 0.;
//   double w1, w2, wSum = .0;
// 
//   rep->lpairs = realloc(rep->lpairs, sizeof(DtDistRec) * kds);
//   rep->nLPairs = 0;
// 
//   /* --- calcul du nombre de points appariés --- */
// 
//   memset (at1App, 0, sizeof(char)*(ng1->n));
//   memset (at2App, 0, sizeof(char)*(ng2->n));
// 
//   i=0;
//   nbAtApp = 0;
//   while(i<(kds) ) {
//     i1=(dist[i].liaison)%(ng1->n);
//     i2=(dist[i].liaison)/(ng1->n);
// 
//     if (verbose) {
//       fprintf(stderr,"enlarge pair %d : %s %d %c - %d : %s %d %c dist %.5lf (%.3lf %.3lf %.3lf / %.3lf %.3lf %.3lf)\n",
// 	      i1, ng1->resName[i1], ng1->resNum[i1], ng1->chn[i1],
// 	      i2, ng2->resName[i2], ng2->resNum[i2], ng2->chn[i2],dist[i].longueur,
// 	      ng1->crds[i1][0], ng1->crds[i1][1], ng1->crds[i1][2],
// 	      ng2->crds[i2][0], ng2->crds[i2][1], ng2->crds[i2][2]);
//       /* printf("*%d\tAT1 - AT2 : %d - %d\t%lf\n",
// 	 i,i1,i2,dist[i].longueur); */
//       /* printPoint3Crds(ng1->crds,i1);
// 	 printPoint3Crds(ng2->crds,i2); */
//     }
// 
//     if(at1App[i1]==1){
//       if (verbose) {
//         fprintf(stderr,"enlarge: skipping on i1\n");
//       }
//       goto LCONT;
//       /* printf("*=> ATOME %d DU NUAGE 1 DEJA APPARIE.\n",i1); */
//     } else {
//       at1App[i1]=1;
//     }
//     if(at2App[i2]==1){
//       if (verbose) {
//         fprintf(stderr,"enlarge: skipping on i2\n");
//       }
//       goto LCONT;
//       /* printf("*=> ATOME %d DU NUAGE 2 DEJA APPARIE.\n",i2); */
//     } else {
//       at2App[i2]=1;
//     }
//     if (dist[i].longueur < tol) {
//       rep->lpairs[rep->nLPairs].liaison = dist[i].liaison;
//       rep->lpairs[rep->nLPairs].longueur = dist[i].longueur;
//       rep->nLPairs ++;
//       w1 = (ng1->weight) ? ng1->weight[i1] : 1.0;
//       w2 = (ng2->weight) ? ng2->weight[i2] : 1.0;
//       sqrsum += (dist[i].longueur * dist[i].longueur) * w1 * w2;
//       wSum += w1 * w2;
//     } else {
//       break;
//     }
// 
//   LCONT:;
//     i++;
//   }
// 
//   rep->lrmsd = sqrt(sqrsum / wSum);
// 
//   return (int) (rep->nLPairs);
// }

int enlarge3(DtNuage *ng1, DtNuage *ng2, int nbAtApp, int kds, DtDistRec *dist, double tol)
{
  char at1App[ng1->n]; //at1App[k]=1 si le k-ème point de ng1 fait déjà parti d'un appariement, 0 sinon
  char at2App[ng2->n]; //at2App[k]=1 si le k-ème atome de ng2 fait déjà parti d'un appariement, 0 sinon
  int i,i1,i2, nLPairs;
  double tol2 = tol * tol;

  nLPairs = 0;

  /* --- calcul du nombre de points appariés --- */

  memset (at1App, 0, sizeof(char)*(ng1->n));
  memset (at2App, 0, sizeof(char)*(ng2->n));

  for (i=0; i < kds; i++) {
    i1=(dist[i].liaison)%(ng1->n);
    i2=(dist[i].liaison)/(ng1->n);

    dist[i].longueur2 = squared_distance((ng1->crds[i1]), (ng2->crds[i2]));

    if(at1App[i1]==1){
      continue;
    } else {
      at1App[i1]=1;
    }

    if(at2App[i2]==1){
      continue;
    } else {
      at2App[i2]=1;
    }

    // Adjustment by P. Tuffery, September 2008
    if (!ng1->re[i1]->atmMask[i2]) continue;
    // End adjustment

    if (dist[i].longueur2 < tol2) {
      nLPairs ++;
    }
  }

  return (int) (nLPairs);
}

// int enlarge2(DtNuage *ng1, DtNuage *ng2, int nbAtApp, int kds, DtDistRec *dist, double tol)
// {
//   char at1App[ng1->n]; //at1App[k]=1 si le k-ème point de ng1 fait déjà parti d'un appariement, 0 sinon
//   char at2App[ng2->n]; //at2App[k]=1 si le k-ème atome de ng2 fait déjà parti d'un appariement, 0 sinon
//   int i,i1,i2, nLPairs;
// //  int shift;
// //  double lg;
// 
//   nLPairs = 0;
// 
//   /* --- calcul du nombre de points appariés --- */
// 
//   memset (at1App, 0, sizeof(char)*(ng1->n));
//   memset (at2App, 0, sizeof(char)*(ng2->n));
// 
//   i=0;
//   nbAtApp = 0;
//   while(i<(kds) ) {
//     i1=(dist[i].liaison)%(ng1->n);
//     i2=(dist[i].liaison)/(ng1->n);
// 
//     /* printf("*%d\tAT1 - AT2 : %d - %d\t%lf\n",
//        i,i1,i2,dist[i].longueur); */
//     /* printPoint3Crds(ng1->crds,i1);
//     printPoint3Crds(ng2->crds,i2); */
//     dist[i].longueur = distance((ng1->crds[i1]),(ng2->crds[i2]));
// 
//     if(at1App[i1]==1){
//       goto LCONT;
//       /* printf("*=> ATOME %d DU NUAGE 1 DEJA APPARIE.\n",i1); */
//     } else {
//       at1App[i1]=1;
//     }
//     if(at2App[i2]==1){
//       goto LCONT;
//       /* printf("*=> ATOME %d DU NUAGE 2 DEJA APPARIE.\n",i2); */
//     } else {
//       at2App[i2]=1;
//     }
//     if (dist[i].longueur < tol) {
//       nLPairs ++;
//     }
//   LCONT:;
//     i++;
//   }
// 
//   // fprintf(stderr,"N lapparies: %d\n",rep->nLPairs);
// 
//   return (int) (nLPairs);
// }


/* =========================================================================
 * Transfert les données de sdm à zuker_superpose.
 * (renvoie les (nbAtApp) coordonnées à superposer au mieux l'une sur l'autre).
 * =========================================================================
 */

void crdsASuperposer(DtNuage *ng1, DtNuage *ng2, int nbAtApp, DtDistRec *dist, DtPoint3 *crdsSup1, DtPoint3 *crdsSup2, double *weightSup1, double *weightSup2)
{
  int i;
  int i1;
  int i2;

  for(i=0 ; i < nbAtApp ; i++){
    i1=(dist[i].liaison)%(ng1->n);
    i2=(dist[i].liaison)/(ng1->n);
    copyPoint3(ng1->crds[i1],crdsSup1[i]);
    copyPoint3(ng2->crds[i2],crdsSup2[i]);
    weightSup1[i] = (ng1->weight) ? ng1->weight[i1] : 1.0; // poids associés aux atomes
    weightSup2[i] = (ng2->weight) ? ng2->weight[i2] : 1.0;
/*    fprintf(stderr, "%lf %lf %lf\t%lf %lf %lf\n", crdsSup1[i][0], crdsSup1[i][1], crdsSup1[i][2], crdsSup2[i][0], crdsSup2[i][1], crdsSup2[i][2]);*/
  }
}

void crdsASuperposerFromVects(DtNuage *ng1, DtNuage *ng2,
			      int nbAtApp, int *v1, int *v2,
			      DtPoint3 *crdsSup1, DtPoint3 *crdsSup2)
{
  int i;
  int i1;
  int i2;

  fprintf(stderr,"crdsASuperposerFromVects\n");
  for(i=0 ; i < nbAtApp ; i++){
    i1=v1[i] - 1;
    i2=v2[i] - 1;
    copyPoint3(ng1->crds[i1],crdsSup1[i]);
    copyPoint3(ng2->crds[i2],crdsSup2[i]);
  }
  fprintf(stderr,"crdsASuperposerFromVects: Done ...\n");
}

/* ====================== ALGORITHME CSR =====================================
 * Superpose au mieux deux nuages de points avec SDM et zuker_superpose.
 * Retourne :
 *   le nombre de points appariés.
 *   la matrice de rotation 4x4 correspondant à la meilleure superposition.
 *   le tableau trié des distances entre points après rotation et translation.
 *   plNg2 est une copie de ng2, permettant de garantir que ng2 sera inchange ...
 * ===========================================================================
 */
DtReponse *csr2(DtNuage *ng1, DtNuage *ng2, DtNuage *plNg2, double dseuil, int maxPairs, DtDistRec *pairs, DtMatrix4x4 mat, DtPoint3 *crdsSup1, DtPoint3 *crdsSup2, double *weightSup1, double *weightSup2, int nbIter, int minAtm, double maxRMSd, double tol, DtReponse *rep, int verbose)
{
  DtMatrix4x4 imat, rmat;
  double ormsd, rmsd;
  double minRMSd = 2.;
  int compteur,nbAtApp,nbLAtApp,nbAtTemp;
  int OK;
  int curNPairs;
  int minN1N2;
  int stockp = 0;

  minN1N2 = min(ng1->n, ng2->n);

  if (verbose) {
    fprintf(stderr,"CSR inits over. searching %d atoms vs %d atoms. Minat %d. nbIter %d\n",ng1->n,ng2->n, minAtm, nbIter);
    fflush(stderr);
  }

  compteur = 0;
  while (compteur < nbIter){

    if (verbose) {
      if (!((compteur+1) % 100)) {
	fprintf(stderr,"iteration.%5d\n",compteur+1);
      }
    }
    if (verbose) {
      fprintf(stderr,"Step 1.\n"); fflush(stderr);
    }

    rmsd = -1.;
    nbAtTemp = 1;
    MkNullMat4x4(imat);

    // Reset ng2 to original orientation
    memcpy(ng2->crds, plNg2->crds, sizeof(DtPoint3) * ng2->n);

    // 1. Orientation aleatoire initiale
    if (verbose) {
      fprintf(stderr,"Will set Initial orientation.\n"); fflush(stderr);
    }
    posAl2(ng1, ng2, imat);
    if (verbose) {
      fprintf(stderr,"Initial orientation set.\n");fflush(stderr);
    }
    // 2. Perform first SDM call
    // SDM: retourne nombre d'atomes apparié
    // curNPairs est l'ecart quadratique moyen entre les coordonnées
    nbAtApp = sdm2(ng1, ng2, dseuil, maxPairs, &curNPairs, pairs);

    // Incorrect starting point: we pass
    if (nbAtApp <= 1) {
      compteur += 1;
      continue;
    }

    // Preparation Zuker
    memcpy(ng2->crds, plNg2->crds, sizeof(DtPoint3) * ng2->n);
    crdsASuperposer(ng1, ng2, nbAtApp, pairs, crdsSup1, crdsSup2, weightSup1, weightSup2);
    // zuker returns squared rmsd
    rmsd = zuker_superpose_weighted(crdsSup1, crdsSup2, nbAtApp, rmat, weightSup1, weightSup2);
    rotateCrds(ng2->crds, ng2->n, rmat);

    /* --- Stockage de la réponse si meilleure --- */
    /* Pb du "meilleur" */
    if (rmsd < maxRMSd) {

      /* BUG: for enlarging, we MUST recompute the distances ... */
      nbLAtApp = enlarge3(ng1, ng2, nbAtApp, curNPairs, pairs, tol);

      if ((nbAtApp >= rep->nbAtMax) && (nbLAtApp >= rep->nLPairs)) {
	stockRep2(curNPairs, nbAtApp, pairs, sqrt(rmsd), rmat,rep);
	stockp = 1;
	nbLAtApp = enlarge_weighted2(ng1, ng2, rep, nbAtApp, curNPairs, pairs, tol, 0);
	if (verbose) {
	  fprintf(stderr,"Iteration : %5d # paired atoms : %4d RMSd : %.2lf enlarged # paired atoms %d\n",
		  compteur+1,nbAtApp, sqrt(rmsd), nbLAtApp);
	}
      }
      // if ((nbAtApp == minN1N2) && (nbLAtApp == minN1N2) && (sqrt(rmsd) < minRMSd)) break;
      if ((nbAtApp == minN1N2) && (sqrt(rmsd) < minRMSd)) break;
    }

    // 3. Iterate on SDM
    // On fait des itérations SDM tant que le nombre d'atomes apparié croît pour que le fit soit meilleur
    if (verbose) {
      fprintf(stderr,"iteration.%5d: Will iterate on SDM.\n",compteur+1);
    }
    OK  = 1;
    while (OK) {

      /* --- Nouvel appel de zuker-sdm pour un meilleur appariement--- */
      nbAtTemp  = nbAtApp;
      ormsd     = rmsd;

      nbAtApp = sdm2(ng1, ng2, dseuil, maxPairs, &curNPairs, pairs);

      /* Should we keep on ? */
      if (nbAtApp < nbAtTemp) {
	break;
      }

      memcpy(ng2->crds, plNg2->crds, sizeof(DtPoint3) * ng2->n);
      crdsASuperposer(ng1, ng2, nbAtApp, pairs, crdsSup1, crdsSup2, weightSup1, weightSup2);
      rmsd = zuker_superpose_weighted(crdsSup1, crdsSup2, nbAtApp, rmat, weightSup1, weightSup2);
      rotateCrds(ng2->crds, ng2->n, rmat);

     /* Should we keep on ? */
      if (nbAtApp == nbAtTemp) {
	/* Check if better fit */
	if (rmsd >= ormsd) {
	  break;
	}
	/* Check if pairs have changed */
      }

      /* --- Stockage de la réponse si meilleure --- */
      /* Pb du "meilleur" */
      if (rmsd < maxRMSd) {
	nbLAtApp = enlarge3(ng1, ng2, nbAtApp, curNPairs, pairs, tol);

        if ((nbAtApp >= rep->nbAtMax) && (nbLAtApp > rep->nLPairs)) {
          stockRep2(curNPairs, nbAtApp, pairs, sqrt(rmsd), rmat,rep);
          stockp = 1;
          nbLAtApp = enlarge_weighted2(ng1, ng2, rep, nbAtApp, curNPairs, pairs, tol, 0);

	  if (verbose) {
	    fprintf(stderr,"Iteration : %5d # paired atoms : %4d RMSd : %.2lf enlarged # paired atoms %d\n",
		    compteur+1,nbAtApp, sqrt(rmsd), nbLAtApp);
	  }
	}
	// if ((nbAtApp == minN1N2) && (nbLAtApp == minN1N2) && (sqrt(rmsd) < minRMSd)) break;
	if ((nbAtApp == minN1N2) && (sqrt(rmsd) < minRMSd)) break;
      }
    }

    if ((rep->nbAtMax == minN1N2) && (rep->nLPairs == minN1N2) && (rep->rmsd < minRMSd)) break;

    compteur=compteur+1;
  }

  memcpy(ng2->crds, plNg2->crds,sizeof(DtPoint3) * ng2->n);

  if (stockp) {
    rotateCrds(ng2->crds,ng2->n,rep->rM);
  }

  return rep;
}

// DtReponse *csr(DtNuage *ng1, DtNuage *ng2, DtNuage *plNg2, double dseuil, int maxPairs, DtDistRec *pairs, DtMatrix4x4 mat, DtPoint3 *crdsSup1, DtPoint3 *crdsSup2, double *weightSup1, double *weightSup2, int nbIter, int minAtm, double maxRMSd, double tol, DtReponse *rep, int verbose)
// {
//   //  DtNuage *plNg2;
//   DtMatrix4x4 imat, rmat;
//   double ormsd, rmsd;
//   double minRMSd = 2.;
//   int compteur,nbAtApp,nbLAtApp,nbAtTemp;
// //  int i, i1, i2;
//   int OK;
//   int curNPairs;
//   int minN1N2;
//   int stockp = 0;
// 
//   minN1N2 = min(ng1->n, ng2->n);
// 
//   // Copy original crds of ng2 !!
//   //  plNg2 = allocNuage(ng2->n);
//   // memcpy(plNg2->crds,ng2->crds, sizeof(DtPoint3) * ng2->n);
// 
//   if (verbose) {
//     fprintf(stderr,"CSR inits over. searching %d atoms vs %d atoms. Minat %d\n",ng1->n,ng2->n, minAtm);
//     fflush(stderr);
//   }
// 
//   // We do not need to copy atmName and resName
//   // DtPoint3 *crds;     /* Coordonnees */
//   // DtStr4   *atmName;  /* Nom atome   */
//   // DtStr3   *resName;  /* Nom residu  */
// 
// #if 0
//   crdsASuperposerFromVects(ng1, ng2, 34, v1, v2, crdsSup1, crdsSup2);
//   rmsd=zuker_superpose(crdsSup1,crdsSup2,34,mat);
//   fprintf(stderr,"v1v2 RMSd %lf\n",sqrt(rmsd));
//   exit(0);
// #endif
// 
//   compteur = 0;
//   while (compteur < nbIter){
// 
//     if (verbose) {
//       if (!((compteur+1) % 100)) {
// 	fprintf(stderr,"iteration.%5d\n",compteur+1);
//       }
//     }
// 
//     rmsd=-1.;
//     nbAtTemp=1;
//     MkNullMat4x4(imat);
//     //posAl(ng1, ng2, mat);
// 
//     // Reset ng2 to original orientation 
//     memcpy(ng2->crds, plNg2->crds,sizeof(DtPoint3) * ng2->n);
// 
//     // 1. Orientation aleatoire initiale
// 
//     // fprintf(stderr,"CSR will posAl2\n");
//     // fflush(stderr);
//     posAl2(ng1, ng2, imat);
//     // Affichage crds pour debug
//     // printNgCrds(ng1->crds,ng1->n);
//     // printNgCrds(ng2->crds,ng2->n);
// 
//     // 2. Perform first SDM call
//     // SDM: retourne nombre d'atomes apparié
//     // curNPairs est l'ecart quadratique moyen entre les coordonnées
//     // fflush(stderr);
//     nbAtApp=sdm(ng1,ng2,dseuil,maxPairs,&curNPairs,pairs);
//     // fprintf(stderr,"CSR  sdm over\n");
//     // fflush(stderr);
//     // fprintf(stderr,"nbAtApp ori %d\n",nbAtApp);
// 
//     // Incorrect starting point: we pass
//     if (nbAtApp <= 1) {
//       compteur += 1;
//       // fprintf(stderr,"Continuing on (nbAtApp <= 1 : %d)\n", nbAtApp);
//       continue;
//     }
// 
//     // Preparation Zuker
//     memcpy(ng2->crds, plNg2->crds,sizeof(DtPoint3) * ng2->n);
//     crdsASuperposer(ng1,ng2,nbAtApp,pairs,crdsSup1,crdsSup2,weightSup1,weightSup2);
//     // zuker returns squared rmsd
//     rmsd=zuker_superpose_weighted(crdsSup1,crdsSup2,nbAtApp,rmat,weightSup1,weightSup2);
//     rotateCrds(ng2->crds,ng2->n,rmat);
// 
// //     if (0 && verbose)
// //       fprintf(stderr,"Iteration : %5d # paired atoms : %4d RMSd : %.2lf n",
// // 	      compteur+1,nbAtApp, sqrt(rmsd));
// 
// 
//     /* --- Stockage de la réponse si meilleure --- */
//     /* Pb du "meilleur" */
//     if(rmsd < maxRMSd) {
// 
//       /* BUG: for enlarging, we MUST recompute the distances ... */
//       nbLAtApp = enlarge2(ng1, ng2, nbAtApp, curNPairs, pairs, tol);
//       //	fprintf(stderr,"Iteration : %5d # paired atoms : %4d RMSd : %.2lf enlarged # paired atoms %d\n",
//       //	compteur+1,nbAtApp, sqrt(rmsd), nbLAtApp);
// 
//       if ((nbAtApp >= rep->nbAtMax) && (nbLAtApp >= rep->nLPairs)) {
// 	stockRep(curNPairs,nbAtApp,pairs,sqrt(rmsd), rmat,rep);
// 	stockp = 1;
// 	nbLAtApp = enlarge_weighted(ng1, ng2, rep, nbAtApp, curNPairs, pairs, tol, 0);
// 	if (verbose) {
// 	  fprintf(stderr,"Iteration : %5d # paired atoms : %4d RMSd : %.2lf enlarged # paired atoms %d\n",
// 		  compteur+1,nbAtApp, sqrt(rmsd), nbLAtApp);
// 	}
//       }
//       // if ((nbAtApp == minN1N2) && (nbLAtApp == minN1N2) && (sqrt(rmsd) < minRMSd)) break;
//       if ((nbAtApp == minN1N2) && (sqrt(rmsd) < minRMSd)) break;
//     }
// 
//     // 3. Iterate on SDM
//     // On fait des itérations SDM tant que le nombre d'atomes apparié croît pour que le fit soit meilleur
//     OK  = 1;
//     while(OK){
// 
//       /* --- Nouvel appel de zuker-sdm pour un meilleur appariement--- */
//       // fprintf(stderr,"Iteration %d OK loop\n",compteur);
//       nbAtTemp  = nbAtApp;
//       ormsd     = rmsd;
// 
//       /* printf("rms1 %lf\t",rmsd);
// 	 crdsASuperposer(ng1,ng2,nbAtApp,pairs,crdsSup1,crdsSup2);
// 	 rmsd=zuker_superpose(crdsSup1,crdsSup2,nbAtApp,mat);
// 	 rotateCrds(ng2->crds,ng2->n,mat);
// 	 printf("rms2 %lf\n",rmsd); */
// 
//       // fprintf(stderr,"CSR will sdm\n");
//       // fflush(stderr);
// 
//       nbAtApp=sdm(ng1,ng2,dseuil,maxPairs,&curNPairs,pairs);
//       // fprintf(stderr,"CSR sdm over\n");
//       // fflush(stderr);
// 
//       /* Should we keep on ? */
//       if (nbAtApp < nbAtTemp) {
// 	break;
//       }
// 
//       // fprintf(stderr,"CSR will zucker\n");
//       // fflush(stderr);
//       memcpy(ng2->crds, plNg2->crds,sizeof(DtPoint3) * ng2->n);
//       crdsASuperposer(ng1,ng2,nbAtApp,pairs,crdsSup1,crdsSup2,weightSup1,weightSup2);
//       rmsd=zuker_superpose_weighted(crdsSup1,crdsSup2,nbAtApp,rmat,weightSup1,weightSup2);
//       rotateCrds(ng2->crds,ng2->n,rmat);
//       // fprintf(stderr,"CSR  zucker done\n");
//       // fflush(stderr);
// 
//      /* Should we keep on ? */
//       if (nbAtApp == nbAtTemp) {
// 	/* Check if better fit */
// 	if (rmsd >= ormsd) {
// 	  break;
// 	}
// 	/* Check if pairs have changed */
//       }
// 
//       /* --- Stockage de la réponse si meilleure --- */
//       /* Pb du "meilleur" */
//       if(rmsd < maxRMSd) {
// 	// fprintf(stderr,"CSR will enlarge\n");
// 	// fflush(stderr);
// 
// 	nbLAtApp = enlarge2(ng1, ng2, nbAtApp, curNPairs, pairs, tol);
// 	// fprintf(stderr,"CSR will enlarge over\n");
// 	// fflush(stderr);
//         if ((nbAtApp >= rep->nbAtMax) && (nbLAtApp > rep->nLPairs)) {
//           stockRep(curNPairs,nbAtApp,pairs,sqrt(rmsd), rmat,rep);
//           stockp = 1;
//           nbLAtApp = enlarge_weighted(ng1, ng2, rep, nbAtApp, curNPairs, pairs, tol, 0);
//           // printMat4x4(rep->rM);
// 
// 	  if (verbose) {
// 	    fprintf(stderr,"Iteration : %5d # paired atoms : %4d RMSd : %.2lf enlarged # paired atoms %d\n",
// 		    compteur+1,nbAtApp, sqrt(rmsd), nbLAtApp);
// 	  }
// 	}
// 	// if ((nbAtApp == minN1N2) && (nbLAtApp == minN1N2) && (sqrt(rmsd) < minRMSd)) break;
// 	if ((nbAtApp == minN1N2) && (sqrt(rmsd) < minRMSd)) break;
//       }
//     }
// 
//     if ((rep->nbAtMax == minN1N2) && (rep->nLPairs == minN1N2) && (rep->rmsd < minRMSd)) break;
// 
//     /* printf("\nSortie boucle : %d <= %d\n",nbAtApp,nbAtTemp); */
//     compteur=compteur+1;
//     /* printf("\n"); */
//     // exit(0);
//   }
// 
//   // fprintf(stderr,"CSR loop over\n");
//   // fflush(stderr);
//   memcpy(ng2->crds, plNg2->crds,sizeof(DtPoint3) * ng2->n);
//   if (stockp) {
//     rotateCrds(ng2->crds,ng2->n,rep->rM);
//     //printMat4x4(rep->rM);
//   }
//   // freeNuage(plNg2);
// 
//   return rep;
// }
