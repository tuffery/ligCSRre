/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
#include <stdio.h>

#include "argstr.h" 
#include "CSR.h"
#include "CSRTypes.h"
#include "pairRules.h"
#include "PDB.h"
#include "regex.h"
#include "TxtFile.h"

/* Valid Mol2 atom names */
DtStr5 Mol2Names[53] = {
  "C.3",   /* sp3 carbon */
  "C.2",   /* sp2 carbon */
  "C.ar",  /* aromatic carbon */
  "C.1",   /* sp carbon */
  "N.3",   /* sp3 nitrogen */
  "N.2",   /* sp2 nitrogen */
  "N.1",   /* sp nitrogen */
  "O.3",   /* sp3 oxygen */
  "O.2",   /* sp2 oxygen */
  "S.3",   /* sp3 sulfur */
  "N.ar",  /* aromatic nitrogen */
  "P.3",   /* sp3 phosphorous */
  "H",     /* hydrogen */
  "Br",    /* bromine */
  "Cl",    /* chlorine */
  "F",     /* fluorine */
  "I",     /* iodine */
  "S.2",   /* sp2 sulfur */
  "N.pl3", /* trigonal planar nitrogen */
  "LP",    /* lone pair */
  "Na",    /* sodium */
  "K",     /* potassium */
  "Ca",    /* calcium */
  "Li",    /* lithium */
  "Al",    /* aluminum */
  "Du",    /* dummy */
  "Du.C",  /* dummy with the weight of carbon */
  "Si",    /* silicon */
  "N.am",  /* amide nitrogen */
  "S.o",   /* sulfoxide sulfur */
  "S.o2",  /* sulfone sulfur */
  "N.4",   /* sp3 positively charged nitrogen */
  "O.co2", /* oxygen in carboxylate or phosphate group */
  "C.cat", /* carbocation, used only in a guadinium group */
  "H.spc", /* hydrogen in SPC water model */
  "O.spc", /* oxygen in SPC water model */
  "H.t3p", /* hydrogen in TIP3P water model */
  "O.t3p", /* oxygen in TIP3P water model */
  "ANY",   /* any atom */
  "HEV",   /* heavy (non H) atom */
  "HET",   /* heteroatom (N, O, S, P) */
  "HAL",   /* halogen */
  "Mg",    /* magnesium */
  "Cr.oh", /* hydroxy chromium */
  "Cr.th", /* chromium */
  "Se",    /* selenium */
  "Fe",    /* iron */
  "Cu",    /* copper */
  "Zn",    /* zinc */
  "Sn",    /* tin */
  "Mo",    /* molybdenum */
  "Mn",    /* manganese */
  "Co.oh"  /* hydroxy cobalt */
};

int isMol2Name(char *name)
{
  int i;

  for (i=0;i<53;i++) {
    if (!strcmp(name, Mol2Names[i])) return 1;
  }
  return 0;
}

/* ajoute une liaison dans la matrice des connexités */
int addConnect(char *line, char **matrix, int n, int *nbConnect) {
  char s[3];
  int i, at1, at2;

  /* lecture correcte de la ligne de description de liaison */
  if (sscanf(line, " %d %d %d %2s", &i, &at1, &at2, s) == 4) {
    /* assert */
    if (at1 > n || at2 > n || at1 < 0 || at2 < 0) {
      return 0;
    }

    matrix[at1-1][at2-1] = matrix[at2-1][at1-1] = 1;
    (*nbConnect) += 1;
    return 1;
  }

  return 0;
}

int computeConnect(int *nConnect, double *nConnectW, DtNuage *pNg1, DtNuage *pNg2,
                   DtReponse *rep) {
  int i, i1, i2;
  int j, j1, j2;
  double w1, w2;

  /* assert */
  if (!pNg1 || !pNg2 || !pNg1->connectMatrix || !pNg2->connectMatrix)
    return 0;

  *nConnect = 0;
  *nConnectW = .0;

  for (i = 0; i < rep->nLPairs - 1; i++) {
    i1=(rep->lpairs[i].liaison)%(pNg1->n);
    i2=(rep->lpairs[i].liaison)/(pNg1->n);

    for (j = i+1; j < rep->nLPairs; j++) {
      j1=(rep->lpairs[j].liaison)%(pNg1->n);
      j2=(rep->lpairs[j].liaison)/(pNg1->n);

      if (pNg1->connectMatrix[i1][j1] && pNg2->connectMatrix[i2][j2]) {
        *nConnect += 1;
        w1 = (pNg1->weight) ? pNg1->weight[i1] : 1.0;
        w2 = (pNg1->weight) ? pNg1->weight[i2] : 1.0;
        *nConnectW += 0.5 * (w1 + w2);
      }
    }
  }

  return 1;
}

/* initialisation de la matrice de connexités */
char **initConnectMatrix(int n) {
  int i;
  char **p = (char **)calloc(n, sizeof(char *));

  for (i = 0; i < n; i++) {
    p[i] = (char *)calloc(n, sizeof(char));
  }

  return p;
}

/* libération de la matrice de connexités */
void freeConnectMatrix(char **matrix, int n) {
  int i;

  if (!matrix)
    return;

  for (i = 0; i < n; i++) {
    free(matrix[i]);
  }

  free(matrix);
}



/* transforme l'écriture mol2 du nom d'atome (atmNm2) pour l'utilisation
   de regex */
void atmMol2InternalName(char *atmName, char *atmNm2) {

  /* cas particuliers, conservés tel quel */
  if (strcmp(atmNm2, "LP") == 0 || strcmp(atmNm2, "Du") == 0
      || strcmp(atmNm2, "Any") == 0 || strcmp(atmNm2, "Hal") == 0
      || strcmp(atmNm2, "Het") == 0 || strcmp(atmNm2, "Hev") == 0) {
    strcpy(atmName, atmNm2);
    return;
  }
  if (strcmp(atmNm2, "Du.C") == 0) {
    strcpy(atmName, "DuC");
    return;
  }

  /* par défaut : premier caractère seul */
  atmName[0] = atmNm2[0];
  atmName[1] = 0;
  
  /* Modification P. Tuffery 10/2008 for donnor, acceptor classes */
  if (atmNm2[2] == 'd' && atmNm2[3] == 'o') {  // X.do -> do
    atmName[0] = atmNm2[2];
    atmName[1] = atmNm2[3];
    atmName[2] = 0;
  }
  else if (atmNm2[2] == 'a' && atmNm2[3] == 'c') {  // X.ac -> ac
    atmName[0] = atmNm2[2];
    atmName[1] = atmNm2[3];
    atmName[2] = 0;
  }
  else if (atmNm2[2] == 'd' && atmNm2[3] == 'a') {  // X.da -> da
    atmName[0] = atmNm2[2];
    atmName[1] = atmNm2[3];
    atmName[2] = 0;
  }
  else   /* END MODIF */
    if (atmNm2[2] == 'p' && atmNm2[3] == 'h') {  // X.ph -> ph
    atmName[0] = atmNm2[2];
    atmName[1] = atmNm2[3];
    atmName[2] = 0;
  }
  else if (atmNm2[0] == 'N' && atmNm2[3] == 'm') { // N.am
    atmName[1] = atmNm2[3];
    atmName[2] = 0;
  }
  else if (atmNm2[0] == 'S' && atmNm2[3] == '2') { // S.O2
    atmName[1] = atmNm2[2];
    atmName[2] = atmNm2[3];
    atmName[3] = 0;
  }
  else if (atmNm2[1] == '.') { // x.x
    atmName[1] = atmNm2[2];
    atmName[2] = 0;
  }
  else if (atmNm2[2] == '.') { // xx.x
    atmName[1] = atmNm2[1];
    atmName[2] = atmNm2[3];
    atmName[3] = 0;
  }
  else if (atmNm2[0] == 'H') {} // H
  else if (atmNm2[1] == 0 || atmNm2[2] == 0) {
     atmName[0] = ':';
     atmName[1] = atmNm2[0];
     atmName[2] = atmNm2[1];
     atmName[3] = 0;
  }
}

/* écrit le nom de l'atome au format PDB pour une sortie PDB */
void atmNameMol2ToPDB(char *atmNme, char *atmNm2, char *atmName) {
  int aPos = 0;
  char *pC = atmNm2;

  if (atmName[0] != ':') {
    atmNme[0] = ' ';
    aPos++;
  }

  while (*pC != '.' && *pC != 0 && aPos < 4) {
    atmNme[aPos] = *pC;
    aPos++;
    pC++;
  }

  while (aPos < 4) {
    atmNme[aPos++] = ' ';
  }

  atmNme[aPos] = 0;
}

/* recherche une association atomNum<>Re a partir du numero d'atome */
DtAtReList *atRePtrFromAtNum(DtAtReList *pAtReList, int atomNumRe) {
  while (pAtReList != NULL && pAtReList->atom != atomNumRe) {
    pAtReList = pAtReList->next;
  }
  return pAtReList;
}

/* récupère les coordonnées d'un atome dans une ligne de la section ATOM */
void getMol2AtomCoords(char *line,DtFloat *x,DtFloat *y,DtFloat *z)
{
  char buff[8];
  int i;

  sscanf(line,"%d %s %lf %lf %lf", &i, &buff, x, y, z);
}

/* récupère le nom de l'atome dans une ligne de la section ATOM */
void getMol2AtomName(char *line,char *anom)
{
  char buff[30];
  float f;
  int i;

  sscanf(line,"%d %29s %f %f %f %5s",&i,buff,&f,&f,&f,anom);

  anom[5] = '\0';
  if (!strcmp(anom,"S.O2")) strcpy(anom,"S.o2");
  if (!isMol2Name(anom)) {
    fprintf(stderr,"\n%s: not a valid mol2 atom type. \nPlease check your data. Consider using a format converter such as openbabel.\n\n",anom);
    exit(0);
  }
}

/* récupère le poids donné dans la section COMMENT */
double getMol2AtomWeight(char *line)
{
  double w;

  if(!sscanf(line," REMARK WEIGHT. %lf", &w))
    return 1.0;

  return w;
}

/* récupère le numéro d'atome qui sera associé à une regex et/ou un poids */
void getMol2AtmNbRe(char *line, int *anum) {
  sscanf(line," REMARK ATOM %d", anum);
}

/* récupère la regex définie dans la section COMMENT */
void getMol2AtmRe(char *line, char *atmStrRe) {
  int len;

  sscanf(line," REMARK MATCH. %s", atmStrRe);

  len = strlen(atmStrRe);

  /* ajoute '^' au début s'il n'y est déjà */
  if (len > 0 && atmStrRe[0] != '^') {
    memmove(atmStrRe+1, atmStrRe, (len+1)*sizeof(char));
    atmStrRe[0] = '^';
    len++;
  }

  /* ajoute '$' à la fin s'il n'y est déjà */
  if (atmStrRe[len-1] != '$') {
    atmStrRe[len] = '$';
    atmStrRe[len+1] = 0;
  }
}

void getMol2AtomNum(char *line, int *anum)
{
  sscanf(line,"%d", anum);
}

int getMol2LineStatus(char *d)
{
  char Entete[25];
  int i;
  float f;
  char s[10];

  strncpy(Entete, d, 9);
  Entete[9] = 0;
  if (strcmp(Entete, "@<TRIPOS>") == 0) {
    strncpy(Entete, d+9, 24);
    Entete[24] = 0;
    if (strcmp(Entete,"ATOM") == 0) return 1;
    if (strcmp(Entete,"MOLECULE") == 0) return 10;
    if (strcmp(Entete,"COMMENT") == 0) return 20;
    if (strcmp(Entete,"BOND") == 0) return 30;
  }

  if (sscanf(d,"%d %s %f %f %f %s", &i, &s, &f, &f, &f, &s) == 6) return 2; // atom data

  if (sscanf(d," REMARK ATOM %d", &i) == 1) return 21;
  if (sscanf(d," REMARK MATCH. %c", s) == 1) return 22;
  if (sscanf(d," REMARK WEIGHT. %f", &f) == 1) return 23;

  return 0;
}

void getMol2ResChainIde(char *line,char *chn)
{
  int i, id = 0;
  char c[30];
  float f;

  sscanf(line, "%d %s %f %f %f %s %d", &i, &c, &f, &f, &f, &c, &id);
  *chn = '0' + (id % 10);
}


/* ============================================================================
 * Remplit les tableaux de données utiles à Csr après lecture des fichiers mol2.
 * Alloue la mémoire pour la structure DtNuage.
 * ============================================================================
 */
void lectMol2Csr(char *fname, int what, int strict, DtNuage **pN, int *onNuages,
                 DtReRec **pReList, int verbose, DtRuleList *pRuleList)
{
  DtNuage *n;
  char   **lines;
  int     *limits;

  DtReRec *pRe;
  DtAtReList *pAtRe, *pAtReList;
  DtRuleList *pRule = NULL;

  int  nLines;
  int  aLine;
  int  lineStart;
  int  lineStat;
  int  nMol;
  int  molOpened;
//  int  nNuages;
  int  aNuage;
  int  next;
  int  atomNbr;
  int  atomNumRe;
  int  aAtom;
  char atmStrRe[BUFSIZ];
  char resStrRe[1];      // char resStrRe[BUFSIZ];
//  char buff[BUFSIZ];
//  char *cmpnd;
//  char EC[BUFSIZ];
//  char HETGRP[BUFSIZ];

  int nHydrogen;

  /* pas d'ID PDB */
  char *id = (char *)malloc(sizeof(char) * 1);
  id[0] = '\0';
  /* pas d'EC non plus */
  char *EC = (char *)malloc(sizeof(char) * 1);
  EC[0] = '\0';


  lines=txtFileRead(fname,&nLines);

  if (!lines) {
    exit(0);
  }
  if (verbose)
    fprintf(stderr,"File %s: Read %d lines\n", fname, nLines);


  /* -- Nombre de molecules -- */
  nMol    = 0;
  limits  = NULL;

  for (aLine=0; aLine < nLines; aLine++) {
    lineStat=getMol2LineStatus(lines[aLine]);

    if (lineStat == 10) { /* @<TRIPOS>MOLECULE */
      if (nMol) {
        limits[nMol-1] = aLine;
      }
      nMol++;
      limits = realloc(limits, sizeof(int)*nMol);
    }
  }

  if (nMol) {
    limits[nMol-1] = aLine;
  }

  if (verbose)
    fprintf(stderr,"File %s: Found %d Mols\n", fname, nMol);

  /* -- Allocate nuages -- */
  *onNuages = nMol;
  *pN  = calloc(nMol, sizeof(DtNuage));


  /* -- Input des nuages -- */
  molOpened = 0;
  for (aNuage = 0; aNuage < nMol; aNuage++) {

    /* -- Taille du nuage -- */
    n    = &(*pN)[aNuage];
    n->n = 0;

    lineStart = (aNuage) ? limits[aNuage-1]: 0;

    for (aLine=lineStart; aLine < limits[aNuage]; aLine++) {
      lineStat=getMol2LineStatus(lines[aLine]);
      if (lineStat==1) { /* @<TRIPOS>ATOM */
        molOpened = 1;
      }
      else if (lineStat==2 && molOpened){ /* atom data */
        n->n++;
      }
      else {
        molOpened = 0;
      }
    }

    /* -- Allocation memoire -- */
    n->crds     =  (DtPoint3 *) calloc(n->n, sizeof(DtPoint3));
    n->atmName  =  (DtStr4 *)   calloc(n->n, sizeof(DtStr4));
    n->atmNme   =  (DtStr4 *)   calloc(n->n, sizeof(DtStr4));
    n->atmNm2   =  (DtStr5 *)   calloc(n->n, sizeof(DtStr5));
    n->atmNum   =  (int *)      calloc(n->n, sizeof(int));
    n->resName  =  (DtStr3 *)   calloc(n->n, sizeof(DtStr3));
    n->resNum   =  (int *)      calloc(n->n, sizeof(int));
    n->chn      =  (char *)     calloc(n->n, sizeof(char));
    n->skip     =  (char *)     calloc(n->n, sizeof(char));
    n->re       =  (DtReRec **) calloc(n->n, sizeof(DtReRec *));
//    n->EC       =  (char *)     malloc(sizeof(char) * 1);
//    n->EC[0]    =  '\0';
    n->EC = EC; // une seule allocation memoire pour tous les nuages du fichier car pas d'EC pour mol2
//    n->id       =  (char *)     malloc(sizeof(char) * 1);
//    n->id[0]    =  '\0';
    n->id = id; // une seule allocation memoire pour tous les nuages du fichier car pas d'id PDB pour mol2
    n->nHtGrps  = 0;
    n->htGrps   = NULL;
/*     n->hetGrps  =  (char *)    malloc(sizeof(char) * 1); */
/*     n->hetGrps[0]=  '\0'; */
    n->header   =  (char *)     malloc(sizeof(char) * 1);
    n->header[0]=  '\0';
//    n->cmpnd    =  (char *)     malloc(sizeof(char) * 1);
//    n->cmpnd[0] =  '\0';
    n->cmpnd    = n->header; // pour mol2, cmpnd et header contiennent le nom de la molécule
    n->weight   = NULL;
    n->connectMatrix = initConnectMatrix(n->n);
    n->nbConnect = 0;

    /* -- Lecture donnees -- */
    n->n = 0;

    nHydrogen = 0;
    atomNbr = 0;
    atomNumRe = 1;
    next = 0;
    pAtReList = NULL;

    for (aLine=lineStart; aLine < limits[aNuage]; aLine++) {
      lineStat=getMol2LineStatus(lines[aLine]);

      if (lineStat == 10) {  /* @<TRIPOS>MOLECULE -> cmpnd */
        next = 10;
      }
      else if (next == 10) { /* lecture du nom de la molecule */
        // la ligne ne contient que le nom de la molecule
        n->header = realloc(n->header,strlen(lines[aLine])+1);
        strcpy(n->header, lines[aLine]);
        n->cmpnd = n->header;
        next = 11;
      }
      else if (next == 11) { /* lecture du nombre d'atomes */
        sscanf(lines[aLine], "%d", &atomNbr);
        next = 0;
      }
      else if (lineStat == 1) { /* @<TRIPOS>ATOM */
        next = 1;
      }
      else if (lineStat == 2 && next == 1) { /* lecture des atomes */
        getMol2AtomCoords(lines[aLine],&n->crds[n->n][0],&n->crds[n->n][1],&n->crds[n->n][2]);
        getMol2AtomName(lines[aLine], n->atmNm2[n->n]);
        atmMol2InternalName(n->atmName[n->n], n->atmNm2[n->n]);
        atmNameMol2ToPDB(n->atmNme[n->n], n->atmNm2[n->n], n->atmName[n->n]);

        getMol2AtomNum(lines[aLine],&n->atmNum[n->n]);

        strcpy(n->resName[n->n], "");
        n->resNum[n->n] = 1;

        getMol2ResChainIde(lines[aLine],&n->chn[n->n]);

        /* mask to ignore hydrogen atoms */
        if (gNoH && strcmp(n->atmNm2[n->n], "H") == 0) {
          n->skip[n->n] = 1;
          nHydrogen++;
        }

        n->n ++;

        if (n->n >= atomNbr) {
          next = 0;
        }
      }
      else if (lineStat == 20) { /* @<TRIPOS>COMMENT -> RE */
        next = 20;
      }
      else if (next == 20) {
        if (lineStat == 21) { /* REMARK ATOM */
          getMol2AtmNbRe(lines[aLine], &atomNumRe);
        }
        else if (lineStat == 22) { /* REMARK MATCH. */
          getMol2AtmRe(lines[aLine], atmStrRe);
          resStrRe[0] = 0;

          pRe = rePtrFromStr(*pReList, atmStrRe, resStrRe); /* Peut etre NULL */
          if (pRe == NULL) {
            pRe = (DtReRec *) calloc(1, sizeof(DtReRec));
            pRe->next = *pReList;
            *pReList  = pRe;
            pRe->reAtmStr  = calloc (strlen(atmStrRe)+1, sizeof(char));
            strcpy(pRe->reAtmStr, atmStrRe);
            pRe->reResStr  = calloc (strlen(resStrRe)+1, sizeof(char));
            strcpy(pRe->reResStr, resStrRe);
            pRe->pAtmRe = reginit(pRe->reAtmStr, pRe->pAtmRe);
            pRe->pResRe = reginit(pRe->reResStr, pRe->pResRe);
          }

          /* associer atmStrRe à atomNumRe */
          pAtRe = atRePtrFromAtNum(pAtReList, atomNumRe);
          if (pAtRe == NULL) {
            pAtRe = (DtAtReList *) calloc(1, sizeof(DtAtReList));
            pAtRe->atom = atomNumRe;
            pAtRe->next = pAtReList;
            pAtReList = pAtRe;
          }
          pAtRe->re = pRe;
        }
        else if (lineStat == 23) { /* REMARK WEIGHT. */
          if (!n->weight) {
            n->weight = (double *)calloc(n->n, sizeof(double));
            initWeight(n->weight, n->n);
          }
          if (atomNumRe > 0 && atomNumRe <= n->n) {
            n->weight[atomNumRe-1] = getMol2AtomWeight(lines[aLine]);
          }
        }
      }
      else if (lineStat == 30) { /* @<TRIPOS>BOND -> matrice des connexités */
        next = 30;
      }
      else if (next == 30) { /* liaison -> matrice des connexités */
        if (!addConnect(lines[aLine], n->connectMatrix, n->n, &(n->nbConnect))) {
          next = 0;
        }
      }
    }

    /* si on masque les hydrogènes, on ne tient pas compte de leur liaison non plus */
    if (gNoH) {
      n->nbConnect -= nHydrogen;
    }

    /* association des atomes avec leur regex */
    while (pAtReList) {
      if (pAtReList->atom > 0 && pAtReList->atom <= n->n) {
        n->re[pAtReList->atom - 1] = pAtReList->re;
      }
      pAtRe = pAtReList->next;
      free(pAtReList);
      pAtReList = pAtRe;
    }

    /* quels atomes n'ont pas eu leur regex ? */
    for (aAtom = 0; aAtom < n->n; aAtom++) {
      if (!n->re[aAtom]) {

        /* si des règles d'appariement ont été définies pour cet atome */
        if (pRuleList && (pRule = findRule(pRuleList, n->atmName[aAtom]))) {
          strcpy(atmStrRe, pRule->re);
        }
        else {
          /* par défaut, un atome n'est apparié qu'à un atome de même type */
          atmName2dftlReStr(n->atmName[aAtom], atmStrRe);
        }

        resStrRe[0] = 0;

        pRe = rePtrFromStr(*pReList, atmStrRe, resStrRe);
        if (pRe == NULL) {
          pRe = (DtReRec *) calloc(1, sizeof(DtReRec));
          pRe->next = *pReList;
          *pReList  = pRe;
          pRe->reAtmStr  = calloc (strlen(atmStrRe)+1, sizeof(char));
          strcpy(pRe->reAtmStr, atmStrRe);
          pRe->reResStr  = calloc (strlen(resStrRe)+1, sizeof(char));
          strcpy(pRe->reResStr, resStrRe);
          pRe->pAtmRe = reginit(pRe->reAtmStr, pRe->pAtmRe);
          pRe->pResRe = reginit(pRe->reResStr, pRe->pResRe);
        }

        n->re[aAtom] = pRe;
      }
    }
  }

  // To debug: print the reStr associated with each atom.
  //  for (aAtom = 0; aAtom < n->n; aAtom++) {
  //  fprintf(stderr,"%s %s: \"%s\" \"%s\"\n",n->atmNm2[aAtom],n->atmName[aAtom], n->re[aAtom]->reAtmStr ,n->re[aAtom]->reResStr);
  // }
  // exit(0);

  /* -- liberation memoire -- */
  txtFileFree(lines,nLines);
  if (limits)
    free(limits);


  if (verbose)
    fprintf(stderr,"Currently %d regexp set\n", rePtrListLen(*pReList));
}
