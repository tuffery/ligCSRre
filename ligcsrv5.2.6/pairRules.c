/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
#include <stdlib.h>
#include <string.h>

#include "CSRTypes.h"
#include "mol2.h"
#include "pairRules.h"
#include "TxtFile.h"

/* trouve la structure de définition de règles à partir du nom de l'atome */
DtRuleList *findRule(DtRuleList *pList, char *atmName) {
  DtRuleList *pR = pList;

  while (pR) {
    if (strcmp(atmName, pR->atmName) == 0)
      return pR;

    pR = pR->next;
  }

  return NULL;
}


void freeRuleList(DtRuleList *pList) {
  DtRuleList *pR;

  while (pList) {
    pR = pList->next;
    free(pList);
    pList = pR;
  }
}

/* lit un fichier contenant les règles d'appariement des atomes.
   syntaxe :
   C.1=C.2=C.3
   O.2=O.co2
*/
DtRuleList *lectPairRules(char *fname) {
  char **lines;
  DtRuleList *pRules = NULL;
  DtRuleList *pR;
  DtRuleList *lPR;
  int i;
  int nLines;

  lines = txtFileRead(fname, &nLines);

  if (!lines) {
    fprintf(stderr, "Unable to open `%s'.\n", fname);
    exit(0);
  }

  for (i = 0; i < nLines; i++) {
    if ((lines[i][0] != '\000') && (lines[i][0] == '#')) continue; /* We accept comments */
    /* There are 2 types of lines: =XXXX and :XXXX                    */
    /* =XXX define equivalence classes (XXXX direct is also accepted) */
    /* all the atoms will have the same regexp                        */
    /* :XXXX define a particular regexp for one atom                  */
    if ((lines[i][0] != '\000') && (lines[i][0] == ':')) {
      pRules = parseExactRuleLine(pRules, lines[i]);
    } else {
      pRules = parseRuleLine(pRules, lines[i]);
    }
  }

  txtFileFree(lines, nLines);

  /* "fermeture" des regex de la liste */
/*   pR = pRules; */
/*   while (pR) { */
/*     strcpy(pR->re + strlen(pR->re), ")$"); */
/*     pR = pR->next; */
/*   } */

  /* UNCOMMENT FOR DEBUG */
/*   lPR = pRules; */
/*   fprintf(stderr,"RULES:\n"); */
/*   while (lPR != NULL) { */
/*     fprintf(stderr,"%s: %s\n", lPR->atmName, lPR->re); */
/*     lPR = lPR->next; */
/*   } */
/*   // exit(0); */

  return pRules;
}


/* extrait l'information contenue dans une ligne                                     */
/* La ligne commence par :, puis contient un nom, puis:, puis une regexp directement */
/* eg: :da:^(do|ac|da)$                                                              */
DtRuleList *parseExactRuleLine(DtRuleList *pRList, char *line) {
  char *pStart = line-1;
  char *pStop = line-1;
  char stop;
  DtRuleList *pR = NULL;
  int i, j;
  int nAtom = 0;
  int maxAtom = 10;
  DtStr4 *atoms = (DtStr4 *)calloc(maxAtom, sizeof(DtStr4));

  char sepchar = ':';


  nAtom = 0;
  /* decoupage de la ligne */ 
  pStop = line;
  /* La ligne commence par ':' on passe */
  pStop++;
  pStart = pStop;

  while ((*pStop) != sepchar && (*pStop) != '\r' && (*pStop) != 0) {
    pStop++;
  }

  stop = (*pStop);
  (*pStop) = 0; /* remplacement du 'sepchar' par '\0' pour manipuler facilement */

  /* formatage et copie du nom d'atome */
  strcpy(atoms[nAtom], pStart);

  /* On extrait le regexp */
  pStop++;

  /* atome encore associé à aucun règle */
  if (pR == NULL) {
    pR = calloc(1, sizeof(DtRuleList));
    strcpy(pR->atmName, atoms[0]);
    strcpy(pR->re, pStop);

    /* ajout à la liste chaînée */
    pR->next = pRList;
    pRList = pR;
  }

  free(atoms);

  return pRList;
}

/* extrait l'information contenue dans une ligne */
DtRuleList *parseRuleLine(DtRuleList *pRList, char *line) {
  char *pStart = line-1;
  char *pStop = line-1;
  char stop;
  DtRuleList *pR;
  int i, j;
  int nAtom = 0;
  int maxAtom = 10;
  DtStr4 *atoms = (DtStr4 *)calloc(maxAtom, sizeof(DtStr4));

  char sepchar = '=';


  /* decoupage de la ligne */
  do {
    pStop++;
    pStart = pStop;

    while ((*pStop) != sepchar && (*pStop) != '\r' && (*pStop) != 0) {
      pStop++;
    }

    /* verification de l'espace pour stocker */
    if (nAtom >= maxAtom) {
      maxAtom *= 2;
      atoms = (DtStr4 *)realloc(atoms, maxAtom * sizeof(DtStr4));
    }

    stop = (*pStop);
    (*pStop) = 0; /* remplacement du 'sepchar' par '\0' pour manipuler facilement */

    /* formatage et copie du nom d'atome */
    if (pStart != pStop) {
      atmMol2InternalName(atoms[nAtom], pStart);
      nAtom++;
    }

  } while (stop != '\r' && stop != 0);

  /* traitement des atomes trouves */
  for (i = 0; i < nAtom; i++) {
    pR = findRule(pRList, atoms[i]);

    /* atome encore associé à aucun règle */
    if (pR == NULL) {
      pR = calloc(1, sizeof(DtRuleList));
      strcpy(pR->atmName, atoms[i]);
      strcpy(pR->re, "^(");
      strcpy(pR->re + 2, atoms[i]);

      /* ajout à la liste chaînée */
      pR->next = pRList;
      pRList = pR;
    }

    pStart = pR->re + strlen(pR->re);

    for (j = 0; j < nAtom; j++) {
      if (i != j) {
        (*pStart) = '|';
        strcpy(pStart+1, atoms[j]);
        pStart += strlen(atoms[j]) + 1;
      }
    }
    // fprintf(stderr,"CRUDE: %s\n",pR->re);
    pStart = pR->re + strlen(pR->re);
    strcpy(pStart, ")$");
    // fprintf(stderr,"COMPL: %s\n",pR->re);
  }

  free(atoms);

  return pRList;
}
