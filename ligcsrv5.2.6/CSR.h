/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
#ifndef __CSR_H__
#define __CSR_H__


#include <stdlib.h>
#include <stdio.h>

#include "CSRTypes.h"
#include "3DFuncs.h"

extern DtNuage  *allocNuage(int sze);
extern void atmInternalName(char *atmName);
extern void atmName2dftlReStr(char *atmName, char *atmStrRe);
extern void      barycentre( DtNuage *n);
extern void      freeNuage(DtNuage *pNg);
extern void      initWeight(double *w, int n);
extern void      lectPdbCsr (char *fname, int what, int strict, DtNuage **pN, int *onNuages, DtReRec **pReList, int verbose);
// extern DtReponse *csr(DtNuage *ng1, DtNuage *ng2, DtNuage *plNg2, double dseuil, int dimd, DtDistRec *dist, DtMatrix4x4 mat, DtPoint3 *crdsSup1, DtPoint3 *crdsSup2, double *weightSup1, double *weightSup2, int nbIter, int minAtm, double maxRMSd, double tol, DtReponse *rep, int verbose);
extern DtReponse *csr2(DtNuage *ng1, DtNuage *ng2, DtNuage *plNg2, double dseuil, int dimd, DtDistRec *dist, DtMatrix4x4 mat, DtPoint3 *crdsSup1, DtPoint3 *crdsSup2, double *weightSup1, double *weightSup2, int nbIter, int minAtm, double maxRMSd, double tol, DtReponse *rep, int verbose);
extern void setupMaskFromRe(DtReRec *pRe, DtNuage*p2);
extern void outPDBNuage(char *fname, DtNuage *pN);
extern void outPDBTemplate(FILE *fd, DtReponse *rep, DtNuage *pN);
extern void outPDBMatch(FILE *fd, DtReponse *rep, DtNuage *pNg, DtNuage *pNg1);
extern DtReRec *rePtrFromStr(DtReRec *pReList, char *atmStrRe, char *resStrRe);
extern int rePtrListLen(DtReRec *pReList);

#endif
