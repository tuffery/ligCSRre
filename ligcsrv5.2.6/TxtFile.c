/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/times.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>


/* ===========================================================
 * Renvoie les lignes d'un fichier et leur nombre de lignes.
 * ===========================================================
 */

char ** txtFileRead(char *fname, int *lSze)
{
  struct stat buf;
  char *tmp;
  char **line;
  int fd;
  int fSze, done;
  int nLines;
  int i,j;

  if ((fd = open(fname,O_RDONLY)) < 0) {
    return NULL;
  }

  if (fstat(fd,&buf)) {
    close(fd);
    return NULL;
  }

  if (!S_ISREG (buf.st_mode)) {
    close(fd);
    return NULL;
  }

  if (!(fSze = buf.st_size)) {
    close(fd);
    return NULL;
  }

  /* La taille du buffer ne dépasse-t-elle pas les capacités de fSze ? */
  if (fSze != buf.st_size) {
    fprintf(stderr, "%s: File too heavy to be loaded.\n", fname);
    close(fd);
    return NULL;
  }

  if (!(tmp = malloc((fSze+1) * sizeof(char)))) {
    close(fd);
    return NULL;
  }

  done = 0;
  while (done < fSze) {
    int r = read(fd, &(tmp[done]), fSze - done);
    if (r < 0) {
      free(tmp);
      close(fd);
      return NULL;
    }
    done += r;
  }
  close(fd);

  nLines = 0;
  for (i = 0; i < fSze; i++) {
    if (tmp[i] == '\n') nLines++;
  }
  if (tmp[fSze-1] != '\n') nLines++;

  if (!(line = malloc((nLines+2) * sizeof(char *)))) {
      free(tmp);
      return NULL;
  }

  line[0] = tmp;
  j = 1;
  for (i = 0; i < fSze; i++) {
    if (tmp[i] == '\n') {
      line[j++] = &tmp[i+1];
      tmp[i] = '\000';
    }
  }

  line[j] = NULL;
  *lSze = nLines;

  return (line);
}

/* ===============================================
 * Réalloue la mémoire occupée par txtFileRead.
 * ===============================================
 */

char ** txtFileFree(char **l, int lSze)
{
  /* Attention: on ne libere que la premiere ligne,
     car c'est un seul malloc pour l'ensemble des lignes
  */
  free(l[0]);
  free(l);
  return (NULL);
}
