/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
#include <stdio.h>

#include "argstr.h" 
#include "CSR.h"
#include "CSRTypes.h"
#include "pairRules.h"
#include "PDB.h"
#include "regex.h"
#include "TxtFile.h"
#include "mol2.h"

/* ajoute une liaison dans la matrice des connexités */
int addSDFConnect(char *line, char **matrix, int n, int *nbConnect) {
  int at1, at2, tpe;

  /* lecture correcte de la ligne de description de liaison */
  if (sscanf(line, "%d %d %d", &at1, &at2, &tpe) == 3) {
    /* assert */
    if (at1 > n || at2 > n || at1 < 0 || at2 < 0) {
      return 0;
    }

    matrix[at1-1][at2-1] = matrix[at2-1][at1-1] = 1;
    (*nbConnect) += 1;
    return 1;
  }

  return 0;
}


/* transforme l'écriture SDF du nom d'atome (atmNm2) pour l'utilisation
   de regex */
void atmSDFInternalName(char *atmName, char *atmNm2) {

  /* no use for SDF */
#if 0
  /* cas particuliers, conservés tel quel */
  if (strcmp(atmNm2, "LP") == 0 || strcmp(atmNm2, "Du") == 0
      || strcmp(atmNm2, "Any") == 0 || strcmp(atmNm2, "Hal") == 0
      || strcmp(atmNm2, "Het") == 0 || strcmp(atmNm2, "Hev") == 0) {
    strcpy(atmName, atmNm2);
    return;
  }
  if (strcmp(atmNm2, "Du.C") == 0) {
    strcpy(atmName, "DuC");
    return;
  }
#endif

  /* par défaut : premier caractère seul */
  atmName[0] = atmNm2[0];
  atmName[1] = 0;
  
  /* no use for SDF */
#if 0
  /* Modification P. Tuffery 10/2008 for donnor, acceptor classes */
  if (atmNm2[2] == 'd' && atmNm2[3] == 'o') {  // X.do -> do
    atmName[0] = atmNm2[2];
    atmName[1] = atmNm2[3];
    atmName[2] = 0;
  }
  else if (atmNm2[2] == 'a' && atmNm2[3] == 'c') {  // X.ac -> ac
    atmName[0] = atmNm2[2];
    atmName[1] = atmNm2[3];
    atmName[2] = 0;
  }
  else if (atmNm2[2] == 'd' && atmNm2[3] == 'a') {  // X.da -> da
    atmName[0] = atmNm2[2];
    atmName[1] = atmNm2[3];
    atmName[2] = 0;
  }
  else   /* END MODIF */
    if (atmNm2[2] == 'p' && atmNm2[3] == 'h') {  // X.ph -> ph
    atmName[0] = atmNm2[2];
    atmName[1] = atmNm2[3];
    atmName[2] = 0;
  }
  else if (atmNm2[0] == 'N' && atmNm2[3] == 'm') { // N.am
    atmName[1] = atmNm2[3];
    atmName[2] = 0;
  }
  else if (atmNm2[0] == 'S' && atmNm2[3] == '2') { // S.O2
    atmName[1] = atmNm2[2];
    atmName[2] = atmNm2[3];
    atmName[3] = 0;
  }
  else if (atmNm2[1] == '.') { // x.x
    atmName[1] = atmNm2[2];
    atmName[2] = 0;
  }
  else if (atmNm2[2] == '.') { // xx.x
    atmName[1] = atmNm2[1];
    atmName[2] = atmNm2[3];
    atmName[3] = 0;
  }
  else if (atmNm2[0] == 'H') {} // H
  else if (atmNm2[1] == 0 || atmNm2[2] == 0) {
     atmName[0] = ':';
     atmName[1] = atmNm2[0];
     atmName[2] = atmNm2[1];
     atmName[3] = 0;
  }
#endif
}

/* écrit le nom de l'atome au format PDB pour une sortie PDB */
void atmNameSDFToPDB(char *atmNme, char *atmNm2, char *atmName) {
  int aPos = 0;
  char *pC = atmNm2;

  if (atmName[0] != ':') {
    atmNme[0] = ' ';
    aPos++;
  }

  while (*pC != '.' && *pC != 0 && aPos < 4) {
    atmNme[aPos] = *pC;
    aPos++;
    pC++;
  }

  while (aPos < 4) {
    atmNme[aPos++] = ' ';
  }

  atmNme[aPos] = 0;
}

/* récupère les coordonnées d'un atome dans une ligne de la section ATOM */
void getSDFAtomCoords(char *line,DtFloat *x,DtFloat *y,DtFloat *z)
{
  sscanf(line,"%lf %lf %lf", x, y, z);
}

/* récupère le nom de l'atome dans une ligne de la section ATOM */
void getSDFAtomName(char *line,char *anom)
{
  float f;

  sscanf(line,"%f %f %f %s",&f,&f,&f,anom);
}

/* récupère le poids donné dans la section COMMENT */
double getSDFAtomWeight(char *line)
{
  double w;

  if(!sscanf(line," REMARK WEIGHT. %lf", &w))
    return 1.0;

  return w;
}

/* récupère le numéro d'atome qui sera associé à une regex et/ou un poids */
void getSDFAtmNbRe(char *line, int *anum) {
  sscanf(line," REMARK ATOM %d", anum);
}

/* récupère la regex définie dans la section COMMENT */
void getSDFAtmRe(char *line, char *atmStrRe) {
  int len;

  sscanf(line," REMARK MATCH. %s", atmStrRe);

  len = strlen(atmStrRe);

  /* ajoute '^' au début s'il n'y est déjà */
  if (len > 0 && atmStrRe[0] != '^') {
    memmove(atmStrRe+1, atmStrRe, (len+1)*sizeof(char));
    atmStrRe[0] = '^';
    len++;
  }

  /* ajoute '$' à la fin s'il n'y est déjà */
  if (atmStrRe[len-1] != '$') {
    atmStrRe[len] = '$';
    atmStrRe[len+1] = 0;
  }
}

void getSDFResChainIde(char *line,char *chn)
{
  int i, id = 0;
  char c[30];
  float f;

  //  sscanf(line, "%d %s %f %f %f %s %d", &i, &c, &f, &f, &f, &c, &id);
  id = 0;
  *chn = '0' + (id % 10);
}

void outSDFMatch(FILE *fd, DtReponse *rep, DtNuage *pNg, DtNuage *pNg1, char *msg)
{
  int aLine, i2;

  if (pNg->lines == NULL) {
    fprintf(stderr,"Problem with lines\n");
    exit(0);
  }
  for (aLine = 0; aLine < 4; aLine++) {
    if ((msg != NULL) && (aLine == 1)) 
      fprintf(fd,"%s%s\n", msg, pNg->lines[aLine]);
    else
      fprintf(fd,"%s\n", pNg->lines[aLine]);
  }

  for (i2 = 0; i2 < pNg->n; i2++) {
    fprintf(fd,"%10.4lf%10.4lf%10.4lf%s\n",pNg->crds[i2][0], pNg->crds[i2][1], pNg->crds[i2][2], pNg->lines[4+i2]+30);
  }
  for (aLine = 4+pNg->n;
 aLine < pNg->nLines; aLine++) {
    fprintf(fd,"%s\n", pNg->lines[aLine]);
  }


}


/* ============================================================================
 * Remplit les tableaux de données utiles à Csr après lecture des fichiers SDF.
 * Alloue la mémoire pour la structure DtNuage.
 * ============================================================================
 */
void lectSDFCsr(char *fname, int what, int strict, DtNuage **pN, int *onNuages,
                 DtReRec **pReList, int verbose, DtRuleList *pRuleList)
{
  DtNuage *n;
  char   **lines;
  int     *limits;

  DtReRec *pRe;
  DtAtReList *pAtRe, *pAtReList;
  DtRuleList *pRule = NULL;

  int  nLines;
  int  aLine;
  int  lineStart;
  int  lineStat;
  int  nMol;
  int  molOpened;
//  int  nNuages;
  int  aNuage;
  int  next;
  int  atomNbr;
  int  connectNbr;
  int  atomNumRe;
  int  aAtom;
  char atmStrRe[BUFSIZ];
  char resStrRe[1];      // char resStrRe[BUFSIZ];
//  char buff[BUFSIZ];
//  char *cmpnd;
//  char EC[BUFSIZ];
//  char HETGRP[BUFSIZ];

  int nHydrogen;

  /* pas d'ID PDB */
  char *id = (char *)malloc(sizeof(char) * 1);
  id[0] = '\0';
  /* pas d'EC non plus */
  char *EC = (char *)malloc(sizeof(char) * 1);
  EC[0] = '\0';


  lines=txtFileRead(fname,&nLines);

  if (!lines) {
    exit(0);
  }
  if (verbose)
    fprintf(stderr,"File %s: Read %d lines\n", fname, nLines);


  /* -- Nombre de molecules -- */
  nMol    = 0;
  limits  = NULL;

  for (aLine=0; aLine < nLines; aLine++) {
    if (!strncmp(lines[aLine],"$$$$",4)) {
      limits = realloc(limits, sizeof(int)*(nMol+1));
      limits[nMol] = aLine + 1;
      nMol++;
    }
  }

  if (verbose)
    fprintf(stderr,"SDF File %s: Found %d Mols\n", fname, nMol);

  /* -- Allocate nuages -- */
  *onNuages = nMol;
  *pN  = calloc(nMol, sizeof(DtNuage));


  /* -- Input des nuages -- */
  molOpened = 0;
  for (aNuage = 0; aNuage < nMol; aNuage++) {

    /* -- Taille du nuage -- */
    n    = &(*pN)[aNuage];
    n->n = 0;

    lineStart = (aNuage) ? limits[aNuage-1]: 0;  /* limits contains the last line of previous compound + 1 */

    /* SDF format has the information of the number of atoms and bonds */
    sscanf(lines[lineStart+3],"%d%d",&n->n, &n->nbConnect);

    /* -- Allocation memoire -- */
    n->crds     =  (DtPoint3 *) calloc(n->n, sizeof(DtPoint3));
    n->atmName  =  (DtStr4 *)   calloc(n->n, sizeof(DtStr4));
    n->atmNme   =  (DtStr4 *)   calloc(n->n, sizeof(DtStr4));
    n->atmNm2   =  (DtStr5 *)   calloc(n->n, sizeof(DtStr5));
    n->atmNum   =  (int *)      calloc(n->n, sizeof(int));
    n->resName  =  (DtStr3 *)   calloc(n->n, sizeof(DtStr3));
    n->resNum   =  (int *)      calloc(n->n, sizeof(int));
    n->chn      =  (char *)     calloc(n->n, sizeof(char));
    n->skip     =  (char *)     calloc(n->n, sizeof(char));
    n->re       =  (DtReRec **) calloc(n->n, sizeof(DtReRec *));
    n->lines    =  (char **)    calloc(limits[aNuage]-lineStart , sizeof(char *));
    n->nLines   = 0;
    for (aLine=lineStart; aLine < limits[aNuage]; aLine++) {
      n->lines[aLine - lineStart] = lines[aLine];
      n->nLines++;
    }
    // fprintf(stdout,"Nuage %d nLines %d from %d to %d next %d\n", aNuage, n->nLines, lineStart, limits[aNuage], limits[aNuage]);
    // exit(0);
//    n->EC       =  (char *)     malloc(sizeof(char) * 1);
//    n->EC[0]    =  '\0';
    n->EC = EC; // une seule allocation memoire pour tous les nuages du fichier car pas d'EC pour mol2
//    n->id       =  (char *)     malloc(sizeof(char) * 1);
//    n->id[0]    =  '\0';
    n->id = id; // une seule allocation memoire pour tous les nuages du fichier car pas d'id PDB pour mol2
    n->nHtGrps  = 0;
    n->htGrps   = NULL;
/*     n->hetGrps  =  (char *)    malloc(sizeof(char) * 1); */
/*     n->hetGrps[0]=  '\0'; */
    n->header   =  (char *)     malloc(sizeof(char) * 1);
    n->header[0]=  '\0';
//    n->cmpnd    =  (char *)     malloc(sizeof(char) * 1);
//    n->cmpnd[0] =  '\0';
    n->cmpnd    = n->header; // pour mol2, cmpnd et header contiennent le nom de la molécule
    n->weight   = NULL;
    n->connectMatrix = initConnectMatrix(n->n);
    // n->nbConnect = 0; /* Already set for SDF */

    /* -- Lecture donnees -- */

    nHydrogen = 0;
    atomNbr = n->n;
    atomNumRe = 1;
    next = 0;
    pAtReList = NULL;

    /* Compound name */
    n->header = realloc(n->header,strlen(lines[lineStart])+1);
    strcpy(n->header, lines[lineStart]);
    n->cmpnd = n->header;

    /* Atomes */
    n->n = 0;
    for (aLine=lineStart+4; aLine < lineStart+4+atomNbr; aLine++) {

      getSDFAtomCoords(lines[aLine],&n->crds[n->n][0],&n->crds[n->n][1],&n->crds[n->n][2]); /* OK                                    */
                                                                                            /* ATTENTION A atmName, atmNme, atmNm2   */
                                                                                            /* atmNme is the TRUE PDB atom name      */
                                                                                            /* atmNm2 is just for mol2 and SDF files */
                                                                                            /* atmName is used for search            */
      getSDFAtomName(lines[aLine], n->atmNm2[n->n]);                                       /* OK. TYPE ATOME. 1 CHAR.               */
      atmSDFInternalName(n->atmName[n->n], n->atmNm2[n->n]);                                /* OK. JUST PROPAGATE ATOM NAME          */
      atmNameSDFToPDB(n->atmNme[n->n], n->atmNm2[n->n], n->atmName[n->n]);                  /* A FAIRE    */
      
      n->atmNum[n->n] = n->n + 1;                                                           /* Atom numbers from 1 */
      
      strcpy(n->resName[n->n], "");
      n->resNum[n->n] = 1;
      
      getSDFResChainIde(lines[aLine],&n->chn[n->n]);                                        /* UTILE ??  LEFT TO 0 */
      
      //      fprintf(stdout,"atom %d \"%s\" \"%s\" \"%s\" %lf %lf %lf\n", n->n, n->atmNme[n->n], n->atmNm2[n->n], n->atmName[n->n], n->crds[n->n][0], n->crds[n->n][1], n->crds[n->n][2]);
      /* mask to ignore hydrogen atoms */
      if (gNoH && strcmp(n->atmNm2[n->n], "H") == 0) {
	n->skip[n->n] = 1;
	nHydrogen++;
      }
      
      n->n ++;
    }

    /* WE DO NOT PRESENTLY ACCEPT REMARK ATOM AND REMARK RES LINES FOR SDF */
#if 0
      else if (next == 20) {
        if (lineStat == 21) { /* REMARK ATOM */
          getMol2AtmNbRe(lines[aLine], &atomNumRe);
        }
        else if (lineStat == 22) { /* REMARK MATCH. */
          getMol2AtmRe(lines[aLine], atmStrRe);
          resStrRe[0] = 0;

          pRe = rePtrFromStr(*pReList, atmStrRe, resStrRe); /* Peut etre NULL */
          if (pRe == NULL) {
            pRe = (DtReRec *) calloc(1, sizeof(DtReRec));
            pRe->next = *pReList;
            *pReList  = pRe;
            pRe->reAtmStr  = calloc (strlen(atmStrRe)+1, sizeof(char));
            strcpy(pRe->reAtmStr, atmStrRe);
            pRe->reResStr  = calloc (strlen(resStrRe)+1, sizeof(char));
            strcpy(pRe->reResStr, resStrRe);
            pRe->pAtmRe = reginit(pRe->reAtmStr, pRe->pAtmRe);
            pRe->pResRe = reginit(pRe->reResStr, pRe->pResRe);
          }

          /* associer atmStrRe à atomNumRe */
          pAtRe = atRePtrFromAtNum(pAtReList, atomNumRe);
          if (pAtRe == NULL) {
            pAtRe = (DtAtReList *) calloc(1, sizeof(DtAtReList));
            pAtRe->atom = atomNumRe;
            pAtRe->next = pAtReList;
            pAtReList = pAtRe;
          }
          pAtRe->re = pRe;
        }
        else if (lineStat == 23) { /* REMARK WEIGHT. */
          if (!n->weight) {
            n->weight = (double *)calloc(n->n, sizeof(double));
            initWeight(n->weight, n->n);
          }
          if (atomNumRe > 0 && atomNumRe <= n->n) {
            n->weight[atomNumRe-1] = getMol2AtomWeight(lines[aLine]);
          }
        }
      }
      else if (lineStat == 30) { /* @<TRIPOS>BOND -> matrice des connexités */
        next = 30;
      }
#endif

    /* Les connexités */
    connectNbr = n->nbConnect;
    n->nbConnect = 0;
    for (aLine=lineStart+4+atomNbr; aLine < lineStart+4+atomNbr+connectNbr; aLine++) {
      if (addSDFConnect(lines[aLine], n->connectMatrix, n->n, &(n->nbConnect)));
    }

    /* si on masque les hydrogènes, on ne tient pas compte de leur liaison non plus */
    if (gNoH) {
      n->nbConnect -= nHydrogen;
    }

    /* association des atomes avec leur regex */
    while (pAtReList) {
      if (pAtReList->atom > 0 && pAtReList->atom <= n->n) {
        n->re[pAtReList->atom - 1] = pAtReList->re;
      }
      pAtRe = pAtReList->next;
      free(pAtReList);
      pAtReList = pAtRe;
    }

    /* quels atomes n'ont pas eu leur regex ? */
    for (aAtom = 0; aAtom < n->n; aAtom++) {
      if (!n->re[aAtom]) {

        /* si des règles d'appariement ont été définies pour cet atome */
        if (pRuleList && (pRule = findRule(pRuleList, n->atmName[aAtom]))) {
          strcpy(atmStrRe, pRule->re);
        }
        else {
          /* par défaut, un atome n'est apparié qu'à un atome de même type */
          atmName2dftlReStr(n->atmName[aAtom], atmStrRe);
        }
	// fprintf(stdout,"atm %d %s re %s\n", aAtom, n->atmName[aAtom], atmStrRe);

        resStrRe[0] = 0;

        pRe = rePtrFromStr(*pReList, atmStrRe, resStrRe);
        if (pRe == NULL) {
          pRe = (DtReRec *) calloc(1, sizeof(DtReRec));
          pRe->next = *pReList;
          *pReList  = pRe;
          pRe->reAtmStr  = calloc (strlen(atmStrRe)+1, sizeof(char));
          strcpy(pRe->reAtmStr, atmStrRe);
          pRe->reResStr  = calloc (strlen(resStrRe)+1, sizeof(char));
          strcpy(pRe->reResStr, resStrRe);
          pRe->pAtmRe = reginit(pRe->reAtmStr, pRe->pAtmRe);
          pRe->pResRe = reginit(pRe->reResStr, pRe->pResRe);
        }

        n->re[aAtom] = pRe;
      }
    }
  }

  // To debug: print the reStr associated with each atom.
  //  for (aAtom = 0; aAtom < n->n; aAtom++) {
  //  fprintf(stderr,"%s %s: \"%s\" \"%s\"\n",n->atmNm2[aAtom],n->atmName[aAtom], n->re[aAtom]->reAtmStr ,n->re[aAtom]->reResStr);
  // }
  // exit(0);

  /* -- liberation memoire -- */
  /* We do not free lines since we store them for ouptut */
  // txtFileFree(lines,nLines);
  if (limits)
    free(limits);


  if (verbose)
    fprintf(stderr,"Currently %d regexp set\n", rePtrListLen(*pReList));
}
