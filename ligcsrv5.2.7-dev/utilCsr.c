/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/times.h>
#include "utilCsr.h"
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>



/* ===================================================
 * Renvoie une chaine de caractères en majuscules.
 * ===================================================
 */

void strupper(char *s)
{
  while (*s != '\0') {
    *s = toupper(*s);
    s++;
  }
}





/* ====================================================
 * Numero d'atome pour le residu
 * ====================================================
 */
void getAtomNum(char *line,int *anum)
{
  char buff[30], charAnum[6];

  charAnum[5] = '\0';
  if (sscanf(line,"%6c%5c",buff,charAnum) != EOF);
  *anum = atoi(charAnum);
}





/* ====================================================
 * Nom de l'atome
 * 3 char lus + 1 blanc pour atom index
 * ====================================================
 */
void getAtomName(char *line,char *anom)
{
    char buff[30];

    anom[3] = ' ';
    anom[4] = '\0';
    if (sscanf(line,"%13c%3c",buff,anom) != EOF);
    strupper(anom);
}





/* ====================================================
 * Index d'atome pour le residu (cf params.c)
 * ====================================================
 */
void getResName(char *line,char *rnom)
{
    char buff[30];

    rnom[3] = '\0';
    if (sscanf(line,"%17c%3c",buff,rnom) != EOF);
    strupper(rnom);
}





/* ====================================================
 * Coordonnees de l'atome
 * ====================================================
 */
void getAtomCoords(char *line,DtFloat *x,DtFloat *y,DtFloat *z)
{
  char buff[30], charX[9],charY[9],charZ[9];

  charX[8] = charY[8] = charZ[8] = '\0';
  if (sscanf(line,"%30c%8c%8c%8c",buff,charX,charY,charZ) != EOF);
  *x = (DtFloat) atof(charX);
  *y = (DtFloat) atof(charY);
  *z = (DtFloat) atof(charZ);
}





/* ====================================================
 * Type d'info contenu dans la ligne du fichier PDB
 * 0: info non codee ou Pb.
 * 1: ATOM
 * 2: HELIX
 * 3: SHEET
 * 4: TURN
 * 5: TER
 * 6: CONECT
 * 7: END
 * 8: MODEL
 * 9: ENDMDL
 * ====================================================
 */
int getPDBLineStatus(char *d)
{
  char Entete[7];

  if (sscanf(d,"%s",&Entete) == EOF) return 0;
  if (strcmp(Entete,"ATOM") == 0) return 1; //intéressant
  if (strcmp(Entete,"HETATM") == 0) return 8; //intéressant
  if (strcmp(Entete,"HELIX") == 0) return 2;
  if (strcmp(Entete,"SHEET") == 0) return 3;
  if (strcmp(Entete,"TURN") == 0) return 4;
  if (strcmp(Entete,"TER") == 0) return 5;
  if (strcmp(Entete,"CONECT") == 0) return 6;
  if (strcmp(Entete,"END") == 0) return 7;
  if (strcmp(Entete,"MODEL") == 0) return 8;
  if (strcmp(Entete,"ENDMDL") == 0) return 9;
  return 0;
}






/* ===========================================================
 * Renvoie les lignes d'un fichier et leur nombre de lignes.
 * ===========================================================
 */

char ** txtFileRead(char *fname, int *lSze)
{
  struct stat buf;
  char *tmp;
  char **line;
  int fd;
  int fSze, done;
  int nLines;
  int i,j;

  if ((fd = open(fname,O_RDONLY)) < 0) {
    return NULL;
  }

  if (fstat(fd,&buf)) {
    close(fd);
    return NULL;
  }

  if (!S_ISREG (buf.st_mode)) {
    close(fd);
    return NULL;
  }

  if (!(fSze = buf.st_size)) {
    close(fd);
    return NULL;
  }

  if (!(tmp = malloc((fSze+1) * sizeof(char)))) {
    close(fd);
    return NULL;
  }

  done = 0;
  while (done < fSze) {
    int r = read(fd, &(tmp[done]), fSze - done);
    if (r < 0) {
      free(tmp);
      close(fd);
      return NULL;
    }
    done += r;
  }
  close(fd);

  nLines = 0;
  for (i = 0; i < fSze; i++) {
    if (tmp[i] == '\n') nLines++;
  }
  if (tmp[fSze-1] != '\n') nLines++;

  if (!(line = malloc((nLines+2) * sizeof(char *)))) {
      free(tmp);
      return NULL;
  }

  line[0] = tmp;
  j = 1;
  for (i = 0; i < fSze; i++) {
    if (tmp[i] == '\n') {
      line[j++] = &tmp[i+1];
      tmp[i] = '\000';
    }
  }
 
  line[j] = NULL;
  *lSze = nLines;

  return (line);
}




/* ===============================================
 * Réalloue la mémoire occupée par txtFileRead.
 * ===============================================
 */

char ** txtFileFree(char **l, int lSze)
{
  /* Attention: on ne libere que la premiere ligne,
     car c'est un seul malloc pour l'ensemble des lignes
  */
  free(l[0]);
  free(l);
  return (NULL);
}





/* ============================================================================
 * Remplie les tableaux de données utiles à Csr après lecture des fichiers Pdb.
 * Alloue la mémoire pour la structure DtNuages.
 * (n1ou2) est un paramètre pour savoir quel jeux de coordonnées l'utilisateur 
désire rentrer, celui de la protéine 1 ou celui de la protéine 2.
 * ============================================================================
 */


void lectPdbCsr (char *fname, char **lines, DtNuagesAt ng, int n1ouN2)
{
  int i,j,lineStat,nLines;
  double *x,*y,*z;
  int *numAt;
  char *typeAt,*typeRes;

  x=(double *) calloc(1,sizeof(double *));
  y=(double *) calloc(1,sizeof(double *));
  z=(double *) calloc(1,sizeof(double *));
  numAt=(int *) calloc(1,sizeof(int *));
  typeAt=(char *) calloc(10,sizeof(char)*10);
  typeRes=(char *) calloc(10,sizeof(char)*10);

  j=0;


  
  lines=txtFileRead(fname,&nLines);


  /* -- remplissage des données du nuage d'atome 1 -- */

  if(n1ouN2==1){
    for(i=0 ; i<nLines ; i++){
      lineStat=getPDBLineStatus(lines[i]);
      if(lineStat==1){	
	j++;
      }
    }

    printf("I = %d\t",nLines);
    printf("J = %d\n",j);

    ng->points->n1=j;
    ng->points->crds1 = (DtPoint3 *) calloc(ng->points->n1,sizeof(DtPoint3)*(ng->points->n1));

    ng->numAt1 = (int *) calloc(ng->points->n1,sizeof(int)*(ng->points->n1));
    ng->typeAt1 = (char **) calloc(ng->points->n1,sizeof(char *)*(ng->points->n1));
    ng->typeRes1 = (char **) calloc(ng->points->n1,sizeof(char *)*(ng->points->n1));

    j=0;

    printf("N1 = %d\n",ng->points->n1);



    for(i=0 ; i<nLines ; i++){
      lineStat=getPDBLineStatus(lines[i]);
      if(lineStat==1){	
	getAtomCoords(lines[i],x,y,z);
	ng->points->crds1[j][0]=(*x);
	ng->points->crds1[j][1]=(*y);
	ng->points->crds1[j][2]=(*z);
	printf("%.2lf\t%.2lf\t%.2lf\t",	
	       ng->points->crds1[j][0],ng->points->crds1[j][1],ng->points->crds1[j][2]);
	getAtomNum(lines[i],numAt);
	ng->numAt1[j]=(*numAt);
	getAtomName(lines[i],typeAt);
	ng->typeAt1[j]=typeAt;
	printf("%s\t",ng->typeAt1[j]);
	getResName(lines[i],typeRes);
	ng->typeRes1[j]=typeRes;
	printf("%s\n",ng->typeRes1[j]);
	j++;
      }
    }
  }
  

  /* -- remplissage des données du nuage d'atome 2 -- */

  if(n1ouN2==2){ 
    for(i=0 ; i<nLines ; i++){
      lineStat=getPDBLineStatus(lines[i]);
      if(lineStat==1){	
	j++;
      }
    }
    printf("I = %d\t",nLines);
    printf("J = %d\n",j);

    ng->points->n2 = j;
    printf("*N2 = %d\n",ng->points->n2);

    /*
    ng->points->crds2 = (DtPoint3 *) calloc(ng->points->n2,sizeof(DtPoint3)*(ng->points->n2));

    ng->numAt2 = (int *) calloc(ng->points->n2,sizeof(int)*(ng->points->n2));
    ng->typeAt2 = (char **) calloc(ng->points->n2,sizeof(char *)*(ng->points->n2));
    ng->typeRes2 = (char **) calloc(ng->points->n2,sizeof(char *)*(ng->points->n2));
    */

    ng->points->crds2 = (DtPoint3 *) calloc(j,sizeof(DtPoint3)*j);

    ng->numAt2 = (int *) calloc(j,sizeof(int)*j);
    ng->typeAt2 = (char **) calloc(j,sizeof(char *)*j);
    ng->typeRes2 = (char **) calloc(j,sizeof(char *)*j);
    
    printf("N2 = %d\tJ= %d\n",ng->points->n2,j);

    ng->points->n2 = j;

   

    j=0;

    for(i=0 ; i<nLines ; i++){
      lineStat=getPDBLineStatus(lines[i]);
      if(lineStat==1){
	printf("%d\n",i);
	/*
	getAtomCoords(lines[i],x,y,z);
	printf("%.2lf\t%.2lf\t%.2lf\t",*x,*y,*z);
	ng->points->crds2[j][0]=(*x);
	ng->points->crds2[j][1]=(*y);
	ng->points->crds2[j][2]=(*z);
	*/
	getAtomCoords(lines[i],&(ng->points->crds2[j][0]),&(ng->points->crds2[j][1]),&(ng->points->crds2[j][2]));
	printf("%.2lf\t%.2lf\t%.2lf\t",	
	       ng->points->crds2[j][0],ng->points->crds2[j][1],ng->points->crds2[j][2]);
	
	getAtomNum(lines[i],numAt);
	ng->numAt2[j]=(*numAt);
	printf("%d\t",ng->numAt2[j]); 
	getAtomName(lines[i],typeAt);
	ng->typeAt2[j]=typeAt;
	printf("%s\t",ng->typeAt2[j]);
	getResName(lines[i],typeRes);
	ng->typeRes2[j]=typeRes;
	printf("%s\n",ng->typeRes2[j]);
	j++;
      }
    }
  }

  if(n1ouN2 != 1 && n1ouN2 != 2){
    printf("/!\\ === ERREUR DE REMPLISSAGE DES DONNEES DE LA PDB === /!\\");
  }


  txtFileFree(lines,nLines);
}


/* =====================================================================
 * Initialize pseudo-random generator.
 * Returns the seed value.
 * =====================================================================
 */

void initRnd(long int *seed)
{
  static int isInit = 0;

  if (isInit) return;
  if ((int) *seed == -1) {
    *seed = (long int) time(0);
    srand48(*seed);
  }
  else srand48(*seed);
  isInit = 1;
}




/* ================================================================
 * Returns a random integer from 0 to n (excluded).
 * ================================================================
 */

int irand(int n)
{
  extern double drand48();
  double rn;
  float rnf;

  do {
    rn   = drand48();           /* suppose drand48 does not accept 1.0  */
    rnf  = (float) rn * n;
  } while ((int) rnf >= n);

  return (int) rnf;
}




/* ==========================================
 * Calcul la distance entre deux points.
 * ==========================================
 */

double distance(DtPoint3 R,DtPoint3 K)
{
  return (double) sqrt((double) ((K[0] - R[0]) * (K[0] - R[0]) +
                                  (K[1] - R[1]) * (K[1] - R[1]) +
                                  (K[2] - R[2]) * (K[2] - R[2])));
}





/* ==============================================================
 * Fonction de comparaison des longueurs dans un DtpDistRec.
 * (Appel avec quicksort).
 * ==============================================================
 */

int cmpdist(double *dist, double *dist2)
{
  if (dist[0] > dist2[0]) return 1;
  if (dist[0] < dist2[0]) return (-1);
  return 0;
}





/* ============================================================
 * Imprime les (nbPrint) premières lignes d'un DtpDistRec
 * du nuage d'atomes (ng).
 * ============================================================
 */

void printDtpDistRec (DtDistRec *dist, int nbPrint, DtNuagesPts ng)
{
  int i,i1,i2;
  for(i=0; i<nbPrint ; i++){
    i1=(dist[i].liaison)%(ng->n1);
    i2=(dist[i].liaison)/(ng->n1);
    printf("distance %d\t longueur=%.8lf\t liaison=(%d,%d)\n",i,dist[i].longueur,i1,i2);
  }
}





/* =======================================================
 * Imprime les (nbCrds) premières coordonnées
 * d'un tableau de coordonnées.
 * =======================================================
 */


void printNgCrds(DtPoint3 *crds, int nbCrds)
{
  int i,j;

  for(i=0; i<nbCrds ; i++){
    printf("*Atome %d :\t",i);
    for (j=0 ; j< 3 ; j++){
      printf("%.8lf\t",crds[i][j]);
    }
    printf("\n");
  }
  printf("*\n");
}




/* ===================================================================
 * Imprime les coordonnées du point i dans un tableau de coordonnées.
 * ===================================================================
 */

void printPoint3Crds(DtPoint3 *crds, int i)
{
  printf("*CRDS ATOME %d\tX=%.8lf\tY=%.8lf\tZ=%.8lf\n",
	 i,crds[i][0],crds[i][1],crds[i][2]);
}







/* ======================================================
 * Multiplication de deux matrices 4x4 (A x B dans C). 
 * ======================================================
 */

void mulMat4x4(DtMatrix4x4 A,DtMatrix4x4 B,DtMatrix4x4 C)
{
  int i,j,k;
  boucle(i,0,4) {
    boucle(j,0,4) {
      C[i][j] = 0.;
      boucle(k,0,4) C[i][j] += A[i][k]*B[k][j];
    }
  }
}



/* ===============================================================
 * Création d'une matrice de rotation 
 * d'angle teta autour d'un axe (A,B). 
 * =============================================================== 
 */

void MkNumXRotMat4x4(DtPoint3 A,double angle,DtMatrix4x4 M)
{
  DtMatrix4x4 t,r,bf;
  double cost,sint;

  cost = cos(angle);
  sint = sin(angle);

  /* Le calcul de la matrice de transformation */

  /* Translation 1 */

  t[0][0] = 1.; t[0][1] = 0.; t[0][2] = 0.; t[0][3] = 0.;
  t[1][0] = 0.; t[1][1] = 1.; t[1][2] = 0.; t[1][3] = 0.;
  t[2][0] = 0.; t[2][1] = 0.; t[2][2] = 1.; t[2][3] = 0.;
  t[3][0] = -A[0]; t[3][1] = -A[1]; t[3][2] = -A[2]; t[3][3] = 1.;

  /* Rotation */
  r[0][0] = 1.; r[0][1] = 0.;    r[0][2] = 0.;     r[0][3] = 0.;
  r[1][0] = 0.; r[1][1] = cost;  r[1][2] = sint;   r[1][3] = 0.;
  r[2][0] = 0.; r[2][1] = -sint; r[2][2] = cost;   r[2][3] = 0.;
  r[3][0] = 0.; r[3][1] = 0.;    r[3][2] = 0.;     r[3][3] = 1.;

  mulMat4x4(t,r,bf);

/* Translation -1 */

  t[0][0] = 1.; t[0][1] = 0.; t[0][2] = 0.; t[0][3] = 0.;
  t[1][0] = 0.; t[1][1] = 1.; t[1][2] = 0.; t[1][3] = 0.;
  t[2][0] = 0.; t[2][1] = 0.; t[2][2] = 1.; t[2][3] = 0.;
  t[3][0] = A[0]; t[3][1] = A[1]; t[3][2] = A[2]; t[3][3] = 1.;

  mulMat4x4(bf,t,M);

/* fprintf(stderr,"  %f  %f  %f  %f\n",M[0][0],M[0][1],M[0][2],M[0][3]);
   fprintf(stderr,"  %f  %f  %f  %f\n",M[1][0],M[1][1],M[1][2],M[1][3]);
   fprintf(stderr,"  %f  %f  %f  %f\n",M[2][0],M[2][1],M[2][2],M[2][3]);
   fprintf(stderr,"  %f  %f  %f  %f\n",M[3][0],M[3][1],M[3][2],M[3][3]); */

}



/* ================================
 * Calcul la norme d'un vecteur.
 * ================================
 */

DtFloat norme(DtPoint3 a)
{
  return (DtFloat) sqrt(a[0] * a[0]  + a[1] * a[1] + a[2] * a[2]);
}





/* ===================================
 * Matrice Nulle 4x4.
 * ===================================
 */

void MkNullMat4x4(DtMatrix4x4 m)
{
 m[0][0] = 0.; m[0][1] = 0.; m[0][2] = 0.; m[0][3] = 0.;
 m[1][0] = 0.; m[1][1] = 0.; m[1][2] = 0.; m[1][3] = 0.;
 m[2][0] = 0.; m[2][1] = 0.; m[2][2] = 0.; m[2][3] = 0.;
 m[3][0] = 0.; m[3][1] = 0.; m[3][2] = 0.; m[3][3] = 0.;
}




/* ==========================================
 * Impression d'une matrice 4x4.
 * ==========================================
 */

void printMat4x4(DtMatrix4x4 m)
{
  int i,j;

  
  boucle(i,0,4) {
    boucle(j,0,4) fprintf(stderr,"%.8lf ",m[i][j]);
    fprintf(stderr,"\n");
  }
  fprintf(stderr,"\n");
}





/* ==========================================================
 * Rotation d'un point par une matrice de rotation.
 * ==========================================================
 */

void singleRotate(DtPoint3 p,DtMatrix4x4 rmat)
{
  double x,y,z;

  x = p[0] * rmat[0][0] + p[1] * rmat[1][0] + p[2] * rmat[2][0] + rmat[3][0];
  y = p[0] * rmat[0][1] + p[1] * rmat[1][1] + p[2] * rmat[2][1] + rmat[3][1];
  z = p[0] * rmat[0][2] + p[1] * rmat[1][2] + p[2] * rmat[2][2] + rmat[3][2];
  p[0] = x;
  p[1] = y;
  p[2] = z;
}




/* ========================================================================
 * Rotation d'un nuage de (nbPoints) points par une matrice de rotation.
 * ========================================================================
 */


void rotateCrds(DtCrds crds2, int nbPoints, DtMatrix4x4 rmat)
{
  int i;

  for (i=0 ; i < nbPoints ; i++){
    singleRotate(crds2[i],rmat);
  }
}




/* ==========================================
 * Translation d'un point par un vecteur.
 * ==========================================
 */

void simpleTranslate(DtPoint3 p, DtPoint3 tr)
{
  p[0] += tr[0];
  p[1] += tr[1];
  p[2] += tr[2];
}





/* ===============================================
 * Symétrique d'un point par rapport à (0,0,0).
 * ===============================================
 */

void negatePoint3(DtPoint3 p1)
{
  p1[0] = - p1[0];
  p1[1] = - p1[1];
  p1[2] = - p1[2];
}






/* ================================================
 * Copie les coordonnées du point1 dans point2.
 * ================================================
 */
void copyPoint3(DtPoint3 p1, DtPoint3 p2)
{
  p2[0] = p1[0];
  p2[1] = p1[1];
  p2[2] = p1[2];
}




/* =================================================
 * Copie d'une matrice 4 x 4 (recopie a dans b).
 * =================================================
 */

void copyMat4x4(DtMatrix4x4 a,DtMatrix4x4 b)
{
  int i,j;
  boucle(i,0,4) {
    boucle(j,0,4) {
      b[i][j] = a[i][j];
    }
  }
}



/* ===================================================================
 * Copie d'un tableau de type DtpDistRec (recopie dist1 dans dist2). 
 * ===================================================================
 */

void copyDtpDistRec(int kds, DtDistRec *dist1, DtDistRec *dist2)
{
  int i,j;

  for(i=0 ; i < kds ; i++){
    for(j=0 ; j < 2 ; j++){
      dist2[i].longueur=dist1[i].longueur;
      dist2[i].liaison=dist1[i].liaison;
    }
  }
}



/* ===================================================================
 * Stockage d'une réponse pour CSR (kds,nbAtApp,dist,rmsd,Mat4x4).
 * ===================================================================
 */

void stockRep(int kds, int nbAtApp, DtDistRec *dist, double rmsd, DtMatrix4x4 mat, DtReponse rep)
{
  rep->nbAtMax=nbAtApp;
  rep->rmsd=rmsd;
  copyMat4x4(mat, rep->M);
  copyDtpDistRec(kds,dist,rep->dist);
}




/* ===============================================
 * Création d'une matrice de rotation aléatoire
 * ===============================================
 */


void rotAl (DtMatrix4x4 mat)
{
  double teta;
  DtPoint3 U;
  double N;
  int compteur=0;

  teta=drand48()*DcPI;

  /* printf("*teta=%.8lf\t",teta); */
  
  U[1]=-1+drand48()*2; //réel compris entre -1 et 1
  U[2]=-1+drand48()*2;
  U[3]=-1+drand48()*2;
  N=norme(U);
 
  while (N>1 && compteur < 1000){

    /* printf("*X=%.8lf\t",U[1]);
    printf("Y=%.8lf\t",U[2]);
    printf("Z=%.8lf\n",U[3]);
    printf("* /!\\========PROBLEME==========/!\\\n"); */
    U[1]=-1+drand48()*2;
    U[2]=-1+drand48()*2;
    U[3]=-1+drand48()*2;
    N=norme(U);
    compteur++;
  }
  if(compteur < 1000){
    /* printf("X=%.8lf\t",U[1]);
    printf("Y=%.8lf\t",U[2]);
    printf("Z=%.8lf\n",U[3]);
    printf("*norme(U)=%.8lf\n\n",N); */
  }
  else {
    /* printf("/!\\========NORME DE U SUPERIEURE A 1==========/!\\\n\n"); */
  }
  MkNumXRotMat4x4(U,teta,mat);
  
}


/* ===================================================
 * Positionnement aléatoire de deux nuages de points.
 * ===================================================
 */

void posAl (DtNuagesPts ng, DtMatrix4x4 mat)
{
  int nAl1, nAl2,i;
  DtPoint3 tr1, tr2;


  /* --- rotation aléatoire --- */

  rotAl(mat); 

  /* --- positionnement aléatoire --- */

  nAl1=irand(ng->n1);
  nAl2=irand(ng->n2);

  /* printf("*COUPLE (ATOME 1, ATOME 2) TIRE AU HASARD : (%d,%d).\n*\n"
     ,nAl1,nAl2); */


  /* --- superposition des deux nuages de points --- */

  /* rotation de tout le nuage 2 */
  for(i=0 ; i < (ng->n2) ; i++){
    singleRotate(ng->crds2[i], mat);
  }

  tr1[0]=ng->crds1[nAl1][0];
  tr1[1]=ng->crds1[nAl1][1];
  tr1[2]=ng->crds1[nAl1][2];

  tr2[0]=ng->crds2[nAl2][0];
  tr2[1]=ng->crds2[nAl2][1];
  tr2[2]=ng->crds2[nAl2][2];
  negatePoint3(tr2);

  /* superposition du nuage 2 sur le nuage 1 */
  for(i=0 ; i < (ng->n2) ; i++){
    simpleTranslate(ng->crds2[i],tr2);
    simpleTranslate(ng->crds2[i],tr1);
  }
}


/* ===========================================================
 * Imprime quels points sont appariés deux à deux par sdm.
 * ===========================================================
 */

void printNbPtsApp (DtNuagesPts ng, int *kds,DtDistRec *dist)
{
  int i,i1,i2,nbAtApp,stop;
  char at1App[ng->n1]; 
  char at2App[ng->n2]; 
  
  memset (at1App, 0, sizeof(char)*(ng->n1));
  memset (at2App, 0, sizeof(char)*(ng->n2));
  
  i=stop=0;
  
  while(i<(*kds) && stop==0){
    i1=(dist[i].liaison)%(ng->n1);
    i2=(dist[i].liaison)/(ng->n1);

    printf("*AT1 - AT2 : %d - %d\n",i1,i2);
    /* printPoint3Crds(ng->crds1,i1);
       printPoint3Crds(ng->crds2,i2); */

    if(at1App[i1]==1){
      stop=1;
      nbAtApp=i;
      printf("*=> ATOME %d DU NUAGE 1 DEJA APPARIE.\n",i1); 
    } else {
      at1App[i1]=1;
    }
    if(at2App[i2]==1){
      stop=1;
      nbAtApp=i;
      printf("*=> ATOME %d DU NUAGE 2 DEJA APPARIE.\n",i2);
    } else {
      at2App[i2]=1;
    }
    i++;
  }
}







/* ========================== ALGORITHME SDM =================================
 * Calcul de toutes les distances entre deux nuages de points (ng).
 * Conservation de toutes les distances inférieures à (dseuil) dans un 
 *   tableau (dist) de type DtpDistRec. 
 *   dist[k].longueur correspond à la k-ième distance inférieure à dseuil
 *   dist[k].liaison permet de retrouver de quels points s'agit cette longueur.
 *
 * Retourne le nombre de points maximum que l'on peut apparier.
 * ===========================================================================
 */

int sdm (DtNuagesPts ng, double dseuil, int dimTab, int *kds, DtDistRec *dist)
{
  char at1App[ng->n1]; // at1App[n1], tableau de n1 cases. 
  //at1App[k]=1 si (k+1)-ème atome du nuage 1 fait déjà parti d'un appariement, 0 sinon
  char at2App[ng->n2]; // at2App[n2], tableau de n2 cases. 
  //at2App[k]=1 si (k+1)-ème atome du nuage 2 fait déjà parti d'un appariement, 0 sinon
  int i,i1,i2,shift,stop,nbAtApp;
  double lg;


  if ((ng->n1)==0 || (ng->n2)==0){
    *kds=0;
    nbAtApp=0;
    return nbAtApp;
  }


  /* --- calcul des distances entre les points --- */

  if (dseuil>=0){
    *kds=0;
    for (i2=0 ; i2 <(ng->n2) ; i2++){
      shift=i2*(ng->n1);
      for (i1=0 ; i1 <(ng->n1) ; i1++){

	lg=distance((ng->crds1[i1]),(ng->crds2[i2]));
	
	if(lg <= dseuil){
	  if(*kds <= dimTab){ 
	    (dist[*kds]).longueur = lg; 
	    (dist[*kds]).liaison = i1 + shift; 
	    *kds=(*kds)+1;
	  }
	}
      } 
    } 
    
    if (*kds>dimTab) return (-1); /* dimd trop petit */
    if (*kds==0) return (-2); /* dseuil trop petit */
    
  } 
  
  else { /* dseuil < 0 (équivalent de dseuil infini) */

    *kds=(ng->n1)*(ng->n2);

    if (*kds>dimTab) return 1; /* dimd trop petit */
    for (i2=0 ; i2<(ng->n2) ; i2++){
      shift=i2*(ng->n1);
      for (i1=0 ; i1 <(ng->n1) ; i1++){
	dist[i1+shift].longueur=distance((ng->crds1[i1]),(ng->crds2[i2]));
	dist[i1+shift].liaison=i1+shift;

      } 
    } 
  }
  
  /* ---  tri du tableau des distances par ordre croissant de distances --- */


  /* printDtpDistRec(dist,100,ng);
     printf("\n"); */
  
  qsort(dist,*kds,sizeof(DtDistRec), cmpdist); 

  /* printDtpDistRec(dist,*kds,ng); */



  /* --- calcul du nombre de points appariés --- */

  memset (at1App, 0, sizeof(char)*(ng->n1));
  memset (at2App, 0, sizeof(char)*(ng->n2));
  
  i=stop=0;
  
  while(i<(*kds) && stop==0){
    i1=(dist[i].liaison)%(ng->n1);
    i2=(dist[i].liaison)/(ng->n1);

    /* printf("*AT1 - AT2 : %d - %d\n",i1,i2);
    printPoint3Crds(ng->crds1,i1);
    printPoint3Crds(ng->crds2,i2); */

    if(at1App[i1]==1){
      stop=1;
      nbAtApp=i;
      /* printf("*=> ATOME %d DU NUAGE 1 DEJA APPARIE.\n",i1); */
    } else {
      at1App[i1]=1;
    }
    if(at2App[i2]==1){
      stop=1;
      nbAtApp=i;
      /* printf("*=> ATOME %d DU NUAGE 2 DEJA APPARIE.\n",i2); */
    } else {
      at2App[i2]=1;
    }
    i++;
  }
  
  /* printDtpDistRec(dist,nbAtApp+1,ng); */

 
  if (nbAtApp==(*kds) && nbAtApp!=min((ng->n1),(ng->n2))) return (-3);
    
  
  return (int) (nbAtApp);
  
}


/* =========================================================================
 * Transfert les données de sdm à zuker_superpose.
 * (renvoie les (nbAtApp) coordonnées à superposer au mieux l'une sur l'autre).
 * =========================================================================
 */

void crdsASuperposer (DtNuagesPts ng, int nbAtApp, DtDistRec *dist, DtPoint3 *crdsSup1, DtPoint3 *crdsSup2)
{
  int i,i1,i2;

  for(i=0 ; i < nbAtApp ; i++){
    i1=(dist[i].liaison)%(ng->n1);
    i2=(dist[i].liaison)/(ng->n1);
    copyPoint3(ng->crds1[i1],crdsSup1[i]);
    copyPoint3(ng->crds2[i2],crdsSup2[i]);
  }
}








/* ==================================================================
 * XYCov = Calcule la matrice d'inertie des points
 *
 * id      : indices des atomes dans At
 * from,tto: indices extremes
 * P       : le barycentre
 * ==================================================================
 */

static double *product(double **A, double *x, int n) 
{
  int i, j;
  double sum;
  double *y=(double *)calloc(n, sizeof(double));

  for (i=0; i<n; i++) {
    sum=0;
    for (j=0; j<n; j++)
      sum+=A[i][j]*x[j];
    y[i]=sum;
  }
  return y;
}

DtMatrix3x3 *XYCov(DtMatrix3x3 *pM,DtPoint3 *X,DtPoint3 *Y,DtPoint3 Xmean,DtPoint3 Ymean,int aSze)
{
  int i;
  double Xx,Xy,Xz;
  double Yx,Yy,Yz;
  double daSze;

  /*   fprintf(stdout,"inertia, len %d\n",aSze); */

  /* X average*/
  Xmean[0] = Xmean[1] = Xmean[2] = 0.;
  for (i=0;i<aSze;i++) {
    Xmean[0] += X[i][0];
    Xmean[1] += X[i][1];
    Xmean[2] += X[i][2];
  }
  if (aSze) {
    daSze = (double) aSze;
    Xmean[0] /= daSze;
    Xmean[1] /= daSze;
    Xmean[2] /= daSze;
  }
  
  /* Y average*/
  Ymean[0] = Ymean[1] = Ymean[2] = 0.;
  for (i=0;i<aSze;i++) {
    Ymean[0] += Y[i][0];
    Ymean[1] += Y[i][1];
    Ymean[2] += Y[i][2];
  }
  if (aSze) {
    daSze = (double) aSze;
    Ymean[0] /= daSze;
    Ymean[1] /= daSze;
    Ymean[2] /= daSze;
  }

  /* Covariance matrix */

  if (pM == NULL) {
    pM = (DtMatrix3x3 *) calloc(1,sizeof(DtMatrix3x3));
  } else {
    memset((void *) pM, 0, sizeof(DtMatrix3x3));
  }
  
  for (i=0;i<aSze;i++) {
    Xx = (double) X[i][0] - Xmean[0];
    Xy = (double) X[i][1] - Xmean[1];
    Xz = (double) X[i][2] - Xmean[2];
    Yx = (double) Y[i][0] - Ymean[0];
    Yy = (double) Y[i][1] - Ymean[1];
    Yz = (double) Y[i][2] - Ymean[2];

    (*pM)[0][0] += Xx*Yx;
    (*pM)[0][1] += Xx*Yy;
    (*pM)[0][2] += Xx*Yz;

    (*pM)[1][0] += Xy*Yx;
    (*pM)[1][1] += Xy*Yy;
    (*pM)[1][2] += Xy*Yz;

    (*pM)[2][0] += Xz*Yx;
    (*pM)[2][1] += Xz*Yy;
    (*pM)[2][2] += Xz*Yz;
  }

  return pM;
}





/* ====================================================================
 * Matrice 4x4 Translation. PREMULTIPLIE -> Y = XM (X vecteur ligne) 
 * ====================================================================
 */

void MkTrnsIIMat4x4(DtMatrix4x4 m, DtPoint3 tr)
{
 m[0][0] = 1.;    m[0][1] = 0.;    m[0][2] = 0.;    m[0][3] = 0.;
 m[1][0] = 0.;    m[1][1] = 1.;    m[1][2] = 0.;    m[1][3] = 0.;
 m[2][0] = 0.;    m[2][1] = 0.;    m[2][2] = 1.;    m[2][3] = 0.;
 m[3][0] = tr[0]; m[3][1] = tr[1]; m[3][2] = tr[2]; m[3][3] = 1.;
}



/* ===================================================================
 * Fonctions utilitaires pour l'algorithme de superposition de Zuker.
 * ====================================================================
 */

int largestEV4(double R[4][4], double v[4], double *vp)
{
  
  double M2[4][4];
  int rs;

  memcpy(M2,R,sizeof(double)*16);

  rs = inverse_power(R, 4, 10000, 1.e-8, vp, v);

  if (!rs)
    return shift_power(&M2[0][0], 4, 10000, 1.e-8, vp, v);

  return rs;

}

int inverse_power(double a[4][4], int n, int maxiter, double eps, double *v, double *w) 
{
  int niter,i;
  double *y;
  double r, sum, l, normy, d;
  y=random_vect(n);
  niter=0;

  r=lmax_estim(a, n);
  for (i=0; i<n; i++) a[i][i]=a[i][i]-r;
  if (lu_c(a, n)==0) {
    /* fprintf(stderr,"ATTENTION ! cas singulier de inverse_power\n"); */
    free(y);     //exit(0);
    return 0;
  }

  do {
    normy=sqrt(inner(y,y,n));
    for (i=0; i<n; i++) {
      w[i]=y[i]/normy;
      y[i]=w[i];
    }

    resol_lu(a, y, n);
    l=inner(w,y,n);
    niter++;
    for (sum=0,i=0; i<n; i++) {
      d=y[i]-l*w[i];
      sum+=d*d;
    }
    d=sqrt(sum);
  } while (d>eps*fabs(l) && niter<maxiter);
  free(y);
  *v=r+1.0/l;
  return niter;
}

int shift_power(double *a, int n, int maxiter, double eps, double *v, double *w)
{
  double **tmp;
  double sh;
  int niter;
  int i,j;

  tmp=alloc_mat(n, n);
  /* copyMat(a, tmp, n, n); */
  for (i=0; i<n; i++) {
    for (j=0; j<n; j++) {
      tmp[i][j] = a[i*n+j];
    }
  }
  sh=best_shift(tmp, n);

  niter=power(tmp, n, maxiter, eps, v, w);
  *v=*v-sh;
  free_mat(tmp, n);
  return niter;
}

double *random_vect(int n) 
{   
  int i;
  double *v=(double *) calloc(n, sizeof(double));
  for (i=0; i<n; i++)
    v[i]= (double)rand()/(RAND_MAX+1.0);
  return(v);
}

static double inner(double *x, double *y, int n) 
{
  int i;
  double sum;

  for (sum=0, i=0; i<n; sum+=x[i]*y[i],i++);
  return sum;

}

double lmax_estim (double a[4][4], int n) 
{   
  double t, sum;
  int i, j;
  t=a[0][0];
  for (i=0; i<n; i++) {
    for (sum=0,j=0; j<n; j++)
      if (j!=i) sum+=fabs(a[i][j]);
    t=max(t, a[i][i]+sum);
  }
  return t;
}

static int lu_c (double a[4][4],  int n)
{
 int i,j,k,err;
 double pivot,coef;

 err=1;

 k=0;
 while (err==1 && k<n) {
  pivot=a[k][k];
  if(fabs(pivot)>=EPS) {
    for(i=k+1;i<n;i++) {
      coef=a[i][k]/pivot;
      for(j=k;j<n;j++)
        a[i][j] -= coef*a[k][j];
      a[i][k]=coef;
    }
  }
  else err=0;
  k++;
 }
 if(a[n-1][n-1]==0) err=0;
 return err;
}

static void resol_lu(double a[4][4], double *b, int n)
{
 int i,j;
 double sum;
 double y[n];
 y[0]=b[0];
 for(i=1;i<n;i++) {
  sum=b[i];
  for(j=0;j<i;j++)
    sum-=a[i][j]*y[j];
  y[i]=sum;
 }
 b[n-1]=y[n-1]/a[n-1][n-1];
 for(i=n-1;i>=0;i--) {
  sum=y[i];
  for(j=i+1;j<n;j++)
     sum-=a[i][j]*b[j];
  b[i]=sum/a[i][i];
 }
}

double best_shift(double *a[], int n) 
{
  double m, M, s;
  double t, sum;
  int i, j;
  t=a[0][0];
  for (i=1; i<n; i++) t=max(t, a[i][i]);
  M=t;
  t=a[0][0];
  for (i=0; i<n; i++) {
    for (sum=0,j=0; j<n; j++)
      if (j!=i) sum+=fabs(a[i][j]);
    t=min(t, a[i][i]-sum);
  }
  m=t;
  s=-0.5*(M+m);
  for (i=0; i<n; i++)
    a[i][i]=a[i][i]+s;
  return s;
}


int power(double *a[], int n, int maxiter, double eps, double *v, double *w) 
{
  int niter,i;
  double *y;
  double sum, l, normy, d;
  y=random_vect(n);
  niter=0;
  do {
    normy=sqrt(inner(y,y,n));
    for (i=0; i<n; i++) w[i]=y[i]/normy;
    y=product(a, w, n);
    l=inner(w,y,n);
    niter++;
    for (sum=0,i=0; i<n; i++) {
      d=y[i]-l*w[i];
      sum+=d*d;
    }
    d=sqrt(sum);
  } while (d>eps*fabs(l) && niter<maxiter);
  free(y);
  *v=l;
  return niter;
}





/* ====================================================================
 * Allocation et libération de l'espace mémoire pour une matrice 4x4.
 * ====================================================================
 */

void free_mat(double **mat, int n)
{
  int i;
  for (i=0; i<n; i++)
    free(mat[i]);
  free(mat);
}

double **alloc_mat(int n, int m) 
{
  int i;
  double **mat = (double **)calloc(m,sizeof(double *));
  for (i=0; i<n; i++)
    mat[i]=(double *)calloc(n, sizeof(double));
  return mat;
}



/* =========================================================
 * Calcul de la distance au carré entre deux points.
 * =========================================================
 */

DtFloat squared_distance(DtPoint3 R,DtPoint3 K)
{
  return (DtFloat) ((K[0] - R[0]) * (K[0] - R[0]) +
                    (K[1] - R[1]) * (K[1] - R[1]) +
                    (K[2] - R[2]) * (K[2] - R[2]));
}







/* =============== ALGORITHME DE SUPERPOSITION (ZUKER) =======================
 * Best fit matrix as proposed by:
 * Zuker & Somorjai, Bulletin of Mathematical Biology,
 * vol. 51, No 1, p 55-78, 1989.
 *
 * Returns M, a transformation matrix ready for use
 * ===========================================================================
 */

double zuker_superpose(DtPoint3 *c1, DtPoint3 *c2, int len, DtMatrix4x4 M)
{
  DtMatrix3x3  C;
  DtMatrix3x3 *pC;
  DtMatrix4x4  RM;
  DtMatrix4x4  TMP;
  DtMatrix4x4  TX;
  DtMatrix4x4  TY;
  DtMatrix4x4  P;
  DtPoint4     V;
  DtPoint3     bc1, bc2;
  DtPoint3     try;
  
  double eval;
  double squared_rms = 0.;
  
  int nCycles;
  int aDot;
  
  /* Compute transformation matrix as proposed by zuker */
  
  pC = &C;
  pC = XYCov(pC, (DtPoint3 *) c1, (DtPoint3 *) c2, bc1, bc2, len);
  
  P[0][0] = -C[0][0]+C[1][1]-C[2][2];
  P[0][1] = P[1][0] = -C[0][1]-C[1][0];
  P[0][2] = P[2][0] = -C[1][2]-C[2][1];
  P[0][3] = P[3][0] =  C[0][2]-C[2][0];
  
  P[1][1] = C[0][0]-C[1][1]-C[2][2];
  P[1][2] = P[2][1] = C[0][2]+C[2][0];
  P[1][3] = P[3][1] = C[1][2]-C[2][1];
  
  P[2][2] = -C[0][0]-C[1][1]+C[2][2];
  P[2][3] = P[3][2] = C[0][1]-C[1][0];
  
  P[3][3] = C[0][0]+C[1][1]+C[2][2];
  
  /* #if 0  
  printMat4x4("zuker P", P);
#endif */

  nCycles = largestEV4(P, V, &eval);
  
  RM[0][0] = -V[0]*V[0]+V[1]*V[1]-V[2]*V[2]+V[3]*V[3];
  RM[1][0] =  2*(V[2]*V[3]-V[0]*V[1]);
  RM[2][0] =  2*(V[1]*V[2]+V[0]*V[3]);
  RM[3][0] =  0.;
  
  RM[0][1] = -2*(V[0]*V[1]+V[2]*V[3]);
  RM[1][1] = V[0]*V[0]-V[1]*V[1]-V[2]*V[2]+V[3]*V[3];
  RM[2][1] =  2*(V[1]*V[3]-V[0]*V[2]);
  RM[3][1] =  0.;
  
  RM[0][2] =  2*(V[1]*V[2]-V[0]*V[3]);
  RM[1][2] = -2*(V[0]*V[2]+V[1]*V[3]);
  RM[2][2] = -V[0]*V[0]-V[1]*V[1]+V[2]*V[2]+V[3]*V[3];
  RM[3][2] =  0.;
  
  RM[0][3] =  0.;
  RM[1][3] =  0.;
  RM[2][3] =  0.;
  RM[3][3] =  1.;
  /* printMat4x4("zuker RM", RM); */
  
  /* Solution eprouvee ! */
  try[0] = - bc2[0]; try[1] = - bc2[1]; try[2] = - bc2[2];
  MkTrnsIIMat4x4(TY, try);
  MkTrnsIIMat4x4(TX, bc1);
  mulMat4x4(TY,RM,TMP);
  mulMat4x4(TMP, TX, M);
  
  /* Now superpose the coordinates */
  for (aDot=0;aDot<len;aDot++) {
    singleRotate(c2[aDot],M);
  }
  
  /* Compute squared RMSd */
  for (aDot=0;aDot<len;aDot++) {
    squared_rms += squared_distance(c1[aDot],c2[aDot]);
  }
  
  return squared_rms / (double) len;
}







/* ====================== ALGORITHME CSR =====================================
 * Superpose au mieux deux nuages de points avec SDM et zuker_superpose.
 * Retourne : 
 *   le nombre de points appariés.
 *   la matrice de rotation 4x4 correspondant à la meilleure superposition.
 *   le tableau trié des distances entre points après rotation et translation.
 * ===========================================================================
 */

DtReponse csr (DtNuagesPts ng, double dseuil, int dimd, int *kds, DtDistRec *dist, DtMatrix4x4 mat, DtPoint3 *crdsSup1, DtPoint3 *crdsSup2, int nbIter, DtReponse rep)
{
  int compteur,nbAtApp,nbAtTemp;
  double rmsd;
  long int gSeed = -1;
  long int *seed;
  
  seed=calloc(1,sizeof(long int*));
  *seed=gSeed;
  
  compteur = 0;
  while (compteur < nbIter){
    nbAtTemp=1;
    initRnd(seed);
    MkNullMat4x4(mat);
    posAl(ng, mat);
    nbAtApp=sdm(ng,dseuil,dimd,kds,dist);
    crdsASuperposer(ng,nbAtApp,dist,crdsSup1,crdsSup2);
    rmsd=zuker_superpose(crdsSup1,crdsSup2,nbAtApp,mat);
    rotateCrds(ng->crds2,ng->n2,mat);
    while(nbAtApp > nbAtTemp){
      nbAtTemp=nbAtApp;
      nbAtApp=sdm(ng,dseuil,dimd,kds,dist);
      crdsASuperposer(ng,nbAtApp,dist,crdsSup1,crdsSup2);
      rmsd=zuker_superpose(crdsSup1,crdsSup2,nbAtApp,mat);
      rotateCrds(ng->crds2,ng->n2,mat);
    }

    if(nbAtApp > (rep->nbAtMax)){
      printf("itération : %d\t nombre d'atomes appariés : %d\t",
	 compteur,nbAtApp);
      stockRep(*kds,nbAtApp,dist,rmsd,mat,rep);

      *kds=1;
      printNbPtsApp(ng,kds,dist);


      if (nbAtApp==24) return rep;
    }

    compteur=compteur+1;
  }

  return rep;
} 















/* ====================== FONCTION MAIN ======================================
 * ===========================================================================
 */

int main (int argc,char *argv[])
{
  DtMatrix4x4 mat;
  DtNuagesAt ng;
  double dseuil;
  int dimTab,minN1N2;
  DtDistRec *dist;
  int *kds;
  int nbAtApp;
  DtReponse rep;
  DtPoint3 *crdsSup1, *crdsSup2;
  char *fname1,*fname2;
  char **line;


  dseuil=(5);
  dimTab=25*25+1;
  
  dist = (DtDistRec *) calloc(dimTab,sizeof(DtDistRec));
  kds = calloc(1,sizeof(int *));
    
  nbAtApp=*kds=0;
  
  ng= (DtNuagesAt) calloc(1,sizeof(DtNuagesAt));
  ng->points= (DtNuagesPts) calloc(1,sizeof(DtNuagesPts));
  ng->points->crds1= (DtPoint3 *) calloc(25,sizeof(DtPoint3)*25);

  ng->points->crds1[0][0]=5.500;
  ng->points->crds1[0][1]=-1.912;
  ng->points->crds1[0][2]=11.073;
  ng->points->crds1[1][0]= 4.340;
  ng->points->crds1[1][1]=-2.585;
  ng->points->crds1[1][2]=11.126;
  ng->points->crds1[2][0]=3.530;
  ng->points->crds1[2][1]=-2.127;
  ng->points->crds1[2][2]=12.162;
  ng->points->crds1[3][0]=2.360;
  ng->points->crds1[3][1]=-2.737;
  ng->points->crds1[3][2]=12.514;
  ng->points->crds1[4][0]=1.489;
  ng->points->crds1[4][1]=-2.111;
  ng->points->crds1[4][2]=13.533;
  ng->points->crds1[5][0]=0.622;
  ng->points->crds1[5][1]=-3.026;
  ng->points->crds1[5][2]=14.179; 
  ng->points->crds1[6][0]=-0.278;
  ng->points->crds1[6][1]=-2.618;
  ng->points->crds1[6][2]=15.145;
  ng->points->crds1[7][0]=-0.406;
  ng->points->crds1[7][1]=-1.254;
  ng->points->crds1[7][2]=15.459;
  ng->points->crds1[8][0]=0.383;
  ng->points->crds1[8][1]=-0.341;
  ng->points->crds1[8][2]=14.779;
  ng->points->crds1[9][0]=1.379;
  ng->points->crds1[9][1]=-0.735;
  ng->points->crds1[9][2]=13.840;  
  ng->points->crds1[10][0]=2.088;
  ng->points->crds1[10][1]=0.321;
  ng->points->crds1[10][2]=13.305;
  ng->points->crds1[11][0]=3.273;
  ng->points->crds1[11][1]=0.285;
  ng->points->crds1[11][2]=12.514;
  ng->points->crds1[12][0]=4.133;
  ng->points->crds1[12][1]=-0.938;
  ng->points->crds1[12][2]=12.794;
  ng->points->crds1[13][0]=5.543;
  ng->points->crds1[13][1]=-0.856;
  ng->points->crds1[13][2]=12.155;
  ng->points->crds1[14][0]= 6.511;
  ng->points->crds1[14][1]=-2.181;
  ng->points->crds1[14][2]=10.076;
  ng->points->crds1[15][0]= 7.734;
  ng->points->crds1[15][1]=-1.625;
  ng->points->crds1[15][2]=10.046;
  ng->points->crds1[16][0]= 8.667;
  ng->points->crds1[16][1]=-1.959;
  ng->points->crds1[16][2]= 8.947;
  ng->points->crds1[17][0]= 9.938;
  ng->points->crds1[17][1]=-1.598;
  ng->points->crds1[17][2]=9.133;
  ng->points->crds1[18][0]=8.299;
  ng->points->crds1[18][1]=-2.539;
  ng->points->crds1[18][2]= 7.904;
  ng->points->crds1[19][0]=2.053;
  ng->points->crds1[19][1]=-3.826;
  ng->points->crds1[19][2]=12.002;
  ng->points->crds1[20][0]= -1.371;
  ng->points->crds1[20][1]= -0.796;
  ng->points->crds1[20][2]=16.519;
  ng->points->crds1[21][0]= 0.264;
  ng->points->crds1[21][1]= 1.016;
  ng->points->crds1[21][2]=14.989;
  ng->points->crds1[22][0]=3.003;
  ng->points->crds1[22][1]= 0.274;
  ng->points->crds1[22][2]=11.104;
  ng->points->crds1[23][0]= 2.479;
  ng->points->crds1[23][1]=1.514;
  ng->points->crds1[23][2]=10.624;
  ng->points->crds1[24][0]= 5.773;
  ng->points->crds1[24][1]=1.571;
  ng->points->crds1[24][2]=14.512;
  
  ng->points->crds2= ( DtPoint3 *) calloc(25,sizeof(DtPoint3)*25);

  ng->points->crds2[0][0]=0.276;
  ng->points->crds2[0][1]=7.712;
  ng->points->crds2[0][2]=7.016;
  ng->points->crds2[1][0]=2.979;
  ng->points->crds2[1][1]=7.011;
  ng->points->crds2[1][2]=3.130;
  ng->points->crds2[2][0]= 2.053;
  ng->points->crds2[2][1]=2.903;
  ng->points->crds2[2][2]=3.980;
  ng->points->crds2[3][0]=8.317;
  ng->points->crds2[3][1]=4.251;
  ng->points->crds2[3][2]=-0.133;
  ng->points->crds2[4][0]=2.156;
  ng->points->crds2[4][1]=1.625;
  ng->points->crds2[4][2]=1.597;
  ng->points->crds2[5][0]=5.558;
  ng->points->crds2[5][1]=5.856;
  ng->points->crds2[5][2]=4.110;
  ng->points->crds2[6][0]= 5.500;
  ng->points->crds2[6][1]=4.804;
  ng->points->crds2[6][2]=3.008;
  ng->points->crds2[7][0]= 4.328;
  ng->points->crds2[7][1]=4.127;
  ng->points->crds2[7][2]=3.073;
  ng->points->crds2[8][0]=3.505;
  ng->points->crds2[8][1]= 4.591;
  ng->points->crds2[8][2]=4.128;
  ng->points->crds2[9][0]=2.366;
  ng->points->crds2[9][1]=3.969;
  ng->points->crds2[9][2]=4.492;
  ng->points->crds2[10][0]=1.496;
  ng->points->crds2[10][1]=4.594;
  ng->points->crds2[10][2]=5.516;
  ng->points->crds2[11][0]=0.620;
  ng->points->crds2[11][1]= 3.689;
  ng->points->crds2[11][2]=6.164;
  ng->points->crds2[12][0]=-0.323;
  ng->points->crds2[12][1]=4.078;
  ng->points->crds2[12][2]=7.092;
  ng->points->crds2[13][0]=-0.416;
  ng->points->crds2[13][1]=5.445;
  ng->points->crds2[13][2]=7.436;   
  ng->points->crds2[14][0]= 0.373;
  ng->points->crds2[14][1]=6.351;
  ng->points->crds2[14][2]=6.767;
  ng->points->crds2[15][0]=1.377;
  ng->points->crds2[15][1]=5.963;
  ng->points->crds2[15][2]=5.850;
  ng->points->crds2[16][0]=2.106;
  ng->points->crds2[16][1]=7.032;
  ng->points->crds2[16][2]= 5.343;
  ng->points->crds2[17][0]=3.254;
  ng->points->crds2[17][1]=7.026;
  ng->points->crds2[17][2]=4.510;
  ng->points->crds2[18][0]=4.117;
  ng->points->crds2[18][1]=5.765;
  ng->points->crds2[18][2]=4.764;
  ng->points->crds2[19][0]=6.500;
  ng->points->crds2[19][1]=4.557;
  ng->points->crds2[19][2]=2.033;
  ng->points->crds2[20][0]=7.688;
  ng->points->crds2[20][1]=5.084;
  ng->points->crds2[20][2]=2.015;
  ng->points->crds2[21][0]=8.648;
  ng->points->crds2[21][1]= 4.798;
  ng->points->crds2[21][2]=0.894;
  ng->points->crds2[22][0]=9.908;
  ng->points->crds2[22][1]=5.162;
  ng->points->crds2[22][2]=1.082;
  ng->points->crds2[23][0]=-1.398;
  ng->points->crds2[23][1]=5.845;
  ng->points->crds2[23][2]=8.508;    
  ng->points->crds2[24][0]=2.501;
  ng->points->crds2[24][1]=8.295;
  ng->points->crds2[24][2]=2.713;

  ng->points->n1=25;
  ng->points->n2=25;


  minN1N2=min(ng->points->n1,ng->points->n2);  

  crdsSup1= ( DtPoint3 *) calloc(minN1N2,sizeof(DtPoint3)*minN1N2);
  crdsSup2= ( DtPoint3 *) calloc(minN1N2,sizeof(DtPoint3)*minN1N2);
  rep = (DtReponse) calloc(1,sizeof(DtReponse));
  rep->dist = (DtDistRec *) calloc(dimTab,sizeof(DtDistRec));
  
  rep=csr(ng->points,dseuil,dimTab,kds,dist,mat,crdsSup1,crdsSup2,5000,rep);

  //printNbPtsApp(ng,kds,dist);
  printf("NOMBRE D'ATOMES APPARIES : %d\n",rep->nbAtMax);
  printf("RMSD : %.8lf\n",rep->rmsd);
  //printDtpDistRec(rep->dist,(rep->nbAtMax)+1,ng);
  printf("\n");



  /*************** tests de lecture de fichiers *****************/


  fname1=argv[1];


  lectPdbCsr(fname1,line,ng,1);
  //printNgCrds(ng->points->crds1,ng->points->n1);
  printf("\n");
 

  fname2=argv[2];
  
  lectPdbCsr(fname2,line,ng,2);
  printNgCrds(ng->points->crds2,ng->points->n2);
  printf("\n");
  

  return 0;
}
