#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#define max(a,b) (a<b?b:a)
#define min(a,b) (a<b?a:b)
#define sign(a,b) ((b)>=0.0 ? fabs(a) : -fabs(a))
#define EPS 1.e-10

double **alloc_mat(int n, int m) {
  int i;
  double **mat = (double **)calloc(m,sizeof(double *));
  for (i=0; i<n; i++)
    mat[i]=(double *)calloc(n, sizeof(double));
  return mat;
}

void free_mat(double **mat, int n){
  int i;
  for (i=0; i<n; i++)
    free(mat[i]);
  free(mat);
}

void copy_mat (double **src, double **dest, int col, int row) {
  int i, j;

  for (i=0; i<col; i++)
    for (j=0; j<col; j++)
      dest[i][j] = src[i][j];
    
}

void matxmat (double **C, double **A, double **B, int rowA, int colA, int colB) {
  int i, j, k;
  double sum;
  for (i=0; i<rowA; i++)
    for (j=0; j<colB; j++) {
      for (sum=0, k=0; k<colA; k++)
	sum+=A[i][k]*B[k][j];
      C[i][j]=sum;
    }
}

void print_matrice(double **a, int n, int m, char *name){
  int i,j;

  for (i=0; i<n; i++) {
    for (j=0; j<m; j++) 
      printf (" %s[%d,%d]=%lf", name, i, j, a[i][j]);
    printf ("\n");
  }
  printf ("\n");
}

void print_vector(double *a, int n, char *name){
  int i;

  for (i=0; i<n; i++) 
      printf (" %s[%d]=%lf\n", name, i, a[i]);
}
double *random_vect(int n) {
  int i;
  double *v=(double *) calloc(n, sizeof(double));
  for (i=0; i<n; i++) 
    v[i]= (double)rand()/(RAND_MAX+1.0);
  return(v);
}

double inner(double *x, double *y, int n) {
  int i;
  double sum;

  for (sum=0, i=0; i<n; sum+=x[i]*y[i],i++);
  return sum;
    
}

void product(double *result, double **A, double *x, int n) {
  int i, j;
  double sum;

  for (i=0; i<n; i++) {
    sum=0;
    for (j=0; j<n; j++)
      sum+=A[i][j]*x[j];
    result[i]=sum;
  }
}

int lu_c (double **a,  int n)
{
 int i,j,k,err;
 double pivot,coef,s;

 err=1;
 k=0;
 while (err==1 && k<n) {
  pivot=a[k][k];
  if(fabs(pivot)>=EPS) {
    for(i=k+1;i<n;i++) {
      coef=a[i][k]/pivot;
      for(j=k;j<n;j++)
	a[i][j] -= coef*a[k][j];
      a[i][k]=coef;
    }
  }
  else err=0;
  k++;
 }
 if(a[n-1][n-1]==0) err=0;
 return err;
}


void resol_lu(double **a, double *b, int n)
{
 int i,j;
 double sum;
 double y[n];
 y[0]=b[0];
 for(i=1;i<n;i++) {
  sum=b[i];
  for(j=0;j<i;j++)
    sum-=a[i][j]*y[j];
  y[i]=sum;
 }
 b[n-1]=y[n-1]/a[n-1][n-1];
 for(i=n-1;i>=0;i--) {
  sum=y[i];
  for(j=i+1;j<n;j++)
     sum-=a[i][j]*b[j];
  b[i]=sum/a[i][i];
 }
}

void qrdcmp(double **a, double *c, double *d, int n, int *sing){
  int i,j,k;
  double scale=0.0, sigma, sum, tau;
  
  *sing=0;
  for (k=0;k<(n-1);k++) {
    for (i=k;i<n;i++) scale = max(scale, fabs(a[i][k]));
    if (scale==0.0) {
      *sing=1;
      c[k]=d[k]=0.0;
    }
    else {
      for (i=k; i<n; i++) a[i][k]/=scale;
      for (sum=0.0,i=k;i<n;i++) sum+=(a[i][k]*a[i][k]);
      sigma=sign(sqrt(sum), a[k][k]);
      a[k][k]+=sigma;
      c[k]=sigma*a[k][k];
      d[k]=-scale*sigma;
      for (j=k+1;j<n;j++) {
	for (sum=0.0,i=k;i<n;i++) sum+= a[i][k]*a[i][j];
	tau=sum/c[k];
	for (i=k;i<n;i++) a[i][j] -= tau*a[i][k];
      }
    }
  }
  d[n-1]=a[n-1][n-1];
  if (d[n-1]== 0.0) *sing=1;
}


void RQ(double **r, double **a, double *c, double *d, int n) {
  int i,j,k,l;
  double sum;
  double *tmp;

  tmp = (double *) calloc(n, sizeof(double));
  for (k=0;k<n;k++) {
    for (l=0;l<k;l++)
      r[k][l]=0;
    r[k][k]=d[k];
    for (l=k+1;l<n;l++)
      r[k][l]=a[k][l];
  }
  for (j=0;j<(n-1);j++) {
      for (k=0; k<n; k++) {
	for (l=j; l<n; l++) {
	  sum=0.;
	  for (i=j;i<n;i++)
	    sum+=r[k][i]*a[i][j];
	  tmp[l]=r[k][l]-sum*a[l][j]/c[j];
	}
	for (l=j; l<n; l++)
	  r[k][l]=tmp[l];
      }
  }
  free(tmp);
}

void PQ(double **p, double **a, double *c, double *d, int n) {
  int i,j,k,l;
  double sum;
  double *tmp;

  tmp = (double *) calloc(n, sizeof(double));
  for (j=0;j<(n-1);j++) {
      for (k=0; k<n; k++) {
	for (l=j; l<n; l++) {
	  sum=0.;
	  for (i=j;i<n;i++)
	    sum+=p[k][i]*a[i][j];
	  tmp[l]=p[k][l]-sum*a[l][j]/c[j];
	}
	for (l=j; l<n; l++)
	  p[k][l]=tmp[l];
      }
  }
  free(tmp);
}

int isDiag(double **a, int n, double eps){
  int i,j;
  double sum=0;

  for (i=0; i<n; i++)
    for (j=0; j<n; j++)
      if (i != j) sum+=fabs(a[i][j]);

  if (sum<=eps) return 1;
  return 0;
}


int qr_eigen(double *a[], int n, int maxiter, double eps, double *v, double **w) {
  int i,j,k;
  double c[n], d[n];
  double **tmp1, **tmp2;
  int sing;

  for (i=0; i<n; i++)
    for (j=0; j<n; j++)
      w[i][j]=0;

  for (i=0; i<n; i++) {
    w[i][i]=1;
  }
  k=0;
  tmp1=alloc_mat(n, n);
  tmp2=alloc_mat(n, n);
  copy_mat (a, tmp1, n, n);
  do {
    k++;
    qrdcmp(tmp1, c, d, n, &sing);
    RQ(tmp2, tmp1, c, d, n);
    PQ(w, tmp1, c, d, n);
    copy_mat(tmp2, tmp1, n, n);
  } while (!isDiag(tmp2, n, eps) && k<maxiter);

  for (i=0; i<n; i++)
    v[i]=tmp2[i][i];

  free_mat(tmp1,n);
  free_mat(tmp2,n);
  return k;
}



int power(double *a[], int n, int maxiter, double eps, double *v, double *w) {
  int niter,i;
  double *y;
  double sum, l, normy, d;
  y=random_vect(n);
  niter=0;
  do {
    normy=sqrt(inner(y,y,n));
    for (i=0; i<n; i++) w[i]=y[i]/normy;
    product(y, a, w, n);
    l=inner(w,y,n);
    niter++;
    for (sum=0,i=0; i<n; i++) {
      d=y[i]-l*w[i];
      sum+=d*d;
    }
    d=sqrt(sum);
  } while (d>eps*fabs(l) && niter<maxiter);
  free(y);
  *v=l;
  return niter;
}

double best_shift(double *a[], int n) {
  double m, M, s;
  double t, sum;
  int i, j;
  t=a[0][0];
  for (i=1; i<n; i++) t=max(t, a[i][i]);
  M=t;
  t=a[0][0];
  for (i=0; i<n; i++) {
    for (sum=0,j=0; j<n; j++)
      if (j!=i) sum+=fabs(a[i][j]);
    t=min(t, a[i][i]-sum);
  }
  m=t;
  s=-0.5*(M+m);
  for (i=0; i<n; i++) 
    a[i][i]=a[i][i]+s;
  return s;
}

double lmax_estim (double *a[], int n) {
  double t, sum;
  int i, j;
  t=a[0][0];
  for (i=0; i<n; i++) {
    for (sum=0,j=0; j<n; j++)
      if (j!=i) sum+=fabs(a[i][j]);
    t=max(t, a[i][i]+sum);
  }
  return t;
}

int shift_power(double *a[], int n, int maxiter, double eps, double *v, double *w) {  
  double **tmp;
  double sh;
  int niter;
  tmp=alloc_mat(n, n);    
  copy_mat(a, tmp, n, n);
  sh=best_shift(tmp, n);   

  niter=power(tmp, n, maxiter, eps, v, w);
  *v=*v-sh;
  free_mat(tmp, n);
  return niter;
}


int inverse_power(double *a[], int n, int maxiter, double eps, double *v, double *w) {
  int niter,i;
  double *y;
  double r, sum, l, normy, d;
  y=random_vect(n);
  niter=0;
  
  r=lmax_estim(a, n);
  for (i=0; i<n; i++) a[i][i]=a[i][i]-r;
  if (lu_c(a, n)==0) {
    printf("attention ! cas singulier de inverse_power\n");
    free(y);
    return 0;
  }

  do {
    normy=sqrt(inner(y,y,n));
    for (i=0; i<n; i++) {
      w[i]=y[i]/normy;
      y[i]=w[i];
    }
    resol_lu(a, y, n);
    l=inner(w,y,n);
    niter++;
    for (sum=0,i=0; i<n; i++) {
      d=y[i]-l*w[i];
      sum+=d*d;
    }
    d=sqrt(sum);
  } while (d>eps*fabs(l) && niter<maxiter);
  free(y);
  *v=r+1.0/l;
  return niter;
  }
