/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
#ifndef __ARGSTR_H__
#define __ARGSTR_H__
 
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/times.h>

extern char ITemplateFichName[BUFSIZ];
extern char IBankFichName[BUFSIZ];
extern char IPairRules[BUFSIZ];
extern char OLogFichName[BUFSIZ];
extern char OClsFichName[BUFSIZ];
extern char OPDBFichName[BUFSIZ];
extern char OSDFFichName[BUFSIZ];
extern char OMOL2FichName[BUFSIZ];
extern char ORasmolFichName[BUFSIZ];
extern char OPymolFichName[BUFSIZ];
extern int  noTemplateCrds;
extern int    gWhat;
extern int    gStrictMatch;
extern int    gNIter;
extern int    gMinAt;
extern double gDMax;
extern double gRMSd;
extern double gTol;
extern int    DEBUG;
extern int    gVerbose;
extern long int gSeed;
extern int    gTempFormat;
extern int    gBankFormat;
extern char   gNoH;
extern int  gMaxNuage;
extern int  gFast;
extern int  gFastOnly;
extern int  gNoFit;
extern int  gClasses;
extern int  gIdentical;

extern void printUsage();
extern void parseargstr(int argc,char *argv[]);
extern void summary(FILE *f);
extern void delimiter(FILE *f);

#endif
