/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/times.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include "CSRTypes.h"
#include "CSR.h"
#include "pairRules.h"
#include "PDB.h"
#include "Random.h" 
#include "argstr.h" 
#include "CpuTime.h" 
#include "mol2.h"

extern int gFound;

char *colors[6] = {"yellow","red","cyan", "green","pink", "magenta"};

/* ====================== FONCTION MAIN ======================================
 * CSR main
 * ===========================================================================
 */

int main (int argc,char *argv[])
{
  /* ===========================================
   * Infos temps CPU elapsed 
   * ===========================================
   */
  struct tms time0, time1;
//  struct tms time2;

  FILE *rmf   = NULL;
  FILE *pmf   = NULL;
  FILE *opdb  = NULL;
  FILE *osdf  = NULL;
  FILE *omol2 = NULL;
//  FILE *fd   = NULL;
  FILE *of   = stdout;
  char oFName[BUFSIZ];
  FILE *cf   = stdout;
  char oCLSName[BUFSIZ];
  char buff1[BUFSIZ], buff2[BUFSIZ];
  DtMatrix4x4 mat;
  DtMatrix3x3 ev1; // eigen vectors from reference 1 (aNuageT)
//  DtNuage ng1;
//  DtNuage ng2;
//  double dseuil;
  int dimTab,minN1N2;
  DtDistRec *dist = NULL;
  DtReRec *pReList;
  DtReRec *pRe;
  int kds;
  int nbAtApp;
  int curMinAt;
  DtReponse *rep;
  DtPoint3  *crdsSup1 = NULL, *crdsSup2 = NULL;
  DtPoint3 *pCrd1, *pCrd2;
  DtPoint3  bc1;

  double *weightSup1 = NULL, *weightSup2 = NULL;  // poids des atomes superposes
  DtNuage *pNg1, *pNg2;
  DtNuage *plNg2;
  // long int gSeed = 268435455;
//  long int *seed;
  DtNuage *pNgT, *pNgB;

  double dx, dy, dz, lrmsd;

  int nNuagesT, nNuagesB;
  int aNuage, nuageStart;
  int aNuageT;
  int aDot;
  int nCrd1, nCrd2;
  int *class = NULL;
  int *clsSeed = NULL;
  int nClasses = 0;

  int i, i1, i2;
  int nHits = 0;
  int hasCnct = 0;
  int isStart = 1;
  DtRuleList *pRuleList = NULL;

  int nConnect;
  double nConnectW;

  int nComps = 0;
  int nTries = 0;

  int doOutput = 1;

//   void (*lectCsr)(char *, int, int, DtNuage **, int *, DtReRec **, int);


  gettime(stderr, &time0);

  parseargstr(argc, argv);
  initRnd(&gSeed);

  if (OLogFichName[0] != '\0') {
    of = fopen(OLogFichName,"w");
    if (of == NULL) {
      fprintf(stderr,"Sorry: could not open file %s for writing !!\n", OLogFichName);
      exit(0);
    }
  }

  if (OClsFichName[0] != '\0') {
    cf = fopen(OClsFichName,"w");
    if (cf == NULL) {
      fprintf(stderr,"Sorry: could not open file %s for writing !!\n", OClsFichName);
      exit(0);
    }
  }

  summary(of);
  fflush(of);

#if 0
  if (ITemplateFichName[0] == '\0') {
    fprintf(stderr,"Sorry: Need at least a template !!\n");
    exit(0);
  }
#endif
  if (IBankFichName[0] == '\0') {
    fprintf(stderr,"Sorry: Need at least a bank !!\n");
    exit(0);
  }


  if (IPairRules[0] != 0) {
    pRuleList = lectPairRules(IPairRules);
  }

  // dseuil=5.; --> gDMax;

  pNgT    = NULL;
  pNgB    = NULL;
  pReList = NULL;

  if (IBankFichName[0] != '\0') {
    if (gBankFormat == PDB)
      lectPdbCsr(IBankFichName, gWhat, gStrictMatch, &pNgB, &nNuagesB, &pReList, gVerbose);
    else if (gBankFormat == MOL2)
      lectMol2Csr(IBankFichName, gWhat, gStrictMatch, &pNgB, &nNuagesB, &pReList, gVerbose, pRuleList);
    else if (gBankFormat == SDF)
      lectSDFCsr(IBankFichName, gWhat, gStrictMatch, &pNgB, &nNuagesB, &pReList, gVerbose, pRuleList);

    // pNg1 = &pNgB[0];
    nuageStart = 1;
  }
  if (ITemplateFichName[0] != '\0') {
    if (gBankFormat == PDB)
      lectPdbCsr(ITemplateFichName, gWhat, gStrictMatch, &pNgT, &nNuagesT, &pReList, gVerbose);
    else if (gBankFormat == MOL2)
      lectMol2Csr(ITemplateFichName, gWhat, gStrictMatch, &pNgT, &nNuagesT, &pReList, gVerbose, pRuleList);
    else if (gBankFormat == SDF)
      lectSDFCsr(ITemplateFichName, gWhat, gStrictMatch, &pNgT, &nNuagesT, &pReList, gVerbose, pRuleList);

    // pNg1 = &pNgT[0];
    nuageStart = 0;
  }

  if (pRuleList) {
    freeRuleList(pRuleList);
  }


  // We log the regexps identified
  pRe = pReList;
  fprintf(of,"%d RegExps installed (Atom  Residue):\n", rePtrListLen(pReList));
  while (pRe != NULL) {
    fprintf(of,"\"%s\" \"%s\"\n",pRe->reAtmStr, pRe->reResStr);
    pRe = pRe->next;
  }
  delimiter(of);
  //  exit(0);

  if (pNgT == NULL) {
    pNgT = pNgB;
    nNuagesT = 1;
    nuageStart = 1;
  }

  if (gVerbose) {
    fprintf(stderr,"Data input done ...\n");
  }

  if (OPDBFichName[0] != '\0') {
    opdb = fopen(OPDBFichName,"w");
    if (opdb == NULL) {
      fprintf(stderr,"Sorry: could not open file %s for writing !!\n", OPDBFichName);
      exit(0);
    }
  }

  if (OSDFFichName[0] != '\0') {
    osdf = fopen(OSDFFichName,"w");
    if (osdf == NULL) {
      fprintf(stderr,"Sorry: could not open file %s for writing !!\n", OSDFFichName);
      exit(0);
    }
  }

  if (OMOL2FichName[0] != '\0') {
    omol2 = fopen(OMOL2FichName,"w");
    if (omol2 == NULL) {
      fprintf(stderr,"Sorry: could not open file %s for writing !!\n", OMOL2FichName);
      exit(0);
    }
  }

  if (ORasmolFichName[0] != '\0') {
    rmf = fopen(ORasmolFichName,"w");
    if (rmf == NULL) {
      fprintf(stderr,"Sorry: could not open file %s for writing !!\n", ORasmolFichName);
      exit(0);
    }
    fprintf(rmf,"load %s\n", IBankFichName);
    fprintf(rmf,"wireframe 100\n");
    fprintf(rmf,"zoom 120\n");
    fprintf(rmf,"set ambient 60\n");
    // fprintf(rmf,"set shadepower 100\n");
    fprintf(rmf,"select all\n");
    fprintf(rmf,"color white\n");
    fprintf(rmf,"background black\n");

  }

  if (OPymolFichName[0] != '\0') {
    pmf = fopen(OPymolFichName,"w");
    if (pmf == NULL) {
      fprintf(stderr,"Sorry: could not open file %s for writing !!\n", ORasmolFichName);
      exit(0);
    }
    if (ITemplateFichName[0] != '\0') {
      fprintf(pmf,"load %s, template\n", ITemplateFichName);
      fprintf(pmf,"color blue, template\n");
    } else {
      sprintf(oFName,"CSRout_template.pdb");
      pNg1 = &pNgB[0];
      outPDBNuage(oFName, pNg1);
      fprintf(pmf,"load %s, template\n", oFName);
      fprintf(pmf,"color blue, template\n");
    }
  }

  if (gVerbose) {
    fprintf(stderr,"Will proceed ...\n");
  }


  rep = (DtReponse *) calloc(1, sizeof(DtReponse));
  rep->dist = NULL;


  /* Initialize equilvalence classes if requested */
  if (gClasses) {
    class = (int *) calloc(nNuagesB,sizeof(int));
    clsSeed = (int *) calloc(nNuagesB,sizeof(int));
    for (aNuage = 0; aNuage < nNuagesB; aNuage++) {
      class[aNuage] = -1;
      clsSeed[aNuage] = aNuage;
    }
    doOutput = 0;
  }

 LSTART: ;

  for (aNuageT = 0; aNuageT < nNuagesT; aNuageT++) {

    if (gClasses && (class[aNuageT] >= 0)) continue;
    if (aNuageT >= gMaxNuage) break;


    pNg1 = &pNgT[aNuageT];
    if (gVerbose) {
      fprintf(stderr,"Template : Compound: %s | %s | EC: %s\n",pNg1->id,pNg1->cmpnd,pNg1->EC); 
    }
    
    if (gIdentical) nuageStart = aNuageT + 1;
    for (aNuage = nuageStart; aNuage < nNuagesB; aNuage++) {

      if (gIdentical && (aNuage == aNuageT)) continue;
      if (gClasses && (class[aNuage] >= 0)) continue;
      if (aNuage >= gMaxNuage) break;

      pNg2 = &pNgB[aNuage];

      // fprintf(stderr,"Nuage %d vs %d (%d crds)\n",aNuageT, aNuage, pNg2->n);
      plNg2 = allocNuage(pNg2->n);
      memcpy(plNg2->crds,pNg2->crds, sizeof(DtPoint3) * pNg2->n);
      // fprintf(stderr,"COUCOU\n");

      dimTab=(pNg1->n)*(pNg2->n)+1;
      dist = (DtDistRec *) calloc(dimTab,sizeof(DtDistRec));

      nbAtApp=kds=0;

      barycentre(pNg1);
      barycentre(pNg2);

      // printNuage(stderr, pNg1, "Nuage1");
      // fprintf(stderr, "===========================\n");

      // printNuage(stderr, pNg2, "Nuage2");
      // fprintf(stderr, "===========================\n");

      if (gVerbose) {
        fprintf(stderr, "Template %d (%d crds) against Bank #%d (%d crds)\n",1,pNg1->n,aNuage,pNg2->n);
      }

      minN1N2=min(pNg1->n,pNg2->n);

      crdsSup1       = ( DtPoint3 *) realloc(crdsSup1, minN1N2 * sizeof(DtPoint3) );
      crdsSup2       = ( DtPoint3 *) realloc(crdsSup2, minN1N2 * sizeof(DtPoint3) );
      weightSup1     = (   double *) realloc(weightSup1, minN1N2 * sizeof(double) );
      weightSup2     = (   double *) realloc(weightSup2, minN1N2 * sizeof(double) );
      rep->dist      = (DtDistRec *) realloc(rep->dist, dimTab * sizeof(DtDistRec));
      rep->nbAtMax   = 0;
      rep->nLPairs   = 0;
//       rep->scoreMax  = 0.0;
//       rep->scoreLMax = 0.0;

      /* What atoms of pNg2 match the Re's in pReList             */
      /* Here: pNg1 is the template (with Re's), pNg2 is the bank */
      setupMaskFromRe(pReList, pNg2);

      /* We check matches are possible */
      i1 = 0;
      boucle (i, 0, pNg1->n) {
	if (pNg1->skip[i]) continue;
	if (pNg1->re[i]->nMask) {
	  i1 = 1;
	  break;
	}
      }
      if (!i1) {
        free(dist);
        freeNuage(plNg2);
        continue;
      }

      /* Presently, minAt not used !! Tolerance is used to enlarge MSSs, RMSd to filter accepted MSSs */
      curMinAt = gMinAt == -1 ? pNg1->n > pNg2->n ? pNg2->n : pNg1->n : gMinAt;

      if (gNoFit) {
	// Just compute RMSd, atoms MUST be paired already. Only works for identical compounds (or same number of atoms).
	lrmsd = 0;
	for (aDot = 0; aDot < pNg1->n; aDot++) {
	  dx = pNg2->crds[aDot][0] - pNg1->crds[aDot][0];
	  dy = pNg2->crds[aDot][1] - pNg1->crds[aDot][1];
	  dz = pNg2->crds[aDot][2] - pNg1->crds[aDot][2];
	  lrmsd += dx*dx + dy*dy + dz*dz;
	}
	lrmsd = lrmsd / (float) pNg1->n;
	fprintf(stdout,"%.3lf ", sqrt(lrmsd));
	continue;
      }

      if (gFast) { // Hack to QR_superpose
	// fprintf(stderr,"Fast QR %d / %d\n", aNuageT, aNuage);

	// Try clouds of most represented atoms : C 
	for (aDot = 0; aDot < pNg1->n; aDot++) {
	  if ( (pNg1->atmName[aDot][0] == 'C') || (pNg1->atmName[aDot][0] == 'c') ) {
	    nCrd1 ++;
	  }
	}
	if (nCrd1 > (pNg1->n / 2)) {
	  pCrd1 = (DtPoint3 *) calloc(nCrd1, sizeof(DtPoint3));
	  nCrd1 = 0;
	  for (aDot = 0; aDot < pNg1->n; aDot++) {
	    if ( (pNg1->atmName[aDot][0] == 'C') || (pNg1->atmName[aDot][0] == 'c') ) {
	      copyPoint3(pNg1->crds[aDot], pCrd1[nCrd1]);
	      nCrd1 ++;
	    }
	  }
	} else {
	  pCrd1 = pNg1->crds;
	  nCrd1 = pNg1->n;
	}

	// Try clouds of most represented atoms : C  for nuage 2
	for (aDot = 0; aDot < pNg2->n; aDot++) {
	  if ( (pNg2->atmName[aDot][0] == 'C') || (pNg2->atmName[aDot][0] == 'c') ) {
	    nCrd2 ++;
	  }
	}
	if (nCrd2 > (pNg2->n / 2)) {
	  pCrd2 = (DtPoint3 *) calloc(nCrd2, sizeof(DtPoint3));
	  nCrd2 = 0;
	  for (aDot = 0; aDot < pNg2->n; aDot++) {
	    if ( (pNg2->atmName[aDot][0] == 'C') || (pNg2->atmName[aDot][0] == 'c') ) {
	      copyPoint3(pNg2->crds[aDot], pCrd2[nCrd2]);
	      nCrd2 ++;
	    }
	  }
	} else {
	  pCrd2 = pNg2->crds;
	  nCrd2 = pNg2->n;
	}

	// QR_superpose(pNg1->crds, pNg1->n, pNg2->crds, pNg2->n, mat); 
	// outSDFMatch(osdf,rep, pNg1, pNg2,"NG1");
	// outSDFMatch(osdf,rep, pNg2, pNg1,"NG2");
	QR_superpose(pCrd1, nCrd1, pCrd2, nCrd2, mat, bc1, ev1);
	  
	/* Now superpose the coordinates */
	for (aDot = 0; aDot < pNg2->n; aDot++) {
	  singleRotate(pNg2->crds[aDot],mat);
	  //    print_vector (c2[aDot], n, "out");
	}
	// outSDFMatch(osdf,rep, pNg2, pNg1,"NG2 QR");

	memcpy(plNg2->crds,pNg2->crds, sizeof(DtPoint3) * pNg2->n);

	if (gFastOnly) {
	  /* Write into PDB file the matching regions */
	  if (opdb != NULL) {
	    // template is unchanged
	    if ((noTemplateCrds == 0) || (isStart)) {
	      //	    fprintf(opdb, "REMARK  REFERENCE OUTPUT\n");
	      outPDBTemplate(opdb, rep, pNg1);
	      isStart = 0;
	    }
	    
	    // bank is moved
	    //	  fprintf(opdb, "REMARK  COMPOUND %d\n",aNuage+1);
	    outPDBMatch(opdb, rep, pNg2, pNg1);
	    // fprintf(stderr,"STORING:\n");
	    // printMat4x4(rep->rM);
	  }
	  
	  if (osdf != NULL) {
#if 0
	    // This needs to be enhanced vs -tOut
	    if ((noTemplateCrds == 0) || (isStart)) {
	      outSDFMatch(osdf,rep, pNg1, pNg2);
	      isStart = 0;
	    }
#endif
	    outSDFMatch(osdf,rep, pNg2, pNg1, "");
	    
	  }
	  continue;
	} 
      }

      rep = csr2(pNg1, pNg2, plNg2,
		 gDMax,
		 dimTab,
		 dist, mat, crdsSup1, crdsSup2, weightSup1, weightSup2,
		 gNIter, curMinAt, gRMSd* gRMSd, gTol, rep, gFast, bc1, ev1, osdf, gVerbose);

      nTries ++;

      if (gVerbose) {
	fprintf(stderr,"CSR search done ...\n");
	fflush(stderr);
      }

      /* We have a positive match */
      if (rep->nLPairs >= curMinAt) {

	
	/* Manage equivalence classes */
	if (gClasses && (rep->nLPairs == pNg2->n) && ((rep->nLPairs == pNg1->n))) {
	  fprintf(stderr,"Bank compound %d same class as query %d\n", aNuage,  aNuageT);
	  class[aNuage] = aNuageT;
	}

	nComps ++;
	// if (rep->nbAtMax >= curMinAt) {
        fprintf(of,"Compound: %s | %s | EC: %s\n",pNg1->id,pNg1->cmpnd,pNg1->EC);
	if (pNg1->nHtGrps) {
	  boucle (i, 0, pNg1->nHtGrps) {
	    fprintf(of,"Catalytic hetero group: %s\n",pNg1->htGrps[i]);
	  }
	}

	hasCnct = computeConnect(&nConnect, &nConnectW, pNg1, pNg2, rep);
        fprintf(of,"Best MSS          : %5d   RMSd : %.8lf \n",rep->nbAtMax,rep->rmsd);

        /* si des poids ont été utilisés : affichage du RMSd enlarge pondéré */
        if (pNg1->weight || pNg2->weight)
	  fprintf(of,"Best Enlarged MSS : %5d   RMSd : %.8lf Weighted RMSd : %.8lf\n", rep->nLPairs, rep->lrmsd, rep->lrmsdWeight);
	else
	  fprintf(of,"Best Enlarged MSS : %5d   RMSd : %.8lf\n", rep->nLPairs, rep->lrmsd);

        /* si la matrice des connexités a été lue : calcul des connexités retrouvées */
        if (hasCnct) {
          fprintf(of,"#EnlgMSSConnct : %5d   Weighted : %5.2lf\n", nConnect, nConnectW);
          fprintf(of,"#Motif connect : %4d   #Bank connect: %4d\n", pNg1->nbConnect, pNg2->nbConnect);
        }

	// fprintf(of,"RMSd       : %.8lf\n",rep->rmsd);
	fprintf(of,"Atomic correspondances: \n");

        /* Affichage de l'id PDB ou du composé Mol2 */
        if (gTempFormat == PDB) {
          stripspace (pNg1->id, buff1);
          buff1[4] = '\0';
        }
        else {
          memcpy(buff1, pNg1->cmpnd, strlen(pNg1->cmpnd)+1);
        }

        if (gBankFormat == PDB) {
          stripspace (pNg2->id, buff2);
          buff2[4] = '\0';
        }
        else {
          memcpy(buff2, pNg2->cmpnd, strlen(pNg2->cmpnd)+1);
        }

        fprintf(of,"   Motif: %s     Query: %s\n", buff1, buff2);
//        fprintf(of,"Motif: %11s Query: %11s\n", buff1, buff2);

	nHits ++;
	boucle(i,0,rep->nLPairs) {
	  i1=(rep->lpairs[i].liaison)%(pNg1->n);
	  i2=(rep->lpairs[i].liaison)/(pNg1->n);

          if (gTempFormat == PDB) {
            fprintf(of, "%3s %4d%c %4s - ", pNg1->resName[i1], pNg1->resNum[i1], pNg1->chn[i1], pNg1->atmNme[i1]);
          }
          else { // MOL2
            fprintf(of, "%5s (%3d ) %4s - ", pNg1->atmNm2[i1], i1+1, pNg1->atmName[i1]);
          }

          if (gBankFormat == PDB) {
            fprintf(of, "%3s %4d%c %4s\n", pNg2->resName[i2], pNg2->resNum[i2], pNg2->chn[i2], pNg2->atmNme[i2]);
          }
          else { // MOL2
            fprintf(of, "%5s (%3d ) %4s\n", pNg2->atmNm2[i2], i2+1, pNg2->atmName[i2]);
          }
	}

	/* Write into PDB file the matching regions */
	// if ( (!gClasses) || ( gClasses && ( (class[aNuage] == -1) || ( (class[aNuage] != -1) && (clsSeed[class[aNuage]] != -1) ) ) ) ) {
	if ( doOutput ) {
	  fprintf(stderr,"Nuages %d vs %d : OUTPUT\n",aNuageT, aNuage);
	  if (opdb != NULL) {
	    // template is unchanged
	    if ((noTemplateCrds == 0) || (isStart)) {
	      //	    fprintf(opdb, "REMARK  REFERENCE OUTPUT\n");
	      outPDBTemplate(opdb, rep, pNg1);
	      isStart = 0;
	    }

	    // bank is moved
	    //	  fprintf(opdb, "REMARK  COMPOUND %d\n",aNuage+1);
	    outPDBMatch(opdb, rep, pNg2, pNg1);
	    // fprintf(stderr,"STORING:\n");
	    // printMat4x4(rep->rM);
	  }

	  if (osdf != NULL) {
#if 1
	    // This needs to be enhanced vs -tOut
	    // if ((noTemplateCrds == 0) || (isStart)) {
	    if ((noTemplateCrds == 0) || (isStart)) {
	      outSDFMatch(osdf,rep, pNg1, pNg2, "");
	      isStart = 0;
	    }
#endif
	    if (gClasses)
	      fprintf(stderr,"OUTPUT %d (gClasses %d class[aNuage] %d)\n", aNuage, gClasses, class[aNuage]);
	    if (class[aNuage] != -1) fprintf(stderr,"   clsSeed[class[aNuage]] %d\n", clsSeed[class[aNuage]]);  
	    outSDFMatch(osdf,rep, pNg2, pNg1, "");
	  }
#if 0
	  if (omol2 != NULL) {
	    outMol2Match(omol2,rep, pNg2, pNg1);
	  }
#endif
	}

#if 0
	// Uncomment in case of doubt on the enlargement process
	fprintf(of,"strict:\n");
	boucle(i,0,rep->nbAtMax) {
	  i1=(rep->dist[i].liaison)%(pNg1->n);
	  i2=(rep->dist[i].liaison)/(pNg1->n);
	  fprintf(of,"%3s %4d%c %4s - %3s %4d%c %4s\n", 
		  pNg1->resName[i1], pNg1->resNum[i1], pNg1->chn[i1], pNg1->atmNme[i1],
		  pNg2->resName[i2], pNg2->resNum[i2], pNg2->chn[i2], pNg2->atmNme[i2]);
	}
#endif
	delimiter(of);
	fflush(of);

	/* rasmol */
	if (rmf != NULL) {
	  // boucle(i,0,rep->nbAtMax) {
	  boucle(i,0,rep->nLPairs) {
	    i1=(rep->dist[i].liaison)%(pNg1->n);
	    // i2=(rep->dist[i].liaison)/(pNg1->n);
	    fprintf(rmf,"select %d%c and *.%s\n",pNg1->resNum[i1],tolower(pNg1->chn[i1]),pNg1->atmNme[i1]);
	    fprintf(rmf,"color %s\n", colors[aNuage%5]);
	  }
#if 0
	  boucle(i,0,rep->nbAtMax) {
	    // i1=(rep->dist[i].liaison)%(pNg1->n);
	    i2=(rep->dist[i].liaison)/(pNg1->n);
	    fprintf(rmf,"select %d%c and *.%s\n",pNg2->resNum[i2],tolower(pNg2->chn[i2]),pNg2->atmNme[i2]);
	    fprintf(rmf,"color blue\n");
	  }
	  fprintf(stderr,"\n");
#endif
	
	  fflush(rmf);
	}
	
#if 1
#endif
	/* pymol */
	if (pmf != NULL) {

	  // rotateCrds(pNg2->crds,pNg2->n,rep->iM);
	  rotateCrds(pNg2->crds,pNg2->n,rep->rM);
	  sprintf(oFName,"CSRout_%d.pdb",aNuage);
	  outPDBNuage(oFName, pNg2);
	  if (gVerbose) {
	    fprintf(stderr,"Writing pmol file for %d atoms\n",rep->nLPairs);
	  }
	  // fprintf(pmf,"load %s, bank\n", IBankFichName);
	  fprintf(pmf,"load %s, bank%d\n", oFName,aNuage);
	  fprintf(pmf,"color gray, bank%d\n",aNuage);
	  boucle(i,0,rep->nLPairs) {
	    i1=(rep->dist[i].liaison)%(pNg1->n);
	    i2=(rep->dist[i].liaison)/(pNg1->n);
	    fprintf(pmf,"select SEL, resi %d and name %s and bank%d\n",pNg2->resNum[i2],pNg2->atmNme[i2],aNuage);
	    fprintf(pmf,"color %s, SEL\n", colors[aNuage%5]);
	    fprintf(pmf,"select SEL, resi %d and name %s and template\n",pNg1->resNum[i1],pNg1->atmNme[i1]);
	    fprintf(pmf,"color magenta, SEL\n");
	  }
	  fflush(pmf);
	}
	
      } else {
	fprintf(stdout, "No MSS FOUND FOR NUAGE %d / %d\n",aNuageT, aNuage);
	// exit(0);
      }

      // printNbPtsApp(ng1,ng2,kds,rep->dist);
      // printDtpDistRec(rep->dist,(rep->nbAtMax)+1,ng1,ng2);


      if (gVerbose) {
	fprintf(stderr,"\n");
	delimiter(stderr);
      }
      free(dist);

      memcpy(pNg2->crds,plNg2->crds, sizeof(DtPoint3) * pNg2->n);
      freeNuage(plNg2);

      // if (gClasses && (class[aNuage] >= 0) ) continue;


    } /* aNuage */

    if (gNoFit) {
      fprintf(stdout,"\n");
    }


  } /* aNuageT*/

  if (gNoFit) {
    exit(0);
  }

  if (gClasses && (doOutput == 0) ) {
    fprintf(of,"Class prototypes:\n");
    for (aNuage = 0; aNuage < nNuagesB; aNuage++) {
      if (aNuage >= gMaxNuage) break;

      /* fprintf(stdout,"Nuage %d class %d\n", aNuage, class[aNuage]); */
      if (class[aNuage] == -1) {
	nClasses ++;
	fprintf(cf,"%d ", aNuage);
	fprintf(of,"%d\n", aNuage);
	for (aNuageT = aNuage; aNuageT < nNuagesT; aNuageT++) {
	  if (aNuageT >= gMaxNuage) break;
	  if (class[aNuageT] == aNuage) {
	    fprintf(cf,"%d ", aNuageT);
	  } 
	}
	fprintf(cf,"\n");

      }
#if 0
      if ((class[aNuage] >= 0) && (class[aNuage] < aNuage)) {
	fprintf(stderr,"%d\n", class[aNuage]);
	clsSeed[class[aNuage]] = -2;
      }
#endif
    }
    if (nClasses == 1) {
      for (aNuage = 0; aNuage < nNuagesB; aNuage++) {
	if (aNuage >= gMaxNuage) break;
	if (class[aNuage] == -1) {
	  pNg1 = &pNgB[aNuage];
	  if (opdb != NULL) {
	    outPDBTemplate(opdb, NULL, pNg1);
	  }
	  
	  if (osdf != NULL) {
	    outSDFMatch(osdf,NULL, pNg1, pNg2, "");
	  }
	  break;
	}
      }
    }
    if (!doOutput) {
      doOutput = 1;
      if (nClasses > 1)
	goto LSTART;
    }
  }

  fprintf(stdout,"Failed: %d\n",nTries-nComps);
  fprintf(stderr, "Average iteration to find best MSS (found %d): %.3lf\n",nComps, (float) gFound / (float) (nComps));

  free(crdsSup1);
  free(crdsSup2);
  free(weightSup1);
  free(weightSup2);

  free(rep->lpairs);
  free(rep->dist);
  free(rep);

  if ((pNgT != pNgB) && (pNgT != NULL)) free(pNgT);
  if (pNgB != NULL) free(pNgB);

  // fprintf(stderr,"EXITING\n");
  // fflush(stderr);
  if (rmf != NULL) {
    fclose(rmf);
  }

  if (pmf != NULL) {
    fclose(pmf);
  }

  if (opdb != NULL) {
    fclose(opdb);
  }

  fprintf(of, "Total of %d hits for the query\n", nHits);
  if ((of != NULL) && (of != stdout))
    fclose(of);

  if(times(&time1) == -1) {
    fprintf(stderr,"\nElapsed time evalutation problem (end)\n");
    exit(0);
  }

  if (gVerbose) {
    fprintf(stderr,"\nElapsed cpu time: %.2lf sec.\n", 
	    (DtFloat) (time1.tms_utime - time0.tms_utime) / CLK_TCK);
  }

  return 0;
}
