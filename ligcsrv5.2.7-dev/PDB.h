/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
#ifndef __PDB_H__
#define __PDB_H__

#include <stdio.h>
#include <string.h>

extern void stripspace(char *o, char *d);
extern void strupper(char *s);
extern void getAtomNum(char *line,int *anum);
extern void getAtomName(char *line,char *anom);
extern void getResName(char *line,char *rnom);
extern void getAtomCoords(char *line,DtFloat *x,DtFloat *y,DtFloat *z);
extern int  getPDBLineStatus(char *d);
extern void getResChainIde(char *line,char *chn);
extern void getResICode(char *line,char *icode);
extern void getResNum(char *line,int *rnum);
extern void outputPDBAtomLine(FILE *f, 
			      int anum, char *aname, char acode, 
			      char *rname, char chn, int rnum, char icode, 
			      double x, double y, double z);
#endif
