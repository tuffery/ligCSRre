/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "3DFuncs.h"
#include "CSRTypes.h"
#include "Zuker.h"


#define max(a,b) (a<b?b:a)
#define min(a,b) (a<b?a:b)
#define sign(a,b) ((b)>=0.0 ? fabs(a) : -fabs(a))
#define EPS 1.e-10

/* ==================================================================
 * XYCov = Calcule la matrice d'inertie des points
 *
 * id      : indices des atomes dans At
 * from,tto: indices extremes
 * P       : le barycentre
 * ==================================================================
 */

static double *product(double **A, double *x, int n) 
{
  int i, j;
  double sum;
  double *y=(double *)calloc(n, sizeof(double));

  for (i=0; i<n; i++) {
    sum=0;
    for (j=0; j<n; j++)
      sum+=A[i][j]*x[j];
    y[i]=sum;
  }
  return y;
}

DtMatrix3x3 *XYCov(DtMatrix3x3 *pM,DtPoint3 *X,DtPoint3 *Y,DtPoint3 Xmean,DtPoint3 Ymean,int aSze)
{
  int i;
  double Xx,Xy,Xz;
  double Yx,Yy,Yz;
  double daSze;

  /*   fprintf(stdout,"inertia, len %d\n",aSze); */

  /* X average*/
  Xmean[0] = Xmean[1] = Xmean[2] = 0.;
  for (i=0;i<aSze;i++) {
    Xmean[0] += X[i][0];
    Xmean[1] += X[i][1];
    Xmean[2] += X[i][2];
  }
  if (aSze) {
    daSze = (double) aSze;
    Xmean[0] /= daSze;
    Xmean[1] /= daSze;
    Xmean[2] /= daSze;
  }

  /* Y average*/
  Ymean[0] = Ymean[1] = Ymean[2] = 0.;
  for (i=0;i<aSze;i++) {
    Ymean[0] += Y[i][0];
    Ymean[1] += Y[i][1];
    Ymean[2] += Y[i][2];
  }
  if (aSze) {
    daSze = (double) aSze;
    Ymean[0] /= daSze;
    Ymean[1] /= daSze;
    Ymean[2] /= daSze;
  }

  /* Covariance matrix */

  if (pM == NULL) {
    pM = (DtMatrix3x3 *) calloc(1,sizeof(DtMatrix3x3));
  } else {
    memset((void *) pM, 0, sizeof(DtMatrix3x3));
  }

  for (i=0;i<aSze;i++) {
    Xx = (double) X[i][0] - Xmean[0];
    Xy = (double) X[i][1] - Xmean[1];
    Xz = (double) X[i][2] - Xmean[2];
    Yx = (double) Y[i][0] - Ymean[0];
    Yy = (double) Y[i][1] - Ymean[1];
    Yz = (double) Y[i][2] - Ymean[2];

    (*pM)[0][0] += Xx*Yx;
    (*pM)[0][1] += Xx*Yy;
    (*pM)[0][2] += Xx*Yz;

    (*pM)[1][0] += Xy*Yx;
    (*pM)[1][1] += Xy*Yy;
    (*pM)[1][2] += Xy*Yz;

    (*pM)[2][0] += Xz*Yx;
    (*pM)[2][1] += Xz*Yy;
    (*pM)[2][2] += Xz*Yz;
  }

  return pM;
}

DtMatrix3x3 *XXCov(DtMatrix3x3 *pM, DtPoint3 *X, DtPoint3 Xmean, int aSze)
{
  int i;
  double Xx,Xy,Xz;
  double daSze;

  /*   fprintf(stdout,"inertia, len %d\n",aSze); */

  /* X average*/
  Xmean[0] = Xmean[1] = Xmean[2] = 0.;
  for (i=0;i<aSze;i++) {
    Xmean[0] += X[i][0];
    Xmean[1] += X[i][1];
    Xmean[2] += X[i][2];
  }
  if (aSze) {
    daSze = (double) aSze;
    Xmean[0] /= daSze;
    Xmean[1] /= daSze;
    Xmean[2] /= daSze;
  }

  fprintf(stderr,"XXCov Mean: %lf %lf %lf\n", Xmean[0], Xmean[1], Xmean[2]);
  /* Covariance matrix */

  if (pM == NULL) {
    pM = (DtMatrix3x3 *) calloc(1,sizeof(DtMatrix3x3));
  } else {
    memset((void *) pM, 0, sizeof(DtMatrix3x3));
  }

  fprintf(stderr,"XXCov zero matrix\n");

  for (i=0;i<aSze;i++) {
    Xx = (double) X[i][0] - Xmean[0];
    Xy = (double) X[i][1] - Xmean[1];
    Xz = (double) X[i][2] - Xmean[2];

    (*pM)[0][0] += Xx*Xx;
    (*pM)[0][1] += Xx*Xy;
    (*pM)[0][2] += Xx*Xz;

    (*pM)[1][0] += Xy*Xx;
    (*pM)[1][1] += Xy*Xy;
    (*pM)[1][2] += Xy*Xz;

    (*pM)[2][0] += Xz*Xx;
    (*pM)[2][1] += Xz*Xy;
    (*pM)[2][2] += Xz*Xz;
  }

  return pM;
}


/* ====================================================================
 * Matrice 4x4 Translation. PREMULTIPLIE -> Y = XM (X vecteur ligne) 
 * ====================================================================
 */

void MkTrnsIIMat4x4(DtMatrix4x4 m, DtPoint3 tr)
{
 m[0][0] = 1.;    m[0][1] = 0.;    m[0][2] = 0.;    m[0][3] = 0.;
 m[1][0] = 0.;    m[1][1] = 1.;    m[1][2] = 0.;    m[1][3] = 0.;
 m[2][0] = 0.;    m[2][1] = 0.;    m[2][2] = 1.;    m[2][3] = 0.;
 m[3][0] = tr[0]; m[3][1] = tr[1]; m[3][2] = tr[2]; m[3][3] = 1.;
}



/* ====================================================================
 * Allocation et libération de l'espace mémoire pour une matrice 4x4.
 * ====================================================================
 */

void free_mat(double **mat, int n)
{
  int i;
  for (i=0; i<n; i++)
    free(mat[i]);
  free(mat);
}

double **alloc_mat(int n, int m) 
{
  int i;
  double **mat = (double **)calloc(m,sizeof(double *));
  for (i=0; i<n; i++)
    mat[i]=(double *)calloc(n, sizeof(double));
  return mat;
}

void copy_mat (double **src, double **dest, int col, int row) {
  int i, j;

  for (i=0; i<col; i++)
    for (j=0; j<col; j++)
      dest[i][j] = src[i][j];
    
}

void copy_mat3 (double src[3][3], double dest[3][3], int col, int row) {
  int i, j;

  for (i=0; i<col; i++)
    for (j=0; j<col; j++)
      dest[i][j] = src[i][j];
    
}

void print_matrice(double **a, int n, int m, char *name){
  int i,j;

  for (i=0; i<n; i++) {
    for (j=0; j<m; j++) 
      printf (" %s[%d,%d]=%lf", name, i, j, a[i][j]);
    printf ("\n");
  }
  printf ("\n");
}

void print_vector(double *a, int n, char *name){
  int i;

  for (i=0; i<n; i++) 
      printf (" %s[%d]=%lf\n", name, i, a[i]);
}

/* =========================================================
 * Calcul de la distance au carré entre deux points.
 * =========================================================
 */

DtFloat squared_distance(DtPoint3 R,DtPoint3 K)
{
  return (DtFloat) ((K[0] - R[0]) * (K[0] - R[0]) +
                    (K[1] - R[1]) * (K[1] - R[1]) +
                    (K[2] - R[2]) * (K[2] - R[2]));
}

double *random_vect(int n)
{
  int i;
  double *v=(double *) calloc(n, sizeof(double));
  for (i=0; i<n; i++)
    v[i]= (double)rand()/(RAND_MAX+1.0);
  return(v);
}

static double inner(double *x, double *y, int n)
{
  int i;
  double sum;

  for (sum=0, i=0; i<n; sum+=x[i]*y[i],i++);
  return sum;

}


/* ===============================================================
 * -------------- Matrice de changement de repere ----------------
 * old reference is pR1
 * new reference is pR2
 * Result is M, usable with  singleRotate(Out)
 * Same common initial origin !!
 * =============================================================== */
void MkReferenceChangeMatrix4x4SameOrigin(DtPoint3 *pR1, DtPoint3 O1, DtPoint3 *pR2, DtPoint3 O2, DtMatrix4x4 M)
{
  DtPoint3 X1, Y1, Z1;
  DtPoint3 X2, Y2, Z2;

  X1[0] = pR1[0][0];
  X1[1] = pR1[1][0];
  X1[2] = pR1[2][0];

  Y1[0] = pR1[0][1];
  Y1[1] = pR1[1][1];
  Y1[2] = pR1[2][1];

  Z1[0] = pR1[0][2];
  Z1[1] = pR1[1][2];
  Z1[2] = pR1[2][2];

  X2[0] = pR2[0][0];
  X2[1] = pR2[1][0];
  X2[2] = pR2[2][0];

  Y2[0] = pR2[0][1];
  Y2[1] = pR2[1][1];
  Y2[2] = pR2[2][1];

  Z2[0] = pR2[0][2];
  Z2[1] = pR2[1][2];
  Z2[2] = pR2[2][2];

  M[0][0] = inner( X1, X2, 3 );
  M[1][0] = inner( X1, Y2, 3 );
  M[2][0] = inner( X1, Z2, 3 );
  M[0][1] = inner( Y1, X2, 3 );
  M[1][1] = inner( Y1, Y2, 3 );
  M[2][1] = inner( Y1, Z2, 3 );
  M[0][2] = inner( Z1, X2, 3 );
  M[1][2] = inner( Z1, Y2, 3 );
  M[2][2] = inner( Z1, Z2, 3 );
  M[3][0] = O2[0] - O1[0];
  M[3][1] = O2[1] - O1[1];
  M[3][2] = O2[2] - O1[2];
  M[0][3] = 0.;
  M[1][3] = 0.;
  M[2][3] = 0.;
  M[3][3] = 0.;

#if 0
  printf("MkReferenceChangeMatrix4x4: \n");
  printf(" %lf %lf %lf %lf\n",M[0][0],M[0][1],M[0][2],M[0][3]);
  printf(" %lf %lf %lf %lf\n",M[1][0],M[1][1],M[1][2],M[1][3]);
  printf(" %lf %lf %lf %lf\n",M[2][0],M[2][1],M[2][2],M[2][3]);
  printf(" %lf %lf %lf %lf\n",M[3][0],M[3][1],M[3][2],M[3][3]);
#endif

}


double best_shift(double *a[], int n)
{
  double m, M, s;
  double t, sum;
  int i, j;
  t=a[0][0];
  for (i=1; i<n; i++) t=max(t, a[i][i]);
  M=t;
  t=a[0][0];
  for (i=0; i<n; i++) {
    for (sum=0,j=0; j<n; j++)
      if (j!=i) sum+=fabs(a[i][j]);
    t=min(t, a[i][i]-sum);
  }
  m=t;
  s=-0.5*(M+m);
  for (i=0; i<n; i++)
    a[i][i]=a[i][i]+s;
  return s;
}


int shift_power(double *a, int n, int maxiter, double eps, double *v, double *w)
{
  double **tmp;
  double sh;
  int niter;
  int i,j;

  tmp=alloc_mat(n, n);
  /* copyMat(a, tmp, n, n); */
  for (i=0; i<n; i++) {
    for (j=0; j<n; j++) {
      tmp[i][j] = a[i*n+j];
    }
  }
  sh=best_shift(tmp, n);

  niter=power(tmp, n, maxiter, eps, v, w);
  *v=*v-sh;
  free_mat(tmp, n);
  return niter;
}

double lmax_estim (double a[4][4], int n)
{
  double t, sum;
  int i, j;
  t=a[0][0];
  for (i=0; i<n; i++) {
    for (sum=0,j=0; j<n; j++)
      if (j!=i) sum+=fabs(a[i][j]);
    t=max(t, a[i][i]+sum);
  }
  return t;
}

static int lu_c (double a[4][4],  int n)
{
 int i,j,k,err;
 double pivot,coef;

 err=1;

 k=0;
 while (err==1 && k<n) {
  pivot=a[k][k];
  if(fabs(pivot)>=EPS) {
    for(i=k+1;i<n;i++) {
      coef=a[i][k]/pivot;
      for(j=k;j<n;j++)
        a[i][j] -= coef*a[k][j];
      a[i][k]=coef;
    }
  }
  else err=0;
  k++;
 }
 if(a[n-1][n-1]==0) err=0;
 return err;
}

static void resol_lu(double a[4][4], double *b, int n)
{
 int i,j;
 double sum;
 double y[n];
 y[0]=b[0];
 for(i=1;i<n;i++) {
  sum=b[i];
  for(j=0;j<i;j++)
    sum-=a[i][j]*y[j];
  y[i]=sum;
 }
 b[n-1]=y[n-1]/a[n-1][n-1];
 for(i=n-1;i>=0;i--) {
  sum=y[i];
  for(j=i+1;j<n;j++)
     sum-=a[i][j]*b[j];
  b[i]=sum/a[i][i];
 }
}

int inverse_power(double a[4][4], int n, int maxiter, double eps, double *v, double *w)
{
  int niter,i;
  double *y;
  double r, sum, l, normy, d;
  y=random_vect(n);
  niter=0;

  r=lmax_estim(a, n);
  for (i=0; i<n; i++) a[i][i]=a[i][i]-r;
  if (lu_c(a, n)==0) {
    /* fprintf(stderr,"ATTENTION ! cas singulier de inverse_power\n"); */
    free(y);     //exit(0);
    return 0;
  }

  do {
    normy=sqrt(inner(y,y,n));
    for (i=0; i<n; i++) {
      w[i]=y[i]/normy;
      y[i]=w[i];
    }

    resol_lu(a, y, n);
    l=inner(w,y,n);
    niter++;
    for (sum=0,i=0; i<n; i++) {
      d=y[i]-l*w[i];
      sum+=d*d;
    }
    d=sqrt(sum);
  } while (d>eps*fabs(l) && niter<maxiter);
  free(y);
  *v=r+1.0/l;
  return niter;
}


int power(double *a[], int n, int maxiter, double eps, double *v, double *w)
{
  int niter,i;
  double *y;
  double sum, l, normy, d;
  y=random_vect(n);
  niter=0;
  do {
    normy=sqrt(inner(y,y,n));
    for (i=0; i<n; i++) w[i]=y[i]/normy;
    y=product(a, w, n);
    l=inner(w,y,n);
    niter++;
    for (sum=0,i=0; i<n; i++) {
      d=y[i]-l*w[i];
      sum+=d*d;
    }
    d=sqrt(sum);
  } while (d>eps*fabs(l) && niter<maxiter);
  free(y);
  *v=l;
  return niter;
}





/* ===================================================================
 * Fonctions utilitaires pour l'algorithme de superposition de Zuker.
 * ====================================================================
 */

int largestEV4(double R[4][4], double v[4], double *vp)
{

  double M2[4][4];
  int rs;

  memcpy(M2,R,sizeof(double)*16);

  rs = inverse_power(R, 4, 10000, 1.e-8, vp, v);

  if (!rs)
    return shift_power(&M2[0][0], 4, 10000, 1.e-8, vp, v);

  return rs;

}

/* =============== ALGORITHME DE SUPERPOSITION (ZUKER) =======================
 * Best fit matrix as proposed by:
 * Zuker & Somorjai, Bulletin of Mathematical Biology,
 * vol. 51, No 1, p 55-78, 1989.
 *
 * Returns M, a transformation matrix ready for use
 * ===========================================================================
 */
// double zuker_superpose(DtPoint3 *c1, DtPoint3 *c2, int len, DtMatrix4x4 M)
// {
//   DtMatrix3x3  C;
//   DtMatrix3x3 *pC;
//   DtMatrix4x4  RM;
//   DtMatrix4x4  TMP;
//   DtMatrix4x4  TX;
//   DtMatrix4x4  TY;
//   DtMatrix4x4  P;
//   DtPoint4     V;
//   DtPoint3     bc1, bc2;
//   DtPoint3     try;
// 
//   double eval;
//   double squared_rms = 0.;
// 
//   int nCycles;
//   int aDot;
// 
//   /* Compute transformation matrix as proposed by zuker */
// 
//   pC = &C;
//   pC = XYCov(pC, (DtPoint3 *) c1, (DtPoint3 *) c2, bc1, bc2, len);
// 
//   P[0][0] = -C[0][0]+C[1][1]-C[2][2];
//   P[0][1] = P[1][0] = -C[0][1]-C[1][0];
//   P[0][2] = P[2][0] = -C[1][2]-C[2][1];
//   P[0][3] = P[3][0] =  C[0][2]-C[2][0];
// 
//   P[1][1] = C[0][0]-C[1][1]-C[2][2];
//   P[1][2] = P[2][1] = C[0][2]+C[2][0];
//   P[1][3] = P[3][1] = C[1][2]-C[2][1];
// 
//   P[2][2] = -C[0][0]-C[1][1]+C[2][2];
//   P[2][3] = P[3][2] = C[0][1]-C[1][0];
// 
//   P[3][3] = C[0][0]+C[1][1]+C[2][2];
// 
//   /* #if 0
//   printMat4x4("zuker P", P);
// #endif */
// 
//   nCycles = largestEV4(P, V, &eval);
// 
//   RM[0][0] = -V[0]*V[0]+V[1]*V[1]-V[2]*V[2]+V[3]*V[3];
//   RM[1][0] =  2*(V[2]*V[3]-V[0]*V[1]);
//   RM[2][0] =  2*(V[1]*V[2]+V[0]*V[3]);
//   RM[3][0] =  0.;
// 
//   RM[0][1] = -2*(V[0]*V[1]+V[2]*V[3]);
//   RM[1][1] = V[0]*V[0]-V[1]*V[1]-V[2]*V[2]+V[3]*V[3];
//   RM[2][1] =  2*(V[1]*V[3]-V[0]*V[2]);
//   RM[3][1] =  0.;
// 
//   RM[0][2] =  2*(V[1]*V[2]-V[0]*V[3]);
//   RM[1][2] = -2*(V[0]*V[2]+V[1]*V[3]);
//   RM[2][2] = -V[0]*V[0]-V[1]*V[1]+V[2]*V[2]+V[3]*V[3];
//   RM[3][2] =  0.;
// 
//   RM[0][3] =  0.;
//   RM[1][3] =  0.;
//   RM[2][3] =  0.;
//   RM[3][3] =  1.;
//   /* printMat4x4("zuker RM", RM); */
// 
//   /* Solution eprouvee ! */
//   try[0] = - bc2[0]; try[1] = - bc2[1]; try[2] = - bc2[2];
//   MkTrnsIIMat4x4(TY, try);
//   MkTrnsIIMat4x4(TX, bc1);
//   mulMat4x4(TY,RM,TMP);
//   mulMat4x4(TMP, TX, M);
// 
//   /* Now superpose the coordinates */
//   for (aDot=0;aDot<len;aDot++) {
//     singleRotate(c2[aDot],M);
//   }
// 
//   /* Compute squared RMSd */
//   for (aDot=0;aDot<len;aDot++) {
//     squared_rms += squared_distance(c1[aDot],c2[aDot]);
//   }
// 
//   return squared_rms / (double) len;
// }

double zuker_superpose_weighted(DtPoint3 *c1, DtPoint3 *c2, int len, DtMatrix4x4 M,
                                double *w1, double *w2)
{
  DtMatrix3x3  C;
  DtMatrix3x3 *pC;
  DtMatrix4x4  RM;
  DtMatrix4x4  TMP;
  DtMatrix4x4  TX;
  DtMatrix4x4  TY;
  DtMatrix4x4  P;
  DtPoint4     V;
  DtPoint3     bc1, bc2;
  DtPoint3     try;

  double eval;
  double squared_rms = 0.;
  double wSum = 0.;

  int nCycles;
  int aDot;

  /* Compute transformation matrix as proposed by zuker */

  pC = &C;
  pC = XYCov(pC, (DtPoint3 *) c1, (DtPoint3 *) c2, bc1, bc2, len);

  P[0][0] = -C[0][0]+C[1][1]-C[2][2];
  P[0][1] = P[1][0] = -C[0][1]-C[1][0];
  P[0][2] = P[2][0] = -C[1][2]-C[2][1];
  P[0][3] = P[3][0] =  C[0][2]-C[2][0];

  P[1][1] = C[0][0]-C[1][1]-C[2][2];
  P[1][2] = P[2][1] = C[0][2]+C[2][0];
  P[1][3] = P[3][1] = C[1][2]-C[2][1];

  P[2][2] = -C[0][0]-C[1][1]+C[2][2];
  P[2][3] = P[3][2] = C[0][1]-C[1][0];

  P[3][3] = C[0][0]+C[1][1]+C[2][2];

  /* #if 0
  printMat4x4("zuker P", P);
#endif */

  nCycles = largestEV4(P, V, &eval);

  RM[0][0] = -V[0]*V[0]+V[1]*V[1]-V[2]*V[2]+V[3]*V[3];
  RM[1][0] =  2*(V[2]*V[3]-V[0]*V[1]);
  RM[2][0] =  2*(V[1]*V[2]+V[0]*V[3]);
  RM[3][0] =  0.;

  RM[0][1] = -2*(V[0]*V[1]+V[2]*V[3]);
  RM[1][1] = V[0]*V[0]-V[1]*V[1]-V[2]*V[2]+V[3]*V[3];
  RM[2][1] =  2*(V[1]*V[3]-V[0]*V[2]);
  RM[3][1] =  0.;

  RM[0][2] =  2*(V[1]*V[2]-V[0]*V[3]);
  RM[1][2] = -2*(V[0]*V[2]+V[1]*V[3]);
  RM[2][2] = -V[0]*V[0]-V[1]*V[1]+V[2]*V[2]+V[3]*V[3];
  RM[3][2] =  0.;

  RM[0][3] =  0.;
  RM[1][3] =  0.;
  RM[2][3] =  0.;
  RM[3][3] =  1.;
  /* printMat4x4("zuker RM", RM); */

  /* Solution eprouvee ! */
  try[0] = - bc2[0]; try[1] = - bc2[1]; try[2] = - bc2[2];
  MkTrnsIIMat4x4(TY, try);
  MkTrnsIIMat4x4(TX, bc1);
  mulMat4x4(TY, RM, TMP);
  mulMat4x4(TMP, TX, M);

  /* Now superpose the coordinates */
  for (aDot = 0; aDot < len; aDot++) {
    singleRotate(c2[aDot],M);
  }

  /* Compute squared RMSd */
  for (aDot = 0; aDot < len; aDot++) {
    squared_rms += squared_distance(c1[aDot],c2[aDot]) * w1[aDot] * w2[aDot];
    wSum += w1[aDot] * w2[aDot];
  }

  return squared_rms / wSum;
}


/* ===========================================================
 * QR section to get all eigen vectors
 * ===========================================================
 */

void qrdcmp3(double a[3][3], double *c, double *d, int n, int *sing){
  int i,j,k;
  double scale=0.0, sigma, sum, tau;
  
  *sing=0;
  for (k=0;k<(n-1);k++) {
    for (i=k;i<n;i++) scale = max(scale, fabs(a[i][k]));
    if (scale==0.0) {
      *sing=1;
      c[k]=d[k]=0.0;
    }
    else {
      for (i=k; i<n; i++) a[i][k]/=scale;
      for (sum=0.0,i=k;i<n;i++) sum+=(a[i][k]*a[i][k]);
      sigma=sign(sqrt(sum), a[k][k]);
      a[k][k]+=sigma;
      c[k]=sigma*a[k][k];
      d[k]=-scale*sigma;
      for (j=k+1;j<n;j++) {
	for (sum=0.0,i=k;i<n;i++) sum+= a[i][k]*a[i][j];
	tau=sum/c[k];
	for (i=k;i<n;i++) a[i][j] -= tau*a[i][k];
      }
    }
  }
  d[n-1]=a[n-1][n-1];
  if (d[n-1]== 0.0) *sing=1;
}



void RQ3(double r[3][3], double a[3][3], double *c, double *d, int n) {
  int i,j,k,l;
  double sum;
  double *tmp;

  tmp = (double *) calloc(n, sizeof(double));
  for (k=0;k<n;k++) {
    for (l=0;l<k;l++)
      r[k][l]=0;
    r[k][k]=d[k];
    for (l=k+1;l<n;l++)
      r[k][l]=a[k][l];
  }
  for (j=0;j<(n-1);j++) {
      for (k=0; k<n; k++) {
	for (l=j; l<n; l++) {
	  sum=0.;
	  for (i=j;i<n;i++)
	    sum+=r[k][i]*a[i][j];
	  tmp[l]=r[k][l]-sum*a[l][j]/c[j];
	}
	for (l=j; l<n; l++)
	  r[k][l]=tmp[l];
      }
  }
  free(tmp);
}

void PQ3(double p[3][3], double a[3][3], double *c, double *d, int n) {
  int i,j,k,l;
  double sum;
  double *tmp;

  tmp = (double *) calloc(n, sizeof(double));
  for (j=0;j<(n-1);j++) {
      for (k=0; k<n; k++) {
	for (l=j; l<n; l++) {
	  sum=0.;
	  for (i=j;i<n;i++)
	    sum+=p[k][i]*a[i][j];
	  tmp[l]=p[k][l]-sum*a[l][j]/c[j];
	}
	for (l=j; l<n; l++)
	  p[k][l]=tmp[l];
      }
  }
  free(tmp);
}

int isDiag3(double a[3][3], int n, double eps){
  int i,j;
  double sum=0;

  for (i=0; i<n; i++)
    for (j=0; j<n; j++)
      if (i != j) sum+=fabs(a[i][j]);

  if (sum<=eps) return 1;
  return 0;
}


/* =====================================================
 * Take a matrix as input a, size n
 * Iterate at max maxiter, to get a precision of eps
 * return vector of eigen values, matrix of eigen vectors (vector by column)
 * =====================================================
 */
int qr_eigen3(double a[3][3], int n, int maxiter, double eps, double *v, double w[3][3]) {
  int i,j,k;
  double c[n], d[n];
  double tmp1[3][3], tmp2[3][3];
  int sing;

  for (i=0; i<n; i++)
    for (j=0; j<n; j++)
      w[i][j]=0;

  for (i=0; i<n; i++) {
    w[i][i]=1;
  }
  k=0;
  //  tmp1=alloc_mat(n, n);
  //  tmp2=alloc_mat(n, n);
  copy_mat3 (a, tmp1, n, n);
  do {
    k++;
    qrdcmp3(tmp1, c, d, n, &sing);
    RQ3(tmp2, tmp1, c, d, n);
    PQ3(w, tmp1, c, d, n);
    copy_mat3(tmp2, tmp1, n, n);
  } while (!isDiag3(tmp2, n, eps) && k<maxiter);

  for (i=0; i<n; i++)
    v[i]=tmp2[i][i];

  //  free_mat(tmp1,n);
  //  free_mat(tmp2,n);
  return k;
}


/* =====================================================
 * Superpose 2 clouds of dots according to eigen vectors
 * 
 * return a transformation matrix M to superpose c2 on c1
 * also return everything to evolve in the c1 eigen vector reference.
 * =====================================================
 */
void QR_superpose(DtPoint3 *c1, int l1, DtPoint3 *c2, int l2, DtMatrix4x4 M, DtPoint3 bc1, DtMatrix3x3 EV1)
{
  DtMatrix3x3  C1;
  DtMatrix3x3  C2;
  // DtMatrix3x3  EV1;
  DtMatrix3x3  EV2;
  DtMatrix3x3 *pC;
  DtMatrix4x4  RM;
  DtMatrix4x4  TMP;
  DtMatrix4x4  TX;
  DtMatrix4x4  TY;
  DtMatrix4x4  P;
  DtMatrix4x4  EV1_4x4;
  DtMatrix4x4  EV2_4x4;
  DtMatrix4x4  EV2t_4x4;

  DtPoint4     V;
  DtPoint3     bc2;
  DtPoint3     v1, v2;
  DtPoint3     av1, av2, av3;
  DtPoint3     try;
  
  double eval;
  double squared_rms = 0.;
  double wSum = 0.;
  double eps = 1.e-8;

  int nCycles;
  int aDot;
  int maxIter = 1000;
  int nIter;

  int n = 3;

  //  fprintf(stderr,"QR_superpose\n");
  /* Inertia matrices */
#if 0
  pC = &C1;
  pC = XXCov(pC, (DtPoint3 *) c1, bc1, l1);
  pC = &C2;
  pC = XXCov(pC, (DtPoint3 *) c2, bc2, l2);
#endif

  pC = &C1;
  pC = XYCov(pC, (DtPoint3 *) c1, (DtPoint3 *) c1, bc1, bc1, l1);
  pC = &C2;
  pC = XYCov(pC, (DtPoint3 *) c2, (DtPoint3 *) c2, bc2, bc2, l2);
  // fprintf(stderr,"QR_superpose XXCov done ...\n");

  //  printMat3x3(C1);
  //  printMat3x3(C2);
  //  print_matrice((double **)C1, n, n, "C1");
  //  print_matrice((double **)C2, n, n, "C2");

  /* QR eigen vectors */
  nIter = qr_eigen3(C1, 3, maxIter, eps, v1, EV1);
  if (nIter == maxIter)
    fprintf(stderr,"Warning: QR 1 did not converge for %d iterations\n", nIter);
  nIter = qr_eigen3(C2, 3, maxIter, eps, v2, EV2);
  if (nIter == maxIter)
    fprintf(stderr,"Warning: QR 2 did not converge for %d iterations\n", nIter);

  // fprintf(stderr,"QR_superpose qr_eigen done ...\n");

  //  printMat3x3(C1);
  //  print_vector (v1, n, "V1");
  //  printMat3x3(EV1);

  //  printMat3x3(C2);
  //  print_vector (v2, n, "V2");
  //  printMat3x3(EV2);

  /* Crds superimposition */
  /* Setup references orthonormal and same hand */
  /* ref 1 */
  transpMat3x3(EV1);
  normalize(EV1[0]);
  normalize(EV1[1]);
  cross_product(EV1[0], EV1[1], EV1[2]);

  transpMat3x3(EV2);
  normalize(EV2[0]);
  normalize(EV2[1]);
  cross_product(EV2[0], EV2[1], EV2[2]);

  copy3x3to4x4(EV1, EV1_4x4);
  copy3x3to4x4(EV2, EV2_4x4);

  copyMat4x4(EV2_4x4, EV2t_4x4);
  transpMat4x4(EV2t_4x4);

  /* Solution eprouvee ! */
  try[0] = - bc2[0]; try[1] = - bc2[1]; try[2] = - bc2[2];
  MkTrnsIIMat4x4(TY, try);
  MkTrnsIIMat4x4(TX, bc1);
  mulMat4x4(EV2t_4x4, EV1_4x4, RM);

  /* Reference 2 onto 1: we want R such that EV2 . R = EV1 */
  /* So R = [EV2]-1 . EV1 = EV2t . EV1                     */

  mulMat4x4(TY, RM, TMP);
  mulMat4x4(TMP, TX, M);

  /* We prepare EV1 so that it is relative to bc1 */
  simpleTranslate(EV1[0], bc1);
  simpleTranslate(EV1[1], bc1);
  simpleTranslate(EV1[2], bc1);

}

#if 0
main() {
  int n = 4;

  double v[n];
  //  double **w = alloc_mat(n,n);
  //  double **a = alloc_mat(n,n);
  DtMatrix4x4 M;

  DtPoint3 a[6] = {
    { -3.2935,   -2.7085,   -1.9383  },
    { -2.5642,   -0.8745,   -0.5136  },
    { -2.4483,   -2.3066,   -3.1449  },
    { -1.6298,   -0.2476,   -1.5499  },
    { -3.7146,   -1.5534,   -1.1308  },
    { -0.9037,   -1.5020,   -2.6418  }
  };

  DtPoint3 b[6] = {
    {  1.7239,   -1.5945,    3.5546 },
    {  0.5148,   -2.3981,    4.0199 },
    { -0.1811,   -2.3661,    1.3762 },
    {  1.1073,   -1.5642,    1.2081 },
    {  1.4409,   -0.7606,    2.3845 },
    { -0.0466,   -3.5559,    2.7400 }
  };


  QR_superpose(a, 6, b, 6, M);

  fprintf(stdout,"HEADER\n");
  fprintf(stdout,"MODEL\n");
  for (n = 0; n < 6; n++) {
    fprintf(stdout,"ATOM  %5d %.4s%c%.3s %c%4d%c   %8.3lf%8.3lf%8.3lf%7.3lf%7.3lf   \n", n,"CA", ' ', "ALA", ' ',n,' ', a[n][0], a[n][1], a[n][2]);
  }

  fprintf(stdout,"ENDMDL\n");
  fprintf(stdout,"MODEL\n");
  for (n = 0; n < 6; n++) {
    fprintf(stdout,"ATOM  %5d %.4s%c%.3s %c%4d%c   %8.3lf%8.3lf%8.3lf%7.3lf%7.3lf   \n", n,"CA", ' ', "ALA", ' ',n,' ',b[n][0], b[n][1], b[n][2]);
  }
  fprintf(stdout,"ENDMDL\n");
  fprintf(stdout,"END\n");

  //  free_mat(w, n);
  //  free_mat(a, n);
}
#endif
