/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
#ifndef _SDF_H_
#define _SDF_H_

extern void outSDFMatch(FILE *fd, DtReponse *rep, DtNuage *pNg, DtNuage *pNg1, char *msg);
extern void lectSDFCsr(char *fname, int what, int strict, DtNuage **pN, int *onNuages,
		       DtReRec **pReList, int verbose, DtRuleList *pRuleList);

#endif _SDF_H_
