/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
#include <stdlib.h>
#include <time.h>

/* =====================================================================
 * Initialize pseudo-random generator.
 * Returns the seed value.
 * =====================================================================
 */

void initRnd(long int *seed)
{
  static int isInit = 0;

  if (isInit) return;
  if ((int) *seed == -1) {
    *seed = (long int) time(0);
    srand48(*seed);
  }
  else srand48(*seed);
  isInit = 1;
}

/* ================================================================
 * Returns a random integer from 0 to n (excluded).
 * ================================================================
 */

int irand(int n)
{
  extern double drand48();
  double rn;
  float rnf;

  do {
    rn   = drand48();           /* suppose drand48 does not accept 1.0  */
    rnf  = (float) rn * n;
  } while ((int) rnf >= n);

  return (int) rnf;
}

