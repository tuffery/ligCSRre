/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
#ifndef __CSRTYPES_H__
#define __CSRTYPES_H__

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/times.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <regex.h>

#define boucle(i,min,max) for((i)=(min);(i)<(max);(i)++)
#define max(a,b) (a<b?b:a)
#define min(a,b) (a<b?a:b)
#define DtFloat         double
#define DcPI 3.1415926535
#define EPS 1.e-10

typedef double DtMatrix3x3[3][3];
typedef double DtMatrix4x4[4][4];
typedef double DtPoint4[4];
typedef double DtPoint3[3]; /* (x,y,z) */
typedef DtPoint3 *DtCrds;

#define DcBB 1
#define DcSC 2
#define DcALL 3

#define PDB  1
#define MOL2 2
#define SDF  4

typedef char DtStr2[3];
typedef char DtStr3[4];
typedef char DtStr4[5];
typedef char DtStr5[6];

typedef struct sReRec {
  char          *reAtmStr; /* La chaine pour definir la regex (atome)    */
  char          *reResStr; /* La chaine pour definir la regex (residu)   */
  regex_t       *pAtmRe;   /* Atom regex */
  regex_t       *pResRe;   /* Res  regex */
  char          *atmMask;  /* Les crds a considerer dans le nuage courant */
  int            nMask;
  struct sReRec *next;
} DtReRec;

typedef struct sNuage {
  int n;               /* Nombre de points */
  char     *id;        /* Identifiant */
  char     *header;    /* Header PDB */
  char     *cmpnd;     /* Nom du compose (s'il y a) */
  char     *EC;        /* EC number (s'il y a) */
  int       nHtGrps;
  char    **htGrps;    /* Heteros in motif if specified */
  DtPoint3  bc;        /* Barycentre  */
  DtPoint3 *crds;      /* Coordonnees */
  DtStr4   *atmName;   /* Nom atome   */
  DtStr4   *atmNme;    /* Nom atome (PDB)  */
  DtStr5   *atmNm2;    /* Nom atome (mol2) */
  int      *atmNum;    /* Numero residu  */
  DtStr3   *resName;   /* Nom residu  */
  int      *resNum;    /* Numero residu  */
  char     *chn;       /* Nom chaine  */
  char     *skip;      /* La coordonnee est-elle a considerer ? */
//  char     *reLcl;     /* RE imposed in PDB file */
  DtReRec **re;        /* regex associee a la crd (if any) */
  double   *weight;    /* poids associe a chaque atome (si differents de 1) */
  char    **connectMatrix; /* Matrice des connexités */
  int       nbConnect; /* Nombre de connexités */
  int       nLines;
  char    **lines;     /* Text of compound input, for mol2 and sdf output */
} DtNuage;

typedef struct sNuagesPts {
  int n1; /*  nombre de points i du nuage 1 */
  DtPoint3 *crds1; /* tableau de coordonnees des points de 1 */


  int n2; /* nombre de points j du nuage 2 */
  DtPoint3 *crds2; /*tableau de coordonnees des points de 2 */
} *DtNuagesPts;

typedef struct sNuagesAt {
  DtNuagesPts points;

  char **typeAt1;
  char **typeRes1;
  int *numAt1;

  char **typeAt2;
  char **typeRes2;
  int *numAt2;
} *DtNuagesAt;

typedef struct sDist {
//  double longueur;  /* Distance entre atomes */
  double longueur2;  /* Distance au carré entre atomes */
  int    liaison;   /* code paire d'atomes   */
} DtDistRec;

typedef DtDistRec *DtpDistRec;

typedef struct sReponse {
  int nbAtMax; /* nombre d'atomes apparies maximum */
  DtDistRec *dist; /* tableau des distances et des appariements */
  double rmsd; /* somme minimale des carres des distances interatomiques */
  DtMatrix4x4 iM; /* matrice 4x4 de positionnement aleatoire initial */
  DtMatrix4x4 rM; /* matrice 4x4 de best fit selon les matches */
  int nLPairs;
  DtDistRec *lpairs;  /* Enlarged pairing */
  double     lrmsd;   /* Enlarged RMSd    */
  double     lrmsdWeight; /* Enlarged weighted RMSd */
} DtReponse;

typedef struct sListe_Reponses {
  DtReponse rep;
  struct sListe_Reponses *suiv;
} *DtListRep;

typedef struct sListe_atomes{
  int at; /* numero de l'atome dans son nuage */
  struct liste_atomes *suiv; /* pointeur vers l'atome suivant */
} *DtAtList;

typedef struct sListe_atomRe { /* utilisé pour le format mol2 */
  DtReRec *re;                /* regex associée */
  int atom;                   /* numéro de l'atome associé */
  struct sListe_atomRe *next; /* association regex-atome suivante*/
} DtAtReList;

typedef struct sList_rule { /* utilisé pour le format mol2 */
  DtStr4 atmName;          /* nom formaté de l'atome */
  char re[BUFSIZ];                /* regex associée */
  struct sList_rule *next;
} DtRuleList;

#endif
