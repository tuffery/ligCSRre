/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
#ifndef __3DFUNCS_H__
#define __3DFUNCS_H__


#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "CSRTypes.h"

extern double  distance(DtPoint3 R, DtPoint3 K);
extern void    cross_product(DtPoint3 a, DtPoint3 b, DtPoint3 c);
//extern int     cmpdist(void *dist, void *dist2);
//extern int     cmpdistRec(const void *dist, const void *dist2);
extern int     cmpdistRec2(const void *dist, const void *dist2);
extern void    printPoint3Crds(DtPoint3 *crds, int i);
extern void    mulMat4x4(DtMatrix4x4 A,DtMatrix4x4 B,DtMatrix4x4 C);
extern void    MkArbitraryAxisRotMat4x4(DtPoint3 A, DtPoint3 B, DtFloat angle, DtMatrix4x4 M );
extern DtFloat norme(DtPoint3 a);
extern void    MkNullMat4x4(DtMatrix4x4 m);
extern void    printMat4x4(DtMatrix4x4 m);
extern void    printMat3x3(DtMatrix3x3 m);
extern void    singleRotate(DtPoint3 p, DtMatrix4x4 rmat);
extern void    rotateCrds(DtCrds crds2, int nbPoints, DtMatrix4x4 rmat);
extern void    simpleTranslate(DtPoint3 p, DtPoint3 tr);
extern void    transpMat3x3(DtMatrix3x3 m);
extern void    negatePoint3(DtPoint3 p1);
extern void    copyPoint3(DtPoint3 p1, DtPoint3 p2);
extern void    copyMat4x4(DtMatrix4x4 a,DtMatrix4x4 b);
extern void    copy3x3to4x4(DtMatrix3x3 a,DtMatrix4x4 b);
extern void    normalize(DtPoint3 a);
extern void    MkNumXRotMat4x4(DtPoint3 A,DtFloat angle,DtMatrix4x4 M);
extern void    MkNumDRotMat4x4(DtPoint3 A,DtFloat angle,DtMatrix4x4 M);

#endif
