/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/times.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include "CSRTypes.h"
#include "PDB.h"
#include "Random.h"
#include "TxtFile.h"
#include "Zuker.h"
#include "regex.h"
#include "mol2.h"
#include "3DFuncs.h"

// Atom pairs, (-1 = BC), teta, dir
#define DcNDrawings 200

int gFound = 0;

/* =====================================================
 * Allocation memoire d'un nuage pour une taille donnee
 * =====================================================
 */
DtNuage *allocNuage(int lsze)
{
  DtNuage *pNg;

  //  fprintf(stderr,"allocNuage size %d\n", lsze);
  pNg           = (DtNuage *)  calloc(1,sizeof(DtNuage));
  //  fprintf(stderr,"allocNuage size %d 1\n", lsze);
  pNg->crds     = (DtPoint3 *) calloc(lsze,sizeof(DtPoint3));
  pNg->atmName  = (DtStr4 *)   calloc(lsze,sizeof(DtStr4));
  pNg->atmNme   = (DtStr4 *)   calloc(lsze,sizeof(DtStr4));
  pNg->resName  = (DtStr3 *)   calloc(lsze,sizeof(DtStr3));
  pNg->resNum   = (int *)      calloc(lsze,sizeof(int));
  pNg->chn      = (char *)     calloc(lsze,sizeof(char));
  pNg->skip     = (char *)     calloc(lsze,sizeof(char));
//  pNg->reLcl    = (char *)     calloc(lsze,sizeof(char));
  pNg->re       = (DtReRec **) calloc(lsze,sizeof(DtReRec *));
  pNg->n        = lsze;

  return pNg;
}

/* =====================================================
 * Allocation memoire d'un nuage pour une taille donnee
 * =====================================================
 */
void freeNuage(DtNuage *pNg)
{

  if (pNg != NULL) {
    if (pNg->crds    != NULL) free(pNg->crds);
    if (pNg->atmName != NULL) free(pNg->atmName);
    if (pNg->atmNme  != NULL) free(pNg->atmNme);
    if (pNg->resName != NULL) free(pNg->resName);
    if (pNg->resNum  != NULL) free(pNg->resNum);
    if (pNg->chn     != NULL) free(pNg->chn);
    if (pNg->skip    != NULL) free(pNg->skip);
    if (pNg->re      != NULL) free(pNg->re);
    freeConnectMatrix(pNg->connectMatrix, pNg->n);
    free(pNg);
  }
}

/* ============================================================================
 * Recherche d'une re a partir des re atomes et res.
 * Retourne NULL si aucun match.
 * ============================================================================
 */
DtReRec *rePtrFromStr(DtReRec *pReList, char *atmStrRe, char *resStrRe)
{
  while (pReList != NULL) {
    if ((!strcmp(atmStrRe, pReList->reAtmStr)) && (!strcmp(resStrRe, pReList->reResStr)) ) {
      return pReList;
    }
    pReList = pReList->next;
  }
  return NULL;
}

int rePtrListLen(DtReRec *pReList)
{
  int n = 0;
  while (pReList != NULL) {
    n++;
    pReList = pReList->next;
  }
  return n;
}

void atmInternalName(char *atmName)
{
  char atmStrRe[BUFSIZ];

  if ((atmName[0] == 'H') 
      || ((atmName[0] >= '1') && (atmName[0] <= '4') && (atmName[1] == 'H'))
      || ((atmName[0] == ' ') && (atmName[1] == 'H')) ) { /* Hydrogens */
    if (atmName[0] == 'H') {
      strcpy(atmStrRe, atmName);
    } else {
      strcpy(atmStrRe, atmName+1);
    }
    if ((atmStrRe[1] >= 'A' ) && (atmStrRe[1] <= 'Z')) atmStrRe[2] = '\0';
    else atmStrRe[1] = '\0';
  } else { /* Heavy atom */
    if (atmName[0] != ' ') {
      strcpy(atmStrRe+1, atmName);
      atmStrRe[0] = ':';
      atmStrRe[3] = '\0';
    } else {
      strcpy(atmStrRe, atmName+1);
      if ((atmName[2] >= 'A' ) && (atmName[2] <= 'Z')) atmStrRe[2] = '\0';
      else atmStrRe[1] = '\0';
    }
  }
  // fprintf(stderr,"%s %s\n",atmName, atmStrRe);
  strcpy(atmName, atmStrRe);
}

/* ======================================================
 * Build a re string from atom name
 * ======================================================
 */
void atmName2dftlReStr(char *atmName, char *atmStrRe)
{
  int l;

  l = strlen(atmName);
  strcpy(atmStrRe+1,atmName);
  atmStrRe[0] = '^';
  atmStrRe[l+1] = '$';
  atmStrRe[l+2] = '\0';
  // fprintf(stderr,"%s %s\n",atmName, atmStrRe);
}

void atmName2fuzzyReStr(char *atmName, char *atmStrRe)
{
  int l;

  l = strlen(atmName);
  strcpy(atmStrRe+1,atmName);
  atmStrRe[0] = '^';
  atmStrRe[l+1] = '$';
  atmStrRe[l+2] = '\0';
  // fprintf(stderr,"%s %s\n",atmName, atmStrRe);
}

/* =====================================================
 * For each regexp of the pRe list, we setup the mask of 
 * the matching atoms in nuage p2.
 * ie: we prepare p1 to be confronted with only some of p1.
 * for each RE, nMask reports the number ot atoms concerned
 * atmMask[i] is 0 or 1. 1 implies atom matches RE.
 * =====================================================
 */
void setupMaskFromRe(DtReRec *pRe, DtNuage*p2)
{
  int aAtm;

  int lp = 0;

  // if (!strcmp(p2->header,"FX.xray.inh1.1F0R"))
  //  lp = 1;
  // if (lp) fprintf(stderr,"Entering setupMaskFromRe for: %s\n", p2->header);
  while (pRe != NULL) {
    pRe->atmMask = realloc(pRe->atmMask, sizeof(char)*p2->n);
    memset(pRe->atmMask,0,sizeof(char)*p2->n);
    pRe->nMask = 0;

    for (aAtm = 0; aAtm<p2->n; aAtm++) {
      if (p2->skip[aAtm]) {
	// if (lp) fprintf(stderr,"skipping %s %s\n", p2->atmName[aAtm], p2->resName[aAtm]);
	continue;
      }
      // if (lp) fprintf(stderr,"%s %s for %s %s",pRe->reAtmStr, pRe->reResStr, p2->atmName[aAtm], p2->resName[aAtm]);
      if ( (!regmatch(p2->atmName[aAtm],pRe->pAtmRe)) && (!regmatch(p2->resName[aAtm],pRe->pResRe)) ) {
	pRe->atmMask[aAtm] = 1;
	pRe->nMask++;
        // if (lp) fprintf(stderr, " OK");
	// if (lp) fprintf(stderr,"%s %s : OK for %s %s\n",pRe->reAtmStr, pRe->reResStr, p2->atmName[aAtm], p2->resName[aAtm]);
      }
      // if (lp) fprintf(stderr,"\n");
    }
    // if (lp) fprintf(stderr,"setupMaskFromRe: %s %s %d\n", pRe->reAtmStr, pRe->reResStr, pRe->nMask);
    pRe = pRe->next;
  }
  // if (lp) fprintf(stderr,"setupMaskFromRe: Done...\n");
  // if (!strcmp(p2->header,"FX.xray.inh1.1F0R"))
  //    exit(0);
}

/* get RE from line such as:
REMARK     MATCH. O.*
*/
void getAtmRe(char *line, char *atmStrRe)
{
  char *pC;
  int aPos = 0;

  atmStrRe[0] = '\0';
  if (strlen(line) >= 19) {
    pC = line+18;
//    while ((*pC != ' ') && (*pC != '\0')) {
    while ((*pC != ' ') && (*pC != '\0') && (*pC != '\r')) {
      atmStrRe[aPos] = *pC;
      aPos++;
      pC++;
    }

    atmStrRe[aPos] = '\0';

    /* add '^' at the beginning of the regex if necessary (1) */
    if (aPos > 0 && atmStrRe[0] != '^') {
      memmove(atmStrRe+1, atmStrRe, (aPos+1)*sizeof(char));
      atmStrRe[0] = '^';
      aPos++;
    }

    /* add '$' at the end of the regex if necessary (1) */
    if (aPos > 0 && atmStrRe[aPos-1] != '$') {
      atmStrRe[aPos] = '$';
      atmStrRe[aPos+1] = 0;
    }

    /* (1) otherwise, a regex such as 'C.*' will match '^.*C.*$' */
  }

}

/* Get RE from line such as:
REMARK     RESMATCH. SER
*/
void getResRe(char *line, char *resStrRe)
{
  char *pC;
  int aPos = 0;

  resStrRe[0] = '\0';
  // fprintf(stderr,"getResre: %s\n", line);
  if (strlen(line) >= 22) {
    pC = line+21;
    while ((*pC != ' ') && (*pC != '\0')) {
      resStrRe[aPos] = *pC;
      aPos++;
      pC++;
    }
    resStrRe[aPos] = '\0';
    // fprintf(stderr,"%s\n", line);
    // fprintf(stderr,"%s\n", resStrRe);
  }
}

/* Get atom weight from line such as:
REMARK     WEIGHT. 2.0
*/
double getAtomWeight(char *line)
{
  char *pC;
  int aPos = 0;
  char buff[30];
  double w = 0.0;

  if (strlen(line) >= 20) {
    pC = line+19;
    while ((*pC != ' ') && (*pC != '\0') && (aPos < 29)) {
      buff[aPos] = *pC;
      aPos++;
      pC++;
    }
    buff[aPos] = '\0';
    w = atof(buff);
  }

  return w;
}

/* Initialise la liste des poids associés aux atomes à 1.0 */
void initWeight(double *w, int n)
{
  n--;
  while (n >= 0) {
    w[n] = 1.0;
    n--;
  }
}


/* ============================================================================
 * Remplit les tableaux de données utiles à Csr après lecture des fichiers Pdb.
 * Alloue la mémoire pour la structure DtNuage.
 * if (strcmp(Entete,"REMARK     MATCH.") == 0)  return 12;
 * if (strcmp(Entete,"REMARK     RESMATCH.") == 0)  return 13;
 * ============================================================================
 */
void lectPdbCsr(char *fname, int what, int strict, DtNuage **pN, int *onNuages, DtReRec **pReList, int verbose)
{
  DtNuage *n;
  char   **lines;
  int     *limits;

  DtReRec *pRe;

  int  nLines;
  int  aLine;
  int  lineStart;
  int  lineStat;
//  int  lastCrd;
  int  nMol, nMod;
  int  molOpened, modOpened;
  int  nNuages;
  int  aNuage;
  int  nAtom;
  char atmStrRe[BUFSIZ];
  char resStrRe[BUFSIZ];
  char buff[BUFSIZ];
  char *cmpnd;
  char EC[BUFSIZ];
  char HETGRP[BUFSIZ];

  lines=txtFileRead(fname,&nLines);

  if (!lines) {
    exit(0);
  }

  // fprintf(stderr,"read %d lines from %s\n",nLines, fname);

  /* -- Nombre de molecules -- */
  nMol    = 0;
  nMod    = 0;
  nNuages = 0;
  molOpened = modOpened = 0;
//  lastCrd = 0;
  limits  = NULL;
  for (aLine=0; aLine < nLines; aLine++) {
    lineStat=getPDBLineStatus(lines[aLine]);
    if (lineStat == 10) { /* HEADER */
      molOpened = 1;
      nMol++;
      nNuages++;
      limits = realloc(limits, sizeof(int)*nNuages);
    }
    if (lineStat == 8) {  /* MODEL */
      modOpened = 1;
      nMod++;
      nNuages++;
      limits = realloc(limits, sizeof(int)*nNuages);
    }
    if (lineStat == 9) {  /* ENMDL */
      if (modOpened) {
	modOpened = 0;
	limits[nNuages-1] = aLine;
      } else {
	fprintf(stderr,"Incorrect file format: ENDMDL does not match MODEL\n");
	exit(0);
      }
    }
    if (lineStat == 7) { /* END */
      if (molOpened) {
	molOpened = 0;
	limits[nNuages-1] = aLine;
      } else {
	fprintf(stderr,"Incorrect file format: END does not match HEADER\n");
	exit(0);
      }
    }
#if 0
    if ((lineStat == 1) || (lineStat==11)) { /* ATOM / HETATM */
      lastCrd = aLine;
    }
#endif
  }
  if ((molOpened) || (modOpened)) {
    if (verbose)
      fprintf(stderr,"molOpened: may be missing END (nNuages %d aLine %d)\n",nNuages, aLine);
    limits[nNuages-1] = aLine - 1;
  }

  if (verbose)
    fprintf(stderr,"File %s: Found %d Mols, %d Models, %d Nuages (vs %d)\n", fname, nMol, nMod, nNuages, nMol+nMod);

  /* -- Allocate nuages -- */
  if (nNuages != (nMol + nMod)) {
    fprintf(stderr,"Inconsistent number of molecules + models vs Nuages\n");
    exit(0);
  }
  *onNuages = nNuages;
  *pN  = calloc(nNuages, sizeof(DtNuage));


  // fprintf(stderr,"lectPdbCsr: will setup nuages\n");


  /* -- Input des nuages -- */
  for (aNuage = 0; aNuage < nNuages; aNuage++) {

    // fprintf(stderr,"Will input Nuage %d limits %d\n",aNuage, limits[aNuage]);
    /* -- Taille du nuage -- */
    n    = &(*pN)[aNuage];
    n->n = 0;
    // fprintf(stderr,"Will input Nuage %d\n",aNuage);
    lineStart = (aNuage) ? limits[aNuage-1]: 0;
    if (lineStart < 0) { continue; } // /!\ limits[0] < 0 if several models in template file
    // fprintf(stderr,"Nuage %d (lines %d - %d)\n",aNuage,lineStart,limits[aNuage],n->n);
    for (aLine=lineStart; aLine < limits[aNuage]; aLine++) {
      lineStat=getPDBLineStatus(lines[aLine]);
      if ((lineStat==1) || (lineStat==11)){ /* ATOM | HETATM */
	n->n++;
      }
    }
    // fprintf(stderr,"Nuage %d (lines %d - %d): %d crds\n",aNuage,lineStart,limits[aNuage],n->n);

    /* -- Allocation memoire -- */
    n->crds     =  (DtPoint3 *) calloc(n->n, sizeof(DtPoint3));
    n->atmName  =  (DtStr4 *)   calloc(n->n, sizeof(DtStr4));
    n->atmNme   =  (DtStr4 *)   calloc(n->n, sizeof(DtStr4));
    n->atmNum   =  (int *)      calloc(n->n, sizeof(int));
    n->resName  =  (DtStr3 *)   calloc(n->n, sizeof(DtStr3));
    n->resNum   =  (int *)      calloc(n->n, sizeof(int));
    n->chn      =  (char *)     calloc(n->n, sizeof(char));
    n->skip     =  (char *)     calloc(n->n, sizeof(char));
    n->re       =  (DtReRec **) calloc(n->n, sizeof(DtReRec *));
    n->EC       =  (char *)     malloc(sizeof(char) * 1);
    n->EC[0]    =  '\0';
    n->cmpnd    =  (char *)     malloc(sizeof(char) * 1);
    n->cmpnd[0] =  '\0';
    n->id       =  (char *)     malloc(sizeof(char) * 1);
    n->id[0]    =  '\0';
    n->nHtGrps  = 0;
    n->htGrps   = NULL;
/*     n->hetGrps  =  (char *)    malloc(sizeof(char) * 1); */
/*     n->hetGrps[0]=  '\0'; */
    n->header   =  (char *)     malloc(sizeof(char) * 1);
    n->header[0]=  '\0';
    n->weight   = NULL;
    nAtom = n->n;

    /* -- Lecture donnees -- */
    n->n = 0;
    // fprintf(stderr,"Reading Crds (from %d to %d\n",lineStart, limits[aNuage] );
    for (aLine=lineStart; aLine < limits[aNuage]; aLine++) {
      lineStat=getPDBLineStatus(lines[aLine]);
      // fprintf(stderr,"%d %s\n", lineStat, lines[aLine]);
      if ((lineStat!=1) && (lineStat!=11) && (lineStat!=12) && (lineStat!=13) && (lineStat!=14) && (lineStat!=15) && (lineStat!=16) && (lineStat!=10)) continue;

      if (lineStat == 14) {  /* EC Number -> EC */
	if (sscanf(lines[aLine],"%14c%s",buff,EC) != EOF);
	// fprintf(stderr,"%s\n",lines[aLine]);
	n->EC = realloc(n->EC,strlen(EC)+1);
	strcpy(n->EC,EC);
      }
      else if (lineStat == 15) { /* COMPND -> cmpnd */
	// fprintf(stderr,"%s\n",lines[aLine]);
	cmpnd = lines[aLine]+10;
	n->cmpnd = realloc(n->cmpnd,strlen(cmpnd)+1);
	strcpy(n->cmpnd, cmpnd);
	cmpnd = n->cmpnd + strlen(n->cmpnd) - 1;
	while (*cmpnd == ' ') cmpnd--;
	cmpnd++;
	*cmpnd = '\0';
      }
      else if (lineStat == 16) { /* HETGRP -> hetGrps */
	// fprintf(stderr,"%s\n",lines[aLine]);
	if (sscanf(lines[aLine],"%17c%s",buff,HETGRP) != EOF);
	n->nHtGrps += 1;
	n->htGrps = (char **) realloc(n->htGrps, sizeof(char *) * n->nHtGrps);
	n->htGrps[n->nHtGrps -1]  =  (char *)    malloc(sizeof(char) * (strlen(HETGRP) + 1));
	strcpy(n->htGrps[n->nHtGrps -1], HETGRP);
	// fprintf(stderr,"Read %s\n",HETGRP);
      }
      else if (lineStat == 10) {  /* HEADER -> id */
	// fprintf(stderr,"%s\n",lines[aLine]);
	cmpnd = lines[aLine]+9;
	n->header = realloc(n->header,strlen(cmpnd)+1);
	strcpy(n->header, cmpnd);
	cmpnd = n->header + strlen(n->header) - 1;
	while (*cmpnd == ' ') cmpnd--;
	cmpnd++;
	*cmpnd = '\0';

	if (strlen(lines[aLine]) >= 66) {
	  n->id = realloc(n->id,sizeof(char)*5);
	  n->id[0] = lines[aLine][62];
	  n->id[1] = lines[aLine][63];
	  n->id[2] = lines[aLine][64];
	  n->id[3] = lines[aLine][65];
	  n->id[4] = '\0';
	} else {
	  n->id = realloc(n->id,sizeof(char)*5);
	  n->id[0] = 'u';
	  n->id[1] = 'n';
	  n->id[2] = 'k';
	  n->id[3] = 'n';
	  n->id[4] = '\0';
	}
      }
      else if ((lineStat==1) || (lineStat==11)) { /* ATOM HETATM */
	if (n->n) {
	  /* Assign RE to previous atom */
	  /* Was the RE already setup in the RE list ? (same masks on both atom and residue) */
	  pRe = rePtrFromStr(*pReList, atmStrRe, resStrRe); /* Peut etre NULL */
	  /* No: we create new RE, add it to the RE list */
	  if (pRe == NULL) {
	    pRe = (DtReRec *) calloc(1, sizeof(DtReRec));
	    pRe->next = *pReList;
	    *pReList  = pRe;
	    pRe->reAtmStr  = calloc (strlen(atmStrRe)+1, sizeof(char));
	    strcpy(pRe->reAtmStr, atmStrRe);
	    pRe->reResStr  = calloc (strlen(resStrRe)+1, sizeof(char));
	    strcpy(pRe->reResStr, resStrRe);
	    pRe->pAtmRe = reginit(pRe->reAtmStr, pRe->pAtmRe);
	    pRe->pResRe = reginit(pRe->reResStr, pRe->pResRe);
	    // fprintf(stderr,"%p %p\n",pRe->pAtmRe,  pRe->pResRe);
	    // fprintf(stderr,"CA : %d ZN : %d\n",regmatch("CA",pRe->pAtmRe),   regmatch("CA ",pRe->pAtmRe));
	    // exit(0);
	  }
	  n->re[n->n -1] = pRe;
	}
	// fprintf(stderr,"%s\n",lines[aLine]);
	
	getAtomCoords(lines[aLine],&n->crds[n->n][0],&n->crds[n->n][1],&n->crds[n->n][2]);
	getAtomName(lines[aLine],n->atmName[n->n]);
	// stripspace(n->atmName[n->n], n->atmNme[n->n]);
	strcpy(n->atmNme[n->n], n->atmName[n->n]);
	getAtomNum(lines[aLine],&n->atmNum[n->n]);

	/* Some masks to select SC atoms only */
	if (what == DcSC) {
	  if (!strcmp(n->atmNme[n->n]," N  "))   n->skip[n->n] = 1;
	  if (!strcmp(n->atmNme[n->n]," CA "))  n->skip[n->n] = 1;
	  if (!strcmp(n->atmNme[n->n]," C  "))   n->skip[n->n] = 1;
	  if (!strcmp(n->atmNme[n->n]," O  "))   n->skip[n->n] = 1;
	  if (!strcmp(n->atmNme[n->n]," OXT")) n->skip[n->n] = 1;
	} else if (what == DcBB) {
	  if ( strcmp(n->atmNme[n->n]," N  ") && 
	       strcmp(n->atmNme[n->n]," CA ") &&
	       strcmp(n->atmNme[n->n]," C  ") &&
	       strcmp(n->atmNme[n->n]," O  ") &&
	       strcmp(n->atmNme[n->n]," OXT") ) n->skip[n->n] = 1;
	}
	  // strcpy(n->atmNme[n->n], n->atmName[n->n]);
	getResName(lines[aLine],n->resName[n->n]);
	getResNum(lines[aLine],&n->resNum[n->n]);
	getResChainIde(lines[aLine],&n->chn[n->n]);

	atmInternalName(n->atmName[n->n]);
	// fprintf(stderr,"Read Crd %d\n", n->n);

	/* Setup ptr to default re */
	atmName2dftlReStr(n->atmName[n->n], atmStrRe);
	if (strict == 1) {
	  strcpy(resStrRe, n->resName[n->n]);
	} else {
	  resStrRe[0] = '.';
	  resStrRe[1] = '*';
	  resStrRe[2] = '\0';
	}
	n->n ++;
      } else {
	/* Here, we define the local regexps, overloading the default regexps strings */
	if (lineStat==12) {  /* MATCH. */
	  getAtmRe(lines[aLine], atmStrRe);
	}
	else if (lineStat==13) {  /* RESMATCH. */
	  getResRe(lines[aLine], resStrRe);
	}

        else if (lineStat==17) { /* WEIGHT. */
          if (!n->weight) {
            n->weight = (double *)calloc(nAtom, sizeof(double));
            initWeight(n->weight, nAtom);
          }
          n->weight[n->n -1] = getAtomWeight(lines[aLine]);
        }
      }
    }

    if (n->n) {
      /* Assign RE to previous atom */
      // fprintf(stderr,"%p\n",*pReList);
      // fflush(stderr);
      pRe = rePtrFromStr(*pReList, atmStrRe, resStrRe); /* Peut etre NULL */
      if (pRe == NULL) {
	pRe = (DtReRec *) calloc(1, sizeof(DtReRec));
	pRe->next = *pReList;
	*pReList  = pRe;
	pRe->reAtmStr  = calloc (strlen(atmStrRe)+1, sizeof(char));
	strcpy(pRe->reAtmStr, atmStrRe);
	pRe->reResStr  = calloc (strlen(resStrRe)+1, sizeof(char));
	strcpy(pRe->reResStr, resStrRe);
	pRe->pAtmRe = reginit(pRe->reAtmStr, pRe->pAtmRe);
	pRe->pResRe = reginit(pRe->reResStr, pRe->pResRe);
	// fprintf(stderr,"%p %p\n",pRe->pAtmRe,  pRe->pResRe);
	// fprintf(stderr,"CA : %d ZN : %d\n",regmatch("CA",pRe->pAtmRe),   regmatch("CA ",pRe->pAtmRe));
	// exit(0);
      }
      n->re[n->n -1] = pRe;
    }
#if 0
    for (aLine = 0; aLine < n->n; aLine++) {
      fprintf(stderr,"Atm %s %s regexp %s %s\n", n->atmName[aLine], n->resName[aLine],n->re[aLine]->reAtmStr, n->re[aLine]->reAtmStr);
    }
#endif

    // fprintf(stderr,"Nuage %d #atoms %d Id: %s COMPND %s EC %s\n",aNuage,n->n, n->id, n->cmpnd, n->EC);
    // exit(0);

  }

  // fprintf(stderr,"lectPdbCsr: will free memory\n");
  /* -- liberation memoire -- */
  txtFileFree(lines,nLines);
  free(limits);

  if (verbose)
    fprintf(stderr,"Currently %d regexp set\n", rePtrListLen(*pReList));
}


void outPDBNuage(char *fname, DtNuage *pN)
{
  FILE *fd;
  int aAtm;

  fd = fopen(fname, "w");
  if (fd == NULL) return;
  fprintf(fd,"HEADER CSRMATCH\n");
  for (aAtm = 0; aAtm<pN->n; aAtm++) {
    outputPDBAtomLine(fd,
		      aAtm, pN->atmName[aAtm], ' ', /* acode */
		      pN->resName[aAtm], pN->chn[aAtm], pN->resNum[aAtm], ' ', /* icode */
		      pN->crds[aAtm][0], pN->crds[aAtm][1], pN->crds[aAtm][2]);
  }
  fprintf(fd,"TER\n");
  fprintf(fd,"END\n");
  fclose(fd);
}

/* ======================================================
 * Write part of template that matches
 * ======================================================
 */
void outPDBTemplate(FILE *fd, DtReponse *rep, DtNuage *pNg)
{
  int i, i1, i2;

  fprintf(fd,"HEADER   %s\n",pNg->header);
  if (pNg->cmpnd[0] != '\0')
    fprintf(fd,"COMPND   %s\n",pNg->cmpnd);
  if (pNg->EC[0] != '\0')
    fprintf(fd,"REMARK    EC NUMBER: %s\n",pNg->EC);
  if (rep != NULL)
    fprintf(fd,"REMARK    RMSD %.2lf\n",rep->lrmsd);
//   boucle(i,0,rep->nLPairs) {
//     i1=(rep->lpairs[i].liaison)%(pNg->n);
// //    i2=(rep->lpairs[i].liaison)/(pNg->n);
  for (i1 = 0; i1 < pNg->n; i1++) {
    outputPDBAtomLine(fd,
		      pNg->atmNum[i1], pNg->atmNme[i1], ' ', /* acode */
		      pNg->resName[i1], pNg->chn[i1], pNg->resNum[i1], ' ', /* icode */
		      pNg->crds[i1][0], pNg->crds[i1][1], pNg->crds[i1][2]);
  }
  fprintf(fd,"END\n");
}

/* ======================================================
 * Write part of template that matches
 * ======================================================
 */
void outPDBMatch(FILE *fd, DtReponse *rep, DtNuage *pNg, DtNuage *pNg1)
{
  int i, i1, i2;

  //fprintf(fd,"HEADER    1avf site    1                                      1avf\n");
  fprintf(fd,"HEADER   %s\n",pNg->header);
  if (pNg->cmpnd[0] != '\0')
    fprintf(fd,"COMPND   %s\n",pNg->cmpnd);
  if (pNg->EC[0] != '\0')
    fprintf(fd,"REMARK    EC NUMBER: %s\n",pNg->EC);
  fprintf(fd,"REMARK    RMSD %.2lf\n",rep->lrmsd);
  fprintf(fd,"REMARK    MATRIX  %10.6lf %10.6lf %10.6lf %10.6lf \n", rep->rM[0][0], rep->rM[0][1], rep->rM[0][2], rep->rM[0][3] );
  fprintf(fd,"REMARK    MATRIX  %10.6lf %10.6lf %10.6lf %10.6lf \n", rep->rM[1][0], rep->rM[1][1], rep->rM[1][2], rep->rM[1][3] );
  fprintf(fd,"REMARK    MATRIX  %10.6lf %10.6lf %10.6lf %10.6lf \n", rep->rM[2][0], rep->rM[2][1], rep->rM[2][2], rep->rM[2][3] );
  fprintf(fd,"REMARK    MATRIX  %10.6lf %10.6lf %10.6lf %10.6lf \n", rep->rM[3][0], rep->rM[3][1], rep->rM[3][2], rep->rM[3][3] );
//   boucle(i,0,rep->nLPairs) {
// //    i1=(rep->lpairs[i].liaison)%(pNg1->n);
//     i2=(rep->lpairs[i].liaison)/(pNg1->n);
  for (i2 = 0; i2 < pNg->n; i2++) {
    outputPDBAtomLine(fd, 
		      pNg->atmNum[i2], pNg->atmNme[i2], ' ', /* acode */
		      pNg->resName[i2], pNg->chn[i2], pNg->resNum[i2], ' ', /* icode */ 
		      pNg->crds[i2][0], pNg->crds[i2][1], pNg->crds[i2][2]);
  }
  fprintf(fd,"REMARK    END MATCH\n" );
  fprintf(fd,"END\n");
}

void barycentre(DtNuage *n)
{
  DtFloat x, y, z;
  DtFloat tmpF;
  int i;

  x=y=z=0.;
  for(i=0 ; i < (n->n) ; i++){
    x += n->crds[i][0];
    y += n->crds[i][1];
    z += n->crds[i][2];
  }

  tmpF = (DtFloat) n->n;
  n->bc[0]=x/(tmpF);
  n->bc[1]=y/(tmpF);
  n->bc[2]=z/(tmpF);
}

void printNuage(FILE *f, DtNuage *n, char *msg)
{
  int aDot;
  fprintf(f, "%s\n",msg);
  fprintf(f, "n points: %d\n",n->n);
  for (aDot = 0; aDot < n->n; aDot++) {
    fprintf(f, "%s %s %lf %lf %lf\n",
	    n->atmName[aDot],n->resName[aDot],
	    n->crds[aDot][0],n->crds[aDot][1],n->crds[aDot][2]);
  }
}


/* ============================================================
 * Imprime les (nbPrint) premières lignes d'un DtpDistRec
 * des nuage d'atomes (ng1) et (ng2).
 * ============================================================
 */

// void printDtpDistRec(DtDistRec *dist, int nbPrint, DtNuage * ng1,DtNuage * ng2)
// {
//   int i,i1,i2;
//   for(i=0; i<nbPrint ; i++){
//     i1=(dist[i].liaison)%(ng1->n);
//     i2=(dist[i].liaison)/(ng1->n);
//     printf("distance %d\t longueur=%.8lf\t liaison=(%d,%d)\n",i,dist[i].longueur,i1,i2);
//   }
// }

/* =======================================================
 * Imprime les (nbCrds) premières coordonnées
 * d'un tableau de coordonnées.
 * =======================================================
 */
void printNgCrds(DtPoint3 *crds, int nbCrds)
{
  int i,j;

  for(i=0; i<nbCrds ; i++){
    fprintf(stderr, "*Atome %d :\t",i);
    for (j=0 ; j< 3 ; j++){
      fprintf(stderr, "%.8lf\t",crds[i][j]);
    }
    fprintf(stderr, "\n");
  }
  fprintf(stderr, "*\n");
}

/* ===================================================================
 * Copie d'un tableau de type DtpDistRec (recopie dist1 dans dist2). 
 * ===================================================================
 */

void copyDtpDistRec2(int kds, DtDistRec *dist1, DtDistRec *dist2)
{
  memcpy(dist2, dist1, kds * sizeof(DtDistRec));
}

// void copyDtpDistRec(int kds, DtDistRec *dist1, DtDistRec *dist2)
// {
//   int i,j;
// 
//   for(i=0 ; i < kds ; i++){
//     for(j=0 ; j < 2 ; j++){
//       dist2[i].longueur=dist1[i].longueur;
//       dist2[i].liaison=dist1[i].liaison;
//     }
//   }
// }


/* ===================================================================
 * Stockage d'une réponse pour CSR (kds,nbAtApp,dist,rmsd,Mat4x4).
 * ===================================================================
 */

void stockRep2(int kds, int nbAtApp, DtDistRec *dist, double rms, DtMatrix4x4 rmat, DtReponse *rep)
{
  rep->nbAtMax=nbAtApp;
  rep->rmsd=rms;
  copyMat4x4(rmat, rep->rM);
  copyDtpDistRec2(kds,dist,rep->dist);
}

// void stockRep(int kds, int nbAtApp, DtDistRec *dist, double rms, DtMatrix4x4 rmat, DtReponse *rep)
// {
//   rep->nbAtMax=nbAtApp;
//   rep->rmsd=rms;
//   copyMat4x4(rmat, rep->rM);
//   copyDtpDistRec(kds,dist,rep->dist);
// }


/* ===============================================
 * Création d'une matrice de rotation aléatoire
 * ===============================================
 */


void rotAl(DtMatrix4x4 mat)
{
  double teta;
  DtPoint3 U;
  double N;
  int compteur=0;

  teta=drand48()*DcPI;

  /* printf("*teta=%.8lf\t",teta); */

  U[1]=-1+drand48()*2; //réel compris entre -1 et 1
  U[2]=-1+drand48()*2;
  U[0]=-1+drand48()*2;
  N=norme(U);

  while (N>1 && compteur < 1000){

    /* printf("*X=%.8lf\t",U[1]);
    printf("Y=%.8lf\t",U[2]);
    printf("Z=%.8lf\n",U[3]);
    printf("* /!\\========PROBLEME==========/!\\\n"); */
    U[1]=-1+drand48()*2;
    U[2]=-1+drand48()*2;
    U[0]=-1+drand48()*2;
    N=norme(U);
    compteur++;
  }
  if(compteur < 1000){
    /* printf("X=%.8lf\t",U[1]);
    printf("Y=%.8lf\t",U[2]);
    printf("Z=%.8lf\n",U[3]);
    printf("*norme(U)=%.8lf\n\n",N); */
  }
  else {
    /* printf("/!\\========NORME DE U SUPERIEURE A 1==========/!\\\n\n"); */
  }


  teta=0.211091720422362 ;
  U[0]=0.776475976742458 ;
  U[1]=-1.12080975556336 ;
  U[2]=-0.06429069295656 ;


  //MkArbitraryAxisRotMat4x4(O,U,teta,mat);

}

/* ===================================================
 * Positionnement aléatoire de deux nuages de points.
 * ===================================================
 */

// void posAl (DtNuage * ng1, DtNuage * ng2, DtMatrix4x4 mat)
// {
//   int nAl1, nAl2,i;
//   DtPoint3 tr1, tr2;
// 
// 
//   /* --- rotation aléatoire --- */
// 
//   rotAl(mat); 
// 
//   /* --- positionnement aléatoire --- */
// 
//   nAl1=irand(ng1->n);
//   nAl2=irand(ng2->n);
// 
//   /* printf("*COUPLE (ATOME 1, ATOME 2) TIRE AU HASARD : (%d,%d).\n*\n"
//      ,nAl1,nAl2); */
// 
// 
//   /* --- superposition des deux nuages de points --- */
// 
// 
//   nAl1=41;
//   nAl2=38;
// 
// 
//   /* rotation de tout le nuage 2 */
//   /* for(i=0 ; i < (ng2->n) ; i++){
//     singleRotate(ng2->crds[i], mat);
//   }
// 
//   tr1[0]=ng1->crds[nAl1][0];
//   tr1[1]=ng1->crds[nAl1][1];
//   tr1[2]=ng1->crds[nAl1][2];
// 
//   tr2[0]=ng2->crds[nAl2][0];
//   tr2[1]=ng2->crds[nAl2][1];
//   tr2[2]=ng2->crds[nAl2][2];
//   negatePoint3(tr2); */
// 
//   /* superposition du nuage 2 sur le nuage 1 */
//   /* for(i=0 ; i < (ng2->n) ; i++){
//     simpleTranslate(ng2->crds[i],tr2);
//     simpleTranslate(ng2->crds[i],tr1);
//     } */
// 
// 
// 
//   tr1[0]=ng1->crds[nAl1][0];
//   tr1[1]=ng1->crds[nAl1][1];
//   tr1[2]=ng1->crds[nAl1][2];
// 
//   tr2[0]=ng2->crds[nAl2][0];
//   tr2[1]=ng2->crds[nAl2][1];
//   tr2[2]=ng2->crds[nAl2][2];
// 
//   negatePoint3(tr2);
// 
//   mat[3][0]=tr1[0]+tr2[0];
//   mat[3][1]=tr1[1]+tr2[1];
//   mat[3][2]=tr1[2]+tr2[2];
// 
//   for(i=0 ; i < (ng2->n) ; i++){
//     singleRotate(ng2->crds[i], mat);
//   }
// 
// }

/* ===================================================
 * Positionnement aléatoire de deux nuages de points.
 * ng1 est fixe, ng2 est mobile (rotation + translation)
 * mat est la matrice de transformation aléatoire.
 * (mat est juste un espace de travail, ecrasee a la sortie
 * de posAl2)
 * ===================================================
 */
void posAl2(DtNuage *ng1, DtNuage *ng2, DtMatrix4x4 mat){
  double teta;
  DtPoint3 U;
  DtPoint3 tr2dt;
  double N;
  int nAl1, nAl2,i,tirage;
  DtPoint3 tr1, tr2;
  int compteur=0;
  double temp,x,y,z;
//  static int rRank = 0;

#if 1
  /* angle aléatoire entre O et Pi */
  teta=drand48()*DcPI;

  /* printf("*teta=%.8lf\t",teta); */

  /* vecteur aléatoire */
  U[1]=-1+drand48()*2; //réel compris entre -1 et 1
  U[2]=-1+drand48()*2;
  U[0]=-1+drand48()*2;
  N=norme(U);

  /* compteur implémenté pour limiter, car des fois plus d'une minute sans solution */
  while ((N>1.) && (compteur < 1000)){

    /* printf("*X=%.8lf\t",U[1]);
    printf("Y=%.8lf\t",U[2]);
    printf("Z=%.8lf\n",U[3]);
    printf("* /!\\========PROBLEME==========/!\\\n"); */
    U[1]=-1+drand48()*2;
    U[2]=-1+drand48()*2;
    U[0]=-1+drand48()*2;
    N=norme(U);
    compteur++;
  }
//  if(compteur < 1000){
/*     printf("X=%.8lf\t",U[1]); */
/*     printf("Y=%.8lf\t",U[2]); */
/*     printf("Z=%.8lf\n",U[3]); */
/*     printf("*norme(U)=%.8lf\n\n",N);  */
//  }
//  else {
/*     printf("/!\\========NORME DE U SUPERIEURE A 1==========/!\\\n\n"); */
//  }

 /* --- positionnement aléatoire --- */

  tirage=irand(6);

  /* fprintf(stderr,"TIRAGE : %d\n",tirage); */

  if (tirage==0) {
    /* tirage 0: barycentres are superimposed */
    //fprintf(stderr,"OK 0");
    //fflush(stderr);
    /* -- Calcul du barycentre du nuage de points 1 -- */
    tr1[0]= ng1->bc[0];
    tr1[1]= ng1->bc[1];
    tr1[2]= ng1->bc[2];

    tr2[0]= ng2->bc[0];
    tr2[1]= ng2->bc[1];
    tr2[2]= ng2->bc[2];
  }

//  else if((tirage>0) && (tirage < 3)) {
  else if (tirage < 3) {
    /* Pour 1 et 2: select atm1 in nuage1 (that has compatible atoms in nuage2, 
       then select atom in nuage2 in the list of compatible atoms of nuage1. 
    */


  // if((tirage>0) && (tirage < 0)) {
    //fprintf(stderr,"OK 1..2 ng2->n %d ng1->n %d", ng2->n, ng1->n);
    //fflush(stderr);
    if(ng2->n < ng1->n) {
      // fprintf(stderr,"ng2->n < ng1->n\n");
      // fflush(stderr);

      /* WARNING: THESE LOOPS MIGHT BE INFINITE !! */
      do {
	do {
	  nAl2=irand(ng2->n); /* Any atom 2 that has a match in other nuage */
	} while ((ng2->skip[nAl2]));
	nAl1=nAl2+irand((ng1->n)-(ng2->n)); /* Any atom 1 in the range ng2->n - ng1->n since  ng1->n larger than ng2->n */
      } while ( (ng1->skip[nAl1]) || (!ng1->re[nAl1]->nMask) || (!ng1->re[nAl1]->atmMask[nAl2]) );
      //printf("%d\t%d\n",nAl1,nAl2);
    }
    else if(ng2->n > ng1->n) {
      // fprintf(stderr,"ng2->n > ng1->n\n");
      // fflush(stderr);
      do {
	do {
	  nAl1=irand(ng1->n);
	} while ((ng1->skip[nAl1]) || (!ng1->re[nAl1]->nMask));
	nAl2=nAl1+irand((ng2->n)-(ng1->n));
      } while ( (ng2->skip[nAl2]) || (!ng1->re[nAl1]->atmMask[nAl2]) );
      //printf("%d\t%d\n",nAl1,nAl2);
    }
    /* si ng1->n == ng2->n, les ièmes atomes sont appariés, quels qu'ils soient ?! */
    /* NOTE: Quel est l'interet de cette heuristique ? */
    else {
      // fprintf(stderr,"ng2->n == ng1->n\n");
      // fflush(stderr);
      do {
	nAl1=irand(ng1->n);
      } while ((ng1->skip[nAl1]) || (!ng1->re[nAl1]->nMask));
      nAl2=nAl1;
    }

    tr1[0]=ng1->crds[nAl1][0];
    tr1[1]=ng1->crds[nAl1][1];
    tr1[2]=ng1->crds[nAl1][2];

    tr2[0]=ng2->crds[nAl2][0];
    tr2[1]=ng2->crds[nAl2][1];
    tr2[2]=ng2->crds[nAl2][2];
  }
  else {
    /* De 3 à 6: select atm1 in nuage1 (that has compatible atoms in nuage2, 
       then select atom in nuage2 in the list of compatible atoms of nuage1. 
    */
//  else if(tirage>2) {
    // fprintf(stderr,"tirage>2 (%d)\n", ng1->n);
    // fflush(stderr);

    compteur = 0;
    do {
      nAl1=irand(ng1->n);
      compteur += 1;
      // fprintf(stderr,"nAl1 %d, skip %d, nMask %d\n",nAl1, ng1->skip[nAl1], ng1->re[nAl1]->nMask);
    } while ( (compteur < 1000) && ( (ng1->skip[nAl1]) || (!ng1->re[nAl1]->nMask) ));
    // } while ( (compteur < 1000) && ( (ng1->skip[nAl1]) || (!ng1->re[nAl1]->nMask) ));
    // nAl2=irand(ng2->n);
    // fprintf(stderr,"nAl1 %d (/%d, nMask %d)\n", nAl1, ng1->n, ng1->re[nAl1]->nMask);
    // fflush(stderr);
    nAl2 = irand(ng1->re[nAl1]->nMask);
    // fprintf(stderr,"nAl2 %d\n", nAl2);
    // fflush(stderr);
    for (i=0; i < ng2->n; i++) {
      if (ng1->re[nAl1]->atmMask[i]) {
	nAl2--;
	// fprintf(stderr,"%d %d\n",i, nAl2);
	if (nAl2 < 0) {
	  nAl2 = i;
	  break;
	}
      }
    }

    tr1[0]=ng1->crds[nAl1][0];
    tr1[1]=ng1->crds[nAl1][1];
    tr1[2]=ng1->crds[nAl1][2];

    // fprintf(stderr,"nAl2 is %d ng2 is %d\n",nAl2, ng2->n);
    tr2[0]=ng2->crds[nAl2][0];
    tr2[1]=ng2->crds[nAl2][1];
    tr2[2]=ng2->crds[nAl2][2];
  }

  tr2dt[0]=tr2[0] + U[0];
  tr2dt[1]=tr2[1] + U[1];
  tr2dt[2]=tr2[2] + U[2];

  //nAl1=41;
  //nAl2=38;



   /* printf("*COUPLE (ATOME 1, ATOME 2) TIRE AU HASARD : (%d,%d).\n*\n"
     ,nAl1,nAl2); */


#else
  /* Orientation imposée du programme Fortran pour le fichier pdbtest.pdb
   */

  /* teta=0.211091720422362 ;
  U[0]=0.776475976742458 ;
  U[1]=-1.12080975556336 ;
  U[2]=-0.06429069295656 ; */
// Atom pairs, (-1 = BC), teta, dir
// #define DcNDrawings 200
// Drawings[200][6] = {
// { 13, 41, 2.35916893650697, 1.29571412130762, -2.27899019262253, -0.541775744101193},

  if (rRank >= DcNDrawings)
    exit(0);
  teta = Drawings[rRank][2];
  U[0] = Drawings[rRank][3];
  U[1] = Drawings[rRank][4];
  U[2] = Drawings[rRank][5];
  nAl1 = Drawings[rRank][0];
  nAl2 = Drawings[rRank][1];
  if (nAl1 != -1) nAl1--;
  if (nAl2 != -1) nAl2--;
  rRank ++;

  if (nAl1 != -1) {
    tr1[0]=ng1->crds[nAl1][0];
    tr1[1]=ng1->crds[nAl1][1];
    tr1[2]=ng1->crds[nAl1][2];
  } else {
#if 0
    /* -- Calcul du barycentre du nuage de points 1 -- */
    x=y=z=0.;
    for(i=0 ; i < (ng1->n) ; i++){
      x += ng1->crds[i][0];
      y += ng1->crds[i][1];
      z += ng1->crds[i][2];
    }

    tr1[0]=x/(ng1->n);
    tr1[1]=y/(ng1->n);
    tr1[2]=z/(ng1->n);
#else
    tr1[0]= ng1->bc[0];
    tr1[1]= ng1->bc[1];
    tr1[2]= ng1->bc[2];
#endif
  }

  if (nAl2 != -1) {
    tr2[0]=ng2->crds[nAl2][0];
    tr2[1]=ng2->crds[nAl2][1];
    tr2[2]=ng2->crds[nAl2][2];
  } else {
#if 0
    /* -- Calcul du barycentre du nuage de points 2 -- */
    x=y=z=0.0;
    for(i=0 ; i < (ng2->n) ; i++){
      x += ng2->crds[i][0];
      y += ng2->crds[i][1];
      z += ng2->crds[i][2];
    }

    tr2[0]=x/(ng2->n);
    tr2[1]=y/(ng2->n);
    tr2[2]=z/(ng2->n);
#else

    tr2[0]= ng2->bc[0];
    tr2[1]= ng2->bc[1];
    tr2[2]= ng2->bc[2];

#endif
  }

  tr2dt[0]=tr2[0] + U[0];
  tr2dt[1]=tr2[1] + U[1];
  tr2dt[2]=tr2[2] + U[2];


  fprintf(stderr,"i1 %d i2 %d teta: %lf U %lf %lf %lf, tr1 %lf %lf %lf tr2 %lf %lf %lf\n",
	  nAl1,nAl2,teta,U[0],U[1],U[2],tr1[0],tr1[1],tr1[2],tr2[0],tr2[1],tr2[2]);
#endif


  /* --- superposition des deux nuages de points --- */

  /* -- Création de la matrice de rotation aléatoire -- */
  MkArbitraryAxisRotMat4x4(tr2,tr2dt,teta,mat);

  /* -- Transposée de la matrice de rotation 3x3 dans la matrice 4x4 -- */
  temp=mat[0][1];
  mat[0][1]=mat[1][0];
  mat[1][0]=temp;

  temp=mat[0][2];
  mat[0][2]=mat[2][0];
  mat[2][0]=temp;

  temp=mat[1][2];
  mat[1][2]=mat[2][1];
  mat[2][1]=temp;


  //printMat4x4(mat);

  //printf("%lf %lf %lf\n",tr1[0],tr1[1],tr1[2]);
  //printf("%lf %lf %lf\n",tr2[0],tr2[1],tr2[2]);



  /* -- Translation du nuage 2 par rapport à tr2 -- */
  for(i=0 ; i < (ng2->n) ; i++){
    ng2->crds[i][0] -= tr2[0];
    ng2->crds[i][1] -= tr2[1];
    ng2->crds[i][2] -= tr2[2];
  }

  /* -- Rotation du nuage 2 par la matrice de rotation aléatoire -- */
  for(i=0 ; i < (ng2->n) ; i++){
    x=ng2->crds[i][0];
    y=ng2->crds[i][1];
    z=ng2->crds[i][2];

    ng2->crds[i][0]=mat[0][0]*(x)+mat[0][1]*(y)+mat[0][2]*(z);
    ng2->crds[i][1]=mat[1][0]*(x)+mat[1][1]*(y)+mat[1][2]*(z);
    ng2->crds[i][2]=mat[2][0]*(x)+mat[2][1]*(y)+mat[2][2]*(z);
  }

  /* -- Superposition du nuage 2 sur le nuage 1 via la translation tr1 -- */
  for(i=0 ; i < (ng2->n) ; i++){
    ng2->crds[i][0] += tr1[0];
    ng2->crds[i][1] += tr1[1];
    ng2->crds[i][2] += tr1[2];
  }

  /* printf("DISTANCE : %lf\n",distance(ng1->crds[nAl1],ng2->crds[nAl2])); */
}

/* ===========================================================
 * Imprime quels points sont appariés deux à deux par sdm.
 * ===========================================================
 */

void printNbPtsApp (DtNuage * ng1, DtNuage * ng2, int *kds,DtDistRec *dist)
{
  int i,i1,i2,nbAtApp,stop;
  char at1App[ng1->n];
  char at2App[ng2->n];

  memset (at1App, 0, sizeof(char)*(ng1->n));
  memset (at2App, 0, sizeof(char)*(ng2->n));

  i=stop=0;

  while(i<(*kds) && stop==0){
    i1=(dist[i].liaison)%(ng1->n);
    i2=(dist[i].liaison)/(ng1->n);

    printf("*AT1 - AT2 : %d - %d\n",i1,i2);
    /* printPoint3Crds(ng1->crds,i1);
       printPoint3Crds(ng2->crds,i2); */

    if(at1App[i1]==1){
      stop=1;
      nbAtApp=i;
      printf("*=> ATOME %d DU NUAGE 1 DEJA APPARIE.\n",i1); 
    } else {
      at1App[i1]=1;
    }
    if(at2App[i2]==1){
      stop=1;
      nbAtApp=i;
      printf("*=> ATOME %d DU NUAGE 2 DEJA APPARIE.\n",i2);
    } else {
      at2App[i2]=1;
    }
    i++;
  }
}

/* ========================== ALGORITHME SDM =================================
 * Calcul de toutes les distances entre deux nuages de points (ng).
 * Conservation de toutes les distances inférieures à (dseuil) dans un 
 *   tableau (dist) de type DtpDistRec. 
 *   dist[k].longueur correspond à la k-ième distance inférieure à dseuil
 *   dist[k].liaison permet de retrouver de quels points s'agit cette longueur.
 *
 * Retourne le nombre de points maximum que l'on peut apparier.
 * ===========================================================================
 */

#define SQUAREDDISTANCE(A,B) ( (A[0]-B[0] * A[0]-B[0]) + (A[1]-B[1] * A[1]-B[1]) + (A[2]-B[2] * A[2]-B[2]) )
int sdm2(DtNuage *ng1, DtNuage *ng2, double dseuil, int dimTab, int *kds, DtDistRec *dist)
{
  char at1App[ng1->n]; //at1App[k]=1 si le k-ème point de ng1 fait déjà parti d'un appariement, 0 sinon
  char at2App[ng2->n]; //at2App[k]=1 si le k-ème atome de ng2 fait déjà parti d'un appariement, 0 sinon
  int i,i1,i2,shift,nbAtApp;
  double lg2;
  double dseuil2 = dseuil * dseuil;


  if ((ng1->n)==0 || (ng2->n)==0){
    *kds=0;
    nbAtApp=0;
    // fprintf(stderr,"NVoid nuage !!\n");
    return nbAtApp;
  }

  /* --- calcul des distances entre les points --- */
  if (dseuil>=0){
    *kds=0;
    for (i1=0 ; i1 <(ng1->n) ; i1++){
      if (ng1->skip[i1]) continue;
      if (ng1->re[i1]->nMask == 0) continue;
      for (i2=0 ; i2 <(ng2->n) ; i2++){
	if (ng2->skip[i2]) continue;
	if (!ng1->re[i1]->atmMask[i2]) continue;

	// fprintf(stderr,"Matching %s %s and %s %s\n", ng1->atmNm2[i1], ng1->atmName[i1], ng2->atmNm2[i2], ng2->atmName[i2]);

	shift=i2*(ng1->n);

	lg2=squared_distance((ng1->crds[i1]),(ng2->crds[i2]));

	if(lg2 <= dseuil2){
	  if(*kds <= dimTab){
	    (dist[*kds]).longueur2 = lg2;
	    (dist[*kds]).liaison = i1 + shift;
	    *kds=(*kds)+1;
	  }
	}
      }
    }
    if (*kds>dimTab) return (-1); /* dimd trop petit */
    if (*kds==0) return (-2); /* dseuil trop petit */
    // exit(0);
  }

  else { /* dseuil < 0 (équivalent de dseuil infini) */

    *kds=(ng1->n)*(ng2->n);

    if (*kds>dimTab) return 1; /* dimd trop petit */
    for (i2=0 ; i2<(ng2->n) ; i2++){
      shift=i2*(ng1->n);
      for (i1=0 ; i1 <(ng1->n) ; i1++){
	dist[i1+shift].longueur2=squared_distance((ng1->crds[i1]),(ng2->crds[i2]));
	dist[i1+shift].liaison=i1+shift;

      }
    }
  }

  /* ---  tri du tableau des distances par ordre croissant de distances --- */
  qsort(dist,*kds,sizeof(DtDistRec), cmpdistRec2);

  /* --- calcul du nombre de points appariés --- */

  memset (at1App, 0, sizeof(char)*(ng1->n));
  memset (at2App, 0, sizeof(char)*(ng2->n));

  i=0;
  nbAtApp = 0;
  while(i<(*kds)) {
    i1=(dist[i].liaison)%(ng1->n);
    i2=(dist[i].liaison)/(ng1->n);

    if(at1App[i1]==1){
      nbAtApp=i;
      break;
    } else {
      at1App[i1]=1;
    }

    if(at2App[i2]==1){
      nbAtApp=i;
      break;
    } else {
      at2App[i2]=1;
    }
    i++;
  }

  if (nbAtApp==(*kds) && nbAtApp!=min((ng1->n),(ng2->n))) return (-3);

  // exit(0);
  return (int) (nbAtApp);
}

// int sdm(DtNuage * ng1, DtNuage * ng2, double dseuil, int dimTab, int *kds, DtDistRec *dist)
// {
//   char at1App[ng1->n]; //at1App[k]=1 si le k-ème point de ng1 fait déjà parti d'un appariement, 0 sinon
//   char at2App[ng2->n]; //at2App[k]=1 si le k-ème atome de ng2 fait déjà parti d'un appariement, 0 sinon
//   int i,i1,i2,shift,nbAtApp;
//   double lg;
// 
// 
//   if ((ng1->n)==0 || (ng2->n)==0){
//     *kds=0;
//     nbAtApp=0;
//     // fprintf(stderr,"NVoid nuage !!\n");
//     return nbAtApp;
//   }
// 
//   /* --- calcul des distances entre les points --- */
//   if (dseuil>=0){
//     *kds=0;
//     for (i1=0 ; i1 <(ng1->n) ; i1++){
//       if (ng1->skip[i1]) continue;
//       if (ng1->re[i1]->nMask == 0) continue;
//       // fprintf(stderr,"SDM: i1 %d %s %s\n",i1, ng1->resName[i1],  ng1->atmName[i1] );
//       for (i2=0 ; i2 <(ng2->n) ; i2++){
// 	// fprintf(stderr,"SDM: i2 %d\n",i2);
// 	if (ng2->skip[i2]) continue;
// 	if (!ng1->re[i1]->atmMask[i2]) continue;
// 	// fprintf(stderr,"SDM: i2 %d %s %s\n",i2, ng2->resName[i2],  ng2->atmName[i2] );
// 
// 	shift=i2*(ng1->n);
// 
// 	lg=distance((ng1->crds[i1]),(ng2->crds[i2]));
// 
// //         fprintf(stderr, "%lf\n", lg);
// 
// 	// lg = SQUAREDDISTANCE((ng1->crds[i1]),(ng2->crds[i2]));
// 
// 	/* liaison: liaison / ng1->n -> i2
// 	            liaison % ng1->n -> i1
// 	*/
// 	if(lg <= dseuil){
// 	  if(*kds <= dimTab){ 
// 	    (dist[*kds]).longueur = lg; 
// 	    (dist[*kds]).liaison = i1 + shift; 
// 	    *kds=(*kds)+1;
// 
// 	    /* printf("dist :%lf\tcor : %d\n",lg*lg,i1+shift); */
// 
// 	  }
// 	}
//       }
//     }
//     if (*kds>dimTab) return (-1); /* dimd trop petit */
//     if (*kds==0) return (-2); /* dseuil trop petit */
//     // exit(0);
//   }
// 
//   else { /* dseuil < 0 (équivalent de dseuil infini) */
// 
//     *kds=(ng1->n)*(ng2->n);
// 
//     if (*kds>dimTab) return 1; /* dimd trop petit */
//     for (i2=0 ; i2<(ng2->n) ; i2++){
//       shift=i2*(ng1->n);
//       for (i1=0 ; i1 <(ng1->n) ; i1++){
// 	dist[i1+shift].longueur=distance((ng1->crds[i1]),(ng2->crds[i2]));
// 	// dist[i1+shift].longueur=SQUAREDDISTANCE((ng1->crds[i1]),(ng2->crds[i2]));
// 	dist[i1+shift].liaison=i1+shift;
// 
//       }
//     }
//   }
// 
//   /* ---  tri du tableau des distances par ordre croissant de distances --- */
// 
//   /* printDtpDistRec(dist,*kds,ng1,ng2);
//      printf("\n"); */
// 
// //  qsort(dist,*kds,sizeof(DtDistRec), cmpdist);
//   qsort(dist,*kds,sizeof(DtDistRec), cmpdistRec);
// 
//   // fprintf(stderr,"%d distances calculated\n",*kds);
//   // printDtpDistRec(dist,*kds,ng1,ng2);
//   // printDtpDistRec(dist,10,ng1,ng2);
// 
//   /* --- calcul du nombre de points appariés --- */
// 
//   memset (at1App, 0, sizeof(char)*(ng1->n));
//   memset (at2App, 0, sizeof(char)*(ng2->n));
// 
//   i=0;
//   nbAtApp = 0;
//   while(i<(*kds) ) {
//     i1=(dist[i].liaison)%(ng1->n);
//     i2=(dist[i].liaison)/(ng1->n);
// 
// #if 0
//     // Uncomment here to debug, if doubt on the sort
//     fprintf(stderr,"%d : %d - %d : %lf (%.3lf %.3lf %.3lf / %.3lf %.3lf %.3lf)\n",
// 	    i,i1,i2,dist[i].longueur,
// 	    ng1->crds[i1][0], ng1->crds[i1][1], ng1->crds[i1][2],
// 	    ng2->crds[i2][0], ng2->crds[i2][1], ng2->crds[i2][2]);
// #endif
// 
//     if(at1App[i1]==1){
//       nbAtApp=i;
//       break;
//       /* printf("*=> ATOME %d DU NUAGE 1 DEJA APPARIE.\n",i1); */
//     } else {
//       at1App[i1]=1;
//     }
//     if(at2App[i2]==1){
//       nbAtApp=i;
//       break;
//       /* printf("*=> ATOME %d DU NUAGE 2 DEJA APPARIE.\n",i2); */
//     } else {
//       at2App[i2]=1;
//     }
//     i++;
//   }
//  
//   // fprintf(stderr,"N apparies: %d\n",nbAtApp);
//   if (nbAtApp==(*kds) && nbAtApp!=min((ng1->n),(ng2->n))) return (-3);
// 
//   return (int) (nbAtApp);
// }


/* ========================== ALGORITHME SDM =================================
 * Reanalyse the dist array, so as to enlarge the matches less than given threshold.
 *   tableau (dist) de type DtpDistRec. 
 *   dist[k].longueur correspond à la k-ième distance inférieure à dseuil
 *   dist[k].liaison permet de retrouver de quels points s'agit cette longueur.
 *
 * Retourne le nombre de points maximum que l'on peut apparier.
 * ===========================================================================
 */
// int enlarge(DtNuage *ng1, DtNuage *ng2, DtReponse *rep, int nbAtApp, int kds, DtDistRec *dist, double tol, int verbose)
// {
//   char at1App[ng1->n]; //at1App[k]=1 si le k-ème point de ng1 fait déjà parti d'un appariement, 0 sinon
//   char at2App[ng2->n]; //at2App[k]=1 si le k-ème atome de ng2 fait déjà parti d'un appariement, 0 sinon
//   int i,i1,i2;
// //  int shift;
// //  double lg;
//   double sqrsum = 0.;
// 
// 
//   rep->lpairs = realloc(rep->lpairs, sizeof(DtDistRec) * kds);
//   rep->nLPairs = 0;
// 
//   /* --- calcul du nombre de points appariés --- */
// 
//   memset (at1App, 0, sizeof(char)*(ng1->n));
//   memset (at2App, 0, sizeof(char)*(ng2->n));
// 
//   i=0;
//   nbAtApp = 0;
//   while(i<(kds) ) {
//     i1=(dist[i].liaison)%(ng1->n);
//     i2=(dist[i].liaison)/(ng1->n);
// 
//     if (verbose) {
//       fprintf(stderr,"enlarge pair %d : %s %d %c - %d : %s %d %c dist %.5lf (%.3lf %.3lf %.3lf / %.3lf %.3lf %.3lf)\n",
// 	      i1, ng1->resName[i1], ng1->resNum[i1], ng1->chn[i1],
// 	      i2, ng2->resName[i2], ng2->resNum[i2], ng2->chn[i2],dist[i].longueur,
// 	      ng1->crds[i1][0], ng1->crds[i1][1], ng1->crds[i1][2],
// 	      ng2->crds[i2][0], ng2->crds[i2][1], ng2->crds[i2][2]);
//       /* printf("*%d\tAT1 - AT2 : %d - %d\t%lf\n",
// 	 i,i1,i2,dist[i].longueur); */
//       /* printPoint3Crds(ng1->crds,i1);
// 	 printPoint3Crds(ng2->crds,i2); */
//     }
// 
//     if(at1App[i1]==1){
//       if (verbose) {
// 	fprintf(stderr,"enlarge: skipping on i1\n");
//       }
//       goto LCONT;
//       /* printf("*=> ATOME %d DU NUAGE 1 DEJA APPARIE.\n",i1); */
//     } else {
//       at1App[i1]=1;
//     }
//     if(at2App[i2]==1){
//       if (verbose) {
//         fprintf(stderr,"enlarge: skipping on i2\n");
//       }
//       goto LCONT;
//       /* printf("*=> ATOME %d DU NUAGE 2 DEJA APPARIE.\n",i2); */
//     } else {
//       at2App[i2]=1;
//     }
//     if (dist[i].longueur < tol) {
//       rep->lpairs[rep->nLPairs].liaison = dist[i].liaison;
//       rep->lpairs[rep->nLPairs].longueur = dist[i].longueur;
//       rep->nLPairs ++;
//       sqrsum += (dist[i].longueur * dist[i].longueur);
//     } else {
//       break;
//     }
// 
//   LCONT:;
//     i++;
//   }
// 
//   rep->lrmsd = sqrt(sqrsum / (double) rep->nLPairs);
//   // fprintf(stderr,"N lapparies: %d\n",rep->nLPairs);
// 
//   return (int) (rep->nLPairs);
// }

int enlarge_weighted2(DtNuage *ng1, DtNuage *ng2, DtReponse *rep, int nbAtApp, int kds, DtDistRec *dist, double tol, int verbose)
{
  char at1App[ng1->n]; //at1App[k]=1 si le k-ème point de ng1 fait déjà parti d'un appariement, 0 sinon
  char at2App[ng2->n]; //at2App[k]=1 si le k-ème atome de ng2 fait déjà parti d'un appariement, 0 sinon
  int i,i1,i2;
  double sqrsum = .0, sqrsumW = .0;
  double w1, w2, wSum = .0;
  double tol2 = tol * tol;

  rep->lpairs = realloc(rep->lpairs, sizeof(DtDistRec) * kds);
  rep->nLPairs = 0;

  /* --- calcul du nombre de points appariés --- */

  memset (at1App, 0, sizeof(char)*(ng1->n));
  memset (at2App, 0, sizeof(char)*(ng2->n));

  i=0;
  nbAtApp = 0;
  while(i<(kds) ) {
    i1=(dist[i].liaison)%(ng1->n);
    i2=(dist[i].liaison)/(ng1->n);

    if (verbose) {
      fprintf(stderr,"enlarge pair %d : %s %d %c - %d : %s %d %c dist %.5lf (%.3lf %.3lf %.3lf / %.3lf %.3lf %.3lf)\n",
	      i1, ng1->resName[i1], ng1->resNum[i1], ng1->chn[i1],
	      i2, ng2->resName[i2], ng2->resNum[i2], ng2->chn[i2], sqrt(dist[i].longueur2),
	      ng1->crds[i1][0], ng1->crds[i1][1], ng1->crds[i1][2],
	      ng2->crds[i2][0], ng2->crds[i2][1], ng2->crds[i2][2]);
    }

    if(at1App[i1]==1){
      if (verbose) {
        fprintf(stderr,"enlarge: skipping on i1\n");
      }
      goto LCONT;
      /* printf("*=> ATOME %d DU NUAGE 1 DEJA APPARIE.\n",i1); */
    } else {
      at1App[i1]=1;
    }
    if(at2App[i2]==1){
      if (verbose) {
        fprintf(stderr,"enlarge: skipping on i2\n");
      }
      goto LCONT;
      /* printf("*=> ATOME %d DU NUAGE 2 DEJA APPARIE.\n",i2); */
    } else {
      at2App[i2]=1;
    }

    // Adjustment by P. Tuffery, September 2008
    if (!ng1->re[i1]->atmMask[i2]) continue;
    // End adjustment

    if (dist[i].longueur2 < tol2) {
      rep->lpairs[rep->nLPairs].liaison = dist[i].liaison;
      rep->lpairs[rep->nLPairs].longueur2 = dist[i].longueur2;
      rep->nLPairs ++;
      sqrsum += dist[i].longueur2;
      w1 = (ng1->weight) ? ng1->weight[i1] : 1.0;
      w2 = (ng2->weight) ? ng2->weight[i2] : 1.0;
      sqrsumW += dist[i].longueur2 * w1 * w2;
      wSum += w1 * w2;
    } else {
      break;
    }

  LCONT:;
    i++;
  }

  rep->lrmsd = sqrt(sqrsum / (double) rep->nLPairs);
  rep->lrmsdWeight = sqrt(sqrsumW / wSum);

  return (int) (rep->nLPairs);
}

// int enlarge_weighted(DtNuage *ng1, DtNuage *ng2, DtReponse *rep, int nbAtApp, int kds, DtDistRec *dist, double tol, int verbose)
// {
//   char at1App[ng1->n]; //at1App[k]=1 si le k-ème point de ng1 fait déjà parti d'un appariement, 0 sinon
//   char at2App[ng2->n]; //at2App[k]=1 si le k-ème atome de ng2 fait déjà parti d'un appariement, 0 sinon
//   int i,i1,i2;
//   double sqrsum = 0.;
//   double w1, w2, wSum = .0;
// 
//   rep->lpairs = realloc(rep->lpairs, sizeof(DtDistRec) * kds);
//   rep->nLPairs = 0;
// 
//   /* --- calcul du nombre de points appariés --- */
// 
//   memset (at1App, 0, sizeof(char)*(ng1->n));
//   memset (at2App, 0, sizeof(char)*(ng2->n));
// 
//   i=0;
//   nbAtApp = 0;
//   while(i<(kds) ) {
//     i1=(dist[i].liaison)%(ng1->n);
//     i2=(dist[i].liaison)/(ng1->n);
// 
//     if (verbose) {
//       fprintf(stderr,"enlarge pair %d : %s %d %c - %d : %s %d %c dist %.5lf (%.3lf %.3lf %.3lf / %.3lf %.3lf %.3lf)\n",
// 	      i1, ng1->resName[i1], ng1->resNum[i1], ng1->chn[i1],
// 	      i2, ng2->resName[i2], ng2->resNum[i2], ng2->chn[i2],dist[i].longueur,
// 	      ng1->crds[i1][0], ng1->crds[i1][1], ng1->crds[i1][2],
// 	      ng2->crds[i2][0], ng2->crds[i2][1], ng2->crds[i2][2]);
//       /* printf("*%d\tAT1 - AT2 : %d - %d\t%lf\n",
// 	 i,i1,i2,dist[i].longueur); */
//       /* printPoint3Crds(ng1->crds,i1);
// 	 printPoint3Crds(ng2->crds,i2); */
//     }
// 
//     if(at1App[i1]==1){
//       if (verbose) {
//         fprintf(stderr,"enlarge: skipping on i1\n");
//       }
//       goto LCONT;
//       /* printf("*=> ATOME %d DU NUAGE 1 DEJA APPARIE.\n",i1); */
//     } else {
//       at1App[i1]=1;
//     }
//     if(at2App[i2]==1){
//       if (verbose) {
//         fprintf(stderr,"enlarge: skipping on i2\n");
//       }
//       goto LCONT;
//       /* printf("*=> ATOME %d DU NUAGE 2 DEJA APPARIE.\n",i2); */
//     } else {
//       at2App[i2]=1;
//     }
//     if (dist[i].longueur < tol) {
//       rep->lpairs[rep->nLPairs].liaison = dist[i].liaison;
//       rep->lpairs[rep->nLPairs].longueur = dist[i].longueur;
//       rep->nLPairs ++;
//       w1 = (ng1->weight) ? ng1->weight[i1] : 1.0;
//       w2 = (ng2->weight) ? ng2->weight[i2] : 1.0;
//       sqrsum += (dist[i].longueur * dist[i].longueur) * w1 * w2;
//       wSum += w1 * w2;
//     } else {
//       break;
//     }
// 
//   LCONT:;
//     i++;
//   }
// 
//   rep->lrmsd = sqrt(sqrsum / wSum);
// 
//   return (int) (rep->nLPairs);
// }

int enlarge3(DtNuage *ng1, DtNuage *ng2, int nbAtApp, int kds, DtDistRec *dist, double tol)
{
  char at1App[ng1->n]; //at1App[k]=1 si le k-ème point de ng1 fait déjà parti d'un appariement, 0 sinon
  char at2App[ng2->n]; //at2App[k]=1 si le k-ème atome de ng2 fait déjà parti d'un appariement, 0 sinon
  int i,i1,i2, nLPairs;
  double tol2 = tol * tol;

  nLPairs = 0;

  /* --- calcul du nombre de points appariés --- */

  memset (at1App, 0, sizeof(char)*(ng1->n));
  memset (at2App, 0, sizeof(char)*(ng2->n));

  for (i=0; i < kds; i++) {
    i1=(dist[i].liaison)%(ng1->n);
    i2=(dist[i].liaison)/(ng1->n);

    dist[i].longueur2 = squared_distance((ng1->crds[i1]), (ng2->crds[i2]));

    if(at1App[i1]==1){
      continue;
    } else {
      at1App[i1]=1;
    }

    if(at2App[i2]==1){
      continue;
    } else {
      at2App[i2]=1;
    }

    // Adjustment by P. Tuffery, September 2008
    if (!ng1->re[i1]->atmMask[i2]) continue;
    // End adjustment

    if (dist[i].longueur2 < tol2) {
      nLPairs ++;
    }
  }

  return (int) (nLPairs);
}

// int enlarge2(DtNuage *ng1, DtNuage *ng2, int nbAtApp, int kds, DtDistRec *dist, double tol)
// {
//   char at1App[ng1->n]; //at1App[k]=1 si le k-ème point de ng1 fait déjà parti d'un appariement, 0 sinon
//   char at2App[ng2->n]; //at2App[k]=1 si le k-ème atome de ng2 fait déjà parti d'un appariement, 0 sinon
//   int i,i1,i2, nLPairs;
// //  int shift;
// //  double lg;
// 
//   nLPairs = 0;
// 
//   /* --- calcul du nombre de points appariés --- */
// 
//   memset (at1App, 0, sizeof(char)*(ng1->n));
//   memset (at2App, 0, sizeof(char)*(ng2->n));
// 
//   i=0;
//   nbAtApp = 0;
//   while(i<(kds) ) {
//     i1=(dist[i].liaison)%(ng1->n);
//     i2=(dist[i].liaison)/(ng1->n);
// 
//     /* printf("*%d\tAT1 - AT2 : %d - %d\t%lf\n",
//        i,i1,i2,dist[i].longueur); */
//     /* printPoint3Crds(ng1->crds,i1);
//     printPoint3Crds(ng2->crds,i2); */
//     dist[i].longueur = distance((ng1->crds[i1]),(ng2->crds[i2]));
// 
//     if(at1App[i1]==1){
//       goto LCONT;
//       /* printf("*=> ATOME %d DU NUAGE 1 DEJA APPARIE.\n",i1); */
//     } else {
//       at1App[i1]=1;
//     }
//     if(at2App[i2]==1){
//       goto LCONT;
//       /* printf("*=> ATOME %d DU NUAGE 2 DEJA APPARIE.\n",i2); */
//     } else {
//       at2App[i2]=1;
//     }
//     if (dist[i].longueur < tol) {
//       nLPairs ++;
//     }
//   LCONT:;
//     i++;
//   }
// 
//   // fprintf(stderr,"N lapparies: %d\n",rep->nLPairs);
// 
//   return (int) (nLPairs);
// }


/* =========================================================================
 * Transfert les données de sdm à zuker_superpose.
 * (renvoie les (nbAtApp) coordonnées à superposer au mieux l'une sur l'autre).
 * =========================================================================
 */

void crdsASuperposer(DtNuage *ng1, DtNuage *ng2, int nbAtApp, DtDistRec *dist, DtPoint3 *crdsSup1, DtPoint3 *crdsSup2, double *weightSup1, double *weightSup2)
{
  int i;
  int i1;
  int i2;

  for(i=0 ; i < nbAtApp ; i++){
    i1=(dist[i].liaison)%(ng1->n);
    i2=(dist[i].liaison)/(ng1->n);
    copyPoint3(ng1->crds[i1],crdsSup1[i]);
    copyPoint3(ng2->crds[i2],crdsSup2[i]);
    weightSup1[i] = (ng1->weight) ? ng1->weight[i1] : 1.0; // poids associés aux atomes
    weightSup2[i] = (ng2->weight) ? ng2->weight[i2] : 1.0;
/*    fprintf(stderr, "%lf %lf %lf\t%lf %lf %lf\n", crdsSup1[i][0], crdsSup1[i][1], crdsSup1[i][2], crdsSup2[i][0], crdsSup2[i][1], crdsSup2[i][2]);*/
  }
}

void crdsASuperposerFromVects(DtNuage *ng1, DtNuage *ng2,
			      int nbAtApp, int *v1, int *v2,
			      DtPoint3 *crdsSup1, DtPoint3 *crdsSup2)
{
  int i;
  int i1;
  int i2;

  fprintf(stderr,"crdsASuperposerFromVects\n");
  for(i=0 ; i < nbAtApp ; i++){
    i1=v1[i] - 1;
    i2=v2[i] - 1;
    copyPoint3(ng1->crds[i1],crdsSup1[i]);
    copyPoint3(ng2->crds[i2],crdsSup2[i]);
  }
  fprintf(stderr,"crdsASuperposerFromVects: Done ...\n");
}

/* ====================== ALGORITHME CSR =====================================
 * Superpose au mieux deux nuages de points avec SDM et zuker_superpose.
 * Retourne :
 *   le nombre de points appariés.
 *   la matrice de rotation 4x4 correspondant à la meilleure superposition.
 *   le tableau trié des distances entre points après rotation et translation.
 *   plNg2 est une copie de ng2, permettant de garantir que ng2 sera inchange ...
 *
 * 2009:
 * fast is a switch to consider bc1 and ev1 to heuristicly propose superimpositions
 * based on eigen vectors.
 *
 * ===========================================================================
 */
DtReponse *csr2(DtNuage *ng1, DtNuage *ng2, DtNuage *plNg2, double dseuil, int maxPairs, DtDistRec *pairs, DtMatrix4x4 mat, DtPoint3 *crdsSup1, DtPoint3 *crdsSup2, double *weightSup1, double *weightSup2, int nbIter, int minAtm, double maxRMSd, double tol, DtReponse *rep, int fast, DtPoint3 bc1, DtMatrix3x3 ev1, FILE *osdf, int verbose)
{
  DtMatrix4x4 imat, rmat;
  DtMatrix4x4 RX, RY, RZ;
  DtMatrix4x4 *pMat;
  double ormsd, rmsd;
  double minRMSd = 2.;
  int compteur,nbAtApp,nbLAtApp,nbAtTemp;
  int bestFound = 0;
  int OK;
  int curNPairs;
  int minN1N2;
  int stockp = 0;
  int aDot;
  int foundp = 0;

  minN1N2 = min(ng1->n, ng2->n);

  if (verbose) {
    fprintf(stderr,"CSR inits over. searching %d atoms vs %d atoms. Minat %d\n",ng1->n,ng2->n, minAtm);
    fflush(stderr);
  }

  if (fast) {
    MkArbitraryAxisRotMat4x4( bc1, ev1[0], DcPI, RX );
    MkArbitraryAxisRotMat4x4( bc1, ev1[1], DcPI, RY );
    MkArbitraryAxisRotMat4x4( bc1, ev1[2], DcPI, RZ );
  }

  compteur = 0;
  while (compteur < nbIter) {

    if (verbose) {
      if (!((compteur+1) % 100)) {
	fprintf(stderr,"iteration.%5d\n",compteur+1);
      }
    }

    rmsd = -1.;
    nbAtTemp = 1;
    MkNullMat4x4(imat);

    // Reset ng2 to original orientation
    memcpy(ng2->crds, plNg2->crds, sizeof(DtPoint3) * ng2->n);

    // 1. Orientation aleatoire initiale. imat sert juste a ne pas reallouer une matrice a chaque fois.
    if (fast && (compteur < 4)) {
      if (compteur) { /* first, we do nothing, then we switch */
	if (compteur == 1) {
	  pMat = &RY;
	} else
	if (compteur == 2) {
	  pMat = &RX;
	} else
	if (compteur == 3) {
	  mulMat4x4(RX,RY,rmat);
	  pMat = & rmat;
	}
	for (aDot = 0; aDot < ng2->n; aDot++) {
	  singleRotate(ng2->crds[aDot],*pMat);
	  //    print_vector (c2[aDot], n, "out");
	}
      }
      // outSDFMatch(osdf, NULL, ng2, ng1,"NG2 AL");

    } else {
      // break;
      posAl2(ng1, ng2, imat);
    }

    // 2. Perform first SDM call
    // SDM: retourne nombre d'atomes apparié
    // curNPairs est l'ecart quadratique moyen entre les coordonnées
    nbAtApp = sdm2(ng1, ng2, dseuil, maxPairs, &curNPairs, pairs);

    // Incorrect starting point: we pass
    if (nbAtApp <= 1) {
      compteur += 1;
      continue;
    }

    // Preparation Zuker
    memcpy(ng2->crds, plNg2->crds, sizeof(DtPoint3) * ng2->n);
    crdsASuperposer(ng1, ng2, nbAtApp, pairs, crdsSup1, crdsSup2, weightSup1, weightSup2);
    // zuker returns squared rmsd
    rmsd = zuker_superpose_weighted(crdsSup1, crdsSup2, nbAtApp, rmat, weightSup1, weightSup2);
    rotateCrds(ng2->crds, ng2->n, rmat);

    /* --- Stockage de la réponse si meilleure --- */
    /* Pb du "meilleur" */
    if (rmsd < maxRMSd) {

      /* BUG: for enlarging, we MUST recompute the distances ... */
      nbLAtApp = enlarge3(ng1, ng2, nbAtApp, curNPairs, pairs, tol);

      if ((nbAtApp >= rep->nbAtMax) && (nbLAtApp >= rep->nLPairs)) {
	stockRep2(curNPairs, nbAtApp, pairs, sqrt(rmsd), rmat,rep);
	stockp = 1;
	bestFound = compteur;
	nbLAtApp = enlarge_weighted2(ng1, ng2, rep, nbAtApp, curNPairs, pairs, tol, 0);
	if (verbose) {
	  fprintf(stderr,"Iteration : %5d # paired atoms : %4d RMSd : %.2lf enlarged # paired atoms %d\n",
		  compteur+1,nbAtApp, sqrt(rmsd), nbLAtApp);
	}
      }
      // if ((nbAtApp == minN1N2) && (nbLAtApp == minN1N2) && (sqrt(rmsd) < minRMSd)) break;
      if ((nbAtApp == minN1N2) && (sqrt(rmsd) < minRMSd)) break;
    }

    // 3. Iterate on SDM
    // On fait des itérations SDM tant que le nombre d'atomes apparié croît pour que le fit soit meilleur
    OK  = 1;
    while (OK) {

      /* --- Nouvel appel de zuker-sdm pour un meilleur appariement--- */
      nbAtTemp  = nbAtApp;
      ormsd     = rmsd;

      nbAtApp = sdm2(ng1, ng2, dseuil, maxPairs, &curNPairs, pairs);

      /* Should we keep on ? */
      if (nbAtApp < nbAtTemp) {
	break;
      }

      memcpy(ng2->crds, plNg2->crds, sizeof(DtPoint3) * ng2->n);
      crdsASuperposer(ng1, ng2, nbAtApp, pairs, crdsSup1, crdsSup2, weightSup1, weightSup2);
      rmsd = zuker_superpose_weighted(crdsSup1, crdsSup2, nbAtApp, rmat, weightSup1, weightSup2);
      rotateCrds(ng2->crds, ng2->n, rmat);

     /* Should we keep on ? */
      if (nbAtApp == nbAtTemp) {
	/* Check if better fit */
	if (rmsd >= ormsd) {
	  break;
	}
	/* Check if pairs have changed */
      }

      /* --- Stockage de la réponse si meilleure --- */
      /* Pb du "meilleur" */
      if (rmsd < maxRMSd) {
	nbLAtApp = enlarge3(ng1, ng2, nbAtApp, curNPairs, pairs, tol);

        if ((nbAtApp >= rep->nbAtMax) && (nbLAtApp > rep->nLPairs)) {
          stockRep2(curNPairs, nbAtApp, pairs, sqrt(rmsd), rmat,rep);
          stockp = 1;
          nbLAtApp = enlarge_weighted2(ng1, ng2, rep, nbAtApp, curNPairs, pairs, tol, 0);
	  bestFound = compteur;

	  if (rep->nLPairs >= minAtm) {
	    foundp = 1;
	  }

	  if (verbose) {
	    fprintf(stderr,"Iteration : %5d # paired atoms : %4d RMSd : %.2lf enlarged # paired atoms %d\n",
		    compteur+1,nbAtApp, sqrt(rmsd), nbLAtApp);
	  }
	}
	// if ((nbAtApp == minN1N2) && (nbLAtApp == minN1N2) && (sqrt(rmsd) < minRMSd)) break;
	if ((nbAtApp == minN1N2) && (sqrt(rmsd) < minRMSd)) {
	  break;
	}
      }
    } /* sdm2 convergence */

    /* Found a solution that contains more than minimal number of atoms, better than given RMSd ? we stop */
    if ((rep->nbAtMax == minN1N2) && (rep->nLPairs == minN1N2) && (rep->rmsd < minRMSd)) {
      break;
    }


    compteur=compteur+1;
  } /* CSR iterations */

  memcpy(ng2->crds, plNg2->crds,sizeof(DtPoint3) * ng2->n);

  if (stockp) {
    rotateCrds(ng2->crds,ng2->n,rep->rM);
  }

  fprintf(stderr,"csr2 found best solution at iteration %d\n",bestFound);
  if (foundp)
    gFound += bestFound;
  return rep;
}

// DtReponse *csr(DtNuage *ng1, DtNuage *ng2, DtNuage *plNg2, double dseuil, int maxPairs, DtDistRec *pairs, DtMatrix4x4 mat, DtPoint3 *crdsSup1, DtPoint3 *crdsSup2, double *weightSup1, double *weightSup2, int nbIter, int minAtm, double maxRMSd, double tol, DtReponse *rep, int verbose)
// {
//   //  DtNuage *plNg2;
//   DtMatrix4x4 imat, rmat;
//   double ormsd, rmsd;
//   double minRMSd = 2.;
//   int compteur,nbAtApp,nbLAtApp,nbAtTemp;
// //  int i, i1, i2;
//   int OK;
//   int curNPairs;
//   int minN1N2;
//   int stockp = 0;
// 
//   minN1N2 = min(ng1->n, ng2->n);
// 
//   // Copy original crds of ng2 !!
//   //  plNg2 = allocNuage(ng2->n);
//   // memcpy(plNg2->crds,ng2->crds, sizeof(DtPoint3) * ng2->n);
// 
//   if (verbose) {
//     fprintf(stderr,"CSR inits over. searching %d atoms vs %d atoms. Minat %d\n",ng1->n,ng2->n, minAtm);
//     fflush(stderr);
//   }
// 
//   // We do not need to copy atmName and resName
//   // DtPoint3 *crds;     /* Coordonnees */
//   // DtStr4   *atmName;  /* Nom atome   */
//   // DtStr3   *resName;  /* Nom residu  */
// 
// #if 0
//   crdsASuperposerFromVects(ng1, ng2, 34, v1, v2, crdsSup1, crdsSup2);
//   rmsd=zuker_superpose(crdsSup1,crdsSup2,34,mat);
//   fprintf(stderr,"v1v2 RMSd %lf\n",sqrt(rmsd));
//   exit(0);
// #endif
// 
//   compteur = 0;
//   while (compteur < nbIter){
// 
//     if (verbose) {
//       if (!((compteur+1) % 100)) {
// 	fprintf(stderr,"iteration.%5d\n",compteur+1);
//       }
//     }
// 
//     rmsd=-1.;
//     nbAtTemp=1;
//     MkNullMat4x4(imat);
//     //posAl(ng1, ng2, mat);
// 
//     // Reset ng2 to original orientation 
//     memcpy(ng2->crds, plNg2->crds,sizeof(DtPoint3) * ng2->n);
// 
//     // 1. Orientation aleatoire initiale
// 
//     // fprintf(stderr,"CSR will posAl2\n");
//     // fflush(stderr);
//     posAl2(ng1, ng2, imat);
//     // Affichage crds pour debug
//     // printNgCrds(ng1->crds,ng1->n);
//     // printNgCrds(ng2->crds,ng2->n);
// 
//     // 2. Perform first SDM call
//     // SDM: retourne nombre d'atomes apparié
//     // curNPairs est l'ecart quadratique moyen entre les coordonnées
//     // fflush(stderr);
//     nbAtApp=sdm(ng1,ng2,dseuil,maxPairs,&curNPairs,pairs);
//     // fprintf(stderr,"CSR  sdm over\n");
//     // fflush(stderr);
//     // fprintf(stderr,"nbAtApp ori %d\n",nbAtApp);
// 
//     // Incorrect starting point: we pass
//     if (nbAtApp <= 1) {
//       compteur += 1;
//       // fprintf(stderr,"Continuing on (nbAtApp <= 1 : %d)\n", nbAtApp);
//       continue;
//     }
// 
//     // Preparation Zuker
//     memcpy(ng2->crds, plNg2->crds,sizeof(DtPoint3) * ng2->n);
//     crdsASuperposer(ng1,ng2,nbAtApp,pairs,crdsSup1,crdsSup2,weightSup1,weightSup2);
//     // zuker returns squared rmsd
//     rmsd=zuker_superpose_weighted(crdsSup1,crdsSup2,nbAtApp,rmat,weightSup1,weightSup2);
//     rotateCrds(ng2->crds,ng2->n,rmat);
// 
// //     if (0 && verbose)
// //       fprintf(stderr,"Iteration : %5d # paired atoms : %4d RMSd : %.2lf n",
// // 	      compteur+1,nbAtApp, sqrt(rmsd));
// 
// 
//     /* --- Stockage de la réponse si meilleure --- */
//     /* Pb du "meilleur" */
//     if(rmsd < maxRMSd) {
// 
//       /* BUG: for enlarging, we MUST recompute the distances ... */
//       nbLAtApp = enlarge2(ng1, ng2, nbAtApp, curNPairs, pairs, tol);
//       //	fprintf(stderr,"Iteration : %5d # paired atoms : %4d RMSd : %.2lf enlarged # paired atoms %d\n",
//       //	compteur+1,nbAtApp, sqrt(rmsd), nbLAtApp);
// 
//       if ((nbAtApp >= rep->nbAtMax) && (nbLAtApp >= rep->nLPairs)) {
// 	stockRep(curNPairs,nbAtApp,pairs,sqrt(rmsd), rmat,rep);
// 	stockp = 1;
// 	nbLAtApp = enlarge_weighted(ng1, ng2, rep, nbAtApp, curNPairs, pairs, tol, 0);
// 	if (verbose) {
// 	  fprintf(stderr,"Iteration : %5d # paired atoms : %4d RMSd : %.2lf enlarged # paired atoms %d\n",
// 		  compteur+1,nbAtApp, sqrt(rmsd), nbLAtApp);
// 	}
//       }
//       // if ((nbAtApp == minN1N2) && (nbLAtApp == minN1N2) && (sqrt(rmsd) < minRMSd)) break;
//       if ((nbAtApp == minN1N2) && (sqrt(rmsd) < minRMSd)) break;
//     }
// 
//     // 3. Iterate on SDM
//     // On fait des itérations SDM tant que le nombre d'atomes apparié croît pour que le fit soit meilleur
//     OK  = 1;
//     while(OK){
// 
//       /* --- Nouvel appel de zuker-sdm pour un meilleur appariement--- */
//       // fprintf(stderr,"Iteration %d OK loop\n",compteur);
//       nbAtTemp  = nbAtApp;
//       ormsd     = rmsd;
// 
//       /* printf("rms1 %lf\t",rmsd);
// 	 crdsASuperposer(ng1,ng2,nbAtApp,pairs,crdsSup1,crdsSup2);
// 	 rmsd=zuker_superpose(crdsSup1,crdsSup2,nbAtApp,mat);
// 	 rotateCrds(ng2->crds,ng2->n,mat);
// 	 printf("rms2 %lf\n",rmsd); */
// 
//       // fprintf(stderr,"CSR will sdm\n");
//       // fflush(stderr);
// 
//       nbAtApp=sdm(ng1,ng2,dseuil,maxPairs,&curNPairs,pairs);
//       // fprintf(stderr,"CSR sdm over\n");
//       // fflush(stderr);
// 
//       /* Should we keep on ? */
//       if (nbAtApp < nbAtTemp) {
// 	break;
//       }
// 
//       // fprintf(stderr,"CSR will zucker\n");
//       // fflush(stderr);
//       memcpy(ng2->crds, plNg2->crds,sizeof(DtPoint3) * ng2->n);
//       crdsASuperposer(ng1,ng2,nbAtApp,pairs,crdsSup1,crdsSup2,weightSup1,weightSup2);
//       rmsd=zuker_superpose_weighted(crdsSup1,crdsSup2,nbAtApp,rmat,weightSup1,weightSup2);
//       rotateCrds(ng2->crds,ng2->n,rmat);
//       // fprintf(stderr,"CSR  zucker done\n");
//       // fflush(stderr);
// 
//      /* Should we keep on ? */
//       if (nbAtApp == nbAtTemp) {
// 	/* Check if better fit */
// 	if (rmsd >= ormsd) {
// 	  break;
// 	}
// 	/* Check if pairs have changed */
//       }
// 
//       /* --- Stockage de la réponse si meilleure --- */
//       /* Pb du "meilleur" */
//       if(rmsd < maxRMSd) {
// 	// fprintf(stderr,"CSR will enlarge\n");
// 	// fflush(stderr);
// 
// 	nbLAtApp = enlarge2(ng1, ng2, nbAtApp, curNPairs, pairs, tol);
// 	// fprintf(stderr,"CSR will enlarge over\n");
// 	// fflush(stderr);
//         if ((nbAtApp >= rep->nbAtMax) && (nbLAtApp > rep->nLPairs)) {
//           stockRep(curNPairs,nbAtApp,pairs,sqrt(rmsd), rmat,rep);
//           stockp = 1;
//           nbLAtApp = enlarge_weighted(ng1, ng2, rep, nbAtApp, curNPairs, pairs, tol, 0);
//           // printMat4x4(rep->rM);
// 
// 	  if (verbose) {
// 	    fprintf(stderr,"Iteration : %5d # paired atoms : %4d RMSd : %.2lf enlarged # paired atoms %d\n",
// 		    compteur+1,nbAtApp, sqrt(rmsd), nbLAtApp);
// 	  }
// 	}
// 	// if ((nbAtApp == minN1N2) && (nbLAtApp == minN1N2) && (sqrt(rmsd) < minRMSd)) break;
// 	if ((nbAtApp == minN1N2) && (sqrt(rmsd) < minRMSd)) break;
//       }
//     }
// 
//     if ((rep->nbAtMax == minN1N2) && (rep->nLPairs == minN1N2) && (rep->rmsd < minRMSd)) break;
// 
//     /* printf("\nSortie boucle : %d <= %d\n",nbAtApp,nbAtTemp); */
//     compteur=compteur+1;
//     /* printf("\n"); */
//     // exit(0);
//   }
// 
//   // fprintf(stderr,"CSR loop over\n");
//   // fflush(stderr);
//   memcpy(ng2->crds, plNg2->crds,sizeof(DtPoint3) * ng2->n);
//   if (stockp) {
//     rotateCrds(ng2->crds,ng2->n,rep->rM);
//     //printMat4x4(rep->rM);
//   }
//   // freeNuage(plNg2);
// 
//   return rep;
// }
