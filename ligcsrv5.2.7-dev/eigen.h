extern int qr_eigen(double **, int , int , double, double *, double **);
extern int power(double **, int , int , double, double *, double *);
extern int shift_power(double **, int , int , double, double *, double *);
extern double **alloc_mat(int , int );
extern void free_mat(double **, int);
extern void print_matrice(double **, int, int, char *);
extern void print_vector (double *, int,  char *);
extern double best_shift(double **, int);
