#include <stdio.h>
#include "eigen.h"

main() {
  int n = 4;

  double v[n];
  double **w = alloc_mat(n,n);
  double **a = alloc_mat(n,n);

  a[0][0]=3;
  a[1][0]=1;
  a[2][0]=1;
  a[3][0]=1;

  a[0][1]=1;
  a[1][1]=-1;
  a[2][1]=2;
  a[3][1]=2;

  a[0][2]=1;   
  a[1][2]=2;
  a[2][2]=-1;
  a[3][2]=2;

  a[0][3]=1;
  a[1][3]=2;
  a[2][3]=2;
  a[3][3]=-1;


  printf ("%d iterations \n",qr_eigen(a, n, 10000, 1.e-8, v, w));

  print_matrice(a, n, n, "a");
  print_vector (v, n, "v");
  print_matrice(w, n, n, "w");
  
  free_mat(w, n);
  free_mat(a, n);
}
