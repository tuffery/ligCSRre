/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "CSRTypes.h"

/* ==========================================
 * Calcul la distance entre deux points.
 * ==========================================
 */

double distance(DtPoint3 R, DtPoint3 K)
{
  return (double) sqrt((double) ((K[0] - R[0]) * (K[0] - R[0]) +
                                  (K[1] - R[1]) * (K[1] - R[1]) +
                                  (K[2] - R[2]) * (K[2] - R[2])));
}


/* -----  produit vectoriel A^B dans C -----------------------------  */
void cross_product(DtPoint3 a, DtPoint3 b, DtPoint3 c)
{
  c[0] = a[1] * b[2] - a[2] * b[1];
  c[1] = b[0] * a[2] - a[0] * b[2];
  c[2] = a[0] * b[1] - a[1] * b[0];
}

/* ==============================================================
 * Fonction de comparaison des longueurs dans un DtpDistRec.
 * (Appel avec quicksort).
 * ==============================================================
 */

int cmpdistRec2(const void *dist, const void *dist2)
{
  DtDistRec *d = (DtDistRec *)dist;
  DtDistRec *d2 = (DtDistRec *)dist2;
  if ((d->longueur2) > (d2->longueur2)) return 1;
  if ((d->longueur2) < (d2->longueur2)) return (-1);
  return 0;
}

// int cmpdistRec(const void *dist, const void *dist2)
// {
//   DtDistRec *d = (DtDistRec *)dist;
//   DtDistRec *d2 = (DtDistRec *)dist2;
//   if ((d->longueur) > (d2->longueur)) return 1;
//   if ((d->longueur) < (d2->longueur)) return (-1);
//   return 0;
// }

// int cmpdist(double *dist, double *dist2)
// {
//   if (dist[0] > dist2[0]) return 1;
//   if (dist[0] < dist2[0]) return (-1);
//   return 0;
// }


/* ===================================================================
 * Imprime les coordonnées du point i dans un tableau de coordonnées.
 * ===================================================================
 */

void printPoint3Crds(DtPoint3 *crds, int i)
{
  printf("*CRDS ATOME %d\tX=%.8lf\tY=%.8lf\tZ=%.8lf\n",
	 i,crds[i][0],crds[i][1],crds[i][2]);
}


/* ======================================================
 * Multiplication de deux matrices 4x4 (A x B dans C). 
 * ======================================================
 */

void mulMat4x4(DtMatrix4x4 A,DtMatrix4x4 B,DtMatrix4x4 C)
{
  int i,j,k;
  boucle(i,0,4) {
    boucle(j,0,4) {
      C[i][j] = 0.;
      boucle(k,0,4) C[i][j] += A[i][k]*B[k][j];
    }
  }
}



/* ===============================================================
 * Création d'une matrice de rotation 
 * d'angle teta autour d'un axe (A,B). 
 * =============================================================== 
 */

void MkArbitraryAxisRotMat4x4(DtPoint3 A,DtPoint3 B,DtFloat angle,DtMatrix4x4 M){
  extern double sqrt(), sin(), cos();

  DtFloat rx, ry, rz, lnorme, rx2, rxry, rxrz, ry2, ryrz, rz2,
  rxry1mcosa, rxrz1mcosa, ryrz1mcosa;

  DtFloat cosa, sina;

  cosa = cos((double) angle);
  sina = sin((double) angle);

  rx = B[0] - A[0];
  ry = B[1] - A[1];
  rz = B[2] - A[2];
  lnorme = (DtFloat) sqrt(rx*rx + ry*ry + rz*rz);
  rx /= lnorme;
  ry /= lnorme;
  rz /= lnorme;

  rx2 = rx*rx;
  ry2 = ry*ry;
  rz2 = rz*rz;
  rxry = rx*ry;
  rxrz = rx*rz;
  ryrz = ry*rz;
  rxry1mcosa = rxry * (1. - cosa);
  rxrz1mcosa = rxrz * (1. - cosa);
  ryrz1mcosa = ryrz * (1. - cosa);

  M[0][0] = rx2 + (1. - rx2) * cosa;
  M[1][0] = rxry1mcosa - rz * sina;
  M[2][0] = rxrz1mcosa + ry * sina;

  M[0][1] = rxry1mcosa + rz * sina;
  M[1][1] = ry2 + (1. - ry2) * cosa;
  M[2][1] = ryrz1mcosa - rx * sina;

  M[0][2] = rxrz1mcosa - ry * sina;
  M[1][2] = ryrz1mcosa + rx * sina;
  M[2][2] = rz2 + (1. - rz2) * cosa;

  M[3][0] =  A[0] * (1 - M[0][0]) - A[1] * M[1][0] - A[2] * M[2][0];
  M[3][1] = -A[0] * M[0][1] + A[1] * (1 - M[1][1]) - A[2] * M[2][1];
  M[3][2] = -A[0] * M[0][2] - A[1] * M[1][2] + A[2] * (1 - M[2][2]);

  M[0][3] = M[1][3] = M[2][3] = 0.;
  M[3][3] = 1.;
/*
fprintf(stderr,"  %f  %f  %f  %f\n",M[0][0],M[0][1],M[0][2],M[0][3]);
fprintf(stderr,"  %f  %f  %f  %f\n",M[1][0],M[1][1],M[1][2],M[1][3]);
fprintf(stderr,"  %f  %f  %f  %f\n",M[2][0],M[2][1],M[2][2],M[2][3]);
fprintf(stderr,"  %f  %f  %f  %f\n",M[3][0],M[3][1],M[3][2],M[3][3]);
*/
}


/* ================================
 * Calcul la norme d'un vecteur.
 * ================================
 */

DtFloat norme(DtPoint3 a)
{
  return (DtFloat) sqrt(a[0] * a[0]  + a[1] * a[1] + a[2] * a[2]);
}



/* ===================================
 * Matrice Nulle 4x4.
 * ===================================
 */

void MkNullMat4x4(DtMatrix4x4 m)
{
 m[0][0] = 0.; m[0][1] = 0.; m[0][2] = 0.; m[0][3] = 0.;
 m[1][0] = 0.; m[1][1] = 0.; m[1][2] = 0.; m[1][3] = 0.;
 m[2][0] = 0.; m[2][1] = 0.; m[2][2] = 0.; m[2][3] = 0.;
 m[3][0] = 0.; m[3][1] = 0.; m[3][2] = 0.; m[3][3] = 0.;
}



/* ==========================================
 * Impression d'une matrice 4x4.
 * ==========================================
 */

void printMat4x4(DtMatrix4x4 m)
{
  int i,j;

  boucle(i,0,4) {
    boucle(j,0,4) fprintf(stderr,"%.8lf ",m[i][j]);
    fprintf(stderr,"\n");
  }
  fprintf(stderr,"\n");
}

void printMat3x3(DtMatrix3x3 m)
{
  int i,j;

  boucle(i,0,3) {
    boucle(j,0,3) fprintf(stderr,"%.8lf ",m[i][j]);
    fprintf(stderr,"\n");
  }
  fprintf(stderr,"\n");
}

/* ==========================================================
 * Rotation d'un point par une matrice de rotation.
 * ==========================================================
 */

void singleRotate(DtPoint3 p,DtMatrix4x4 rmat)
{
  double x,y,z;

  x = p[0] * rmat[0][0] + p[1] * rmat[1][0] + p[2] * rmat[2][0] + rmat[3][0];
  y = p[0] * rmat[0][1] + p[1] * rmat[1][1] + p[2] * rmat[2][1] + rmat[3][1];
  z = p[0] * rmat[0][2] + p[1] * rmat[1][2] + p[2] * rmat[2][2] + rmat[3][2];
  p[0] = x;
  p[1] = y;
  p[2] = z;
}




/* ========================================================================
 * Rotation d'un nuage de (nbPoints) points par une matrice de rotation.
 * ========================================================================
 */


void rotateCrds(DtCrds crds2, int nbPoints, DtMatrix4x4 rmat)
{
  int i;

  for (i=0 ; i < nbPoints ; i++){
    singleRotate(crds2[i],rmat);
  }
}




/* ==========================================
 * Translation d'un point par un vecteur.
 * ==========================================
 */

void simpleTranslate(DtPoint3 p, DtPoint3 tr)
{
  p[0] += tr[0];
  p[1] += tr[1];
  p[2] += tr[2];
}


/* Matrices 3 x 3 (a transposee dans a) ----------------------------- */
void transpMat3x3(DtMatrix3x3 m)
{ 
  DtFloat c;
  int i,j;
  boucle(i,0,3) {
    boucle(j,i+1,3) {
      c = m[i][j];
      m[i][j] = m[j][i];
      m[j][i] = c;
    }
  }
}

/* Matrices 4 x 4 (a transposee dans a) ----------------------------- */
void transpMat4x4(DtMatrix4x4 m)
{ 
  DtFloat c;
  int i,j;
  boucle(i,0,4) {
    boucle(j,i+1,4) {
      c = m[i][j];
      m[i][j] = m[j][i];
      m[j][i] = c;
    }
  }
}

/* ===============================================
 * Symétrique d'un point par rapport à (0,0,0).
 * ===============================================
 */

void negatePoint3(DtPoint3 p1)
{
  p1[0] = - p1[0];
  p1[1] = - p1[1];
  p1[2] = - p1[2];
}






/* ================================================
 * Copie les coordonnées du point1 dans point2.
 * ================================================
 */
void copyPoint3(DtPoint3 p1, DtPoint3 p2)
{
  p2[0] = p1[0];
  p2[1] = p1[1];
  p2[2] = p1[2];
}




/* =================================================
 * Copie d'une matrice 4 x 4 (recopie a dans b).
 * =================================================
 */

void copyMat4x4(DtMatrix4x4 a,DtMatrix4x4 b)
{
  int i,j;
  boucle(i,0,4) {
    boucle(j,0,4) {
      b[i][j] = a[i][j];
    }
  }
}


/* Matrices 4 x 4 (a dans b) ----------------------------------------- */
void copy3x3to4x4(DtMatrix3x3 a,DtMatrix4x4 b)
{
  int i,j;
  boucle(i,0,3) {
    boucle(j,0,3) {
      b[i][j] = a[i][j];
    }
  }
  b[0][3] = b[1][3] = b[2][3] = b[3][0] = b[3][1] = b[3][2] = 0.;
  b[3][3] = 1.;
}

/* ----- normalise le vecteur A ----------------------------------------- */
void normalize(DtPoint3 a)
{
  double n;
  double s;
  s = (double) (a[0] * a[0]  + a[1] * a[1] + a[2] * a[2]);
  n = sqrt(s);
  a[0] /= (DtFloat) n;
  a[1] /= (DtFloat) n;
  a[2] /= (DtFloat) n;
}


/* ===============================================================
 * ---------- Matrice de rotation autour d'un axe a b = X. -------
 * =============================================================== */
void MkNumXRotMat4x4(DtPoint3 A,DtFloat angle,DtMatrix4x4 M)
{
  DtMatrix4x4 t,r,bf;
  double cost,sint;

  cost = cos(angle);
  sint = sin(angle);

  /* Le calcul de la matrice de transformation */

  /* Translation 1 */

  t[0][0] = 1.; t[0][1] = 0.; t[0][2] = 0.; t[0][3] = 0.;
  t[1][0] = 0.; t[1][1] = 1.; t[1][2] = 0.; t[1][3] = 0.;
  t[2][0] = 0.; t[2][1] = 0.; t[2][2] = 1.; t[2][3] = 0.;
  t[3][0] = -A[0]; t[3][1] = -A[1]; t[3][2] = -A[2]; t[3][3] = 1.;

  /* Rotation */
  r[0][0] = 1.; r[0][1] = 0.;    r[0][2] = 0.;     r[0][3] = 0.;
  r[1][0] = 0.; r[1][1] = cost;  r[1][2] = sint;   r[1][3] = 0.;
  r[2][0] = 0.; r[2][1] = -sint; r[2][2] = cost;   r[2][3] = 0.;
  r[3][0] = 0.; r[3][1] = 0.;    r[3][2] = 0.;     r[3][3] = 1.;

  mulMat4x4(t,r,bf);
/* Translation -1 */

  t[0][0] = 1.; t[0][1] = 0.; t[0][2] = 0.; t[0][3] = 0.;
  t[1][0] = 0.; t[1][1] = 1.; t[1][2] = 0.; t[1][3] = 0.;
  t[2][0] = 0.; t[2][1] = 0.; t[2][2] = 1.; t[2][3] = 0.;
  t[3][0] = A[0]; t[3][1] = A[1]; t[3][2] = A[2]; t[3][3] = 1.;

  mulMat4x4(bf,t,M);

/* fprintf(stderr,"  %f  %f  %f  %f\n",M[0][0],M[0][1],M[0][2],M[0][3]);
   fprintf(stderr,"  %f  %f  %f  %f\n",M[1][0],M[1][1],M[1][2],M[1][3]);
   fprintf(stderr,"  %f  %f  %f  %f\n",M[2][0],M[2][1],M[2][2],M[2][3]);
   fprintf(stderr,"  %f  %f  %f  %f\n",M[3][0],M[3][1],M[3][2],M[3][3]); */

}





/* ===============================================================
 * ------- Matrice de rotation autour d'un axe a b= A. -----------
 * =============================================================== */
void MkNumDRotMat4x4(DtPoint3 A, DtFloat angle, DtMatrix4x4 M)
{
  DtMatrix4x4 t,r,bf,bf1;
  double a,d,cosa,sina,cost,sint;
  /* a = ? */
//  double b,c,f;

  /* Vecteur norme. */
  normalize(A);

  d = sqrt(A[1]*A[1] + A[2]*A[2]);
  if(d != 0.) {
    cosa = A[2]/d;
    sina = A[1]/d;
  } else {
    /* Cas particulier de rotation autour axe X */
    MkNumXRotMat4x4(A,angle,M);
    return;
  }
  cost = cos(angle);
  sint = sin(angle);

  /* fprintf(stderr," x: %f y: %f z: %f f: %f d: %f cosa: %f sina: %f cost: %f sint: %f\n",
     a,b,c,f,d,cosa,sina,cost,sint);
     */

  /* Le calcul de la matrice de transformation */

  /* Translation 1 */
  t[0][0] = 1.; t[0][1] = 0.; t[0][2] = 0.; t[0][3] = 0.;
  t[1][0] = 0.; t[1][1] = 1.; t[1][2] = 0.; t[1][3] = 0.;
  t[2][0] = 0.; t[2][1] = 0.; t[2][2] = 1.; t[2][3] = 0.;
  t[3][0] = -A[0]; t[3][1] = -A[1]; t[3][2] = -A[2]; t[3][3] = 1.;

  /* Rotation 1 */
  r[0][0] = 1.; r[0][1] = 0.;     r[0][2] = 0.;    r[0][3] = 0.;
  r[1][0] = 0.; r[1][1] = cosa;  r[1][2] = sina; r[1][3] = 0.;
  r[2][0] = 0.; r[2][1] = -sina; r[2][2] = cosa; r[2][3] = 0.;
  r[3][0] = 0.; r[3][1] = 0.;     r[3][2] = 0.;    r[3][3] = 1.;

 mulMat4x4(bf,r,bf1);

  /* Rotation */
  r[0][0] = cost;  r[0][1] = sint; r[0][2] = 0.; r[0][3] = 0.;
  r[1][0] = -sint; r[1][1] = cost; r[1][2] = 0.; r[1][3] = 0.;
  r[2][0] = 0.;     r[2][1] = 0.;    r[2][2] = 1.; r[2][3] = 0.;
  r[3][0] = 0.;     r[3][1] = 0.;    r[3][2] = 0.; r[3][3] = 1.;

  mulMat4x4(bf1,r,bf);

  /* Rotation -2 */
  r[0][0] = d; r[0][1] = 0.; r[0][2] = -a; r[0][3] = 0.;
  r[1][0] = 0.; r[1][1] = 1.; r[1][2] = 0.;  r[1][3] = 0.;
  r[2][0] = a; r[2][1] = 0.; r[2][2] = d;  r[2][3] = 0.;
  r[3][0] = 0.; r[3][1] = 0.; r[3][2] = 0.;  r[3][3] = 1.;

  mulMat4x4(bf,r,bf1);

  /* Rotation -1 */
  r[0][0] = 1.; r[0][1] = 0.;    r[0][2] = 0.;     r[0][3] = 0.;
  r[1][0] = 0.; r[1][1] = cosa; r[1][2] = -sina; r[1][3] = 0.;
  r[2][0] = 0.; r[2][1] = sina; r[2][2] = cosa;  r[2][3] = 0.;
  r[3][0] = 0.; r[3][1] = 0.;    r[3][2] = 0.;     r[3][3] = 1.;

  mulMat4x4(bf1,r,bf);

  /* Translation -1 */
  t[0][0] = 1.; t[0][1] = 0.; t[0][2] = 0.; t[0][3] = 0.;
  t[1][0] = 0.; t[1][1] = 1.; t[1][2] = 0.; t[1][3] = 0.;
  t[2][0] = 0.; t[2][1] = 0.; t[2][2] = 1.; t[2][3] = 0.;
  t[3][0] = A[0]; t[3][1] = A[1]; t[3][2] = A[2]; t[3][3] = 1.;

  mulMat4x4(bf,t,M);

}

