/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/times.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include "csr.h"



/* ===================================================
* Renvoie une chaine de caractères en majuscules.
* ===================================================
*/

void strupper(char *s)
{
  while (*s != '\0') {
    *s = toupper(*s);
    s++;
  }
}





/* ====================================================
* Numero d'atome pour le residu
* ====================================================
*/
void getAtomNum(char *line,int *anum)
{
  char buff[30], charAnum[6];

  charAnum[5] = '\0';
  if (sscanf(line,"%6c%5c",buff,charAnum) != EOF);
  *anum = atoi(charAnum);
}





/* ====================================================
* Nom de l'atome
* 3 char lus + 1 blanc pour atom index
* ====================================================
*/
void getAtomName(char *line,char *anom)
{
    char buff[30];

    anom[3] = ' ';
    anom[4] = '\0';
    if (sscanf(line,"%13c%3c",buff,anom) != EOF);
    strupper(anom);
}





/* ====================================================
* Index d'atome pour le residu (cf params.c)
* ====================================================
*/
void getResName(char *line,char *rnom)
{
    char buff[30];

    rnom[3] = '\0';
    if (sscanf(line,"%17c%3c",buff,rnom) != EOF);
    strupper(rnom);
}





/* ====================================================
* Coordonnees de l'atome
* ====================================================
*/
void getAtomCoords(char *line,DtFloat *x,DtFloat *y,DtFloat *z)
{
  char buff[30], charX[9],charY[9],charZ[9];

  charX[8] = charY[8] = charZ[8] = '\0';
  if (sscanf(line,"%30c%8c%8c%8c",buff,charX,charY,charZ) != EOF);
  *x = (DtFloat) atof(charX);
  *y = (DtFloat) atof(charY);
  *z = (DtFloat) atof(charZ);
}





/* ====================================================
* Type d'info contenu dans la ligne du fichier PDB
* 0: info non codee ou Pb.
* 1: ATOM
* 2: HELIX
* 3: SHEET
* 4: TURN
* 5: TER
* 6: CONECT
* 7: END
* 8: MODEL
* 9: ENDMDL
* ====================================================
*/
int getPDBLineStatus(char *d)
{
  char Entete[7];

  if (sscanf(d,"%s",&Entete) == EOF) return 0;
  if (strcmp(Entete,"ATOM") == 0) return 1; //int%Gï¿½%@ressant
  if (strcmp(Entete,"HETATM") == 0) return 8; //int%Gï¿½%@ressant
  if (strcmp(Entete,"HELIX") == 0) return 2;
  if (strcmp(Entete,"SHEET") == 0) return 3;
  if (strcmp(Entete,"TURN") == 0) return 4;
  if (strcmp(Entete,"TER") == 0) return 5;
  if (strcmp(Entete,"CONECT") == 0) return 6;
  if (strcmp(Entete,"END") == 0) return 7;
  if (strcmp(Entete,"MODEL") == 0) return 8;
  if (strcmp(Entete,"ENDMDL") == 0) return 9;
  return 0;
}


/* ===========================================================
 * Renvoie les lignes d'un fichier et leur nombre de lignes.
 * ===========================================================
 */

char ** txtFileRead(char *fname, int *lSze)
{
  struct stat buf;
  char *tmp;
  char **line;
  int fd;
  int fSze, done;
  int nLines;
  int i,j;

  if ((fd = open(fname,O_RDONLY)) < 0) {
    return NULL;
  }

  if (fstat(fd,&buf)) {
    close(fd);
    return NULL;
  }

  if (!S_ISREG (buf.st_mode)) {
    close(fd);
    return NULL;
  }

  if (!(fSze = buf.st_size)) {
    close(fd);
    return NULL;
  }

  if (!(tmp = malloc((fSze+1) * sizeof(char)))) {
    close(fd);
    return NULL;
  }

  done = 0;
  while (done < fSze) {
    int r = read(fd, &(tmp[done]), fSze - done);
    if (r < 0) {
      free(tmp);
      close(fd);
      return NULL;
    }
    done += r;
  }
  close(fd);

  nLines = 0;
  for (i = 0; i < fSze; i++) {
    if (tmp[i] == '\n') nLines++;
  }
  if (tmp[fSze-1] != '\n') nLines++;

  if (!(line = malloc((nLines+2) * sizeof(char *)))) {
      free(tmp);
      return NULL;
  }

  line[0] = tmp;
  j = 1;
  for (i = 0; i < fSze; i++) {
    if (tmp[i] == '\n') {
      line[j++] = &tmp[i+1];
      tmp[i] = '\000';
    }
  }

  line[j] = NULL;
  *lSze = nLines;

  return (line);
}




/* ===============================================
 * Réalloue la mémoire occupée par txtFileRead.
 * ===============================================
 */

char ** txtFileFree(char **l, int lSze)
{
  /* Attention: on ne libere que la premiere ligne,
     car c'est un seul malloc pour l'ensemble des lignes
  */
  free(l[0]);
  free(l);
  return (NULL);
}


/* ============================================================================
 * Remplie les tableaux de données utiles à Csr après lecture des fichiers
 Pdb.
 * Alloue la mémoire pour la structure DtNuage.
 * ============================================================================
 */


void lectPdbCsr (char *fname, DtNuage *n)
{
  int nLines;
  int aLine;
  int lineStat;
  char **lines;


  lines=txtFileRead(fname,&nLines);

  // fprintf(stderr,"read %d lines from %s\n",nLines, fname);


  /* -- Nombre d'atomes ? -- */
  n->n = 0;
  for (aLine=0; aLine < nLines; aLine++) {
      lineStat=getPDBLineStatus(lines[aLine]);
      if(lineStat==1){
	n->n++;
      }
      if(lineStat==8){
	n->n++;
      }
  }


  /* -- Allocation memoire -- */
  n->crds = (DtPoint3 *) calloc(n->n, sizeof(DtPoint3));
  n->atmName = (DtStr4 *) calloc(n->n, sizeof(DtStr4));
  n->resName = (DtStr3 *) calloc(n->n, sizeof(DtStr3));


  /* -- Lecture donnees -- */
  n->n = 0;
  for (aLine=0; aLine < nLines; aLine++) {
      lineStat=getPDBLineStatus(lines[aLine]);
      if ((lineStat!=1) && (lineStat!=8)) continue;

      getAtomCoords(lines[aLine],&n->crds[n->n][0],&n->crds[n->n][1],&n->crds[n->n][2]);
      getAtomName(lines[aLine],n->atmName[n->n]);
      getResName(lines[aLine],n->resName[n->n]);
      n->n ++;
  }


  /* -- liberation memoire -- */
  txtFileFree(lines,nLines);
}


void printNuage(FILE *f, DtNuage *n, char *msg)
{
  int aDot;
  fprintf(f, "%s\n",msg);
  fprintf(f, "n points: %d\n",n->n);
  for (aDot = 0; aDot < n->n; aDot++) {
    fprintf(f, "%s %s %lf %lf %lf\n",
	    n->atmName[aDot],n->resName[aDot], 
	    n->crds[aDot][0],n->crds[aDot][1],n->crds[aDot][2]);
  }
}


/* =====================================================================
 * Initialize pseudo-random generator.
 * Returns the seed value.
 * =====================================================================
 */

void initRnd(long int *seed)
{
  static int isInit = 0;

  if (isInit) return;
  if ((int) *seed == -1) {
    *seed = (long int) time(0);
    srand48(*seed);
  }
  else srand48(*seed);
  isInit = 1;
}




/* ================================================================
 * Returns a random integer from 0 to n (excluded).
 * ================================================================
 */

int irand(int n)
{
  extern double drand48();
  double rn;
  float rnf;

  do {
    rn   = drand48();           /* suppose drand48 does not accept 1.0  */
    rnf  = (float) rn * n;
  } while ((int) rnf >= n);

  return (int) rnf;
}




/* ==========================================
 * Calcul la distance entre deux points.
 * ==========================================
 */

double distance(DtPoint3 R,DtPoint3 K)
{
  return (double) sqrt((double) ((K[0] - R[0]) * (K[0] - R[0]) +
                                  (K[1] - R[1]) * (K[1] - R[1]) +
                                  (K[2] - R[2]) * (K[2] - R[2])));
}





/* ==============================================================
 * Fonction de comparaison des longueurs dans un DtpDistRec.
 * (Appel avec quicksort).
 * ==============================================================
 */

int cmpdist(double *dist, double *dist2)
{
  if (dist[0] > dist2[0]) return 1;
  if (dist[0] < dist2[0]) return (-1);
  return 0;
}





/* ============================================================
 * Imprime les (nbPrint) premières lignes d'un DtpDistRec
 * des nuage d'atomes (ng1) et (ng2).
 * ============================================================
 */

void printDtpDistRec (DtDistRec *dist, int nbPrint, DtNuage ng1,DtNuage ng2)
{
  int i,i1,i2;
  for(i=0; i<nbPrint ; i++){
    i1=(dist[i].liaison)%(ng1.n);
    i2=(dist[i].liaison)/(ng1.n);
    printf("distance %d\t longueur=%.8lf\t liaison=(%d,%d)\n",i,dist[i].longueur,i1,i2);
  }
}






/* =======================================================
 * Imprime les (nbCrds) premières coordonnées
 * d'un tableau de coordonnées.
 * =======================================================
 */


void printNgCrds(DtPoint3 *crds, int nbCrds)
{
  int i,j;

  for(i=0; i<nbCrds ; i++){
    printf("*Atome %d :\t",i);
    for (j=0 ; j< 3 ; j++){
      printf("%.8lf\t",crds[i][j]);
    }
    printf("\n");
  }
  printf("*\n");
}




/* ===================================================================
 * Imprime les coordonnées du point i dans un tableau de coordonnées.
 * ===================================================================
 */

void printPoint3Crds(DtPoint3 *crds, int i)
{
  printf("*CRDS ATOME %d\tX=%.8lf\tY=%.8lf\tZ=%.8lf\n",
	 i,crds[i][0],crds[i][1],crds[i][2]);
}







/* ======================================================
 * Multiplication de deux matrices 4x4 (A x B dans C). 
 * ======================================================
 */

void mulMat4x4(DtMatrix4x4 A,DtMatrix4x4 B,DtMatrix4x4 C)
{
  int i,j,k;
  boucle(i,0,4) {
    boucle(j,0,4) {
      C[i][j] = 0.;
      boucle(k,0,4) C[i][j] += A[i][k]*B[k][j];
    }
  }
}



/* ===============================================================
 * Création d'une matrice de rotation 
 * d'angle teta autour d'un axe (A,B). 
 * =============================================================== 
 */

void MkArbitraryAxisRotMat4x4(DtPoint3 A,DtPoint3 B,DtFloat angle,DtMatrix4x4 M){
  extern double sqrt(), sin(), cos();

  DtFloat rx, ry, rz, lnorme, rx2, rxry, rxrz, ry2, ryrz, rz2,
  rxry1mcosa, rxrz1mcosa, ryrz1mcosa;

  DtFloat cosa, sina;

  cosa = cos((double) angle);
  sina = sin((double) angle);

  rx = B[0] - A[0];
  ry = B[1] - A[1];
  rz = B[2] - A[2];
  lnorme = (DtFloat) sqrt(rx*rx + ry*ry + rz*rz);
  rx /= lnorme;
  ry /= lnorme;
  rz /= lnorme;

  rx2 = rx*rx;
  ry2 = ry*ry;
  rz2 = rz*rz;
  rxry = rx*ry;
  rxrz = rx*rz;
  ryrz = ry*rz;
  rxry1mcosa = rxry * (1. - cosa);
  rxrz1mcosa = rxrz * (1. - cosa);
  ryrz1mcosa = ryrz * (1. - cosa);

  M[0][0] = rx2 + (1. - rx2) * cosa;
  M[1][0] = rxry1mcosa - rz * sina;
  M[2][0] = rxrz1mcosa + ry * sina;

  M[0][1] = rxry1mcosa + rz * sina;
  M[1][1] = ry2 + (1. - ry2) * cosa;
  M[2][1] = ryrz1mcosa - rx * sina;

  M[0][2] = rxrz1mcosa - ry * sina;
  M[1][2] = ryrz1mcosa + rx * sina;
  M[2][2] = rz2 + (1. - rz2) * cosa;

  M[3][0] =  A[0] * (1 - M[0][0]) - A[1] * M[1][0] - A[2] * M[2][0];
  M[3][1] = -A[0] * M[0][1] + A[1] * (1 - M[1][1]) - A[2] * M[2][1];
  M[3][2] = -A[0] * M[0][2] - A[1] * M[1][2] + A[2] * (1 - M[2][2]);

  M[0][3] = M[1][3] = M[2][3] = 0.;
  M[3][3] = 1.;
/*
fprintf(stderr,"  %f  %f  %f  %f\n",M[0][0],M[0][1],M[0][2],M[0][3]);
fprintf(stderr,"  %f  %f  %f  %f\n",M[1][0],M[1][1],M[1][2],M[1][3]);
fprintf(stderr,"  %f  %f  %f  %f\n",M[2][0],M[2][1],M[2][2],M[2][3]);
fprintf(stderr,"  %f  %f  %f  %f\n",M[3][0],M[3][1],M[3][2],M[3][3]);
*/
}


/* ================================
 * Calcul la norme d'un vecteur.
 * ================================
 */

DtFloat norme(DtPoint3 a)
{
  return (DtFloat) sqrt(a[0] * a[0]  + a[1] * a[1] + a[2] * a[2]);
}





/* ===================================
 * Matrice Nulle 4x4.
 * ===================================
 */

void MkNullMat4x4(DtMatrix4x4 m)
{
 m[0][0] = 0.; m[0][1] = 0.; m[0][2] = 0.; m[0][3] = 0.;
 m[1][0] = 0.; m[1][1] = 0.; m[1][2] = 0.; m[1][3] = 0.;
 m[2][0] = 0.; m[2][1] = 0.; m[2][2] = 0.; m[2][3] = 0.;
 m[3][0] = 0.; m[3][1] = 0.; m[3][2] = 0.; m[3][3] = 0.;
}




/* ==========================================
 * Impression d'une matrice 4x4.
 * ==========================================
 */

void printMat4x4(DtMatrix4x4 m)
{
  int i,j;

  
  boucle(i,0,4) {
    boucle(j,0,4) fprintf(stderr,"%.8lf ",m[i][j]);
    fprintf(stderr,"\n");
  }
  fprintf(stderr,"\n");
}





/* ==========================================================
 * Rotation d'un point par une matrice de rotation.
 * ==========================================================
 */

void singleRotate(DtPoint3 p,DtMatrix4x4 rmat)
{
  double x,y,z;

  x = p[0] * rmat[0][0] + p[1] * rmat[1][0] + p[2] * rmat[2][0] + rmat[3][0];
  y = p[0] * rmat[0][1] + p[1] * rmat[1][1] + p[2] * rmat[2][1] + rmat[3][1];
  z = p[0] * rmat[0][2] + p[1] * rmat[1][2] + p[2] * rmat[2][2] + rmat[3][2];
  p[0] = x;
  p[1] = y;
  p[2] = z;
}




/* ========================================================================
 * Rotation d'un nuage de (nbPoints) points par une matrice de rotation.
 * ========================================================================
 */


void rotateCrds(DtCrds crds2, int nbPoints, DtMatrix4x4 rmat)
{
  int i;

  for (i=0 ; i < nbPoints ; i++){
    singleRotate(crds2[i],rmat);
  }
}




/* ==========================================
 * Translation d'un point par un vecteur.
 * ==========================================
 */

void simpleTranslate(DtPoint3 p, DtPoint3 tr)
{
  p[0] += tr[0];
  p[1] += tr[1];
  p[2] += tr[2];
}





/* ===============================================
 * Symétrique d'un point par rapport à (0,0,0).
 * ===============================================
 */

void negatePoint3(DtPoint3 p1)
{
  p1[0] = - p1[0];
  p1[1] = - p1[1];
  p1[2] = - p1[2];
}






/* ================================================
 * Copie les coordonnées du point1 dans point2.
 * ================================================
 */
void copyPoint3(DtPoint3 p1, DtPoint3 p2)
{
  p2[0] = p1[0];
  p2[1] = p1[1];
  p2[2] = p1[2];
}




/* =================================================
 * Copie d'une matrice 4 x 4 (recopie a dans b).
 * =================================================
 */

void copyMat4x4(DtMatrix4x4 a,DtMatrix4x4 b)
{
  int i,j;
  boucle(i,0,4) {
    boucle(j,0,4) {
      b[i][j] = a[i][j];
    }
  }
}



/* ===================================================================
 * Copie d'un tableau de type DtpDistRec (recopie dist1 dans dist2). 
 * ===================================================================
 */

void copyDtpDistRec(int kds, DtDistRec *dist1, DtDistRec *dist2)
{
  int i,j;

  for(i=0 ; i < kds ; i++){
    for(j=0 ; j < 2 ; j++){
      dist2[i].longueur=dist1[i].longueur;
      dist2[i].liaison=dist1[i].liaison;
    }
  }
}



/* ===================================================================
 * Stockage d'une réponse pour CSR (kds,nbAtApp,dist,rmsd,Mat4x4).
 * ===================================================================
 */

void stockRep(int kds, int nbAtApp, DtDistRec *dist, double rms, DtMatrix4x4 mat, DtReponse rep)
{
  rep->nbAtMax=nbAtApp;
  rep->rmsd=rms;
  copyMat4x4(mat, rep->M);
  copyDtpDistRec(kds,dist,rep->dist);
}




/* ===============================================
 * Création d'une matrice de rotation aléatoire
 * ===============================================
 */


void rotAl (DtMatrix4x4 mat)
{
  double teta;
  DtPoint3 U;
  double N;
  int compteur=0;

  teta=drand48()*DcPI;

  /* printf("*teta=%.8lf\t",teta); */
  
  U[1]=-1+drand48()*2; //réel compris entre -1 et 1
  U[2]=-1+drand48()*2;
  U[0]=-1+drand48()*2;
  N=norme(U);
 
  while (N>1 && compteur < 1000){

    /* printf("*X=%.8lf\t",U[1]);
    printf("Y=%.8lf\t",U[2]);
    printf("Z=%.8lf\n",U[3]);
    printf("* /!\\========PROBLEME==========/!\\\n"); */
    U[1]=-1+drand48()*2;
    U[2]=-1+drand48()*2;
    U[0]=-1+drand48()*2;
    N=norme(U);
    compteur++;
  }
  if(compteur < 1000){
    /* printf("X=%.8lf\t",U[1]);
    printf("Y=%.8lf\t",U[2]);
    printf("Z=%.8lf\n",U[3]);
    printf("*norme(U)=%.8lf\n\n",N); */
  }
  else {
    /* printf("/!\\========NORME DE U SUPERIEURE A 1==========/!\\\n\n"); */
  }


  teta=0.211091720422362 ;
  U[0]=0.776475976742458 ;
  U[1]=-1.12080975556336 ;
  U[2]=-0.06429069295656 ;

  
  //MkArbitraryAxisRotMat4x4(O,U,teta,mat);
  
}



/* ===================================================
 * Positionnement aléatoire de deux nuages de points.
 * ===================================================
 */

void posAl (DtNuage ng1, DtNuage ng2, DtMatrix4x4 mat)
{
  int nAl1, nAl2,i;
  DtPoint3 tr1, tr2;


  /* --- rotation aléatoire --- */

  rotAl(mat); 

  /* --- positionnement aléatoire --- */

  nAl1=irand(ng1.n);
  nAl2=irand(ng2.n);

  /* printf("*COUPLE (ATOME 1, ATOME 2) TIRE AU HASARD : (%d,%d).\n*\n"
     ,nAl1,nAl2); */


  /* --- superposition des deux nuages de points --- */


  nAl1=41;
  nAl2=38;


  /* rotation de tout le nuage 2 */
  /* for(i=0 ; i < (ng2.n) ; i++){
    singleRotate(ng2.crds[i], mat);
  }
  
  tr1[0]=ng1.crds[nAl1][0];
  tr1[1]=ng1.crds[nAl1][1];
  tr1[2]=ng1.crds[nAl1][2];
  
  tr2[0]=ng2.crds[nAl2][0];
  tr2[1]=ng2.crds[nAl2][1];
  tr2[2]=ng2.crds[nAl2][2];
  negatePoint3(tr2); */
  
  /* superposition du nuage 2 sur le nuage 1 */
  /* for(i=0 ; i < (ng2.n) ; i++){
    simpleTranslate(ng2.crds[i],tr2);
    simpleTranslate(ng2.crds[i],tr1);
    } */



  tr1[0]=ng1.crds[nAl1][0];
  tr1[1]=ng1.crds[nAl1][1];
  tr1[2]=ng1.crds[nAl1][2];
  
  tr2[0]=ng2.crds[nAl2][0];
  tr2[1]=ng2.crds[nAl2][1];
  tr2[2]=ng2.crds[nAl2][2];

  negatePoint3(tr2);

  mat[3][0]=tr1[0]+tr2[0];
  mat[3][1]=tr1[1]+tr2[1];
  mat[3][2]=tr1[2]+tr2[2];

  for(i=0 ; i < (ng2.n) ; i++){
    singleRotate(ng2.crds[i], mat);
  }

}


/* ============
 * ============
 */
/* ----- normalise le vecteur A ----------------------------------------- */
void normalize(DtPoint3 a)
{
  double n;
  double s;
  s = (double) (a[0] * a[0]  + a[1] * a[1] + a[2] * a[2]);
  n = sqrt(s);
  a[0] /= (DtFloat) n;
  a[1] /= (DtFloat) n;
  a[2] /= (DtFloat) n;
}


/* ===============================================================
 * ---------- Matrice de rotation autour d'un axe a b = X. -------
 * =============================================================== */
void MkNumXRotMat4x4(DtPoint3 A,DtFloat angle,DtMatrix4x4 M)
{
  DtMatrix4x4 t,r,bf;
  double cost,sint;

  cost = cos(angle);
  sint = sin(angle);

  /* Le calcul de la matrice de transformation */

  /* Translation 1 */

  t[0][0] = 1.; t[0][1] = 0.; t[0][2] = 0.; t[0][3] = 0.;
  t[1][0] = 0.; t[1][1] = 1.; t[1][2] = 0.; t[1][3] = 0.;
  t[2][0] = 0.; t[2][1] = 0.; t[2][2] = 1.; t[2][3] = 0.;
  t[3][0] = -A[0]; t[3][1] = -A[1]; t[3][2] = -A[2]; t[3][3] = 1.;

  /* Rotation */
  r[0][0] = 1.; r[0][1] = 0.;    r[0][2] = 0.;     r[0][3] = 0.;
  r[1][0] = 0.; r[1][1] = cost;  r[1][2] = sint;   r[1][3] = 0.;
  r[2][0] = 0.; r[2][1] = -sint; r[2][2] = cost;   r[2][3] = 0.;
  r[3][0] = 0.; r[3][1] = 0.;    r[3][2] = 0.;     r[3][3] = 1.;

  mulMat4x4(t,r,bf);
/* Translation -1 */

  t[0][0] = 1.; t[0][1] = 0.; t[0][2] = 0.; t[0][3] = 0.;
  t[1][0] = 0.; t[1][1] = 1.; t[1][2] = 0.; t[1][3] = 0.;
  t[2][0] = 0.; t[2][1] = 0.; t[2][2] = 1.; t[2][3] = 0.;
  t[3][0] = A[0]; t[3][1] = A[1]; t[3][2] = A[2]; t[3][3] = 1.;

  mulMat4x4(bf,t,M);

/* fprintf(stderr,"  %f  %f  %f  %f\n",M[0][0],M[0][1],M[0][2],M[0][3]);
   fprintf(stderr,"  %f  %f  %f  %f\n",M[1][0],M[1][1],M[1][2],M[1][3]);
   fprintf(stderr,"  %f  %f  %f  %f\n",M[2][0],M[2][1],M[2][2],M[2][3]);
   fprintf(stderr,"  %f  %f  %f  %f\n",M[3][0],M[3][1],M[3][2],M[3][3]); */

}





/* ===============================================================
 * ------- Matrice de rotation autour d'un axe a b= A. -----------
 * =============================================================== */
void MkNumDRotMat4x4(DtPoint3 A,DtFloat angle,DtMatrix4x4 M)
{
  DtMatrix4x4 t,r,bf,bf1;
  double a,b,c,f,d,cosa,sina,cost,sint;

/* Vecteur norme. */
normalize( A);

  d = sqrt( A[1]*A[1] + A[2]*A[2]);
  if(d != 0.) {
    cosa = A[2]/d;
    sina = A[1]/d;
  } else {
    /* Cas particulier de rotation autour axe X */
    MkNumXRotMat4x4(A,angle,M);
    return;
  }
  cost = cos(angle);
  sint = sin(angle);

  /* fprintf(stderr," x: %f y: %f z: %f f: %f d: %f cosa: %f sina: %f cost: %f sint: %f\n",
     a,b,c,f,d,cosa,sina,cost,sint);
     */

  /* Le calcul de la matrice de transformation */

  /* Translation 1 */
  t[0][0] = 1.; t[0][1] = 0.; t[0][2] = 0.; t[0][3] = 0.;
  t[1][0] = 0.; t[1][1] = 1.; t[1][2] = 0.; t[1][3] = 0.;
  t[2][0] = 0.; t[2][1] = 0.; t[2][2] = 1.; t[2][3] = 0.;
  t[3][0] = -A[0]; t[3][1] = -A[1]; t[3][2] = -A[2]; t[3][3] = 1.;

  /* Rotation 1 */
  r[0][0] = 1.; r[0][1] = 0.;     r[0][2] = 0.;    r[0][3] = 0.;
  r[1][0] = 0.; r[1][1] = cosa;  r[1][2] = sina; r[1][3] = 0.;
  r[2][0] = 0.; r[2][1] = -sina; r[2][2] = cosa; r[2][3] = 0.;
  r[3][0] = 0.; r[3][1] = 0.;     r[3][2] = 0.;    r[3][3] = 1.;


 mulMat4x4(bf,r,bf1);

  /* Rotation */
  r[0][0] = cost;  r[0][1] = sint; r[0][2] = 0.; r[0][3] = 0.;
  r[1][0] = -sint; r[1][1] = cost; r[1][2] = 0.; r[1][3] = 0.;
  r[2][0] = 0.;     r[2][1] = 0.;    r[2][2] = 1.; r[2][3] = 0.;
  r[3][0] = 0.;     r[3][1] = 0.;    r[3][2] = 0.; r[3][3] = 1.;

  mulMat4x4(bf1,r,bf);

  /* Rotation -2 */
  r[0][0] = d; r[0][1] = 0.; r[0][2] = -a; r[0][3] = 0.;
  r[1][0] = 0.; r[1][1] = 1.; r[1][2] = 0.;  r[1][3] = 0.;
  r[2][0] = a; r[2][1] = 0.; r[2][2] = d;  r[2][3] = 0.;
  r[3][0] = 0.; r[3][1] = 0.; r[3][2] = 0.;  r[3][3] = 1.;

  mulMat4x4(bf,r,bf1);

  /* Rotation -1 */
  r[0][0] = 1.; r[0][1] = 0.;    r[0][2] = 0.;     r[0][3] = 0.;
  r[1][0] = 0.; r[1][1] = cosa; r[1][2] = -sina; r[1][3] = 0.;
  r[2][0] = 0.; r[2][1] = sina; r[2][2] = cosa;  r[2][3] = 0.;
  r[3][0] = 0.; r[3][1] = 0.;    r[3][2] = 0.;     r[3][3] = 1.;

  mulMat4x4(bf1,r,bf);

  /* Translation -1 */
  t[0][0] = 1.; t[0][1] = 0.; t[0][2] = 0.; t[0][3] = 0.;
  t[1][0] = 0.; t[1][1] = 1.; t[1][2] = 0.; t[1][3] = 0.;
  t[2][0] = 0.; t[2][1] = 0.; t[2][2] = 1.; t[2][3] = 0.;
  t[3][0] = A[0]; t[3][1] = A[1]; t[3][2] = A[2]; t[3][3] = 1.;

  mulMat4x4(bf,t,M);

}


/* ===================================================
 * Positionnement aléatoire de deux nuages de points.
 * ng1 est fixe, ng2 est mobile (rotation + translation)
 * mat est la matrice de transformation aléatoire.
 * (mat est juste un espace de travail, ecrasee a la sortie
 * de posAl2)
 * ===================================================
 */


void posAl2(DtNuage ng1, DtNuage ng2, DtMatrix4x4 mat){
  double teta;
  DtPoint3 U;
  DtPoint3 tr2dt;
  double N;
  int nAl1, nAl2,i,tirage;
  DtPoint3 tr1, tr2;
  int compteur=0;
  double temp,x,y,z;

  /* angle aléatoire entre O et Pi */
  teta=drand48()*DcPI;

  /* printf("*teta=%.8lf\t",teta); */
  
  /* vecteur aléatoire */
  U[1]=-1+drand48()*2; //réel compris entre -1 et 1
  U[2]=-1+drand48()*2;
  U[0]=-1+drand48()*2;
  N=norme(U);
 
  /* compteur implémenté pour limiter, car des fois plus d'une minute sans solution */
  while ((N>1.) && (compteur < 1000)){

    /* printf("*X=%.8lf\t",U[1]);
    printf("Y=%.8lf\t",U[2]);
    printf("Z=%.8lf\n",U[3]);
    printf("* /!\\========PROBLEME==========/!\\\n"); */
    U[1]=-1+drand48()*2;
    U[2]=-1+drand48()*2;
    U[0]=-1+drand48()*2;
    N=norme(U);
    compteur++;
  }
  if(compteur < 1000){
    /* printf("X=%.8lf\t",U[1]);
    printf("Y=%.8lf\t",U[2]);
    printf("Z=%.8lf\n",U[3]);
    printf("*norme(U)=%.8lf\n\n",N); */
  }
  else {
    /* printf("/!\\========NORME DE U SUPERIEURE A 1==========/!\\\n\n"); */
  }



  /* Orientation imposée du programme Fortran pour le fichier pdbtest.pdb
   */
  
  /* teta=0.211091720422362 ;
  U[0]=0.776475976742458 ;
  U[1]=-1.12080975556336 ;
  U[2]=-0.06429069295656 ; */
  


 /* --- positionnement aléatoire --- */


  tirage=irand(6);

  //fprintf(stderr,"TIRAGE : %d\n",tirage);

  if (tirage==0) {
    //fprintf(stderr,"OK 0");
    //fflush(stderr);
    /* -- Calcul du barycentre du nuage de points 1 -- */
    x=y=z=0.;
    for(i=0 ; i < (ng1.n) ; i++){
      x += ng1.crds[i][0];
      y += ng1.crds[i][1];
      z += ng1.crds[i][2];
    }

    tr1[0]=x/(ng1.n);
    tr1[1]=y/(ng1.n);
    tr1[2]=z/(ng1.n);


    /* -- Calcul du barycentre du nuage de points 2 -- */
    x=y=z=0.0;
    for(i=0 ; i < (ng2.n) ; i++){
      x += ng2.crds[i][0];
      y += ng2.crds[i][1];
      z += ng2.crds[i][2];
    }

    tr2[0]=x/(ng2.n);
    tr2[1]=y/(ng2.n);
    tr2[2]=z/(ng2.n);
  }
  
  if((tirage>0) && (tirage < 3)){
    //fprintf(stderr,"OK 1..2 ng2.n %d ng1.n %d", ng2.n, ng1.n);
    //fflush(stderr);
    if(ng2.n<ng1.n){
      nAl2=irand(ng2.n);
      nAl1=nAl2+irand((ng1.n)-(ng2.n));
      //printf("%d\t%d\n",nAl1,nAl2);
    } 
    else if(ng2.n>ng1.n) {
      nAl1=irand(ng1.n);
      nAl2=nAl1+irand((ng2.n)-(ng1.n));
      //printf("%d\t%d\n",nAl1,nAl2);
    } else {
      nAl1=irand(ng1.n);
      nAl2=nAl1;
    }

    tr1[0]=ng1.crds[nAl1][0];
    tr1[1]=ng1.crds[nAl1][1];
    tr1[2]=ng1.crds[nAl1][2];
    
    tr2[0]=ng2.crds[nAl2][0];
    tr2[1]=ng2.crds[nAl2][1];
    tr2[2]=ng2.crds[nAl2][2];

  

    tr2dt[0]=tr2[0] + U[0];
    tr2dt[1]=tr2[1] + U[1];
    tr2dt[2]=tr2[2] + U[2];
  }

  if(tirage>2){
    nAl1=irand(ng1.n);
    nAl2=irand(ng2.n);
    

    tr1[0]=ng1.crds[nAl1][0];
    tr1[1]=ng1.crds[nAl1][1];
    tr1[2]=ng1.crds[nAl1][2];
    
    tr2[0]=ng2.crds[nAl2][0];
    tr2[1]=ng2.crds[nAl2][1];
    tr2[2]=ng2.crds[nAl2][2];
    
  

    tr2dt[0]=tr2[0] + U[0];
    tr2dt[1]=tr2[1] + U[1];
    tr2dt[2]=tr2[2] + U[2];
  }

  //nAl1=41;
  //nAl2=38;

  

  /* printf("*COUPLE (ATOME 1, ATOME 2) TIRE AU HASARD : (%d,%d).\n*\n"
     ,nAl1,nAl2); */





  /* --- superposition des deux nuages de points --- */

  /* -- Création de la matrice de rotation aléatoire -- */
  MkArbitraryAxisRotMat4x4(tr2,tr2dt,teta,mat);

  /* -- Transposée de la matrice de rotation 3x3 dans la matrice 4x4 -- */
  temp=mat[0][1];
  mat[0][1]=mat[1][0];
  mat[1][0]=temp;

  temp=mat[0][2];
  mat[0][2]=mat[2][0];
  mat[2][0]=temp;

  temp=mat[1][2];
  mat[1][2]=mat[2][1];
  mat[2][1]=temp;
  

  //printMat4x4(mat);

  //printf("%lf %lf %lf\n",tr1[0],tr1[1],tr1[2]);
  //printf("%lf %lf %lf\n",tr2[0],tr2[1],tr2[2]);

  

  /* -- Translation du nuage 2 par rapport à tr2 -- */
  for(i=0 ; i < (ng2.n) ; i++){
    ng2.crds[i][0] -= tr2[0];
    ng2.crds[i][1] -= tr2[1];
    ng2.crds[i][2] -= tr2[2];
  }
  
  /* -- Rotation du nuage 2 par la matrice de rotation aléatoire -- */
  for(i=0 ; i < (ng2.n) ; i++){
    x=ng2.crds[i][0];
    y=ng2.crds[i][1];
    z=ng2.crds[i][2];
    
    ng2.crds[i][0]=mat[0][0]*(x)+mat[0][1]*(y)+mat[0][2]*(z);
    ng2.crds[i][1]=mat[1][0]*(x)+mat[1][1]*(y)+mat[1][2]*(z);
    ng2.crds[i][2]=mat[2][0]*(x)+mat[2][1]*(y)+mat[2][2]*(z);
  }

  /* -- Superposition du nuage 2 sur le nuage 1 via la translation tr1 -- */
  for(i=0 ; i < (ng2.n) ; i++){
    ng2.crds[i][0] += tr1[0];
    ng2.crds[i][1] += tr1[1];
    ng2.crds[i][2] += tr1[2];
  }

  /* printf("DISTANCE : %lf\n",distance(ng1.crds[nAl1],ng2.crds[nAl2])); */
}






/* ===========================================================
 * Imprime quels points sont appariés deux à deux par sdm.
 * ===========================================================
 */

void printNbPtsApp (DtNuage ng1, DtNuage ng2, int *kds,DtDistRec *dist)
{
  int i,i1,i2,nbAtApp,stop;
  char at1App[ng1.n]; 
  char at2App[ng2.n]; 
  
  memset (at1App, 0, sizeof(char)*(ng1.n));
  memset (at2App, 0, sizeof(char)*(ng2.n));
  
  i=stop=0;
  
  while(i<(*kds) && stop==0){
    i1=(dist[i].liaison)%(ng1.n);
    i2=(dist[i].liaison)/(ng1.n);

    printf("*AT1 - AT2 : %d - %d\n",i1,i2);
    /* printPoint3Crds(ng1.crds,i1);
       printPoint3Crds(ng2.crds,i2); */

    if(at1App[i1]==1){
      stop=1;
      nbAtApp=i;
      printf("*=> ATOME %d DU NUAGE 1 DEJA APPARIE.\n",i1); 
    } else {
      at1App[i1]=1;
    }
    if(at2App[i2]==1){
      stop=1;
      nbAtApp=i;
      printf("*=> ATOME %d DU NUAGE 2 DEJA APPARIE.\n",i2);
    } else {
      at2App[i2]=1;
    }
    i++;
  }
}






/* ========================== ALGORITHME SDM =================================
 * Calcul de toutes les distances entre deux nuages de points (ng).
 * Conservation de toutes les distances inférieures à (dseuil) dans un 
 *   tableau (dist) de type DtpDistRec. 
 *   dist[k].longueur correspond à la k-ième distance inférieure à dseuil
 *   dist[k].liaison permet de retrouver de quels points s'agit cette longueur.
 *
 * Retourne le nombre de points maximum que l'on peut apparier.
 * ===========================================================================
 */

int sdm (DtNuage ng1, DtNuage ng2, double dseuil, int dimTab, int *kds, DtDistRec *dist)
{
  char at1App[ng1.n]; //at1App[k]=1 si le k-ème point de ng1 fait déjà parti d'un appariement, 0 sinon
  char at2App[ng2.n]; //at2App[k]=1 si le k-ème atome de ng2 fait déjà parti d'un appariement, 0 sinon
  int i,i1,i2,shift,stop,nbAtApp;
  double lg;


  if ((ng1.n)==0 || (ng2.n)==0){
    *kds=0;
    nbAtApp=0;
    return nbAtApp;
  }


  /* --- calcul des distances entre les points --- */

  if (dseuil>=0){
    *kds=0;
    for (i2=0 ; i2 <(ng2.n) ; i2++){
      shift=i2*(ng1.n);
      for (i1=0 ; i1 <(ng1.n) ; i1++){

	lg=distance((ng1.crds[i1]),(ng2.crds[i2]));
	
	if(lg <= dseuil){
	  if(*kds <= dimTab){ 
	    (dist[*kds]).longueur = lg; 
	    (dist[*kds]).liaison = i1 + shift; 
	    *kds=(*kds)+1;
	    
	    /* printf("dist :%lf\tcor : %d\n",lg*lg,i1+shift); */

	  }
	}
      } 
    } 
    
    if (*kds>dimTab) return (-1); /* dimd trop petit */
    if (*kds==0) return (-2); /* dseuil trop petit */
    
  } 
  
  else { /* dseuil < 0 (équivalent de dseuil infini) */

    *kds=(ng1.n)*(ng2.n);

    if (*kds>dimTab) return 1; /* dimd trop petit */
    for (i2=0 ; i2<(ng2.n) ; i2++){
      shift=i2*(ng1.n);
      for (i1=0 ; i1 <(ng1.n) ; i1++){
	dist[i1+shift].longueur=distance((ng1.crds[i1]),(ng2.crds[i2]));
	dist[i1+shift].liaison=i1+shift;

      } 
    } 
  }
  
  /* ---  tri du tableau des distances par ordre croissant de distances --- */

  /* printDtpDistRec(dist,*kds,ng1,ng2);
     printf("\n"); */ 
  
  qsort(dist,*kds,sizeof(DtDistRec), cmpdist); 

  /* printDtpDistRec(dist,*kds,ng1,ng2); */



  /* --- calcul du nombre de points appariés --- */

  memset (at1App, 0, sizeof(char)*(ng1.n));
  memset (at2App, 0, sizeof(char)*(ng2.n));
  
  i=stop=0;
  
  while(i<(*kds) && stop==0){
    i1=(dist[i].liaison)%(ng1.n);
    i2=(dist[i].liaison)/(ng1.n);

    /* printf("*%d\tAT1 - AT2 : %d - %d\t%lf\n",
       i,i1,i2,dist[i].longueur); */
    /* printPoint3Crds(ng1.crds,i1);
    printPoint3Crds(ng2.crds,i2); */

    if(at1App[i1]==1){
      stop=1;
      nbAtApp=i;
      /* printf("*=> ATOME %d DU NUAGE 1 DEJA APPARIE.\n",i1); */
    } else {
      at1App[i1]=1;
    }
    if(at2App[i2]==1){
      stop=1;
      nbAtApp=i;
      /* printf("*=> ATOME %d DU NUAGE 2 DEJA APPARIE.\n",i2); */
    } else {
      at2App[i2]=1;
    }
    i++;
  }
 
  if (nbAtApp==(*kds) && nbAtApp!=min((ng1.n),(ng2.n))) return (-3);
    
  if(nbAtApp>30){
  printf("%d\n",nbAtApp);
  }
  return (int) (nbAtApp);
  
}





/* =========================================================================
 * Transfert les données de sdm à zuker_superpose.
 * (renvoie les (nbAtApp) coordonnées à superposer au mieux l'une sur l'autre).
 * =========================================================================
 */

void crdsASuperposer (DtNuage ng1, DtNuage ng2, int nbAtApp, DtDistRec *dist, DtPoint3 *crdsSup1, DtPoint3 *crdsSup2)
{
  int i;
  int i1;
  int i2;

  for(i=0 ; i < nbAtApp ; i++){
    i1=(dist[i].liaison)%(ng1.n);
    i2=(dist[i].liaison)/(ng1.n);
    copyPoint3(ng1.crds[i1],crdsSup1[i]);
    copyPoint3(ng2.crds[i2],crdsSup2[i]);
  }
}








/* ==================================================================
 * XYCov = Calcule la matrice d'inertie des points
 *
 * id      : indices des atomes dans At
 * from,tto: indices extremes
 * P       : le barycentre
 * ==================================================================
 */

static double *product(double **A, double *x, int n) 
{
  int i, j;
  double sum;
  double *y=(double *)calloc(n, sizeof(double));

  for (i=0; i<n; i++) {
    sum=0;
    for (j=0; j<n; j++)
      sum+=A[i][j]*x[j];
    y[i]=sum;
  }
  return y;
}

DtMatrix3x3 *XYCov(DtMatrix3x3 *pM,DtPoint3 *X,DtPoint3 *Y,DtPoint3 Xmean,DtPoint3 Ymean,int aSze)
{
  int i;
  double Xx,Xy,Xz;
  double Yx,Yy,Yz;
  double daSze;

  /*   fprintf(stdout,"inertia, len %d\n",aSze); */

  /* X average*/
  Xmean[0] = Xmean[1] = Xmean[2] = 0.;
  for (i=0;i<aSze;i++) {
    Xmean[0] += X[i][0];
    Xmean[1] += X[i][1];
    Xmean[2] += X[i][2];
  }
  if (aSze) {
    daSze = (double) aSze;
    Xmean[0] /= daSze;
    Xmean[1] /= daSze;
    Xmean[2] /= daSze;
  }
  
  /* Y average*/
  Ymean[0] = Ymean[1] = Ymean[2] = 0.;
  for (i=0;i<aSze;i++) {
    Ymean[0] += Y[i][0];
    Ymean[1] += Y[i][1];
    Ymean[2] += Y[i][2];
  }
  if (aSze) {
    daSze = (double) aSze;
    Ymean[0] /= daSze;
    Ymean[1] /= daSze;
    Ymean[2] /= daSze;
  }

  /* Covariance matrix */

  if (pM == NULL) {
    pM = (DtMatrix3x3 *) calloc(1,sizeof(DtMatrix3x3));
  } else {
    memset((void *) pM, 0, sizeof(DtMatrix3x3));
  }
  
  for (i=0;i<aSze;i++) {
    Xx = (double) X[i][0] - Xmean[0];
    Xy = (double) X[i][1] - Xmean[1];
    Xz = (double) X[i][2] - Xmean[2];
    Yx = (double) Y[i][0] - Ymean[0];
    Yy = (double) Y[i][1] - Ymean[1];
    Yz = (double) Y[i][2] - Ymean[2];

    (*pM)[0][0] += Xx*Yx;
    (*pM)[0][1] += Xx*Yy;
    (*pM)[0][2] += Xx*Yz;

    (*pM)[1][0] += Xy*Yx;
    (*pM)[1][1] += Xy*Yy;
    (*pM)[1][2] += Xy*Yz;

    (*pM)[2][0] += Xz*Yx;
    (*pM)[2][1] += Xz*Yy;
    (*pM)[2][2] += Xz*Yz;
  }

  return pM;
}





/* ====================================================================
 * Matrice 4x4 Translation. PREMULTIPLIE -> Y = XM (X vecteur ligne) 
 * ====================================================================
 */

void MkTrnsIIMat4x4(DtMatrix4x4 m, DtPoint3 tr)
{
 m[0][0] = 1.;    m[0][1] = 0.;    m[0][2] = 0.;    m[0][3] = 0.;
 m[1][0] = 0.;    m[1][1] = 1.;    m[1][2] = 0.;    m[1][3] = 0.;
 m[2][0] = 0.;    m[2][1] = 0.;    m[2][2] = 1.;    m[2][3] = 0.;
 m[3][0] = tr[0]; m[3][1] = tr[1]; m[3][2] = tr[2]; m[3][3] = 1.;
}



/* ===================================================================
 * Fonctions utilitaires pour l'algorithme de superposition de Zuker.
 * ====================================================================
 */

int largestEV4(double R[4][4], double v[4], double *vp)
{
  
  double M2[4][4];
  int rs;

  memcpy(M2,R,sizeof(double)*16);

  rs = inverse_power(R, 4, 10000, 1.e-8, vp, v);

  if (!rs)
    return shift_power(&M2[0][0], 4, 10000, 1.e-8, vp, v);

  return rs;

}

int inverse_power(double a[4][4], int n, int maxiter, double eps, double *v, double *w) 
{
  int niter,i;
  double *y;
  double r, sum, l, normy, d;
  y=random_vect(n);
  niter=0;

  r=lmax_estim(a, n);
  for (i=0; i<n; i++) a[i][i]=a[i][i]-r;
  if (lu_c(a, n)==0) {
    /* fprintf(stderr,"ATTENTION ! cas singulier de inverse_power\n"); */
    free(y);     //exit(0);
    return 0;
  }

  do {
    normy=sqrt(inner(y,y,n));
    for (i=0; i<n; i++) {
      w[i]=y[i]/normy;
      y[i]=w[i];
    }

    resol_lu(a, y, n);
    l=inner(w,y,n);
    niter++;
    for (sum=0,i=0; i<n; i++) {
      d=y[i]-l*w[i];
      sum+=d*d;
    }
    d=sqrt(sum);
  } while (d>eps*fabs(l) && niter<maxiter);
  free(y);
  *v=r+1.0/l;
  return niter;
}

int shift_power(double *a, int n, int maxiter, double eps, double *v, double *w)
{
  double **tmp;
  double sh;
  int niter;
  int i,j;

  tmp=alloc_mat(n, n);
  /* copyMat(a, tmp, n, n); */
  for (i=0; i<n; i++) {
    for (j=0; j<n; j++) {
      tmp[i][j] = a[i*n+j];
    }
  }
  sh=best_shift(tmp, n);

  niter=power(tmp, n, maxiter, eps, v, w);
  *v=*v-sh;
  free_mat(tmp, n);
  return niter;
}

double *random_vect(int n) 
{   
  int i;
  double *v=(double *) calloc(n, sizeof(double));
  for (i=0; i<n; i++)
    v[i]= (double)rand()/(RAND_MAX+1.0);
  return(v);
}

static double inner(double *x, double *y, int n) 
{
  int i;
  double sum;

  for (sum=0, i=0; i<n; sum+=x[i]*y[i],i++);
  return sum;

}

double lmax_estim (double a[4][4], int n) 
{   
  double t, sum;
  int i, j;
  t=a[0][0];
  for (i=0; i<n; i++) {
    for (sum=0,j=0; j<n; j++)
      if (j!=i) sum+=fabs(a[i][j]);
    t=max(t, a[i][i]+sum);
  }
  return t;
}

static int lu_c (double a[4][4],  int n)
{
 int i,j,k,err;
 double pivot,coef;

 err=1;

 k=0;
 while (err==1 && k<n) {
  pivot=a[k][k];
  if(fabs(pivot)>=EPS) {
    for(i=k+1;i<n;i++) {
      coef=a[i][k]/pivot;
      for(j=k;j<n;j++)
        a[i][j] -= coef*a[k][j];
      a[i][k]=coef;
    }
  }
  else err=0;
  k++;
 }
 if(a[n-1][n-1]==0) err=0;
 return err;
}

static void resol_lu(double a[4][4], double *b, int n)
{
 int i,j;
 double sum;
 double y[n];
 y[0]=b[0];
 for(i=1;i<n;i++) {
  sum=b[i];
  for(j=0;j<i;j++)
    sum-=a[i][j]*y[j];
  y[i]=sum;
 }
 b[n-1]=y[n-1]/a[n-1][n-1];
 for(i=n-1;i>=0;i--) {
  sum=y[i];
  for(j=i+1;j<n;j++)
     sum-=a[i][j]*b[j];
  b[i]=sum/a[i][i];
 }
}

double best_shift(double *a[], int n) 
{
  double m, M, s;
  double t, sum;
  int i, j;
  t=a[0][0];
  for (i=1; i<n; i++) t=max(t, a[i][i]);
  M=t;
  t=a[0][0];
  for (i=0; i<n; i++) {
    for (sum=0,j=0; j<n; j++)
      if (j!=i) sum+=fabs(a[i][j]);
    t=min(t, a[i][i]-sum);
  }
  m=t;
  s=-0.5*(M+m);
  for (i=0; i<n; i++)
    a[i][i]=a[i][i]+s;
  return s;
}


int power(double *a[], int n, int maxiter, double eps, double *v, double *w) 
{
  int niter,i;
  double *y;
  double sum, l, normy, d;
  y=random_vect(n);
  niter=0;
  do {
    normy=sqrt(inner(y,y,n));
    for (i=0; i<n; i++) w[i]=y[i]/normy;
    y=product(a, w, n);
    l=inner(w,y,n);
    niter++;
    for (sum=0,i=0; i<n; i++) {
      d=y[i]-l*w[i];
      sum+=d*d;
    }
    d=sqrt(sum);
  } while (d>eps*fabs(l) && niter<maxiter);
  free(y);
  *v=l;
  return niter;
}





/* ====================================================================
 * Allocation et libération de l'espace mémoire pour une matrice 4x4.
 * ====================================================================
 */

void free_mat(double **mat, int n)
{
  int i;
  for (i=0; i<n; i++)
    free(mat[i]);
  free(mat);
}

double **alloc_mat(int n, int m) 
{
  int i;
  double **mat = (double **)calloc(m,sizeof(double *));
  for (i=0; i<n; i++)
    mat[i]=(double *)calloc(n, sizeof(double));
  return mat;
}



/* =========================================================
 * Calcul de la distance au carré entre deux points.
 * =========================================================
 */

DtFloat squared_distance(DtPoint3 R,DtPoint3 K)
{
  return (DtFloat) ((K[0] - R[0]) * (K[0] - R[0]) +
                    (K[1] - R[1]) * (K[1] - R[1]) +
                    (K[2] - R[2]) * (K[2] - R[2]));
}







/* =============== ALGORITHME DE SUPERPOSITION (ZUKER) =======================
 * Best fit matrix as proposed by:
 * Zuker & Somorjai, Bulletin of Mathematical Biology,
 * vol. 51, No 1, p 55-78, 1989.
 *
 * Returns M, a transformation matrix ready for use
 * ===========================================================================
 */

double zuker_superpose(DtPoint3 *c1, DtPoint3 *c2, int len, DtMatrix4x4 M)
{
  DtMatrix3x3  C;
  DtMatrix3x3 *pC;
  DtMatrix4x4  RM;
  DtMatrix4x4  TMP;
  DtMatrix4x4  TX;
  DtMatrix4x4  TY;
  DtMatrix4x4  P;
  DtPoint4     V;
  DtPoint3     bc1, bc2;
  DtPoint3     try;
  
  double eval;
  double squared_rms = 0.;
  
  int nCycles;
  int aDot;
  
  /* Compute transformation matrix as proposed by zuker */
  
  pC = &C;
  pC = XYCov(pC, (DtPoint3 *) c1, (DtPoint3 *) c2, bc1, bc2, len);
  
  P[0][0] = -C[0][0]+C[1][1]-C[2][2];
  P[0][1] = P[1][0] = -C[0][1]-C[1][0];
  P[0][2] = P[2][0] = -C[1][2]-C[2][1];
  P[0][3] = P[3][0] =  C[0][2]-C[2][0];
  
  P[1][1] = C[0][0]-C[1][1]-C[2][2];
  P[1][2] = P[2][1] = C[0][2]+C[2][0];
  P[1][3] = P[3][1] = C[1][2]-C[2][1];
  
  P[2][2] = -C[0][0]-C[1][1]+C[2][2];
  P[2][3] = P[3][2] = C[0][1]-C[1][0];
  
  P[3][3] = C[0][0]+C[1][1]+C[2][2];
  
  /* #if 0  
  printMat4x4("zuker P", P);
#endif */

  nCycles = largestEV4(P, V, &eval);
  
  RM[0][0] = -V[0]*V[0]+V[1]*V[1]-V[2]*V[2]+V[3]*V[3];
  RM[1][0] =  2*(V[2]*V[3]-V[0]*V[1]);
  RM[2][0] =  2*(V[1]*V[2]+V[0]*V[3]);
  RM[3][0] =  0.;
  
  RM[0][1] = -2*(V[0]*V[1]+V[2]*V[3]);
  RM[1][1] = V[0]*V[0]-V[1]*V[1]-V[2]*V[2]+V[3]*V[3];
  RM[2][1] =  2*(V[1]*V[3]-V[0]*V[2]);
  RM[3][1] =  0.;
  
  RM[0][2] =  2*(V[1]*V[2]-V[0]*V[3]);
  RM[1][2] = -2*(V[0]*V[2]+V[1]*V[3]);
  RM[2][2] = -V[0]*V[0]-V[1]*V[1]+V[2]*V[2]+V[3]*V[3];
  RM[3][2] =  0.;
  
  RM[0][3] =  0.;
  RM[1][3] =  0.;
  RM[2][3] =  0.;
  RM[3][3] =  1.;
  /* printMat4x4("zuker RM", RM); */
  
  /* Solution eprouvee ! */
  try[0] = - bc2[0]; try[1] = - bc2[1]; try[2] = - bc2[2];
  MkTrnsIIMat4x4(TY, try);
  MkTrnsIIMat4x4(TX, bc1);
  mulMat4x4(TY,RM,TMP);
  mulMat4x4(TMP, TX, M);
  
  /* Now superpose the coordinates */
  for (aDot=0;aDot<len;aDot++) {
    singleRotate(c2[aDot],M);
  }
  
  /* Compute squared RMSd */
  for (aDot=0;aDot<len;aDot++) {
    squared_rms += squared_distance(c1[aDot],c2[aDot]);
  }
  
  return squared_rms / (double) len;
}







/* ====================== ALGORITHME CSR =====================================
 * Superpose au mieux deux nuages de points avec SDM et zuker_superpose.
 * Retourne : 
 *   le nombre de points appariés.
 *   la matrice de rotation 4x4 correspondant à la meilleure superposition.
 *   le tableau trié des distances entre points après rotation et translation.
 * ===========================================================================
 */

DtReponse csr (DtNuage ng1, DtNuage ng2, double dseuil, int dimd, int *kds, DtDistRec *dist, DtMatrix4x4 mat, DtPoint3 *crdsSup1, DtPoint3 *crdsSup2, int nbIter, DtReponse rep)
{
  int compteur,nbAtApp,nbAtTemp;
  double rmsd;
  long int gSeed = -1;
  // long int gSeed = 268435455;
  long int *seed;
  
  seed=calloc(1,sizeof(long int*));
  *seed=gSeed;
  
  compteur = 0;
  while (compteur < nbIter){

    rmsd=-1;
    nbAtTemp=1;
    initRnd(seed);
    MkNullMat4x4(mat);
    //posAl(ng1, ng2, mat);

    // Orientation alieatoire initiale
    posAl2(ng1, ng2, mat);

    // Affichage crds pour debug
    //printNgCrds(ng2.crds,ng2.n);

    // SMD: retourne nombre d'atomes appariés
    // kds est l'ecart quadratique moyen entre les coordonnées
    nbAtApp=sdm(ng1,ng2,dseuil,dimd,kds,dist);
    // Preparation Zuker
    crdsASuperposer(ng1,ng2,nbAtApp,dist,crdsSup1,crdsSup2);
    rmsd=zuker_superpose(crdsSup1,crdsSup2,nbAtApp,mat);
    rotateCrds(ng2.crds,ng2.n,mat);

    while(nbAtApp > nbAtTemp){

      /* --- Stockage de la réponse si meilleure --- */

      if(nbAtApp > (rep->nbAtMax)){
	printf("itération : %d\t nombre d'atomes appariés : %d\n",
	 compteur,nbAtApp);
	stockRep(*kds,nbAtApp,dist,rmsd,mat,rep);
      }
      
      
      
      /* --- Nouvel appel de zuker-sdm pour un meilleur appariement--- */

      nbAtTemp=nbAtApp;
      
      /* printf("rms1 %lf\t",rmsd);
	 crdsASuperposer(ng1,ng2,nbAtApp,dist,crdsSup1,crdsSup2);
	 rmsd=zuker_superpose(crdsSup1,crdsSup2,nbAtApp,mat);
	 rotateCrds(ng2.crds,ng2.n,mat);
	 printf("rms2 %lf\n",rmsd); */
      
      nbAtApp=sdm(ng1,ng2,dseuil,dimd,kds,dist);
      /* printf("%d\t",nbAtApp); */
      crdsASuperposer(ng1,ng2,nbAtApp,dist,crdsSup1,crdsSup2);
      //printf("rms1 %lf\t",rmsd);
      rmsd=zuker_superpose(crdsSup1,crdsSup2,nbAtApp,mat);
      //printf("rms2 %lf\n",rmsd);
      rotateCrds(ng2.crds,ng2.n,mat);
  
    }

    /* printf("\nSortie boucle : %d <= %d\n",nbAtApp,nbAtTemp); */
    

    compteur=compteur+1;
    /* printf("\n"); */
  }

  return rep;
} 




/* ====================== FONCTION MAIN ======================================
 * ===========================================================================
 */

int main (int argc,char *argv[])
{
  DtMatrix4x4 mat;
  DtNuage ng1;
  DtNuage ng2;
  double dseuil;
  int dimTab,minN1N2;
  DtDistRec *dist;
  int *kds;
  int nbAtApp;
  DtReponse rep;
  DtPoint3 *crdsSup1, *crdsSup2;
  char *fname1,*fname2;
  


  dseuil=50.;
  dimTab=46*46+1;
  
  dist = (DtDistRec *) calloc(dimTab,sizeof(DtDistRec));
  kds = calloc(1,sizeof(int *));
    
  nbAtApp=*kds=0;
  fname1=argv[1];
  fname2=argv[2];



  
  ng2.crds= (DtPoint3 *) calloc(46,sizeof(DtPoint3));

  
  ng2.crds[0][0]=  	16.967;
  ng2.crds[1][0]=  	13.856;
  ng2.crds[2][0]=  	13.660;
  ng2.crds[3][0]=  	10.646;
  ng2.crds[4][0]=  	9.448 ;
  ng2.crds[5][0]=  	8.673 ;
  ng2.crds[6][0]=  	8.912 ;
  ng2.crds[7][0]=  	5.145 ;
  ng2.crds[8][0]=  	5.598 ;
  ng2.crds[9][0]=  	8.496 ;
  ng2.crds[10][0]= 	6.500 ;
  ng2.crds[11][0]= 	3.545 ;
  ng2.crds[12][0]= 	5.929 ;
  ng2.crds[13][0]= 	7.331 ;
  ng2.crds[14][0]= 	3.782 ;
  ng2.crds[15][0]= 	2.874 ;
  ng2.crds[16][0]= 	6.405 ;
  ng2.crds[17][0]= 	7.299 ;
  ng2.crds[18][0]= 	5.462 ;
  ng2.crds[19][0]=	7.139 ;
  ng2.crds[20][0]= 	4.776 ;
  ng2.crds[21][0]= 	5.453 ;
  ng2.crds[22][0]= 	7.662 ;
  ng2.crds[23][0]= 	4.902 ;
  ng2.crds[24][0]= 	2.547 ;
  ng2.crds[25][0]= 	5.305 ;
  ng2.crds[26][0]= 	6.066 ;
  ng2.crds[27][0]= 	2.574 ;
  ng2.crds[28][0]= 	2.404 ;
  ng2.crds[29][0]= 	5.603 ;
  ng2.crds[30][0]= 	6.202 ;
  ng2.crds[31][0]=  	9.261 ;
  ng2.crds[32][0]= 	9.498 ;
  ng2.crds[33][0]= 	10.632;
  ng2.crds[34][0]= 	12.842;
  ng2.crds[35][0]= 	14.260;
  ng2.crds[36][0]= 	17.801;
  ng2.crds[37][0]= 	20.196;
  ng2.crds[38][0]= 	20.652;
  ng2.crds[39][0]= 	18.137;
  ng2.crds[40][0]= 	17.426;
  ng2.crds[41][0]= 	18.417;
  ng2.crds[42][0]= 	14.875;
  ng2.crds[43][0]= 	14.664;
  ng2.crds[44][0]= 	18.099;
  ng2.crds[45][0]= 	17.340;
  ng2.crds[0][1]=  	12.784;
  ng2.crds[1][1]=  	11.469;
  ng2.crds[2][1]=  	10.707;
  ng2.crds[3][1]=  	8.991 ;
  ng2.crds[4][1]=  	9.034 ;
  ng2.crds[5][1]=  	5.314 ;
  ng2.crds[6][1]=  	2.083 ;
  ng2.crds[7][1]=  	2.209 ;
  ng2.crds[8][1]=  	5.767 ;
  ng2.crds[9][1]=  	4.609 ;
  ng2.crds[10][1]= 	1.584 ;
  ng2.crds[11][1]= 	3.935 ;
  ng2.crds[12][1]= 	6.358 ;
  ng2.crds[13][1]= 	3.607 ;
  ng2.crds[14][1]= 	2.599 ;
  ng2.crds[15][1]= 	6.282 ;
  ng2.crds[16][1]= 	7.471 ;
  ng2.crds[17][1]= 	4.533 ;
  ng2.crds[18][1]= 	5.890 ;
  ng2.crds[19][1]= 	9.305 ;
  ng2.crds[20][1]= 	11.261;
  ng2.crds[21][1]= 	15.033;
  ng2.crds[22][1]= 	16.429;
  ng2.crds[23][1]= 	18.830;
  ng2.crds[24][1]= 	15.948;
  ng2.crds[25][1]= 	13.996;
  ng2.crds[26][1]= 	17.177;
  ng2.crds[27][1]= 	17.467;
  ng2.crds[28][1]= 	13.722;
  ng2.crds[29][1]= 	13.630;
  ng2.crds[30][1]= 	17.068;
  ng2.crds[31][1]= 	17.630;
  ng2.crds[32][1]= 	21.005;
  ng2.crds[33][1]= 	22.274;
  ng2.crds[34][1]= 	25.407;
  ng2.crds[35][1]= 	27.274;
  ng2.crds[36][1]= 	27.434;
  ng2.crds[37][1]= 	24.668;
  ng2.crds[38][1]= 	25.337;
  ng2.crds[39][1]= 	23.634;
  ng2.crds[40][1]= 	25.240;
  ng2.crds[41][1]= 	23.612;
  ng2.crds[42][1]= 	22.432;
  ng2.crds[43][1]= 	20.609;
  ng2.crds[44][1]= 	19.052;
  ng2.crds[45][1]= 	15.616;
  ng2.crds[0][2]=  	4.338 ;
  ng2.crds[1][2]=  	6.066 ;
  ng2.crds[2][2]=  	9.787 ;
  ng2.crds[3][2]=  	11.408;
  ng2.crds[4][2]=  	15.012;
  ng2.crds[5][2]=  	15.279;
  ng2.crds[6][2]=  	13.258;
  ng2.crds[7][2]=  	12.453;
  ng2.crds[8][2]=  	11.082;
  ng2.crds[9][2]=  	8.837 ;
  ng2.crds[10][2]= 	7.565 ;
  ng2.crds[11][2]= 	6.751 ;
  ng2.crds[12][2]= 	5.055 ;
  ng2.crds[13][2]= 	2.791 ;
  ng2.crds[14][2]= 	1.742 ;
  ng2.crds[15][2]= 	1.131 ;
  ng2.crds[16][2]= 	0.319 ;
  ng2.crds[17][2]= 	-1.985;
  ng2.crds[18][2]= 	-5.027;
  ng2.crds[19][2]= 	-4.684;
  ng2.crds[20][2]= 	-2.388;
  ng2.crds[21][2]= 	-2.629;
  ng2.crds[22][2]= 	0.099 ;
  ng2.crds[23][2]= 	1.192 ;
  ng2.crds[24][2]= 	2.046 ;
  ng2.crds[25][2]= 	3.791 ;
  ng2.crds[26][2]= 	5.807 ;
  ng2.crds[27][2]= 	7.292 ;
  ng2.crds[28][2]= 	7.767 ;
  ng2.crds[29][2]= 	9.860 ;
  ng2.crds[30][2]= 	11.376;
  ng2.crds[31][2]= 	9.123 ;
  ng2.crds[32][2]= 	7.337 ;
  ng2.crds[33][2]= 	3.978 ;
  ng2.crds[34][2]= 	3.849 ;
  ng2.crds[35][2]= 	0.908 ;
  ng2.crds[36][2]= 	2.359 ;
  ng2.crds[37][2]= 	3.400 ;
  ng2.crds[38][2]= 	7.117 ;
  ng2.crds[39][2]= 	9.380 ;
  ng2.crds[40][2]= 	12.740;
  ng2.crds[41][2]= 	16.013;
  ng2.crds[42][2]= 	16.837;
  ng2.crds[43][2]= 	16.445;
  ng2.crds[44][2]= 	13.697;
  ng2.crds[45][2]= 	12.241;
  


  /*
ng2.crds[0][0]=  18.7275634746643;  	
ng2.crds[1][0]=  15.3656133734301 ; 	
ng2.crds[2][0]=  14.5305533883296  ;	
ng2.crds[3][0]=  11.2828192477460  ;	
ng2.crds[4][0]=  9.48060062325788  ;	
ng2.crds[5][0]=  8.67300000000000  ;	
ng2.crds[6][0]=  9.25892445735943  ;	
ng2.crds[7][0]=  5.68739477119533  ;	
ng2.crds[8][0]=  6.36850253591539  ;	
ng2.crds[9][0]=  9.61113884446259  ;	
ng2.crds[10][0]= 7.86621290464178  ;	
ng2.crds[11][0]= 5.09493424573563  ;	
ng2.crds[12][0]= 7.73472245427428  ;	
ng2.crds[13][0]= 9.50789608630068  ;	
ng2.crds[14][0]= 6.19378283494010  ;	
ng2.crds[15][0]= 5.40305881983044  ;	
ng2.crds[16][0]= 9.02062993041855  ;	
ng2.crds[17][0]= 10.3004298570482  ;	
ng2.crds[18][0]= 9.01549688907400  ;	
ng2.crds[19][0]= 10.6063978245919  ;	
ng2.crds[20][0]= 7.88152578092061  ;	
ng2.crds[21][0]= 8.58808331125242  ;	
ng2.crds[22][0]= 10.2922645250148  ;	
ng2.crds[23][0]= 7.38381377393497  ;	
ng2.crds[24][0]= 4.91817987132730  ;	
ng2.crds[25][0]= 7.33449192421039  ;	
ng2.crds[26][0]= 7.73444095721041  ;	
ng2.crds[27][0]= 4.03836101901621  ;	
ng2.crds[28][0]= 3.79078045673609  ;	
ng2.crds[29][0]= 6.58046469203470  ;	
ng2.crds[30][0]= 6.90703642978232  ;	
ng2.crds[31][0]= 10.3087777861264  ; 	
ng2.crds[32][0]= 10.8488645442519  ;	
ng2.crds[33][0]= 12.5450929748737  ;	
ng2.crds[34][0]= 14.7426166739730  ;	
ng2.crds[35][0]= 16.6461200186158  ;	
ng2.crds[36][0]= 19.8833765594606  ;	
ng2.crds[37][0]= 22.0640819507694  ;	
ng2.crds[38][0]= 21.8712085473147  ;	
ng2.crds[39][0]= 19.0041424756422  ;	
ng2.crds[40][0]= 17.7229548964136  ;	
ng2.crds[41][0]= 18.1348463495170  ;	
ng2.crds[42][0]= 14.5043642597906  ;	
ng2.crds[43][0]= 14.8830280810067  ;	
ng2.crds[44][0]= 18.2237254538147  ;	
ng2.crds[45][0]= 17.7291968267176  ;	
ng2.crds[0][1]=  13.8567808101031;	  	
ng2.crds[1][1]=  12.4097724843803;    	
ng2.crds[2][1]=  11.2169456356104;   	
ng2.crds[3][1]=  9.38253560366691;   	
ng2.crds[4][1]=  9.02300843000931;  	
ng2.crds[5][1]=  5.31400000000000; 	
ng2.crds[6][1]=  2.34067335387075;	
ng2.crds[7][1]=  2.63724741205684;	     
ng2.crds[8][1]=  6.32257917405678;	     
ng2.crds[9][1]=  5.37995846077919;	     
ng2.crds[10][1]= 2.56774140483222;	     
ng2.crds[11][1]= 5.05785774373330;	     
ng2.crds[12][1]= 7.61577843693444;	     
ng2.crds[13][1]= 5.12419976745598;	     
ng2.crds[14][1]= 4.31943559307381;	     
ng2.crds[15][1]= 8.06647458881967;	     
ng2.crds[16][1]= 9.27150512716754;	     
ng2.crds[17][1]= 6.60929201878419;	     
ng2.crds[18][1]= 8.35361060682930;	     
ng2.crds[19][1]= 11.6693906649870;	     
ng2.crds[20][1]= 13.3873791653930;	     
ng2.crds[21][1]= 17.1469181306885;	     
ng2.crds[22][1]= 18.1653178578452;	     
ng2.crds[23][1]= 20.4754723040827;	     
ng2.crds[24][1]= 17.5609175502594;	     
ng2.crds[25][1]= 15.3607375578650;	     
ng2.crds[26][1]= 18.2647811601175;	     
ng2.crds[27][1]= 18.4476298220661;	     
ng2.crds[28][1]= 14.6769467151001;	     
ng2.crds[29][1]= 14.2732126752829;	     
ng2.crds[30][1]= 17.4948406906812;	     
ng2.crds[31][1]= 18.2574698230047;	     
ng2.crds[32][1]= 21.8146027611178;	     
ng2.crds[33][1]= 23.4489461845796;	     
ng2.crds[34][1]= 26.5298311720298;	     
ng2.crds[35][1]= 28.7026411542365;	     
ng2.crds[36][1]= 28.6181313795945;	     
ng2.crds[37][1]= 25.7004877875876;	     
ng2.crds[38][1]= 25.9155914509547;	     
ng2.crds[39][1]= 24.0080220051386;	     
ng2.crds[40][1]= 25.2192087922033;	     
ng2.crds[41][1]= 23.1956358265536;	     
ng2.crds[42][1]= 21.9983428476608;	     
ng2.crds[43][1]= 20.5941697621303;	     
ng2.crds[44][1]= 18.9490990747849;	     
ng2.crds[45][1]= 15.7255946350394;	     
ng2.crds[0][2]=    6.89905570636177;	
ng2.crds[1][2]=    7.89754878200116;	
ng2.crds[2][2]=    11.4110569903504;	
ng2.crds[3][2]=    12.2734117820651;	
ng2.crds[4][2]=    15.5973578166676;	
ng2.crds[5][2]=    15.2790000000000;	
ng2.crds[6][2]=    12.9558675918739;	
ng2.crds[7][2]=    11.5379784816612;	
ng2.crds[8][2]=    10.7021339884495;	
ng2.crds[9][2]=    8.86469855791279;	
ng2.crds[10][2]=   6.91552086083605;	
ng2.crds[11][2]=   5.89514974529564;	
ng2.crds[12][2]=   4.93631663085200;	
ng2.crds[13][2]=   2.63258227019185;	
ng2.crds[14][2]=  0.877340411989044;	
ng2.crds[15][2]=  0.566328681842537;	
ng2.crds[16][2]=  0.520430299655735;	
ng2.crds[17][2]=  -1.93196231029695;	
ng2.crds[18][2]=  -5.05863497164371;	
ng2.crds[19][2]=  -4.02575558890242;	
ng2.crds[20][2]=  -1.95092816039380;	
ng2.crds[21][2]=  -1.61770567502419;	
ng2.crds[22][2]=   1.59618750714391;	
ng2.crds[23][2]=   2.48001478838826;	
ng2.crds[24][2]=   2.56536726507866;	
ng2.crds[25][2]=   4.51024182765580;	
ng2.crds[26][2]=   6.99393052135692;	
ng2.crds[27][2]=   7.88215822712949;	
ng2.crds[28][2]=   7.86791842295828;	
ng2.crds[29][2]=   10.4519800887123;	
ng2.crds[30][2]=   12.4498201299832;	
ng2.crds[31][2]=   10.8386446115899;	
ng2.crds[32][2]=   9.53799033227951;	
ng2.crds[33][2]=   6.60014610813530;	
ng2.crds[34][2]=   7.22900800553463;	
ng2.crds[35][2]=   4.82038478922273;	
ng2.crds[36][2]=   6.86555230968342;	
ng2.crds[37][2]=   7.96206581454235;	
ng2.crds[38][2]=   11.7552328638886;	
ng2.crds[39][2]=   13.3324817187157;	
ng2.crds[40][2]=   16.6889593297940;	
ng2.crds[41][2]=   19.8639383665047;	
ng2.crds[42][2]=   19.9207654627682;	
ng2.crds[43][2]=   16.3488716154690;	
ng2.crds[44][2]=   16.9973016401829;	
ng2.crds[45][2]=   15.0309411534149;	
  */


  
  ng1.crds= ( DtPoint3 *) calloc(46,sizeof(DtPoint3));

  ng1.crds[0][0]=   16.967;
  ng1.crds[1][0]=   13.856;
  ng1.crds[2][0]=   13.660;
  ng1.crds[3][0]=   10.646;
  ng1.crds[4][0]=   9.448 ;
  ng1.crds[5][0]=   8.673  ;
  ng1.crds[6][0]=   8.912 ;
  ng1.crds[7][0]=   5.145 ;
  ng1.crds[8][0]=   5.598 ; 
  ng1.crds[9][0]=   8.496 ;
  ng1.crds[10][0]=  6.500 ;
  ng1.crds[11][0]=  3.545 ;
  ng1.crds[12][0]=  5.929 ;
  ng1.crds[13][0]=  7.331 ;
  ng1.crds[14][0]=  3.782 ;
  ng1.crds[15][0]=  2.890 ;
  ng1.crds[16][0]=  5.895 ;
  ng1.crds[17][0]=  4.933 ;
  ng1.crds[18][0]=  2.792 ;
  ng1.crds[19][0]=  5.366  ;
  ng1.crds[20][0]=  3.767 ;
  ng1.crds[21][0]=  6.143 ;
  ng1.crds[22][0]=  8.114 ;
  ng1.crds[23][0]=  6.614 ;
  ng1.crds[24][0]=  3.074 ;
  ng1.crds[25][0]=  4.180 ;
  ng1.crds[26][0]=  5.879 ;
  ng1.crds[27][0]=  2.691 ;
  ng1.crds[28][0]=   .715 ;
  ng1.crds[29][0]=  2.986 ;
  ng1.crds[30][0]=  4.769 ;
  ng1.crds[31][0]=  8.140  ; 
  ng1.crds[32][0]=  10.280;
  ng1.crds[33][0]=  12.552;
  ng1.crds[34][0]=  15.930;
  ng1.crds[35][0]=  18.635;
  ng1.crds[36][0]=  21.452;
  ng1.crds[37][0]=  22.019;
  ng1.crds[38][0]=  21.936;
  ng1.crds[39][0]=  18.504;
  ng1.crds[40][0]=  17.924;
  ng1.crds[41][0]=  17.334;
  ng1.crds[42][0]=  13.564;
  ng1.crds[43][0]=  13.257;
  ng1.crds[44][0]=  15.445;
  ng1.crds[45][0]=  13.512;
  ng1.crds[0][1]=   12.784;
  ng1.crds[1][1]=   11.469;
  ng1.crds[2][1]=   10.707;
  ng1.crds[3][1]=   8.991 ;
  ng1.crds[4][1]=   9.034 ;
  ng1.crds[5][1]=   5.314 ;
  ng1.crds[6][1]=   2.083 ;
  ng1.crds[7][1]=   2.209 ;
  ng1.crds[8][1]=   5.767 ;
  ng1.crds[9][1]=   4.609 ;
  ng1.crds[10][1]=   1.584 ;
  ng1.crds[11][1]=  3.935 ;
  ng1.crds[12][1]=  6.358 ;
  ng1.crds[13][1]=  3.607 ;
  ng1.crds[14][1]=  2.599 ;
  ng1.crds[15][1]=  6.285 ;
  ng1.crds[16][1]=  6.489 ;
  ng1.crds[17][1]=  3.431 ;
  ng1.crds[18][1]=  5.376 ;
  ng1.crds[19][1]=  8.191 ;
  ng1.crds[20][1]=  10.609;
  ng1.crds[21][1]=   13.513;
  ng1.crds[22][1]=  13.103;
  ng1.crds[23][1]=  16.317;
  ng1.crds[24][1]=  14.894;
  ng1.crds[25][1]=  11.549;
  ng1.crds[26][1]=  13.502;
  ng1.crds[27][1]=  15.221;
  ng1.crds[28][1]=  12.045;
  ng1.crds[29][1]=  9.994 ;
  ng1.crds[30][1]=  12.336;
  ng1.crds[31][1]=  11.694;
  ng1.crds[32][1]=  14.760;
  ng1.crds[33][1]=  15.877;
  ng1.crds[34][1]=  17.454;
  ng1.crds[35][1]=  18.861;
  ng1.crds[36][1]=  16.969;
  ng1.crds[37][1]=  13.242;
  ng1.crds[38][1]=  12.911;
  ng1.crds[39][1]=  12.312;
  ng1.crds[40][1]=  13.421;
  ng1.crds[41][1]=  10.956;
  ng1.crds[42][1]=  11.573;
  ng1.crds[43][1]=  10.745;
  ng1.crds[44][1]=  7.667 ;
  ng1.crds[45][1]=  5.395 ;
  ng1.crds[0][2]=   4.338 ;
  ng1.crds[1][2]=   6.066; 
  ng1.crds[2][2]=   9.787; 
  ng1.crds[3][2]=   11.408;
  ng1.crds[4][2]=   15.012;
  ng1.crds[5][2]=   15.279;
  ng1.crds[6][2]=   13.258;
  ng1.crds[7][2]=   12.453;
  ng1.crds[8][2]=   11.082;
  ng1.crds[9][2]=   8.837 ;
  ng1.crds[10][2]=  7.565 ;
  ng1.crds[11][2]=  6.751 ;
  ng1.crds[12][2]=  5.055 ;
  ng1.crds[13][2]=  2.791 ;
  ng1.crds[14][2]=  1.742 ;
  ng1.crds[15][2]=  1.126 ;
  ng1.crds[16][2]=  -1.213;
  ng1.crds[17][2]=  -3.326;
  ng1.crds[18][2]=  -5.797;
  ng1.crds[19][2]=  -6.018;
  ng1.crds[20][2]=  -3.513;
  ng1.crds[21][2]=  -2.696;
  ng1.crds[22][2]=  .500  ;
  ng1.crds[23][2]=  1.913 ;
  ng1.crds[24][2]=  1.756 ;
  ng1.crds[25][2]=  3.187 ;
  ng1.crds[26][2]=  6.026 ;
  ng1.crds[27][2]=  7.194 ;
  ng1.crds[28][2]=  6.657 ;
  ng1.crds[29][2]=  8.950 ;
  ng1.crds[30][2]=  11.360;
  ng1.crds[31][2]=  9.635 ;
  ng1.crds[32][2]=  8.823 ;
  ng1.crds[33][2]=  6.036 ; 
  ng1.crds[34][2]=  6.941 ;
  ng1.crds[35][2]=  4.738 ;
  ng1.crds[36][2]=  6.513 ;
  ng1.crds[37][2]=  7.020 ;
  ng1.crds[38][2]=  10.809;
  ng1.crds[39][2]=  12.298;
  ng1.crds[40][2]=  15.877;
  ng1.crds[41][2]=  18.691;
  ng1.crds[42][2]=  18.836;
  ng1.crds[43][2]=  15.081;
  ng1.crds[44][2]=  15.246;
  ng1.crds[45][2]=  12.878;
  ng1.crds[1][0]=   13.856;
  ng1.crds[2][0]=   13.660;
  ng1.crds[3][0]=   10.646;
  ng1.crds[4][0]=   9.448 ;
  ng1.crds[5][0]=   8.673  ;
  ng1.crds[6][0]=   8.912 ;
  ng1.crds[7][0]=   5.145 ;
  ng1.crds[8][0]=   5.598; 
  ng1.crds[9][0]=   8.496 ;
  ng1.crds[10][0]=  6.500 ;
  ng1.crds[11][0]=  3.545 ;
  ng1.crds[12][0]=  5.929 ;
  ng1.crds[13][0]=  7.331 ;
  ng1.crds[14][0]=  3.782 ;
  ng1.crds[15][0]=  2.890 ;
  ng1.crds[16][0]=  5.895 ;
  ng1.crds[17][0]=  4.933 ;
  ng1.crds[18][0]=  2.792 ;
  ng1.crds[19][0]=  5.366  ;
  ng1.crds[20][0]=  3.767 ;
  ng1.crds[21][0]=  6.143 ;
  ng1.crds[22][0]=  8.114 ;
  ng1.crds[23][0]=  6.614 ;
  ng1.crds[24][0]=  3.074 ;
  ng1.crds[25][0]=  4.180 ;
  ng1.crds[26][0]=  5.879 ;
  ng1.crds[27][0]=  2.691 ;
  ng1.crds[28][0]=   .715 ;
  ng1.crds[29][0]=  2.986 ;
  ng1.crds[30][0]=  4.769 ;
  ng1.crds[31][0]=  8.140  ; 
  ng1.crds[32][0]=  10.280;
  ng1.crds[33][0]=  12.552;
  ng1.crds[34][0]=  15.930;
  ng1.crds[35][0]=  18.635;
  ng1.crds[36][0]=  21.452;
  ng1.crds[37][0]=  22.019;
  ng1.crds[38][0]=  21.936;
  ng1.crds[39][0]=  18.504;
  ng1.crds[40][0]=  17.924;
  ng1.crds[41][0]=  17.334;
  ng1.crds[42][0]=  13.564;
  ng1.crds[43][0]=  13.257;
  ng1.crds[44][0]=  15.445;
  ng1.crds[45][0]=  13.512;
  ng1.crds[0][1]=   12.784;
  ng1.crds[1][1]=   11.469;
  ng1.crds[2][1]=   10.707;
  ng1.crds[3][1]=   8.991 ;
  ng1.crds[4][1]=   9.034 ;
  ng1.crds[5][1]=   5.314 ;
  ng1.crds[6][1]=   2.083 ;
  ng1.crds[7][1]=   2.209 ;
  ng1.crds[8][1]=   5.767 ;
  ng1.crds[9][1]=   4.609 ;
  ng1.crds[10][1]=   1.584 ;
  ng1.crds[11][1]=  3.935 ;
  ng1.crds[12][1]=  6.358 ;
  ng1.crds[13][1]=  3.607 ;
  ng1.crds[14][1]=  2.599 ;
  ng1.crds[15][1]=  6.285 ;
  ng1.crds[16][1]=  6.489 ;
  ng1.crds[17][1]=  3.431 ;
  ng1.crds[18][1]=  5.376 ;
  ng1.crds[19][1]=  8.191 ;
  ng1.crds[20][1]=  10.609;
  ng1.crds[21][1]=   13.513;
  ng1.crds[22][1]=  13.103;
  ng1.crds[23][1]=  16.317;
  ng1.crds[24][1]=  14.894;
  ng1.crds[25][1]=  11.549;
  ng1.crds[26][1]=  13.502;
  ng1.crds[27][1]=  15.221;
  ng1.crds[28][1]=  12.045;
  ng1.crds[29][1]=  9.994 ;
  ng1.crds[30][1]=  12.336;
  ng1.crds[31][1]=  11.694;
  ng1.crds[32][1]=  14.760;
  ng1.crds[33][1]=  15.877;
  ng1.crds[34][1]=  17.454;
  ng1.crds[35][1]=  18.861;
  ng1.crds[36][1]=  16.969;
  ng1.crds[37][1]=  13.242;
  ng1.crds[38][1]=  12.911;
  ng1.crds[39][1]=  12.312;
  ng1.crds[40][1]=  13.421;
  ng1.crds[41][1]=  10.956;
  ng1.crds[42][1]=  11.573;
  ng1.crds[43][1]=  10.745;
  ng1.crds[44][1]=  7.667 ;
  ng1.crds[45][1]=  5.395 ;
  ng1.crds[0][2]=   4.338 ;
  ng1.crds[1][2]=   6.066; 
  ng1.crds[2][2]=   9.787; 
  ng1.crds[3][2]=   11.408;
  ng1.crds[4][2]=   15.012;
  ng1.crds[5][2]=   15.279;
  ng1.crds[6][2]=   13.258;
  ng1.crds[7][2]=   12.453;
  ng1.crds[8][2]=   11.082;
  ng1.crds[9][2]=   8.837 ;
  ng1.crds[10][2]=  7.565 ;
  ng1.crds[11][2]=  6.751 ;
  ng1.crds[12][2]=  5.055 ;
  ng1.crds[13][2]=  2.791 ;
  ng1.crds[14][2]=  1.742 ;
  ng1.crds[15][2]=  1.126 ;
  ng1.crds[16][2]=  -1.213;
  ng1.crds[17][2]=  -3.326;
  ng1.crds[18][2]=  -5.797;
  ng1.crds[19][2]=  -6.018;
  ng1.crds[20][2]=  -3.513;
  ng1.crds[21][2]=  -2.696;
  ng1.crds[22][2]=  .500  ;
  ng1.crds[23][2]=  1.913 ;
  ng1.crds[24][2]=  1.756 ;
  ng1.crds[25][2]=  3.187 ;
  ng1.crds[26][2]=  6.026 ;
  ng1.crds[27][2]=  7.194 ;
  ng1.crds[28][2]=  6.657 ;
  ng1.crds[29][2]=  8.950 ;
  ng1.crds[30][2]=  11.360;
  ng1.crds[31][2]=  9.635 ;
  ng1.crds[32][2]=  8.823 ;
  ng1.crds[33][2]=  6.036 ; 
  ng1.crds[34][2]=  6.941 ;
  ng1.crds[35][2]=  4.738 ;
  ng1.crds[36][2]=  6.513 ;
  ng1.crds[37][2]=  7.020 ;
  ng1.crds[38][2]=  10.809;
  ng1.crds[39][2]=  12.298;
  ng1.crds[40][2]=  15.877;
  ng1.crds[41][2]=  18.691;
  ng1.crds[42][2]=  18.836;
  ng1.crds[43][2]=  15.081;
  ng1.crds[44][2]=  15.246;
  ng1.crds[45][2]=  12.878;



  ng1.n=46;
  ng2.n=46;
     

  /*
  lectPdbCsr(fname1,&ng1);
  printNuage(stderr, &ng1, "Nuage1");
  
  getchar();


  lectPdbCsr(fname2, &ng2);
  printNuage(stderr, &ng2, "Nuage2");

  getchar();
  */

  minN1N2=min(ng1.n,ng2.n);  



  crdsSup1= ( DtPoint3 *) calloc(minN1N2,sizeof(DtPoint3));
  crdsSup2= ( DtPoint3 *) calloc(minN1N2,sizeof(DtPoint3));
  rep = (DtReponse) calloc(1,sizeof(DtReponse));
  rep->dist = (DtDistRec *) calloc(dimTab,sizeof(DtDistRec));
  

  rep=csr(ng1,ng2,dseuil,dimTab,kds,dist,mat,crdsSup1,crdsSup2,5000,rep);

 
  printf("NOMBRE D'ATOMES APPARIES : %d\n",rep->nbAtMax);
  printf("RMSD : %.8lf\n",rep->rmsd);
  //printNbPtsApp(ng1,ng2,kds,rep->dist);
  //printDtpDistRec(rep->dist,(rep->nbAtMax)+1,ng1,ng2);
  printf("\n");


  return 0;
}

