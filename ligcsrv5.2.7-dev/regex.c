/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
#include <stdio.h>
#include <stdlib.h>
#include <regex.h>


#if 0
/*
   1. On commence par compiler notre expression r�guli�re avec l'option REG_NOSUB pour v�rifier uniquement la concordance
   2. Si aucune erreur s'est produite on demande l'analyse de notre cha�ne
   3. On n'a plus besoin de l'expression compil�e, on demande la lib�ration de la m�moire
   4. La cha�ne est identifi�e
   5. On ne retrouve pas le motif de notre expression dans la cha�ne analys�e
   6. Ou il s'est produite une erreur
   7. Dans ce cas, on appel une premi�re fois regerror pour conna�tre la taille du message d'erreur
   8. Et une seconde fois pour r�cup�rer la cha�ne
*/

int main (void)
{
   int err;
   regex_t preg;
   const char *str_request = "www.developpez.com";
   const char *str_regex = "www\\.[-_[:alnum:]]+\\.[[:lower:]]{2,4}";

/* (1) */
   err = regcomp (&preg, str_regex, REG_NOSUB | REG_EXTENDED);
   if (err == 0)
   {
      int match;

/* (2) */
      match = regexec (&preg, str_request, 0, NULL, 0);
/* (3) */
      regfree (&preg);
/* (4) */
      if (match == 0)
      {
         printf ("%s est une adresse internet valide\n", str_request);
      }
/* (5) */
      else if (match == REG_NOMATCH)
      {
         printf ("%s n\'est pas une adresse internet valide\n", str_request);
      }
/* (6) */
      else
      {
         char *text;
         size_t size;

/* (7) */
         size = regerror (err, &preg, NULL, 0);
         text = malloc (sizeof (*text) * size);
         if (text)
         {
/* (8) */
            regerror (err, &preg, text, size);
            fprintf (stderr, "%s\n", text);
            free (text);
         }
         else
         {
            fprintf (stderr, "Memoire insuffisante\n");
            exit (EXIT_FAILURE);
         }
      }
   }
   puts ("\nPress any key\n");
/* Dev-cpp */
   getchar ();
   return (EXIT_SUCCESS);
}
#endif


regex_t *reginit(char *str_regex, regex_t *preg)
{
   int err;


   if (preg == NULL) {
     preg = calloc(1,sizeof(regex_t));
   }
   if (preg == NULL) {
     return preg;
   }

   err = regcomp (preg, str_regex, REG_NOSUB | REG_EXTENDED);
   if (err != 0) {
     char *text;
     size_t size;

/* (7) */
     size = regerror (err, preg, NULL, 0);
     text = malloc (sizeof (*text) * size);
     if (text) {
/* (8) */
       regerror (err, preg, text, size);
       fprintf (stderr, "%s\n", text);
       free (text);
       return NULL;
     } else {
       fprintf (stderr, "Memoire insuffisante\n");
       exit (EXIT_FAILURE);
     }
   }
   return preg;
}

/* Returns 0 (success) or REG_NOMATCH (failure) */
int regmatch(char *str_request, regex_t *preg)
{
  return regexec (preg, str_request, 0, NULL, 0);
}
