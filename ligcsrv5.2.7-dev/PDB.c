/* ========================================================================== */
/*                                                                            */
/*  LigCSRre (C) 2008 - Small compounds 3D molecular similarity screening by: */
/*                                                                            */
/* Pierre Tuffery(1), Flavien Quintus(1), J. Grynberg(1), O. Sperandio(1),    */
/* M. Petitjean(2)                                                            */
/*  (1): MTi, INSERM UMR-S 973, Université Paris Diderot - Paris 7,           */
/*     	 F75013, Paris, France.                                               */
/* 	 (http://www.mti.univ-paris-diderot.fr)                               */
/* (2): CEA/DSV/iBiTec-S/SB2SM (CNRS URA 2096),                               */
/*       F91191, Gif-sur-Yvette, France.                                      */
/*                                                                            */
/* Grants to use: See the LICENSE.TXT file that comes with the package.       */
/*                                                                            */
/* ========================================================================== */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/times.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include "CSRTypes.h"

/* -------------------------------------------------------------------- */

void stripspace(char *o, char *d)
{
        while ((*o == ' ') && (*o != '\0')) o++;
        while ((*o != ' ') && (*o != '\0')) {
                *d = *o;
                d++;
                o++;
        }
        *d = '\0';
}



/* ===================================================
* Renvoie une chaine de caractères en majuscules.
* ===================================================
*/

void strupper(char *s)
{
  while (*s != '\0') {
    *s = toupper(*s);
    s++;
  }
}


/* ====================================================
* Numero d'atome pour le residu
* ====================================================
*/
void getAtomNum(char *line,int *anum)
{
  char buff[30], charAnum[6];

  charAnum[5] = '\0';
  if (sscanf(line,"%6c%5c",buff,charAnum) != EOF);
  *anum = atoi(charAnum);
}


/* ====================================================
* Nom de l'atome
* 3 char lus + 1 blanc pour atom index
* ====================================================
*/
void getAtomName(char *line,char *anom)
{
    char buff[30];

    anom[3] = ' ';
    anom[4] = '\0';
    if (sscanf(line,"%12c%4c",buff,anom) != EOF);
    strupper(anom);
}


/* ====================================================
* Index d'atome pour le residu (cf params.c)
* ====================================================
*/
void getResName(char *line,char *rnom)
{
    char buff[30];

    rnom[3] = '\0';
    if (sscanf(line,"%17c%3c",buff,rnom) != EOF);
    strupper(rnom);
}


/* ====================================================
 * Numero du residu
 * ====================================================
 */
void getResNum(char *line,int *rnum)
{
  char buff[30], charRnum[5];

  charRnum[4] = '\0';
  if (sscanf(line,"%22c%4c",buff,charRnum) != EOF);
  *rnum = atoi(charRnum);
}

/* ====================================================
 * Chain Id for Res
 * ====================================================
 */
void getResChainIde(char *line,char *chn)
{
  *chn = line[21];
}

/* ====================================================
 * Insertion code for Res
 * ====================================================
 */
void getResICode(char *line,char *icode)
{
  *icode = line[27];
}



/* ====================================================
* Coordonnees de l'atome
* ====================================================
*/
void getAtomCoords(char *line,DtFloat *x,DtFloat *y,DtFloat *z)
{
  char buff[30], charX[9],charY[9],charZ[9];

  charX[8] = charY[8] = charZ[8] = '\0';
  if (sscanf(line,"%30c%8c%8c%8c",buff,charX,charY,charZ) != EOF);
  *x = (DtFloat) atof(charX);
  *y = (DtFloat) atof(charY);
  *z = (DtFloat) atof(charZ);
}





/* ====================================================
* Type d'info contenu dans la ligne du fichier PDB
* 0: info non codee ou Pb.
* 1: ATOM
* 2: HELIX
* 3: SHEET
* 4: TURN
* 5: TER
* 6: CONECT
* 7: END
* 8: MODEL
* 9: ENDMDL
* ====================================================
*/
int getPDBLineStatus(char *d)
{
  char Entete[7];

  if (sscanf(d,"%s",&Entete) == EOF) return 0;
  if (strcmp(Entete,"ATOM") == 0)    return 1; //interessant
  if (strcmp(Entete,"HETATM") == 0)  return 11; //interessant
  if (strcmp(Entete,"HELIX") == 0)   return 2;
  if (strcmp(Entete,"SHEET") == 0)   return 3;
  if (strcmp(Entete,"TURN") == 0)    return 4;
  if (strcmp(Entete,"TER") == 0)     return 5;
  if (strcmp(Entete,"CONECT") == 0)  return 6;
  if (strcmp(Entete,"END") == 0)     return 7;
  if (strcmp(Entete,"MODEL") == 0)   return 8;
  if (strcmp(Entete,"ENDMDL") == 0)  return 9;
  if (strcmp(Entete,"HEADER") == 0)  return 10;
  if (strcmp(Entete,"REMARK") == 0) {
    if (strstr(d,"REMARK     MATCH.") != NULL)  return 12;
    if (strstr(d,"REMARK     RESMATCH.") != NULL)  return 13;
    if (strstr(d,"REMARK    EC:") != NULL)  return 14;
    if (strstr(d,"REMARK    HETGRP:") != NULL)  return 16;
    if (strstr(d,"REMARK     WEIGHT.") != NULL)  return 17;
  }
  if (strcmp(Entete,"COMPND") == 0) return 15;
  return 0;
}


/* ==================================================== 
 * Output atom info for one atom
 * ==================================================== 
 */
void outputPDBAtomLine(FILE *f, 
		       int anum, char *aname, char acode, 
		       char *rname, char chn, int rnum, char icode, 
		       double x, double y, double z)
{
  //ATOM     54  CG2 VAL     8       3.758   1.032  14.208  1.00 11.97      1CRN 123
  //ATOM      0 CA   ARG    10      -3.612  15.532   6.965               

  // fprintf(f,"ATOM  %5d %-4s%c%3s %c%4d%c   %8.3lf%8.3lf%8.3lf               \n",
    fprintf(f,"ATOM  %5d %-4s%c%3s %c%4d%c   %8.3lf%8.3lf%8.3lf                      %2s\n",
	    anum, aname, acode, 
	    rname, chn, rnum, icode, 
	    x, y, z, aname);
}
